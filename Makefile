.default: all

all:
	cd client && nmake all
	cd server && nmake all
	cd worldd && nmake all
	cd db-server && nmake all
	cd proxy && nmake all
	cd login-server && nmake all

all-force:
	cd client && nmake /A all
	cd server && nmake /A all
	cd worldd && nmake /A all
	cd db-server && nmake /A all
	cd proxy && nmake /A all
	cd login-server && nmake /A all

depend:
	cd dep && nmake all
	cd shared\lua && nmake all

all-opt:
	cd client && nmake all-opt
	cd server && nmake all-opt
	cd worldd && nmake all-opt
	cd proxy && nmake all-opt
	cd login-server && nmake all-opt

ship-client:
	cd client && call release.bat

ship-client-db:
	cd client && nmake
	nmake ship-client-pack

ship-server:
	cd worldd && nmake all-opt
	cd server && nmake all-opt
	nmake ship-server-pack

ship-server-db:
	cd worldd && nmake
	cd server && nmake
	nmake ship-server-pack

clean:
	cd client && nmake clean
	cd server && nmake clean
	cd worldd && nmake clean
	cd shared\lua && nmake clean
