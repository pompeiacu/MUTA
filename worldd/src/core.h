#ifndef MUTA_WORLDD_CORE_H
#define MUTA_WORLDD_CORE_H
#include "../../shared/types.h"

int
core_init(const char *cfg_fp);

int
core_update(); /* Returns non-zero if should exit program */

int
core_shutdown();

int
core_add_th_job(void (*func)(void*), void *args);

bool32
core_do_th_work();

#endif
