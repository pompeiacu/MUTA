#ifndef MUTA_ENTITY_H
#define MUTA_ENTITY_H

#include "../../shared/common_utils.h"
#include "../../shared/containers.h"

/* Forward declaration(s) */
typedef struct instance_part_t  instance_part_t;
typedef struct pf_path_t        pf_path_t;

/* Types defined here (except for ones defined via macros */
typedef struct entity_t             entity_t;
typedef struct ent_cont_t           ent_cont_t;
typedef struct cmp_mobility_t       cmp_mobility_t;
typedef struct cmp_pathfind_t       cmp_pathfind_t;

/* Declare an updatable entity component pool. Functions must then be defined
 * inside the C file. Both type and name must lack the _t postfix. The member
 * added to the instance_part_t structure must have a name equal to the "name"
 * parameter of the macro. */
#define COMPONENT_DECLARATION(type, name, index) \
typedef struct name##_item_t    name##_item_t; \
struct name##_item_t {entity_t *ent; type##_t cmp;}; \
typedef name##_item_t* name##_t; \
int                 name##_init(name##_t *p, uint32 max); \
void                name##_destroy(name##_t *p); \
name##_item_t       *name##_get_items(name##_t *p, uint32 *ret_num); \
int                 entity_attach_##type(entity_t *e, type##_t *cmp); \
void                entity_remove_##type(entity_t *e); \
type##_t            *entity_get_##type(entity_t *e); \
entity_t            *type##_get_entity(type##_t *cmp);

/* Define the functions for removing and adding an entity component */
#define COMPONENT_DEFINITION(type, name, index, rem_func) \
name##_item_t \
*name##_get_items(name##_t *p, uint32 *ret_num) \
    {*ret_num = darr_num(*p); return *p;} \
\
int \
name##_init(name##_t *p, uint32 max) \
    {darr_reserve(*p, max); return 0;} \
\
void \
name##_destroy(name##_t *p) \
    {darr_free(*p);} \
\
int \
entity_attach_##type(entity_t *e, type##_t *cmp) \
{ \
    if (e->cmp_flags & (1 << index)) return 1; \
    instance_part_t *inst = e->inst; \
    uint32 i            = darr_num(inst->name); \
    name##_item_t *item = darr_push_empty(inst->name); \
    if (!item) return 2; \
    item->cmp = *cmp; \
    item->ent = e; \
    e->cmps[index] = i; \
    e->cmp_flags |= (1 << index); \
    return 0; \
} \
\
void \
entity_remove_##type(entity_t *e) \
{ \
    if (!(e->cmp_flags & (1 << index))) return; \
    instance_part_t *inst = e->inst; \
    name##_item_t *item = &inst->name[e->cmps[index]]; \
    name##_item_t *last = &inst->name[darr_num(inst->name) - 1]; \
    rem_func(e, item, last); \
    last->ent->cmps[index] = e->cmps[index]; \
    *item = *last; \
    _darr_head(inst->name)->num--; \
    e->cmp_flags &= ~(1 << index); \
} \
\
type##_t * \
entity_get_##type(entity_t *e) \
{ \
    if (!(e->cmp_flags & (1 << index))) return 0; \
    muta_assert(e->inst); \
    muta_assert(e->inst->name); \
    return &e->inst->name[e->cmps[index]].cmp; \
} \
entity_t * \
type##_get_entity(type##_t *c) \
{ \
    name##_item_t *item = (name##_item_t*)((char*)c - \
        offsetof(name##_item_t, cmp)); \
   return item->ent; \
}

#define BLANK_CMP_REM(e, cmp_a, cmp_b)

#define ENTITY_CONTAINER(ep) \
    ((ent_cont_t*)((char*)(ep) - offsetof(ent_cont_t, e)))

enum entity_types
{
    ENT_TYPE_UNKNOWN = 0,
    ENT_TYPE_PLAYER,
    ENT_TYPE_DYNAMIC_OBJ,
    ENT_TYPE_CREATURE
};

enum entity_cmp_indices
{
    ENT_CMP_MOBILITY = 0,
    ENT_CMP_PATHFIND,
    ENT_CMP_LUA,
    MAX_ENT_CMPS
};

struct entity_t
{
    union
    {
        struct
        {
            player_guid_t   id;
        } player;
        struct
        {
            dobj_guid_t     id;
            dobj_type_id_t  type_id;
        } dynamic_obj;
        struct
        {
            creature_guid_t     id;
            creature_type_id_t  type_id;
            uint8               flags;
        } creature;
    } type_data;
    instance_part_t *inst;
    uint32          cmps[MAX_ENT_CMPS];
    uint32          darr_index;
    world_pos_t     pos;
    uint8           cmp_flags;
    uint8           dir;
    uint8           type;
    uint8           flags;
};

/*-- Components defined in this file --*/

/* Timers for an entity to walk from tile to tile */
struct cmp_mobility_t
{
    float walk_timer;
    uint8 walk_speed;
};

struct cmp_pathfind_t
{
    pf_path_t   *path;
    uint16      path_node_index;
};

struct ent_cont_t {entity_t e; ent_cont_t *next;};
DYNAMIC_OBJ_POOL_DEFINITION(ent_cont_t, entity_pool, 64);

COMPONENT_DECLARATION(cmp_mobility, cmp_mobility_pool, ENT_CMP_MOBILITY);
COMPONENT_DECLARATION(cmp_pathfind, cmp_pathfind_pool, ENT_CMP_PATHFIND);

world_pos_t
entity_get_local_pos(entity_t *e);

world_pos_t
entity_get_pos(entity_t *e);

void
entity_set_pos(entity_t *e, int32 x, int32 y, int8 z);

void
entity_set_dir(entity_t *e, int dir);

int
entity_get_dir(entity_t *e);

int
entity_find_path(entity_t *e, int32 x, int32 y, int8 z);

void
cmp_pathfind_stop(cmp_pathfind_t *cmp);

#endif /* MUTA_ENTITY_H */
