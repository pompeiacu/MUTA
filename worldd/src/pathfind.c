#include "pathfind.h"
#include "core.h"
#include "instance_part.h"
#include "../../shared/common_utils.h"
#include "../../shared/world_common.h"
#include "../../shared/world_common.inl"
#include "../../shared/sv_time.h" /* Debugging */

#define PF_NODE_F(n)            ((n)->g + (n)->h)
#define PF_NODE_OPEN            (1 << 0)
#define PF_NODE_CLOSED          (1 << 1)
#define MARK_NODE_OPEN(n)       SET_BITFLAG_ON((n)->flags, PF_NODE_OPEN)
#define MARK_NODE_CLOSED(n)     SET_BITFLAG_ON((n)->flags, PF_NODE_CLOSED)
#define MARK_NODE_NOT_OPEN(n)   SET_BITFLAG_OFF((n)->flags, PF_NODE_OPEN)
#define MARK_NODE_NOT_CLOSED(n) SET_BITFLAG_OFF((n)->flags, PF_NODE_CLOSED)
#define IS_NODE_OPEN(n)         GET_BITFLAG((n)->flags, PF_NODE_OPEN)
#define IS_NODE_CLOSED(n)       GET_BITFLAG((n)->flags, PF_NODE_CLOSED)
#define PF_PQ_FIRST(pq)         ((pq)->nodes[(pq)->first])
#define PF_PQ_LAST(pq) \
    ((pq)->nodes[(pq->first + ((pq)->num - 1)) % (pq)->max])

typedef struct pf_pq_t          pf_pq_t;
typedef struct pf_node_t        pf_node_t;
typedef struct pf_grid_t        pf_grid_t;
typedef struct pf_path_pool_t   pf_path_pool_t;
typedef struct pf_args_t        pf_args_t;

typedef uint16 pf_score_t;

struct pf_pq_t
{
    pf_node_t   **nodes;
    int         max;
    int         num;
    int         first;
};

struct pf_node_t
{
    pf_node_t   *p;
    pf_score_t  g;
    pf_score_t  h;
    char        flags;
};

struct pf_grid_t
{
    int         w, t;
    int         base_x, base_y, base_z;
    pf_node_t   *nodes;
    pf_pq_t     open;
};

struct pf_path_pool_t
{
    mutex_t     mtx;
    pf_path_t   **path_arrs;
    pf_path_t   *free;
    pf_path_t   *res;
    int         num_path_arrs;
    int         max_path_arrs;
    int         max_paths;
};

struct pf_args_t
{
    entity_t        *entity;
    int32           sx, sy, dx, dy;
    int8            sz, dz;
    pf_args_t       *next;
};

DYNAMIC_OBJ_POOL_DEFINITION(pf_args_t, pf_arg_pool, 16);

static pf_grid_t        _grids[MAX_PF_GRIDS];
static pf_grid_t        *_free_grids[MAX_PF_GRIDS];
static int              _max_grids;
static int              _num_free_grids;
static mutex_t          _grid_mtx;
static cond_var_t       _grid_cvar;
static pf_path_pool_t   _path_pool;
static pf_arg_pool_t    _arg_pool;
static mutex_t          _arg_pool_mtx;
static int32            _num_path_finds_running;

static int
_pf_pq_init(pf_pq_t *pq, int num);

void
_pf_pq_destroy(pf_pq_t *pq);

static inline pf_node_t *
_pf_pq_pop_first(pf_pq_t *pq);

static inline void
_pf_pq_update_priority(pf_pq_t *pq, pf_node_t *n);

static inline void
_pf_pq_clear(pf_pq_t *pq);

static int
_pf_grid_init(pf_grid_t *g, int w, int t);

static void
_pf_grid_destroy(pf_grid_t *g);

static pf_path_t *
_pf_astar(instance_part_t *m, pf_grid_t *grid, int32 sx, int32 sy,
    int8 sz, int32 dx, int32 dy, int8 dz);
/* This is the a-star implementation */

static inline pf_node_t *
_get_pf_node(pf_grid_t *g, int x, int y, int z);

static inline void
_compute_node_pos(pf_grid_t *g, pf_node_t *n, int *ret_x, int *ret_y,
    int *ret_z);

static int
_pf_path_pool_init(pf_path_pool_t *pool, int num_paths);

static void
_pf_path_pool_destroy(pf_path_pool_t *p);

static pf_path_t *
_pf_path_pool_reserve(pf_path_pool_t *p);

static void
_pf_path_pool_free(pf_path_pool_t *p, pf_path_t *pp);

static int
_pf_path_init(pf_path_t *p, int num_nodes);

static inline int
_pf_path_push(pf_path_t *pp, int32 x, int32 y, int8 z);

static inline pf_args_t *
_pf_reserve_args();

static inline void
_pf_free_args(pf_args_t *args);

static void
_pf_astar_callback(void *args);

static inline pf_grid_t *
_pf_reserve_grid();

static inline void
_pf_free_grid(pf_grid_t *g);

int
pf_init(pf_config_t *cfg, int num_initial_requests)
{
    int grid_w      = cfg->grid_w;
    int grid_t      = cfg->grid_t;
    int max_grids   = cfg->num_grids;

    if (max_grids < 1)              return 1;
    if (max_grids > MAX_PF_GRIDS)   return 2;
    if (grid_w    < 1)              return 2;
    if (grid_t    < 1)              return 3;

    bool32 had_errs = 0;

    memset(_grids, 0, sizeof(_grids));

    for (int i = 0; i < max_grids; ++i)
    {
        if (_pf_grid_init(&_grids[i], grid_w, grid_t) != 0)
            {had_errs = 1; break;}
    }

    if (_pf_path_pool_init(&_path_pool, 32) != 0)
        had_errs = 1;

    if (had_errs)
    {
        for (int i = 0; i < max_grids; ++i)
            _pf_grid_destroy(&_grids[i]);
        return 4;
    } else
    {
        _max_grids      = max_grids;
        _num_free_grids = max_grids;
        mutex_init(&_grid_mtx);
        cond_var_init(&_grid_cvar);
        for (int i = 0; i < max_grids; ++i)
            _free_grids[i] = &_grids[i];
    }

    if (pf_arg_pool_init(&_arg_pool, num_initial_requests, 8) != 0)
        return 6;

    mutex_init(&_arg_pool_mtx);

    return 0;
}

int
pf_trace(entity_t *e, int32 sx, int32 sy, int8 sz, int32 dx, int32 dy, int8 dz)
{
    pf_args_t *args = _pf_reserve_args();
    if (!args) return PFERR_NO_MEM;

    args->entity    = e;
    args->sx        = sx;
    args->sy        = sy;
    args->sz        = sz;
    args->dx        = dx;
    args->dy        = dy;
    args->dz        = dz;

    if (core_add_th_job(_pf_astar_callback, args) != 0)
    {
        _pf_free_args(args);
        DEBUG_PRINTF("%s: failed to add find-path job to thread pool!\n",
            __func__);
        return PFERR_THREADS;
    } else
        interlocked_increment_int32(&_num_path_finds_running);

    return 0;
}

void
pf_free_path(pf_path_t *pp)
    {_pf_path_pool_free(&_path_pool, pp);}

void
pf_wait_for_jobs()
{
    while (_num_path_finds_running > 0)
        core_do_th_work();
}

static int
_pf_pq_init(pf_pq_t *pq, int num)
{
    if (num <= 0) return 1;
    pf_pq_t tmp = {0};
    tmp.nodes = malloc(num * sizeof(pf_node_t*));
    if (!tmp.nodes) return 2;
    tmp.max = num;
    *pq = tmp;
    return 0;
}

void
_pf_pq_destroy(pf_pq_t *pq)
{
    free(pq->nodes);
    memset(pq, 0, sizeof(pf_pq_t));
}

static inline void
_pf_pq_insert_at(pf_pq_t *pq, pf_node_t *n, int i)
{
    int tot         = pq->first + pq->num;
    int num_left    = tot >= pq->max ? tot % pq->max : 0;

    int amount;

    if (i < num_left)
    {
        amount = (num_left - i) * sizeof(pf_node_t*);
        memmove(pq->nodes + i + 1, pq->nodes + i, amount);
    } else
    {
        if (num_left > 0)
        {
            amount = num_left * sizeof(pf_node_t*);
            memmove(pq->nodes + 1, pq->nodes, amount);
            pq->nodes[0] = pq->nodes[pq->max -1];

        }

        amount = (pq->max - i - 1) * sizeof(pf_node_t*);
        muta_assert(i + amount <= pq->max * (int)sizeof(pf_node_t*));
        memmove(pq->nodes + i + 1, pq->nodes + i, amount);
    }

    pq->nodes[i] = n;
}

static int
_pf_pq_push(pf_pq_t *pq, pf_node_t *n)
{
    muta_assert(n);

    /* Enlarge array if necessary */
    if (pq->num == pq->max)
    {
        int new_max = pq->max + 4;

        DEBUG_PRINTF("%s: enlarging (from %d to %d.)\n", __func__, pq->max,
            new_max);

        pf_node_t **nn = realloc(pq->nodes, new_max * sizeof(pf_node_t*));
        if (!nn) return 1;

        /* Copy the items at the beginning of the array to the end */
        int num_unfilled    = new_max - pq->num;
        int num_cpy         = MIN(pq->first, num_unfilled);

        DEBUG_PRINTF("%s: moving %d items to beginning of array.\n", __func__,
            num_cpy);

        memcpy(nn + pq->max, nn, num_cpy * sizeof(pf_node_t*));

        /* Copy the remaining items to the beginning of the array */
        int sub = pq->first - num_cpy;

        if (sub > 0)
        {
            DEBUG_PRINTF("%s: moving %d left over items to beginning of "
                "array.\n", __func__, sub);
            muta_assert(num_cpy + sub < pq->max);
            memmove(nn, nn + num_cpy, sub * sizeof(pf_node_t*));
        }

        pq->nodes   = nn;
        pq->max     = new_max;
    }

    if (pq->num > 0)
    {
        uint16 f = PF_NODE_F(n);

        if (f < PF_NODE_F(PF_PQ_FIRST(pq)))
        {
            int new_first = (pq->first - 1);
            if (new_first < 0)
                new_first = pq->max + new_first;
            pq->first = new_first;
            pq->nodes[pq->first] = n;
        } else
        if (f > PF_NODE_F(PF_PQ_LAST(pq)))
        {
            pq->nodes[(pq->first + pq->num) % pq->max] = n;
        } else
        {
            pf_node_t   *on;
            int         index;
            bool32      inserted    = 0;
            int         num         = pq->num;

            for (int i = 0; i < num; ++i)
            {
                index   = (pq->first + i) % pq->max;
                on      = pq->nodes[index];

                if (f <= PF_NODE_F(on))
                {
                    _pf_pq_insert_at(pq, n, index);
                    inserted = 1;
                    break;
                }
            }

            muta_assert(inserted);
        }
    } else
        pq->nodes[pq->first] = n;

    pq->num++;
    return 0;
}

static inline pf_node_t *
_pf_pq_pop_first(pf_pq_t *pq)
{
    if (pq->num > 0)
    {
        pf_node_t *ret = pq->nodes[pq->first];
        pq->first = (pq->first + 1) % pq->max;
        pq->num--;
        return ret;
    }
    return 0;
}

static inline void
_pf_pq_update_priority(pf_pq_t *pq, pf_node_t *n)
{
    int i, j, k, h;

    for (i = 0; i < pq->num; ++i)
    {
        j = (pq->first + i) % pq->max;

        if (pq->nodes[j] == n)
        {
            for (k = i; k > 0; --k)
            {
                h = (pq->first + k - 1) % pq->max;

                if (PF_NODE_F(n) < PF_NODE_F(pq->nodes[h]))
                {
                    pq->nodes[(pq->first + k) % pq->max] = pq->nodes[h];
                    pq->nodes[h] = n;
                } else
                    break;
            }
        }
    }
}

static inline void
_pf_pq_clear(pf_pq_t *pq)
{
    pq->num     = 0;
    pq->first   = 0;
}

static int
_pf_grid_init(pf_grid_t *g, int w, int t)
{
    pf_grid_t tmp = {0};
    tmp.nodes = malloc(w * w * t * sizeof(pf_node_t));
    if (!tmp.nodes) return 1;

    int ret = 0;

    if (_pf_pq_init(&tmp.open, 64) != 0)
        {ret = 2; goto cleanup;}

    cleanup:

    if (ret == 0)
    {
        tmp.w   = w;
        tmp.t   = t;
        *g      = tmp;
    } else
        _pf_grid_destroy(&tmp);

    return ret;
}

static void
_pf_grid_destroy(pf_grid_t *g)
{
    free(g->nodes);
    _pf_pq_destroy(&g->open);
}

static pf_path_t *
_pf_astar(instance_part_t *map, pf_grid_t *grid, int32 sx, int32 sy, int8 sz,
    int32 dx, int32 dy, int8 dz)
{
    uint64 time_now = get_program_ticks_ms();

    int x_diff = sx - dx;
    int y_diff = sy - dy;
    int z_diff = sz - dz;

    if (ABS(x_diff) > grid->w) return 0;
    if (ABS(y_diff) > grid->w) return 0;
    if (ABS(z_diff) > grid->t) return 0;

    int pbase_x = ((sx + dx) / 2) - grid->w / 2;
    int pbase_y = ((sy + dy) / 2) - grid->w / 2;;
    int pbase_z = ((sz + dz) / 2) - grid->t / 2;

    grid->base_x = CLAMP(pbase_x, 0, (int)instance_part_tiled_w(map) - grid->w);
    grid->base_y = CLAMP(pbase_y, 0, (int)instance_part_tiled_h(map) - grid->w);
    grid->base_z = CLAMP(pbase_z, -1, MAP_CHUNK_T - grid->t);

    memset(grid->nodes, 0, grid->w * grid->w * grid->t * sizeof(pf_node_t));

    _pf_pq_clear(&grid->open);

    int         i, j, nx, ny, nz, dist_x, dist_y, dist_z, onx, ony, onz, dir;
    pf_score_t  g, h, f, old_f;
    pf_node_t   *n, *on;
    tile_def_t  *tdef;

    (void)dist_z;

    n = _get_pf_node(grid, sx, sy, sz);
    MARK_NODE_OPEN(n);
    _pf_pq_push(&grid->open, n);

    while (grid->open.num > 0)
    {
        n = _pf_pq_pop_first(&grid->open);
        _compute_node_pos(grid, n, &nx, &ny, &nz);

        for (i = -1; i < 2; ++i)
        {
            for (j = -1; j < 2; ++j)
            {
                if (i == 0 && j == 0)
                    continue;

                onx = nx + i;
                ony = ny + j;
                onz = nz;

                on = _get_pf_node(grid, onx, ony, onz);

                if (!on || IS_NODE_CLOSED(on))
                    continue;

                tdef    = instance_part_get_tile_def(map, onx, ony, onz);
                dir     = vec2_to_iso_dir(onx - nx, ony - ny);

                /* Check if diagonal movement is blocked */
                if (instance_part_check_diagonals(map, dir, nx, ny, nz))
                    continue;

                /* Found destination - build path and exit */
                if (onx == dx && ony == dy && onz == dz)
                {
                    build_path:

                    on->p = n;
                    pf_path_t *ret = _pf_path_pool_reserve(&_path_pool);

                    if (!ret)
                    {
                        printf("%s:  failed to allocate finished path!\n",
                            __func__);
                        return 0;
                    }

                    /* End at np->p because the first node isn't needed */
                    for (pf_node_t *np = on; np->p; np = np->p)
                    {
                        _compute_node_pos(grid, np, &nx, &ny, &nz);
                        /* TODO: pre-compute the amount of nodes that are
                         * going to be pushed */
                        if (_pf_path_push(ret, nx, ny, nz) != 0)
                        {
                            printf("%s: failed to push node - out of "
                                "memory.\n", __func__);
                            _pf_path_pool_free(&_path_pool, ret);
                            return 0;
                        }
                    }

                    printf("Pathfinding from [%d,%d,%d] to [%d,%d,%d] took %llu"
                        " ms, path length %d.\n", sx, sy, (int)sz, dx, dy,
                        (int)dz, (lluint)(get_program_ticks_ms() - time_now),
                        ret->num_nodes);
                    return ret;
                }

                if (!tdef->passthrough)
                {
                    if (tdef->ramp == RAMP_NONE || onz == MAX_TILE_Z)
                        continue;

                    /* The tile is a ramp - check if its approachable from this
                     * direction */

                    if (check_ramp_up(dir, tdef->ramp))//(tdef->ramp != ramp_up_table[dir])
                        continue;

                    /* Attempt to re-fetch the other node (on) as the target
                     * z-position has changed */
                    onz++;
                    on =  _get_pf_node(grid, onx, ony, onz);

                    if (!on || IS_NODE_CLOSED(on))
                        continue;

                    if (onx == dx && ony == dy && onz == dz)
                        goto build_path;

                    /* It's a ramp, but is the tile above is passthrough? */
                    if (!instance_part_get_tile_def(map, onx, ony,
                        onz)->passthrough)
                        continue;
                } else
                {
                    /* Don't walk on top of a passthrough tile, unless there's a
                     * ramp down under the current tile */

                    /* Check if we're standing on a ramp down, unless we're at
                     * the lowest layer possible. */
                    if (nz > 0)
                    {
                        tdef = instance_part_get_tile_def(map, onx, ony, onz - 1);

                        if (tdef->passthrough)
                        {
                            tdef = instance_part_get_tile_def(map, nx, ny,
                                nz - 1);

                            if (tdef->ramp == RAMP_NONE || onz == 0)
                                continue;

                            if (check_ramp_down(dir, tdef->ramp))
                                continue;

                            /* It's a ramp down */
                            onz--;
                            on = _get_pf_node(grid, onx, ony, onz);

                            if (onx == dx && ony == dy && onz == dz)
                                goto build_path;

                            if (!on || IS_NODE_CLOSED(on))
                                continue;
                        }
                    }
                }

                dist_x = onx - nx;
                dist_y = ony - ny;
                dist_z = onz - nz;

                // if (dir)
                // g = n->g + ABS(dist_x) + ABS(dist_y); /* + ABS(dist_z); */

                if (dir_is_diagonal(dir))
                    g = n->g + (((ABS(dist_x) + ABS(dist_y)) * 140) / 100);
                else
                    g = n->g + ABS(dist_x) + ABS(dist_y);

                /* Sub the corner that will be formed */
                if (dist_x != 0 && dist_y != 0) g--;

                dist_x = onx - dx;
                dist_y = ony - dy;
                dist_z = onz - dz;

                h = ABS(dist_x) + ABS(dist_y); /* + ABS(dist_z); */

                f       = g + h;
                old_f   = PF_NODE_F(on);

                if (IS_NODE_OPEN(on) && old_f < f)
                    continue;

                on->g = g;
                on->h = h;
                on->p = n;

                if (!IS_NODE_OPEN(on))
                {
                    _pf_pq_push(&grid->open, on);
                    MARK_NODE_OPEN(on);
                } else
                    _pf_pq_update_priority(&grid->open, on);
            }
        }

        MARK_NODE_CLOSED(n);
    }

    DEBUG_PRINTF("%s: failed to build path from %d, %d, %d to %d, %d, %d!\n",
        __func__, sx, sy, (int)sz, dx, dy, (int)dz);
    return 0;
}

static inline pf_node_t *
_get_pf_node(pf_grid_t *g, int x, int y, int z)
{
    if (x >= g->base_x && x < g->base_x + g->w
    &&  y >= g->base_y && y < g->base_y + g->w
    &&  z >= g->base_z && z < g->base_z + g->t)
    {
        int rx = x - g->base_x;
        int ry = y - g->base_y;
        int rz = z - g->base_z;
        return &g->nodes[rz * (g->w * g->w) + ry * g->w + rx];
    }
    return 0;
}

static inline void
_compute_node_pos(pf_grid_t *g, pf_node_t *n, int *ret_x, int *ret_y,
    int *ret_z)
{
    int index   = (int)(n - g->nodes);
    int lsz     = g->w * g->w;
    int z       = (int)index / lsz;
    int y       = z > 0 ? (index % (z * lsz)) / g->w : 0;
    int x       = (int)index % g->w;
    *ret_x      = g->base_x + x;
    *ret_y      = g->base_y + y;
    *ret_z      = g->base_z + z;
}

static int
_pf_path_pool_init(pf_path_pool_t *p, int num_paths)
{
    if (num_paths <= 0) return 1;

    pf_path_pool_t  tmp = {0};
    int             ret = 0;

    mutex_init(&tmp.mtx);

    tmp.path_arrs = malloc(8 * sizeof(pf_path_t*));
    if (!tmp.path_arrs) {ret = 2; goto cleanup;}

    tmp.max_path_arrs = 8;

    /* Allocate the first path array of num_paths */
    tmp.path_arrs[0] = calloc(num_paths, sizeof(pf_path_t));
    if (!tmp.path_arrs[0]) {ret = 3; goto cleanup;}

    tmp.num_path_arrs = 1;

    for (int i = 0; i < num_paths; ++i)
    {
        tmp.path_arrs[0][i].prev = &tmp.path_arrs[0][i] - 1;
        tmp.path_arrs[0][i].next = &tmp.path_arrs[0][i] + 1;
    }

    tmp.path_arrs[0][0].prev = 0;
    tmp.path_arrs[0][num_paths - 1].next = 0;
    tmp.free = &tmp.path_arrs[0][0];

    for (int i = 0; i < num_paths; ++i)
    {
        if (_pf_path_init(&tmp.path_arrs[0][i], 32) != 0)
            {ret = 4; goto cleanup;}
    }

    cleanup:

    if (ret == 0)
        *p = tmp;
    else
        _pf_path_pool_destroy(&tmp);

    return ret;
}

static void
_pf_path_pool_destroy(pf_path_pool_t *p)
{
    mutex_destroy(&p->mtx);

    for (pf_path_t *pp = p->res; pp; pp = pp->next)
        free(pp->nodes);

    for (pf_path_t *pp = p->free; pp; pp = pp->next)
        free(pp->nodes);

    for (int i = 0; i < p->num_path_arrs; ++i)
        free(p->path_arrs[i]);

    free(p->path_arrs);
    memset(p, 0, sizeof(pf_path_pool_t));
}

static pf_path_t *
_pf_path_pool_reserve(pf_path_pool_t *p)
{
    pf_path_t *ret = 0;

    mutex_lock(&p->mtx);

    if (!p->free)
    {
        /* Allocate more */
        const int num_paths = 8;
        pf_path_t *paths = calloc(num_paths, sizeof(pf_path_t));

        if (!paths)
            goto cleanup;

        for (int i = 0; i < num_paths; ++i)
        {
            if (_pf_path_init(&paths[i], 32) == 0)
                continue;
            for (int j = 0; j < i; ++j)
                free(paths[j].nodes);
            goto cleanup;
        }

        if (p->num_path_arrs == p->max_path_arrs)
        {
            int new_max = p->num_path_arrs + 4;
            pf_path_t **path_arrs = realloc(p->path_arrs,
                new_max * sizeof(pf_path_t*));

            if (!path_arrs)
            {
                for (int j = 0; j < num_paths; ++j)
                    free(paths[j].nodes);
                goto cleanup;
            }

            p->path_arrs        = path_arrs;
            p->max_path_arrs    = new_max;
        }

        p->path_arrs[p->num_path_arrs++] = paths;

        for (int i = 0; i < num_paths; ++i)
        {
            paths[i].prev = &paths[i] - 1;
            paths[i].next = &paths[i] + 1;
        }

        paths[0].prev               = 0;
        paths[num_paths - 1].next   = 0;
        p->free                     = paths;
    }

    ret     = p->free;
    p->free = ret->next;

    if (p->res)
        p->res->prev = ret;

    ret->next       = p->res;
    ret->prev       = 0;
    ret->num_nodes  = 0;

#ifdef _MUTA_DEBUG
    muta_assert(!ret->is_reserved);
    ret->is_reserved = 1;
#endif

    cleanup:
    mutex_unlock(&p->mtx);

    return ret;
}

static void
_pf_path_pool_free(pf_path_pool_t *p, pf_path_t *pp)
{
    mutex_lock(&p->mtx);

#ifdef _MUTA_DEBUG
    muta_assert(pp->is_reserved);
    pp->is_reserved = 0;
#endif

    if (pp->prev)
        pp->prev->next = pp->next;
    else
        p->res = pp->next;

    if (pp->next)
        pp->next->prev = pp->prev;

    pp->next    = p->free;
    p->free     = pp;

    mutex_unlock(&p->mtx);
}

static int
_pf_path_init(pf_path_t *p, int max_nodes)
{
    p->nodes = malloc(max_nodes * sizeof(world_pos_t));
    if (!p->nodes) return 1;
    p->num_nodes = 0;
    p->max_nodes = max_nodes;
    return 0;
}

static inline int
_pf_path_push(pf_path_t *pp, int32 x, int32 y, int8 z)
{
    if (pp->num_nodes == pp->max_nodes)
    {
        int new_max = pp->max_nodes + 4;
        world_pos_t *wps = realloc(pp->nodes, new_max * sizeof(world_pos_t));
        if (!wps) return 1;
        pp->max_nodes   = new_max;
        pp->nodes       = wps;
    }

    world_pos_t *wp = &pp->nodes[pp->num_nodes++];
    wp->x = x;
    wp->y = y;
    wp->z = z;
    return 0;
}

static inline pf_args_t *
_pf_reserve_args()
{
    pf_args_t *ret;
    mutex_lock(&_arg_pool_mtx);
    ret = pf_arg_pool_reserve(&_arg_pool);
    mutex_unlock(&_arg_pool_mtx);
    if (!ret) DEBUG_PRINTF("%s: failed to allocate args!\n", __func__);
    return ret;
}

static inline void
_pf_free_args(pf_args_t *args)
{
    mutex_lock(&_arg_pool_mtx);
    pf_arg_pool_free(&_arg_pool, args);
    mutex_unlock(&_arg_pool_mtx);
}

static void
_pf_astar_callback(void *args)
{
    pf_args_t *a    = (pf_args_t*)args;
    pf_grid_t *grid = _pf_reserve_grid();

    pf_path_t *p = _pf_astar(a->entity->inst, grid, a->sx, a->sy, a->sz, a->dx,
        a->dy, a->dz);
    _pf_free_grid(grid);
    pf_result_t res;
    res.entity = a->entity;

    if (p)
    {
        res.error   = 0;
        res.path    = p;

        if (instance_part_push_pf_result(a->entity->inst, res) != 0)
        {
            printf("%s: failed to push result to world!\n", __func__);
            pf_free_path(p);
        }
    } else
    {
        res.error   = 1;
        res.path    = 0;
    }
    _pf_free_args(a);
    interlocked_decrement_int32(&_num_path_finds_running);
}

static inline pf_grid_t *
_pf_reserve_grid()
{
    pf_grid_t *ret;

    while (_num_free_grids == 0)
        cond_var_wait(&_grid_cvar, &_grid_mtx);

    ret = _free_grids[_num_free_grids - 1];
    _num_free_grids--;

    mutex_unlock(&_grid_mtx);
    return ret;
}

static inline void
_pf_free_grid(pf_grid_t *g)
{
    mutex_lock(&_grid_mtx);
    _free_grids[_num_free_grids++] = g;
    mutex_unlock(&_grid_mtx);
    cond_var_signal_one(&_grid_cvar);
}
