#include "core.h"
#include "../../shared/common_defs.h"
#include "../../shared/common_utils.h"

int main(int argc, char **argv)
{
    int     ret         = 0;
    bool32  daemonize   = 0;
    char    *cfg_fp = 0;

    for (int i = 1; i < argc; ++i)
    {
        if (streq(argv[i], "-d") || streq(argv[i], "--daemon"))
            daemonize = 1; else
        if ((streq(argv[i], "-c") || streq(argv[i], "--config"))
            && argc >= i + 2)
        {
            cfg_fp = argv[++i];
            printf("Config file specified: %s\n", cfg_fp);
        } else
        if (streq(argv[i], "-h") || streq(argv[i], "--help"))
        {
            puts("--help,   -h: print this message.\n"
                 "--daemon, -d: run in daemon mode (Linux)");
            ret = 0; goto out;
        } else
            {printf("Invalid param: %s.", argv[i]); ret = 2; goto out;}
    }

    if (daemonize && muta_daemonize())
        {puts("Daemon mode only supported on Linux."); return 1;}

    puts("MUTA worldd v. " MUTA_VERSION_STR);

    int r = core_init(cfg_fp);
    if (r)
    {
        printf("core_init() returned %d.\n", r);
        ret = 3;
        goto out;
    }


    for (;!core_update(););
    core_shutdown();

    out:
        printf("MUTA worldd: returning with code %d.\n", ret);
        sleep_ms(5000);
        return ret;
}
