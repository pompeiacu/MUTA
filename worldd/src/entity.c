#include <stddef.h>
#include "entity.h"
#include "instance_part.h"
#include "../../shared/world_common.h"
#include "pathfind.h"

/*-- Mobility component --*/
COMPONENT_DEFINITION(cmp_mobility, cmp_mobility_pool, ENT_CMP_MOBILITY,
    BLANK_CMP_REM);

/*-- Pathfind component --*/
#define CMP_PATHFIND_REM(e, cmp_a, cmp_b) cmp_pathfind_stop(&cmp_a->cmp);
COMPONENT_DEFINITION(cmp_pathfind, cmp_pathfind_pool, ENT_CMP_PATHFIND,
    CMP_PATHFIND_REM);

world_pos_t
entity_get_local_pos(entity_t *e)
{
    return instance_part_global_to_local_pos(e->inst, e->pos.x, e->pos.y,
        e->pos.z);
}

world_pos_t
entity_get_pos(entity_t *e)
    {return e->pos;}

void
entity_set_pos(entity_t *e, int32 x, int32 y, int8 z)
    {e->pos.x = x; e->pos.y = y; e->pos.z = z;}

void
entity_set_dir(entity_t *e, int dir)
    {if (dir >= 0 && dir < NUM_ISODIRS) e->dir = dir;}

int
entity_get_dir(entity_t *e)
    {return e->dir;}

int
entity_find_path(entity_t *e, int32 x, int32 y, int8 z)
{
    cmp_pathfind_t *pf = entity_get_cmp_pathfind(e);
    if (!pf)
        return 1;

    /* Check if the entity is already finding it's path to this position */
    if (pf->path)
    {
        world_pos_t *end = &pf->path->nodes[pf->path->num_nodes - 1];
        if (end->x == x && end->y == y && end->z == z)
            return 0;
        cmp_pathfind_stop(pf);
    }

    world_pos_t p = entity_get_local_pos(e);
    if (pf_trace(e, p.x, p.y, p.z, x, y, z))
        return 3;
    return 0;
}

void
cmp_pathfind_stop(cmp_pathfind_t *cmp)
{
    if (!cmp->path)
        return;
    pf_free_path(cmp->path);
    cmp->path = 0;
}
