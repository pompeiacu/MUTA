#ifndef MUTA_ENTITY_SCRIPT_H
#define MUTA_ENTITY_SCRIPT_H

#include "entity.h"

/* Forward declaration(s) */
typedef struct instance_part_t instance_part_t;

/* Types defined here (except for ones defined inside macros) */
typedef struct lua_script_t lua_script_t;
typedef struct cmp_lua_t    cmp_lua_t;
typedef struct lua_t        lua_t;

struct lua_script_t
{
    dchar   *name;
    dchar   *path;
    dchar   *code;
    dchar   *init_key;
    dchar   *tick_key;
};

/* Lua script component */
struct cmp_lua_t
{
    lua_script_t    *script;
    const char      *tick_key;
};

struct lua_t
{
    void *state;
};

COMPONENT_DECLARATION(cmp_lua, cmp_lua_pool, ENT_CMP_LUA);

int
lua_init(lua_t *lua);

void
lua_destroy(lua_t *lua);

void
update_cmp_lua_scripts(instance_part_t *inst_part, double dt);

void
cmp_lua_set_script(cmp_lua_t *cmp, const char *script_name);
/* If script_name is 0, current script will be unloaded */

#define cmp_lua_reload(cmp) \
    cmp_lua_set_script((cmp), (cmp)->script ? (cmp)->script->name : 0)

int
sbnk_reload(const char *script_name);

/*-- Script bank --*/

int
sbnk_init();
/* Load all lua scripts from disc as described in data files */

void
sbnk_destroy();

lua_script_t *
sbnk_get(const char *name);

#endif /* MUTA_ENTITY_SCRIPT_H */
