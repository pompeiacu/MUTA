#ifndef WORLD_INTERNAL_H
#define WORLD_INTERNAL_H

#include "../../shared/types.h"
#include "../../shared/containers.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/muta_map_format.h"
#include "pathfind.h"

/* A map object represents a part of the world. Generally there's probably a
 * single one of these per worldd process, but in case we want to create
 * instances later, it'll be it's own structure. */

/* Forward declaration(s) */
typedef struct tile_def_t tile_def_t;

/* Types defined here */
typedef struct map_chunk_t          map_chunk_t;
typedef struct map_data_t           map_data_t;

struct map_chunk_t
{
    muta_chunk_file_t file;
};

struct map_data_t
{
    muta_map_db_entry_t *map_db_entry;
    map_chunk_t         *chunks;
    uint32              cx;
    uint32              cy;
    uint32              cw;
    uint32              ch;
    uint32              user_count; /* For maps_claim/unclaim */
};
/* Static data for an instance part. May be used by multiple instance parts
 * simultaneously. */

int
maps_init();

void
maps_destroy();

map_data_t *
maps_claim(uint32 map_id, uint32 chunk_x, uint32 chunk_y, uint32 chunk_w,
    uint32 chunk_h);
/* Loads map if not yet loaded, assuming it exists. */

void
maps_unclaim(map_data_t *data);

#define map_data_get_chunk(d, x, y) \
    (&(d)->chunks[(y - (d)->cy) * (d)->cw + (x - (d)->cx)])

/* Tile getting macros */
#define GET_CHUNK_OF_TILE(inst, tx, ty) \
    ((inst)->data->chunks[((ty) / MAP_CHUNK_W) * (inst)->data->cw + \
        ((tx) / MAP_CHUNK_W)])
#define GET_TILE_FROM_CHUNK(ch, x, y, z) \
    (ch)->file.tiles[x * MAP_CHUNK_W * MAP_CHUNK_T + y * MAP_CHUNK_T + z]

#endif /* WORLD_INTERNAL_H */
