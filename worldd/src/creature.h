#ifndef MUTA_WORLDD_CREATURE_H
#define MUTA_WORLDD_CREATURE_H

/* Functions for manipulating creatures as commanded by the worldd over the
 * server. These functions will automatically post events to the server. */

#include "entity.h"

int
creature_set_dir(entity_t *cr, int dir);

int
creature_find_path(entity_t *cr, int32 x, int32 y, int8 z);

void
creature_emote(entity_t *cr, entity_t *target, uint16 emote_id);

#endif /* MUTA_WORLDD_CREATURE_H */
