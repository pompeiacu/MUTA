@echo off
CD ..\..\server\rundir
MODE 80,60
START /B muta_server.exe
CD ..\..\worldd\rundir
START CMD /C CALL "muta_worldd.exe"
CD ..\..\db-server\build
START CMD /C CALL "muta_db.exe"
cd ..\..\login-server\rundir
START CMD /C CALL "muta_login_server.exe"
