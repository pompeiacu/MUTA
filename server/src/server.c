#include <time.h>
#include <inttypes.h>
#include "server.h"
#include "../../shared/crypt.h"
#include "../../shared/netqueue.h"
#include "../../shared/sv_time.h"
#include "../../shared/common_defs.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/rwbits.inl"
#include "../../shared/world_packets.h"
#include "../../shared/world_common.h"
#include "../../shared/packets.h"
#include "../../shared/acc_utils.h"
#include "../../shared/containers.h"
#include "db.h"
#include "world.h"
#include "character.h"
#include "client.h"
#include "player_msgs.h"
#include "proxy.h"
#include "common.h"
#include "login_server.h"

#define DEFAULT_TICKRATE    60
#define MAX_TICKRATE        900
#define SYS_CL_BUF_SZ       MUTA_MTU

#define W_CLIENT_DATA_VALID(cd) \
    ((cd)->c->flags.reserved && (cd)->c->id == (cd)->id)

typedef struct client_t                 client_t;
typedef struct authed_user_t            authed_user_t;
typedef struct client_msg_out_notice_t  client_msg_out_notice_t;
typedef client_msg_out_notice_t         client_msg_out_notice_darr_t;
typedef struct disconnect_cmd_t         disconnect_cmd_t;
typedef disconnect_cmd_t                disconnect_cmd_darr_t;

struct client_msg_out_notice_t
{
    client_t    *client;
    uint32      id;
};

struct disconnect_cmd_t
{
    client_t    *client;
    uint32      client_id;
};

/* Externs */
sv_config_t sv_config;
int         w_conn_status = CONN_DISCONNECTED;

static struct
{
    uint32  *ips;
    int     num, max;
} _allowed_sys_client_ips;

static struct
{
    double_msg_buf_t    msgs_out;
    double_msg_buf_t    msgs_in;
} _w_conn;

static net_queue_t              _net_queue;
static net_handle_t             _worldd_accept_net_handle;
static bool32                   _running;
static perf_clock_t             _main_thread_clock;
static w_config_t               _world_cfg;
uint64                          sv_program_ticks_ms;

static void
_read_config(const char *fp);

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val);

static int
_push_allowed_sys_client_ip(uint8 a, uint8 b, uint8 c, uint8 d);

static bool32
_is_sys_client_ip_allowed(uint8 a, uint8 b, uint8 c, uint8 d);

static int
_init_world_msg_buffers();

static int
_update_db_queries();

static int
_begin_listening_to_worldds(uint16 port);

static void
_nq_accept_callback(net_handle_t *h, socket_t fd, addr_t addr, int err);

static void
_nq_read_callback(net_handle_t *h, int num_bytes);

static int
_read_known_client_packet(client_t *cl, uint8 *pckt, int pckt_len);
/* Returns the amount of bytes left in the buffer after the read, or < 0 if
 * the client should be disconnected. */

/* Client to server message handlers: */

static int
_handle_clmsg_create_character(client_t *cl, clmsg_create_character_t *s);

static int
_handle_clmsg_log_in_character(client_t *cl, clmsg_log_in_character_t *s);

/* Database query event handlers */

static void
_on_query_completed_account_characters(db_query_result_t *qr);

static void
_on_query_completed_create_character(db_query_result_t *res);

int
sv_init(const char *cfg_path)
{
    srand((uint)time(0));
    _read_config(cfg_path);
    perf_clock_init(&_main_thread_clock, sv_config.tickrate);
    if (crypt_init())
        return 2;
    if (cl_init(sv_config.max_players))
        return 3;
    if (_init_world_msg_buffers())
        return 4;
    if (db_init(5000, &sv_config.db_addr))
        return 5;
    if (ls_init())
        return 6;
    if (w_init(&_world_cfg))
        return 7;
    if (net_queue_init(&_net_queue, 1, _nq_read_callback, _nq_accept_callback))
        return 8;
    if (charcache_init())
        return 9;
    LOG("Using proxy instead of direct connections as defined in config.");
    if (proxy_init())
       return 12;
    return 0;
}

int
sv_shutdown()
{
    puts("Server shutdown called.");
    if (!_running)
        return 1;
    _running = 0;
    if (sv_config.use_proxy)
        proxy_stop();
    return 0;
}

int
sv_run()
{
    if (_running)
        return 1;
    sv_program_ticks_ms = get_program_ticks_ms();
    if (net_queue_run(&_net_queue))
        return 2;
    _running = 1;
    int err;
    if (_begin_listening_to_worldds(sv_config.world_port))
        {err = 3; goto failure;};
    if (ls_start())
        {err = 4; goto failure;}
    if (proxy_start())
        {err = 5; goto failure;}
    puts("Began to listen for new connections.");
    return 0;
    failure:
        _running = 0;
        printf("%s failed: error %d.", __func__, err);
        return err;
}

bool32
sv_update()
{
    perf_clock_tick(&_main_thread_clock);
    sv_program_ticks_ms = get_program_ticks_ms();
    if (db_conn_status() == DB_CONN_DISCONNECTED)
        db_connect();
    _update_db_queries();
    proxy_update();
    w_update();
    cl_complete_deferred_disconnects();
    cl_check_timeouts(sv_program_ticks_ms);
    ls_update();
    return _running ? 0 : 1;
}

void
sv_stop()
    {_running = 0;}

int
sv_handle_w_event_spawn_player(w_event_spawn_player_t *ev)
{
    w_client_data_t *cd = &ev->cd;

    if (!W_CLIENT_DATA_VALID(cd))
    {
        printf("%s: client invalid - despawning.", __func__);
        return 1;
    }

    client_t    *cl             = cd->c;
    uint64      character_id    = cl_current_character_props(cl)->id;

    if (character_id != ev->id)
    {
        DEBUG_PRINTFF("invalid character id: has: %" PRIu64 ","
            "event: %" PRIu64 ".", character_id, ev->id);
        w_despawn_player(ev->id);
        return 2;
    }

    switch (cl->account.character_state)
    {
        case AU_CHAR_NOT_LOGGED:
        case AU_CHAR_IN_WORLD:
            /* Update pos */
            return 0;
    }

    /* Add character to player */
    cl->account.character_state = AU_CHAR_IN_WORLD;
    return 0;
}

int
sv_handle_w_event_spawn_player_fail(w_event_spawn_player_fail_t *ev)
{
    printf("%s: IMPLEMENTME!\n", __func__);
    return 0;
}

int
sv_handle_w_event_despawn_player(w_event_despawn_player_t *ev)
{
    printf("%s: IMPLEMENTME!\n", __func__);
    return 0;
}

int
sv_new_client(uint32 proxy_socket_index, void **ret_ptr)
{
    client_t *cl = cl_new(proxy_socket_index);
    if (!cl)
        return 1;
    *ret_ptr = cl;
    if (!db_query_account_characters(
        proxy_socket_account_id(proxy_socket_index), 8))
        return 0;
    cl_disconnect_deferred(cl);
    return 2;
}

void
sv_read_client_packet(void *client, const void *memory, int num_bytes)
{
    client_t *cl = client;
    if (!cl->flags.reserved)
        return;
    int num_read = 0;
    while (num_read < num_bytes)
    {
        int num_to_read = MIN(num_bytes - num_read,
            BBUF_FREE_SPACE(&cl->msgs_in));
        muta_assert(num_to_read);
        BBUF_WRITE_BYTES(&cl->msgs_in, (uint8*)memory + num_read, num_to_read);
        int left = _read_known_client_packet(cl, cl->msgs_in.mem,
            cl->msgs_in.num_bytes);
        if (left < 0)
        {
            cl_disconnect_deferred(cl);
            break;
        }
        bbuf_cut_portion(&cl->msgs_in, 0, cl->msgs_in.num_bytes - left);
        num_read += num_to_read;
    }
}

static void
_read_config(const char *fp)
{
    sv_config.max_players               = 2048;
    sv_config.max_unah_clients          = 4096;
    sv_config.unauthed_client_timeout   = 5000;
    sv_config.authed_client_timeout     = 30000;
    sv_config.max_pf_req_freq           = DEFAULT_MAX_PF_REQUEST_FREQ;
    sv_config.tickrate                  = DEFAULT_TICKRATE;
    sv_config.world_port                = DEFAULT_WORLD_PORT;
    sv_config.db_addr = create_addr(127, 0, 0, 1, DEFAULT_DB_PORT);
    sv_config.login_addr = create_addr(127, 0, 0, 1,
        DEFAULT_SHARD_TO_LOGIN_PORT);
    sv_config.client_timeout_check_freq = 2500;
    sv_config.use_proxy                 = 1;
    sv_config.proxy_port                = DEFAULT_PROXY_PORT;
    _push_allowed_sys_client_ip(127, 0, 0, 1);
    strcpy(sv_config.shard_name, "Hellground");
    /* World configuration */
    _world_cfg.num_h_daemons        = 1;
    _world_cfg.num_v_daemons        = 1;
    _world_cfg.num_extra_daemons    = 0;
    _world_cfg.max_players          = sv_config.max_players;
    parse_cfg_file(fp ? fp : "config.cfg", _read_cfg_callback, 0);
}

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val)
{
    (void)ctx;
    if (streq(opt, "max players"))
    {
        int v = atoi(val);
        if (v <= 0) return;
        sv_config.max_players = v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "max unauthed clients"))
    {
        int v = atoi(val);
        if (v <= 0) return;
        sv_config.max_unah_clients = v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "unauthed client timeout"))
    {
        int v = atoi(val);
        if (v <= 0) return;
        sv_config.unauthed_client_timeout = v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "authed client timeout"))
    {
        int v = atoi(val);
        if (v <= 0) return;
        sv_config.authed_client_timeout = v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "world port"))
    {
        int v = atoi(val);
        if (v < 0 || v > 0xFFFF) return;
        sv_config.world_port = (uint16)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "max pathfind request frequency"))
    {
        int v = atoi(val);
        if (v < 0) return;
        sv_config.max_pf_req_freq = v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "tickrate"))
    {
        int v = atoi(val);
        if (v <= 0 || v > MAX_TICKRATE) return;
        sv_config.tickrate = v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "sys client ip"))
    {
        uint vs[4];
        if (sscanf(val, "%u.%u.%u.%u", &vs[0], &vs[1], &vs[2], &vs[3]) != 4)
            return;
        bool32 legal = 1;
        for (int i = 0; i < 4; ++i) if (vs[i] > 0xFF) {legal = 0; break;}
        if (!legal) return;
        _push_allowed_sys_client_ip(vs[0], vs[1], vs[2], vs[3]);
        printf("cfg - Allowed system client IP %d.%d.%d.%d.\n", vs[0], vs[1],
            vs[2], vs[3]);
    } else
    if (streq(opt, "db ip"))
    {
        uint vs[4];
        if (sscanf(val, "%u.%u.%u.%u", &vs[0], &vs[1], &vs[2], &vs[3]) != 4)
            return;
        bool32 legal = 1;
        for (int i = 0; i < 4; ++i) if (vs[i] > 0xFF) {legal = 0; break;}
        if (!legal) return;
        sv_config.db_addr.ip = uint32_ip_from_uint8s(vs[0], vs[1], vs[2],
            vs[3]);
        printf("cfg - %s: %u.%u.%u.%u\n", opt, vs[0], vs[1], vs[2], vs[3]);
    } else
    if (streq(opt, "login ip"))
    {
        uint vs[4];
        if (sscanf(val, "%u.%u.%u.%u", &vs[0], &vs[1], &vs[2], &vs[3]) != 4)
            return;
        bool32 legal = 1;
        for (int i = 0; i < 4; ++i) if (vs[i] > 0xFF) {legal = 0; break;}
        if (!legal) return;
        sv_config.login_addr.ip = uint32_ip_from_uint8s(vs[0], vs[1], vs[2],
            vs[3]);
        printf("cfg - %s: %u.%u.%u.%u\n", opt, vs[0], vs[1], vs[2], vs[3]);
    } else
    if (streq(opt, "world num h daemons"))
    {
        int v = atoi(val); if (v <= 0) return;
        _world_cfg.num_h_daemons = v;
    } else
    if (streq(opt, "world num v daemons"))
    {
        int v = atoi(val); if (v <= 0) return;
        _world_cfg.num_v_daemons = v;
    } else
    if (streq(opt, "world num extra daemons"))
    {
        int v = atoi(val); if (v <= 0) return;
        _world_cfg.num_extra_daemons = v;
    } else
    if (streq(opt, "client timeout check frequency"))
    {
        sv_config.client_timeout_check_freq = str_to_uint64(val);
        printf("cfg - %s: %s\n", opt, val);
    } else
    if (streq(opt, "use proxy"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
        {
            sv_config.use_proxy = v;
            printf("cfg - %s: %s\n", opt, val);
        }

    } else
    if (streq(opt, "proxy port"))
    {
        int v = atoi(val);
        if (v >= 0 && v <= 0xFFFF)
        {
            sv_config.proxy_port = (uint16)v;
            printf("cfg - %s: %s\n", opt, val);
        }
    } else
    if (streq(opt, "shard name"))
    {
        if (strlen(val) > MAX_SHARD_NAME_LEN)
        {
            LOG("Shard name '%s' is too long (max %u characters)",
                val, MAX_SHARD_NAME_LEN);
            return;
        }
        strcpy(sv_config.shard_name, val);
    }
}

static int
_push_allowed_sys_client_ip(uint8 a, uint8 b, uint8 c, uint8 d)
{
    if (_is_sys_client_ip_allowed(a, b, c, d))
        return 0;

    int num = _allowed_sys_client_ips.num;

    if (num == _allowed_sys_client_ips.max)
    {
        int new_max = _allowed_sys_client_ips.max + 8;

        uint32 *ips = realloc(_allowed_sys_client_ips.ips,
            new_max * sizeof(uint32));
        if (!ips) return 1;

        _allowed_sys_client_ips.ips = ips;
        _allowed_sys_client_ips.max = new_max;
    }

    uint32 ip;
    uint8 *ptr = (uint8*)&ip;
    ptr[0] = a; ptr[1] = b; ptr[2] = c; ptr[3] = d;

    _allowed_sys_client_ips.ips[num]    = ip;
    _allowed_sys_client_ips.num         = num + 1;

    return 0;
}

static bool32
_is_sys_client_ip_allowed(uint8 a, uint8 b, uint8 c, uint8 d)
{
    uint32 ip;
    uint8 *ptr = (uint8*)&ip;
    ptr[0] = a; ptr[1] = b; ptr[2] = c; ptr[3] = d;

    int num     = _allowed_sys_client_ips.num;
    uint32 *ips  = _allowed_sys_client_ips.ips;

    for (int i = 0; i < num; ++i)
        if (ips[i] == ip) return 1;
    return 0;
}

static int
_init_world_msg_buffers()
{
    if (double_msg_buf_init(&_w_conn.msgs_out, MUTA_MTU * 2))   return 1;
    if (double_msg_buf_init(&_w_conn.msgs_in, 65536))           return 2;
    return 0;
}

static int
_update_db_queries()
{
    if (db_update())
        return 1;
    uint32              num_results;
    db_query_result_t   *results = db_get_query_results(&num_results);
    for (uint32 i = 0; i < num_results; ++i)
    {
        switch (results[i].query.code)
        {
        case DB_QUERY_ACCOUNT_EXISTS:
            DEBUG_PUTS("DB_QUERY_ACCOUNT_EXISTS");
            break;
        case DB_UPDATE_CHARACTER_POSITION:
            DEBUG_PUTS("DB_UPDATE_CHARACTER_POSITION");
            break;
        case DB_QUERY_ACCOUNT_CHARACTERS:
            DEBUG_PUTS("DB_QUERY_ACCOUNT_CHARACTERS");
            _on_query_completed_account_characters(&results[i]);
            break;
        case DB_QUERY_CREATE_CHARACTER:
            DEBUG_PUTS("DB_QUERY_CREATE_CHARACTER");
            _on_query_completed_create_character(&results[i]);
            break;
        default:
            muta_assert(0);
        }
    }
    return 0;
}

static int
_begin_listening_to_worldds(uint16 port)
{
    socket_t    s = net_tcp_ipv4_sock();
    int         err;

    if (s == KSYS_INVALID_SOCKET)        {err = 1; goto failure;}
    if (net_make_sock_reusable(s) != 0)  {err = 2; goto failure;}
    if (net_bind(s, port) != 0)          {err = 3; goto failure;}
    if (net_listen(s, 5) != 0)           {err = 4; goto failure;}
    if (net_handle_init(&_worldd_accept_net_handle, s, NETQOP_LISTEN, 0, 0))
        {err = 5; goto failure;}
    if (net_queue_add(&_net_queue, &_worldd_accept_net_handle, 0, 0))
        {err = 6; goto failure;}
    printf("Successfully began listening to new system clients on port %u.\n",
        (uint)port);
    return 0;

    failure:
    {
        net_shutdown_sock(s, SOCKSD_BOTH);
        DEBUG_PRINTF("%s failed with code %d\n", __func__, err);
        return err;
    }
}

static void
_nq_accept_callback(net_handle_t *h, socket_t fd, addr_t addr, int err)
{
    muta_assert(!err);
    if (h == &_worldd_accept_net_handle)
        w_on_accept(&_net_queue, fd);
    else
        muta_assert(0);
    int r = net_queue_add(&_net_queue, h, 0, 0);
    muta_assert(!r);
}

static void
_nq_read_callback(net_handle_t *h, int num_bytes)
{
    switch (h->user_data.id)
    {
    case CL_WORLDD_CLIENT:
        w_on_read(h, num_bytes);
        break;
    default:
        muta_assert(0);
    }
}

/* Returns the amount of bytes left in the buffer after the read, or < 0 if
 * the client should be disconnected. */
static int
_read_known_client_packet(client_t *cl, uint8 *pckt, int pckt_len)
{
    byte_buf_t  bb          = BBUF_INITIALIZER(pckt, pckt_len);
    int         incomplete  = 0;
    int         dc          = 0;
    msg_type_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= MSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, msg_type_t, &msg_type);

        switch (msg_type)
        {
        #define HANDLE_MSG(higher_name, lower_name, read_postfix) \
            case higher_name: \
            { \
                lower_name##_t s; \
                incomplete = lower_name##_##read_postfix(&bb, &s); \
                if (!incomplete) dc = _handle_##lower_name(cl, &s); \
            } \
                break;
        case CLMSG_KEEP_ALIVE:
            break;
        HANDLE_MSG(CLMSG_CREATE_CHARACTER, clmsg_create_character, read);
        HANDLE_MSG(CLMSG_LOG_IN_CHARACTER, clmsg_log_in_character, read);
        #undef HANDLE_MSG
        default:
        {
            if (cl->account.character_state != AU_CHAR_IN_WORLD)
            {
                dc = 1;
                break;
            }

            switch (wpm_handle_player_msg(cl, msg_type, &bb))
            {
            case WPM_ACTION_OK:
                break;
            case WPM_ACTION_INCOMPLETE:
                DEBUG_PRINTFF("WPM_ACTION_INCOMPLETE\n");
                incomplete = 1;
                break;
            case WPM_ACTION_DISCONNECT:
                DEBUG_PRINTFF("WPM_ACTION_DISCONNECT\n");
                dc = 1;
                break;
            case WPM_ACTION_UNDEFINED_MSG:
                DEBUG_PRINTFF("WPM_ACTION_UNDEFINED_MSG\n");
                dc = 1;
                break;
            default:
                DEBUG_PRINTFF("Invalid WPM ACTION!\n");
                muta_assert(0);
            }
        }
            break;
        }

        if (dc || incomplete < 0)
        {
            DEBUG_PRINTFF("bad msg: type %d, dc %d, incomplete %d.\n",
                (int)msg_type, dc, incomplete);
            return -1;
        }
    }

    cl_save_received_timestamp(cl, sv_program_ticks_ms);
    return BBUF_FREE_SPACE(&bb);
}

static int
_handle_clmsg_create_character(client_t *cl, clmsg_create_character_t *s)
{
    if (cl->account.character_state != AU_CHAR_CHARACTER_SELECTION)
        return 1;
    if (cl->account.num_characters == MAX_CHARACTERS_PER_ACC)
        return 2;
    if (!accut_check_character_name(s->name, s->name_len))
        return 3;
    muta_assert(s->name_len <= MAX_CHARACTER_NAME_LEN);

    character_props_t cp;
    cp.account_id           = proxy_socket_account_id(cl->proxy_socket_index);
    memcpy(cp.name, s->name, s->name_len);
    cp.name[s->name_len]    = 0;
    cp.race                 = s->race;
    cp.sex                  = s->sex;
    cp.position.x           = 15;
    cp.position.y           = 15;
    cp.position.z           = 33;
    cp.map_id               = 0;
    cp.instance_id          = 0;

    if (db_query_create_character(&cp))
    {
        svmsg_create_character_fail_t s;
        s.reason = CREATE_CHARACTER_FAIL_DB_BUSY;
        bbuf_t bb = cl_send(cl, SVMSG_CREATE_CHARACTER_FAIL_SZ);
        if (!bb.max_bytes)
            return 4;
        svmsg_create_character_fail_write(&bb, &s);
    }
    cl->account.character_state = AU_CHAR_WAIT_FOR_NEW_CHARACTER;
    DEBUG_PRINTFF("sent request for character %s to db.\n", cp.name);
    return 0;
}

static int
_handle_clmsg_log_in_character(client_t *cl, clmsg_log_in_character_t *s)
{
    if (cl->account.character_state != AU_CHAR_CHARACTER_SELECTION)
        return 1;
    character_props_t *cp = 0;
    uint32 num_chars    = cl->account.num_characters;
    uint32 i            = 0;
    for (; i < num_chars; ++i)
    {
        if (cl->account.characters[i]->id != s->id)
            continue;
        cp = cl->account.characters[i];
        break;
    }
    if (!cp)
        return 2;
    /* TODO: handle this */
    w_client_data_t cd;
    cd.c    = cl;
    cd.id   = cl->id;
    int r;
    if ((r = w_spawn_player(proxy_socket_account_id(cl->proxy_socket_index), cd,
        cp)))
    {
        DEBUG_PRINTFF("w_spawn_player() returned %d.\n", r);
        return 3;
    }
    cl->account.character_state = AU_CHAR_IN_WORLD;
    cl->account.character_index = i;
    return 0;
}

static void
_on_query_completed_account_characters(db_query_result_t *qr)
{
    DEBUG_LOGF("completed!");
    client_t *cl = cl_get_by_account_id(qr->query.d.acc_id);
    if (!cl)
    {
        muta_assert(0);
        DEBUG_PRINTFF("account not found.\n");
        return;
    }
    db_res_account_characters_t *res = &qr->data.account_characters;
    uint32 num_chars = res->num_character_props;
    /* Cache the props */
    cl->account.num_characters = num_chars;
    for (uint32 i = 0; i < num_chars; ++i)
        cl->account.characters[i] = charcache_store(&res->character_props[i]);
    char    names[MAX_CHARACTERS_PER_ACC * (MAX_CHARACTER_NAME_LEN + 1)];
    uint64  ids[MAX_CHARACTERS_PER_ACC];
    uint8   races[MAX_CHARACTERS_PER_ACC];
    uint8   sexes[MAX_CHARACTERS_PER_ACC];
    uint8   name_indices[MAX_CHARACTERS_PER_ACC];
    svmsg_character_list_t s;
    s.ids_len           = num_chars;
    s.races_len         = num_chars;
    s.sexes_len         = num_chars;
    s.name_indices_len  = num_chars;
    s.names             = names;
    s.ids               = ids;
    s.races             = races;
    s.sexes             = sexes;
    s.name_indices      = name_indices;
    s.names_len         = 0;
    char *name = names;
    for (uint32 i = 0; i < num_chars; ++i)
    {
        character_props_t *cp = &res->character_props[i];
        uint32 tot_len = strlen(cp->name) + 1;
        name_indices[i] = s.names_len;
        memcpy(name, cp->name, tot_len);
        name        += tot_len;
        s.names_len += tot_len;
        s.ids[i]    = cp->id;
        s.races[i]  = cp->race;
        s.sexes[i]  = cp->sex;
    }
    bbuf_t bb = cl_send(cl, (int)SVMSG_CHARACTER_LIST_COMPUTE_SZ(s.names_len,
        s.ids_len, s.races_len, s.sexes_len, s.name_indices_len));
    if (!bb.max_bytes)
    {
        cl_disconnect_deferred(cl);
        return;
    }
    int r = svmsg_character_list_write(&bb, &s);
    muta_assert(!r);
    cl->account.character_state = AU_CHAR_CHARACTER_SELECTION;
    DEBUG_PRINTFF("sent character list to player.\n");
}

static void
_on_query_completed_create_character(db_query_result_t *qr)
{
    db_res_create_character_t *res = &qr->data.create_character;
    client_t *cl = cl_get_by_account_id(qr->query.d.acc_id);
    if (!cl)
        return;
    bbuf_t bb;
    if (res->error)
    {
        DEBUG_PRINTFF("character creation failed: %s.\n",
            character_creation_fail_to_str(res->error));
        svmsg_create_character_fail_t s;
        s.reason = (uint8)res->error;
        bb = cl_send(cl, SVMSG_CREATE_CHARACTER_FAIL_SZ);
        if (!bb.max_bytes)
            goto out;
        svmsg_create_character_fail_write(&bb, &s);
        goto out;
    }
    if (cl->account.num_characters > MAX_CHARACTERS_PER_ACC)
    {
        DEBUG_PRINTFF("warning: too many characters!\n");
        goto out;
    }
    character_props_t *cp = res->character_props;
    cl->account.characters[cl->account.num_characters++] = charcache_store(cp);
    svmsg_new_character_created_t s;
    s.id        = cp->id;
    s.name      = cp->name;
    s.name_len  = (uint8)cp->name_len;
    s.race      = cp->race;
    s.sex       = cp->sex;
    bb = cl_send(cl, SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(s.name_len));
    if (!bb.max_bytes)
        goto out;
    svmsg_new_character_created_write(&bb, &s);
    out:
        if (cl->account.character_state == AU_CHAR_WAIT_FOR_NEW_CHARACTER)
            cl->account.character_state = AU_CHAR_CHARACTER_SELECTION;
}
