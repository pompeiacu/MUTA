#ifndef MUTA_INSTANCE_H
#define MUTA_INSTANCE_H

#include "../../shared/sv_common_defs.h"
#include "../../shared/world_common.h"
#include "../../shared/common_utils.h"
#include "../../shared/muta_map_format.h"

/* Forward declaration(s) */
typedef struct player_t         player_t;
typedef player_t*               player_ptr_darr_t;
typedef struct dynamic_obj_t    dynamic_obj_t;
typedef struct creature_t       creature_t;
typedef struct daemon_t         daemon_t;
typedef struct map_data_t       map_data_t;

/* Types defined here */
typedef struct interest_area_t      interest_area_t;
typedef struct instance_part_t      instance_part_t;
typedef struct creature_spawn_t     creature_spawn_t;
typedef creature_spawn_t            creature_spawn_darr_t;
typedef struct dynamic_obj_spawn_t  dynamic_obj_spawn_t;
typedef dynamic_obj_spawn_t         dynamic_obj_spawn_darr_t;
typedef struct instance_t           instance_t;
typedef struct ia_obj_t             ia_obj_t;
typedef ia_obj_t                    ia_obj_darr_t;

enum ia_obj_types
{
    IA_OBJ_DYNAMIC_OBJ,
    IA_OBJ_CREATURE
};
/* Players ase not included here because they're in a separate array for faster
 * iteration while sending data to everyone in an interest area. */

struct creature_spawn_t
{
    uint32  type_id;
    wpos_t  position;
};

struct dynamic_obj_spawn_t
{
    uint32  type_id;
    wpos_t  position;
};

struct ia_obj_t
{
    void    *ptr;
    uint8   type;
};
/* A pointer from an interest area to a some kind of an entity */

struct interest_area_t
{
    player_ptr_darr_t   *players;
    ia_obj_darr_t       *objs;
};
/* A structure to help with determining which objects are relevant to each
 * player (as in, what's nearby) */

struct instance_part_t
{
    instance_t      *inst;
    uint32          runtime_id;
    uint32          cx, cy, cw, ch;
    instance_part_t *next;
    daemon_t        *daemon;
    bool32          confirmed; /* Confirmed as initialized by the daemon */
};
/* A single portion of a map simulated by some daemon */

struct instance_t
{
    dchar                       *path;  /* TODO: remove */
    uint32                      pw, ph; /* Width and height in map parts */
    uint32                      tw, th; /* Width and height in tiles */
    instance_part_t             *parts;
    map_data_t                  *map_data;
    interest_area_t             *int_areas;
    uint32                      num_x_ias, num_y_ias, num_z_ias;
    instance_guid_t             id;
    int                         shared;
    creature_spawn_darr_t       *creature_spawns;
    dynamic_obj_spawn_darr_t    *dynamic_obj_spawns;
};
/* A single instance simulating some map. Simulation may be devided to multiple
 * worldds. */

int
instance_init(instance_t *inst, int shared, inst_guid_t id,
    map_data_t *map_data, uint32 part_w, uint32 part_h);

void
instance_destroy(instance_t *inst);

#define instance_part_ok(ip) ((ip)->daemon && (ip)->confirmed)

static inline int
instance_compute_interest_area_index_by_tile_pos(instance_t *inst, int32 x,
    int32 y, int8 z);

static inline bool32
instance_is_pos_same_interest_area(instance_t *map, wpos_t *op, wpos_t *np);

static inline instance_part_t *
instance_get_part_by_pos(instance_t *map, int32 x, int32 y);
/* Position in 2D grid space (not in tiles) */

static inline interest_area_t *
instance_get_interest_area_by_tile_pos(instance_t *inst, int32 x, int32 y,
    int8 z);

static inline interest_area_t *
instance_get_interest_area(instance_t *map, int iax, int iay, int iaz);

uint32
instance_get_relevant_interest_areas(instance_t *inst, int32 x, int32 y, int8 z,
    interest_area_t *ret_arr[27]);

static inline int
instance_compute_interest_area_index_by_tile_pos(instance_t *inst, int32 x,
    int32 y, int8 z)
{
    int     tw  = inst->tw;
    int     th  = inst->th;
    int32   rx  = CLAMP(x, 0, tw - 1);
    int32   ry  = CLAMP(y, 0, th - 1);
    int8    rz  = CLAMP(z, 0, MAX_TILE_Z);
    int     iax = rx / IA_W;
    int     iay = ry / IA_W;
    int     iaz = rz / IA_T;
    return iaz * (inst->num_x_ias * inst->num_y_ias) + iay * inst->num_x_ias + \
        iax;
}


static inline bool32
instance_is_pos_same_interest_area(instance_t *inst, wpos_t *op, wpos_t *np)
{
    int oi = instance_compute_interest_area_index_by_tile_pos(inst, op->x,
        op->y, op->z);
    int ni = instance_compute_interest_area_index_by_tile_pos(inst, np->x,
        np->y, np->z);
    return oi == ni;
}

static instance_part_t *
instance_get_part_by_pos(instance_t *inst, int32 x, int32 y)
{
    int px = CLAMP(x, 0, (int)inst->pw - 1);
    int py = CLAMP(y, 0, (int)inst->ph - 1);
    return &inst->parts[py * inst->pw + px];
}

static inline interest_area_t *
instance_get_interest_area_by_tile_pos(instance_t *inst, int32 x, int32 y,
    int8 z)
{
    int i = instance_compute_interest_area_index_by_tile_pos(inst, x, y, z);
    return &inst->int_areas[i];
}


static inline interest_area_t *
instance_get_interest_area(instance_t *inst, int x, int y, int z)
{
    int x_ias = inst->num_x_ias;
    int y_ias = inst->num_y_ias;
    int z_ias = inst->num_z_ias;
    if (x < 0 || x >= x_ias || y < 0 || y >= y_ias || z < 0 || z >= z_ias)
        return 0;
    int i = z * (x_ias * y_ias) + y * x_ias + x;
    return &inst->int_areas[i];
}

#endif /* MUTA_INSTANCE_H */
