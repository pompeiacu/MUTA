#include <ctype.h>
#include "player_msgs.h"
#include "world.h"
#include "client.h"
#include "character.h"
#include "server.h"
#include "../../shared/packets.h"
#include "../../shared/world_common.h"
#include "../../shared/sv_common_defs.h"

static bool32
_try_execute_dot_cmd(client_t *cl, const char *cmd);
/* Returns non-zero if a command was executed */

static int
_handle_clmsg_chat_msg(client_t *cl, clmsg_chat_msg_t *s);

static int
_handle_clmsg_player_move(client_t *cl, clmsg_player_move_t *s);

static int
_handle_clmsg_find_path(client_t *cl, clmsg_find_path_t *s);

static int
_handle_clmsg_player_emote(client_t *cl, clmsg_player_emote_t *s);

int
wpm_handle_player_msg(client_t *cl, int msg_type, bbuf_t *bb)
{
    int incomplete  = 0;
    int dc          = 0;

    switch (msg_type)
    {
    #define HANDLE_MSG(higher_name, lower_name, read_postfix) \
        case higher_name: \
        { \
            lower_name##_t s; \
            incomplete = lower_name##_##read_postfix(bb, &s); \
            if (!incomplete) dc = _handle_##lower_name(cl, &s); \
        } \
            break;

    HANDLE_MSG(CLMSG_CHAT_MSG,      clmsg_chat_msg,     read);
    HANDLE_MSG(CLMSG_PLAYER_MOVE,   clmsg_player_move,  read);
    HANDLE_MSG(CLMSG_FIND_PATH,     clmsg_find_path,    read);
    HANDLE_MSG(CLMSG_PLAYER_EMOTE,  clmsg_player_emote, read);
    default:
        return WPM_ACTION_UNDEFINED_MSG;

    #undef HANDLE_MSG
    }

    if (incomplete < 0 || dc)
        return WPM_ACTION_DISCONNECT;

    if (incomplete)
        return WPM_ACTION_INCOMPLETE;

    return WPM_ACTION_OK;
}

/* Returns true if a command was executed */
static bool32
_try_execute_dot_cmd(client_t *cl, const char *cmd)
{
    if (cl->account.gm_level == GM_LEVEL_NONE)
        return 0;

    char buf[33]    = {0};
    char *dst       = buf;

    for (const char *c = cmd;
         dst < buf + 32 && *c && *c != ' ' && *c != '\t';
         ++c)
        *(dst++) = (char)tolower(*c);
    *dst = 0;
    if (streq(buf, ".teleport"))
    {
        int pos[3];
        if (sscanf(cmd, ".teleport %d %d %d", &pos[0], &pos[1], &pos[2]) == 3)
            w_teleport_player(cl_current_character_props(cl)->id, pos[0],
                pos[1], pos[2]);
        return 1;
    } else
    if (streq(buf, ".spawn"))
    {
        uint32  type_id;
        wpos_t  pos;
        if (sscanf(cmd + 6, " creature %u %d %d %c", &type_id, &pos.x, &pos.y,
            &pos.z) == 4)
            w_spawn_creature(0, type_id, pos, 0, 0);
        DEBUG_PRINTFF("spawning a creature.\n");
        return 1;
    } else
    if (streq(buf, ".reload_script"))
    {
        DEBUG_PRINTFF(".reload_script\n");
        const char *script_name = cmd + 14;
        while (*script_name && *script_name == ' ')
            script_name++;
        if (!*script_name)
            return 0;
        w_reload_entity_script(script_name);
        return 1;
    }
    /* TODO: send "invalid arguments" message if necessary */
    return 0;
}

static int
_handle_clmsg_chat_msg(client_t *cl, clmsg_chat_msg_t *s)
{
    if (s->msg_len <= 0 || s->msg_len > 255)
        return 2;
    if (!is_chat_msg_legaln(s->msg, s->msg_len))
        return 0;

    char str[256]; /* +2 for ": " */
    memcpy(str, s->msg, s->msg_len);
    str[s->msg_len] = 0;

    if (str[0] == '.' && _try_execute_dot_cmd(cl, str))
        return 0;

    w_tmp_broadcast_chat_msg(cl_current_character_props(cl)->id, s->msg,
        s->msg_len);
    return 0;
}

static int
_handle_clmsg_player_move(client_t *cl, clmsg_player_move_t *s)
{
    if (s->dir >= NUM_ISODIRS)
        return 2;
    if (w_walk_player(cl->account.characters[cl->account.character_index]->id,
        s->dir))
        return 3;
    return 0;
}

static int
_handle_clmsg_find_path(client_t *cl, clmsg_find_path_t *s)
{
    /* Temporarily do frequency checking here specifically for pathfinding */
    uint64 time_now     = sv_program_ticks_ms;
    uint64 time_passed  = time_now - cl->account.last_pf_request_time;
    if (time_passed <= sv_config.max_pf_req_freq)
        return 0;
    cl->account.last_pf_request_time = time_now;
    if (w_find_path_player(cl_current_character_props(cl)->id, s->x, s->y,
        s->z))
        return 2;
    return 0;
}

static int
_handle_clmsg_player_emote(client_t *cl, clmsg_player_emote_t *s)
{
    muta_assert(cl_current_character_props(cl));
    if (w_player_player_emote(cl_current_character_props(cl)->id,
        s->have_target, s->target_id, s->emote_id))
        return 2;
    return 0;
}
