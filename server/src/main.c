#include <stdio.h>
#include "server.h"
#include "../../shared/common_defs.h"
#include "../../shared/sv_time.c"
#include "../../shared/common_utils.h"

static const char *_title = "MUTA server v. " MUTA_VERSION_STR;

static int
_init(const char *cfg_file);

int main(int argc, char **argv)
{
    sleep_ms(2000);
    bool32  daemonize = 0;
    char    *param, *cfg_fp = 0;

    for (int i = 1; i < argc; ++i)
    {
        if (streq(argv[i], "-d") || streq(argv[i], "--daemon"))
            daemonize = 1;
        else if ((streq(argv[i], "-c") || streq(argv[i], "--config"))
            && argc >= i + 2)
        {
            cfg_fp = argv[++i];
            printf("Config file specified: %s\n", cfg_fp);
        } else
            {param = argv[i]; goto invalid_param;}
    }

    if (daemonize && muta_daemonize())
        {puts("Daemon mode only supported on Linux."); return 1;}

    puts(_title);

    int res = _init(cfg_fp);
    if (res)
    {
        puts("Exiting in 5 seconds...");
        sleep_ms(5000);
        return 2;
    }

    if (sv_run()) return 3;
    for (bool32 do_exit = 0; !do_exit; do_exit = sv_update()){}
    return 0;

    invalid_param:
        printf("Invalid parameter: %s.\n", param);
        puts("Exiting in 5 seconds...");
        return 4;
}

static int
_init(const char *cfg_file)
{
    puts("Initializing time...");
    init_time();
    puts("Done.");

    puts("Initializing socket API...");
    if (init_socket_api()) return 1;
    puts("Done.");

    puts("Initializing server...");
    int r = sv_init(cfg_file);
    puts("Done.");

    if (r) {printf("sv_init() failed with code %d\n", r); return 2;}
    return 0;
}
