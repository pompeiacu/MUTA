#ifndef MUTA_DB_QUERY_CACHE_H
#define MUTA_DB_QUERY_CACHE_H

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct pc_props_t                   pc_props_t;
typedef struct addr_t                       addr_t;
typedef struct character_props_t            character_props_t;

/* Types defined here */
typedef struct db_query_t                   db_query_t;
typedef struct db_query_event_t             db_query_event_t;

/* ...query result types */
typedef struct db_res_login_account_t       db_res_login_account_t;
typedef struct db_res_create_character_t    db_res_create_character_t;
typedef struct db_res_account_characters_t  db_res_account_characters_t;
typedef struct db_query_result_t            db_query_result_t;
typedef db_query_result_t                   db_query_result_darr_t;

enum db_query_event_type_t
{
    DB_EVENT_QUERY_TIMEOUT,
    DB_EVENT_QUERY_COMPLETED,
};

enum db_query_type_t
{
    DB_QUERY_ACCOUNT_EXISTS,
    DB_QUERY_LOGIN_ACCOUNT,
    DB_QUERY_CREATE_CHARACTER,
    DB_QUERY_LOGIN_CHARACTER,
    DB_UPDATE_CHARACTER_POSITION,
    DB_QUERY_ACCOUNT_CHARACTERS
};

enum db_error_t
{
    DB_ERR_OK     = 0,
    DB_ERR_NOMEM  = (1 << 0),
    DB_ERR_MISUSE = (1 << 1)
};

enum db_conn_status_t
{
    DB_CONN_DISCONNECTED,
    DB_CONN_CONNECTING,
    DB_CONN_CONNECTED
};

struct db_query_t
{
    union
    {
        uint64  acc_id;
        uint32  client_id;
    } d;
    uint8 code;
};

struct db_query_event_t
{
    db_query_t  query;
    uint8       type;
    int         num_bytes, offset;
};

struct db_res_login_account_t
{
    uint64  account_id;
    uint8   result;
};

struct db_res_create_character_t
{
    int                 error; /* 0 or one of enum character_creation_fail_reasons */
    character_props_t   *character_props;
};

struct db_res_account_characters_t
{
    character_props_t   *character_props;
    uint32              num_character_props;
};

struct db_query_result_t
{
    db_query_t query;
    union
    {
        db_res_login_account_t      login_account;
        db_res_create_character_t   create_character;
        db_res_account_characters_t account_characters;
    } data;
};

int
db_init(uint64 timeout, addr_t *db_server_addr);

int
db_update();

db_query_event_t *
db_events(uint *num);
/* Polled for new events */

void
db_clear_events();

db_query_result_t *
db_get_query_results(uint32 *num);

/* db_connect() and all query calls are asynchronous and non-blocking. Queries
 * will return their results later as events. */

int
db_conn_status();

bool32
db_ok();

int
db_connect();

void
db_disconnect();

void *
db_get_query_event_data(db_query_event_t *ev);

int
db_query_account_exists(const char *name, int name_len);

int
db_query_login_account(uint32 client_id, const char *name, uint8 name_len,
    const char *pw, uint8 pw_len);

int
db_query_create_character(character_props_t *cp);
int
db_update_char_pos(uint64 char_id, int32 x, int32 y, int8 z);

int
db_query_account_characters(uint64 acc_id, uint8 max);
/* Get all characters this account owns */

#endif /* MUTA_DB_QUERY_CACHE_H */
