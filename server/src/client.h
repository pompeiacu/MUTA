#ifndef MUTA_CLIENT_H
#define MUTA_CLIENT_H

#include "../../shared/crypt.h"
#include "../../shared/common_utils.h"
#include "../../shared/common_defs.h"
#include "../../shared/common_utils.h"
#include "world.h"

/* Forward declaration(s) */
typedef struct character_props_t character_props_t;

/* Types defined here */
typedef struct      client_t client_t;
typedef client_t *  client_ptr_darr_t;

#define CL_BUF_SZ MUTA_MTU

#define CL_IS_IN_WORLD(cl_) \
    ((cl_)->flags.authed && (cl_)->account.character_state == AU_CHAR_IN_WORLD)

/* Used as identifiers at the top of the client structs to differentiate
 * between system and user clients in async operations */
enum client_type_t
{
    CL_WORLDD_CLIENT = W_DAEMON_CLIENT_TYPE_ID,
    CL_USER_CLIENT
};

enum authed_user_character_state_t
{
    AU_CHAR_NOT_LOGGED,             /* Default when just logged in. */
    AU_CHAR_CHARACTER_SELECTION,    /* User is in the character selection screen */
    AU_CHAR_WAIT_FOR_NEW_CHARACTER, /* User needs a db reply for a new character */
    AU_CHAR_IN_WORLD                /* Logged on a character */
};

/* The connection structure for any player TCP client. */
struct client_t
{
    uint32          id; /* Runtime id used for identif. in async operations */
    uint32          timer_index; /* Timeout timer */
    uint32          proxy_socket_index;
    cryptchan_t     cryptchan;
    uint64          last_sent_to;
    struct
    {
        uint reserved:1;
        uint disconnect_deferred:1;
    } flags;
    struct
    {
        uint64              last_pf_request_time;
        character_props_t   *characters[MAX_CHARACTERS_PER_ACC];
        uint8               character_index;
        uint8               gm_level;
        /* One of enum authed_user_char_state */
        uint8               character_state;
        uint8               num_characters;
    } account; /* Data for a logged-in user */
    char            account_name[MAX_ACC_NAME_LEN + 1];
    uint32          account_name_len;
    bbuf_t          msgs_in;
    char            msgs_in_mem[CL_BUF_SZ];
};

#define cl_current_character_props(cl_) \
    ((cl_)->account.characters[(cl_)->account.character_index])

int
cl_init(uint32 max_clients);

void
cl_destroy();

client_t *
cl_new(uint32 proxy_socket_index);
/* Initializes a new client structure, but does not add it to a netqueue */

void
cl_disconnect(client_t *cl);

void
cl_disconnect_deferred(client_t *cl);
/* Don't call disconnect procedures immediately - instead, defer until end of
 * next call to cl_complete_deferred_disconnects() */

bbuf_t
cl_send(client_t *cl, int size);

bbuf_t
cl_send_const_encrypted(client_t *cl, int size);

bbuf_t
cl_send_var_encrypted(client_t *cl, int size);

client_t *
cl_get_by_account_id(uint64 account_id);

void
cl_send_pending_msgs();

void
cl_complete_deferred_disconnects();

void
cl_check_timeouts(uint64 program_ticks_ms);

void
cl_save_received_timestamp(client_t *cl, uint64 program_ticks_ms);

int
cl_init_cryptchan(client_t *cl, uint8 *wx, uint8 *rx, uint8 *sk, uint8 *pk,
    uint8 *read_key, uint8 *read_nonce, uint8 *write_key, uint8 *write_nonce);

#endif /* MUTA_CLIENT_H */
