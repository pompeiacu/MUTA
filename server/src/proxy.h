#ifndef MUTA_SERVER_PROXY_H
#define MUTA_SERVER_PROXY_H

/* Connection handling to proxy servers. Players connect to the server through
 * them. */

#include "../../shared/types.h"
#include "../../shared/common_utils.h"

#define INVALID_PROXY_SOCKET 0xFFFFFFFF

int
proxy_init();

void
proxy_destroy();

int
proxy_start();

void
proxy_stop();

void
proxy_update();

uint32
proxy_new_socket(const char *account_name, uint64 account_id,
    uint32 login_session_id, uint32 ip, uint8 *token);
/* Create an new player socket. The socket represents a single player's
 * connection to the shard through a (randomly?) selected proxy server.
 * This is actually an asynchronous call (a time out should be implemented). */

void
proxy_cancel_socket(uint64 account_id, uint32 login_session_id);
/* Cancel a dispatched command to create a new player socket. This is fired when
 * a client cancels a connection to a server before they have logged in to their
 * character. */

void
proxy_delete_socket(uint32 socket_index);

bbuf_t
proxy_forward_msg(uint32 socket_index, int size);

uint64
proxy_socket_account_id(uint32 socket_index);

void *
proxy_get_client_by_account_id(uint64 account_id);
/* Hack, remove later */

#endif /* MUTA_SERVER_PROXY_H */
