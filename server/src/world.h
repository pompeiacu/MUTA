#ifndef MUTA_SV_WORLD_H
#define MUTA_SV_WORLD_H

#include "../../shared/net.h"
#include "../../shared/common_defs.h"

#define W_DAEMON_CLIENT_TYPE_ID 101
#define W_MAIN_INSTANCE_ID      0
/* This will added to a daemon client's net_handle as user data */

/* Forward declaration(s) */
typedef struct net_handle_t         net_handle_t;
typedef struct net_queue_t          net_queue_t;
typedef struct client_t             client_t;
typedef struct pc_props_t           pc_props_t;
typedef struct character_props_t    character_props_t;

/* Types defined here */
typedef struct w_event_spawn_player_t       w_event_spawn_player_t;
typedef struct w_event_despawn_player_t     w_event_despawn_player_t;
typedef struct w_event_spawn_player_fail_t  w_event_spawn_player_fail_t;
typedef struct w_config_t                   w_config_t;
typedef struct w_client_data_t              w_client_data_t;

enum w_err_codes
{
    W_ERR_OK = 0,
    W_ERR_DAEMON_NOT_CONNECTED,
    W_ERR_PLAYER_ALREADY_LOGGED,
    W_ERR_TOO_MANY_PLAYERS,
    W_ERR_NO_MEM,
    W_ERR_INSTANCE_NOT_FOUND,
    W_ERR_CHARACTER_NOT_FOUND,
    W_ERR_WRONG_ACCOUNT_ID,
    NUM_W_ERR_CODES
};

struct w_config_t
{
    uint32 main_map_path_index;
    uint32 num_h_daemons;
    uint32 num_v_daemons;
    uint32 num_extra_daemons;
    uint32 max_players;
};

struct w_client_data_t
{
    client_t    *c;
    uint32      id;
};

struct w_event_spawn_player_t
{
    player_guid_t   id;
    int32           x;
    int32           y;
    int8            z;
    uint8           dir;
    uint8           sex;
    w_client_data_t cd;
};

struct w_event_despawn_player_t
    {player_guid_t char_id;};

struct w_event_spawn_player_fail_t
{
    player_guid_t   char_id;
    w_client_data_t cd;
};

int
w_init(w_config_t *cfg);

void
w_destroy();

void
w_on_accept(net_queue_t *nq, socket_t fd);
/* Called when a new client on a daemon port is accepted */

void
w_on_read(net_handle_t *h, int num_bytes);

void
w_update();

int
w_spawn_player(uint64 account_id, w_client_data_t cd, character_props_t *cp);
/* A spawn event will be fired later indicating success or failure (immediate
 * failure is also possible). Will also fail if player already exists. */

int
w_despawn_player(player_guid_t id);
/* Returns 0 if the character was already out */

int
w_walk_player(player_guid_t id, int dir);

int
w_find_path_player(player_guid_t id, int32 x, int32 y, int8 z);

const char *
w_err_str(int err);

int
w_player_player_emote(player_guid_t id, bool32 have_target,
    player_guid_t tar_id, emote_id_t eid);

int
w_teleport_player(player_guid_t id, int32 x, int32 y, int8 z);

int
w_spawn_creature(instance_guid_t inst, creature_type_id_t type_id,
    world_pos_t pos, int dir, uint8 flags);

uint32
w_get_default_shared_instance_id();

void
w_tmp_broadcast_chat_msg(player_guid_t talker, const char *msg, int msg_len);

void
w_reload_entity_script(const char *script_name);

#endif /* MUTA_SV_WORLD_H */
