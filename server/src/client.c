#include <inttypes.h>
#include "client.h"
#include "server.h"
#include "character.h"
#include "proxy.h"
#include "../../shared/common_utils.h"
#include "../../shared/rwbits.inl"
#include "../../shared/packets.h"
#include "../../shared/containers.h"
#include "../../shared/sv_time.h"

typedef struct cl_send_cmd_t        cl_send_cmd_t;
typedef struct cl_disconnect_cmd_t  cl_disconnect_cmd_t;
typedef cl_disconnect_cmd_t         cl_disconnect_cmd_darr_t;
typedef struct cl_timer_t           cl_timer_t;
typedef cl_timer_t                  cl_timer_darr_t;

DYNAMIC_HASH_TABLE_DEFINITION(uint64_client_table, client_t*, uint64, uint64,
    HASH_FROM_NUM, 2);
DYNAMIC_HASH_TABLE_DEFINITION(uint32_client_table, client_t*, uint32, uint32,
    HASH_FROM_NUM, 2);
DYNAMIC_HASH_TABLE_DEFINITION(str_client_table, client_t*, const char*, uint64,
    fnv_hash64_from_str, 2);

struct cl_send_cmd_t
{
    uint32      runtime_id;
    client_t    *client;
};

struct cl_disconnect_cmd_t
{
    uint32      runtime_id;
    client_t    *client;
};

struct cl_timer_t
{
    client_t    *client;
    uint32      runtime_id;
    uint64      timeout;
};

static obj_pool_t               _pool;
static uint32                   _running_id;
static uint32                   _max_clients;
static uint32_client_table_t    _runtime_id_client_table;
static uint64_client_table_t    _account_id_client_table;
static str_client_table_t       _account_name_client_table;
static cl_disconnect_cmd_darr_t *_deferred_disconnects;
static cl_timer_darr_t          *_timeout_timers;
static uint64                   _next_timeout_check;

int
cl_init(uint32 max_clients)
{
    _running_id     = 0;
    _max_clients    = max_clients;
    if (!max_clients)
        max_clients = 4096;
    obj_pool_init(&_pool, max_clients, sizeof(client_t));
    uint32_client_table_init(&_runtime_id_client_table, max_clients);
    uint64_client_table_init(&_account_id_client_table, max_clients);
    str_client_table_init(&_account_name_client_table, max_clients);
    darr_reserve(_deferred_disconnects, max_clients);
    darr_reserve(_timeout_timers, max_clients);
    _next_timeout_check = get_program_ticks_ms();
    return 0;
}

void
cl_destroy()
{
    obj_pool_destroy(&_pool);
    uint32_client_table_destroy(&_runtime_id_client_table);
    uint64_client_table_destroy(&_account_id_client_table);
    str_client_table_destroy(&_account_name_client_table);
    darr_free(_deferred_disconnects);
    darr_free(_timeout_timers);
}

client_t *
cl_new(uint32 proxy_socket_index)
{
    int err = 0;
    client_t *cl = obj_pool_reserve_static(&_pool);
    if (!cl)
        return 0;
    uint32 runtime_id = _running_id++;
    cl->id                              = runtime_id;
    cl->last_sent_to                    = 0;
    cl->flags.reserved                  = 1;
    cl->flags.disconnect_deferred       = 0;
    cl->account_name_len                = 0;
    cl->account_name[0]                 = 0;
    cl->proxy_socket_index              = proxy_socket_index;
    cl->account.last_pf_request_time    = 0;
    cl->account.character_state         = 0;
    cl->account.gm_level                = 0;
    cl->account.num_characters          = 0;
    if (cryptchan_init(&cl->cryptchan, 0))
        {err = 2; goto fail;}
    BBUF_INIT(&cl->msgs_in,  cl->msgs_in_mem,  CL_BUF_SZ);
    muta_assert(!uint32_client_table_exists(&_runtime_id_client_table,
        runtime_id));
    uint32_client_table_einsert(&_runtime_id_client_table, runtime_id, cl);
    /* Create a timeout timer for this client */
    cl->timer_index = darr_num(_timeout_timers);
    cl_timer_t timer;
    timer.client                = cl;
    timer.runtime_id            = runtime_id;
    timer.timeout = get_program_ticks_ms() + sv_config.authed_client_timeout;
    darr_push(_timeout_timers, timer);
    DEBUG_PRINTFF("reserved timer %u, timeout will be: %" PRIu64 "\n",
        cl->timer_index, timer.timeout);
    return cl;
    fail:
        cl->flags.reserved = 0;
        obj_pool_free(&_pool, cl);
        DEBUG_PRINTFF("error %d.\n", err);
        return 0;
}

void
cl_disconnect(client_t *cl)
{
    DEBUG_PRINTFF("called for client %u.\n", cl->id);
    proxy_delete_socket(cl->proxy_socket_index);
    muta_assert(!cl->flags.reserved ||
        (cl->flags.reserved && !cl->flags.disconnect_deferred));
    obj_pool_free(&_pool, cl);
    uint32_client_table_erase(&_runtime_id_client_table, cl->id);
    cl->flags.reserved              = 0;
    cl->flags.disconnect_deferred   = 0;
    /* Swap the last timer in the timeout timers array with the timer of this
     * one. */
    uint32      this_timer_index        = cl->timer_index;
    uint32      last_timer_index        = darr_num(_timeout_timers) - 1;
    _timeout_timers[this_timer_index]   = _timeout_timers[last_timer_index];
    _timeout_timers[this_timer_index].client->timer_index = this_timer_index;
    _darr_head(_timeout_timers)->num = last_timer_index;
    DEBUG_PRINTFF("swapped timers %u, %u\n", this_timer_index,
        last_timer_index);
    uint32 num_chars = cl->account.num_characters;
    for (uint32 i = 0; i < num_chars; ++i)
        charcache_unclaim(cl->account.characters[i]);
    if (cl->account.character_state != AU_CHAR_NOT_LOGGED)
        w_despawn_player(cl_current_character_props(cl)->id);
    if (cl->flags.disconnect_deferred)
        return;
    uint64_client_table_erase(&_account_id_client_table,
        proxy_socket_account_id(cl->proxy_socket_index));
    str_client_table_erase(&_account_name_client_table, cl->account_name);
    DEBUG_PRINTFF("removed client from account id and name tables.\n");
}

void
cl_disconnect_deferred(client_t *cl)
{
    DEBUG_PRINTFF("called.\n");
    muta_assert(cl->flags.reserved);
    cl->flags.reserved              = 0;
    cl->flags.disconnect_deferred   = 1;
    cl_disconnect_cmd_t cmd;
    cmd.client      = cl;
    cmd.runtime_id  = cl->id;
    darr_push(_deferred_disconnects, cmd);
}

bbuf_t
cl_send(client_t *cl, int size)
    {return proxy_forward_msg(cl->proxy_socket_index, MSGTSZ + size);}

bbuf_t
cl_send_const_encrypted(client_t *cl, int size)
{
    return proxy_forward_msg(cl->proxy_socket_index,
        MSGTSZ + CRYPT_MSG_ADDITIONAL_BYTES + size);
}

bbuf_t
cl_send_var_encrypted(client_t *cl, int size)
{
    return proxy_forward_msg(cl->proxy_socket_index,
        MSGTSZ + sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + size);
}

client_t *
cl_get_by_runtime_id(uint32 id)
{
    client_t **p = uint32_client_table_get_ptr(&_runtime_id_client_table, id);
    return p ? *p : 0;
}

client_t *
cl_get_by_account_id(uint64 account_id)
    {return proxy_get_client_by_account_id(account_id);}

void
cl_complete_deferred_disconnects()
{
    cl_disconnect_cmd_t *cmds       = _deferred_disconnects;
    uint32              num_cmds    = darr_num(cmds);
    client_t            *cl;
    for (uint32 i = 0; i < num_cmds; ++i)
    {
        cl = cmds[i].client;
        muta_assert(!cl->flags.reserved);
        if (cmds[i].runtime_id != cl->id)
            continue;
        if (!cl->flags.disconnect_deferred)
            continue;
        cl_disconnect(cl);
    }
    darr_clear(cmds);
}

void
cl_check_timeouts(uint64 program_ticks_ms)
{
    if (program_ticks_ms < _next_timeout_check)
        return;

    uint64 uth                  = sv_config.unauthed_client_timeout;
    uint64 ath                  = sv_config.authed_client_timeout;
    uint64 next_timeout_check   = _next_timeout_check + ath > uth ? uth : ath;

    cl_timer_t  *timers             = _timeout_timers;
    uint32      num_timers          = darr_num(_timeout_timers);
    uint64      timeout;
    cl_timer_t  *timer;

    for (uint32 i = 0; i < num_timers; ++i)
    {
        timer   = &timers[i];
        timeout = timer->timeout;
        if (timeout < next_timeout_check)
            next_timeout_check = timeout;
        if (timeout > program_ticks_ms)
            continue;
        DEBUG_PRINTFF("client %u timed out. %" PRIu64 ", %" PRIu64 ".\n",
            timer->client->id, timeout, program_ticks_ms);
        cl_disconnect_deferred(timer->client);
    }

    _next_timeout_check = next_timeout_check;
}

void
cl_save_received_timestamp(client_t *cl, uint64 program_ticks_ms)
{
    muta_assert(cl->flags.reserved);
    cl_timer_t  *timer      = &_timeout_timers[cl->timer_index];
    uint64      addition    = sv_config.authed_client_timeout;
    uint64      timeout     = program_ticks_ms + addition;
    timer->timeout = program_ticks_ms + addition;
    if (timeout < _next_timeout_check)
        _next_timeout_check = timeout;
}

int
cl_init_cryptchan(client_t *cl, uint8 *wx, uint8 *rx, uint8 *sk, uint8 *pk,
    uint8 *read_key, uint8 *read_nonce, uint8 *write_key, uint8 *write_nonce)
{
    if (cryptchan_is_encrypted(&cl->cryptchan))
        return 1;
    cryptchan_init_from_keys(&cl->cryptchan, wx, rx, sk, pk, read_key,
        read_nonce, write_key, write_nonce);
    muta_assert(cryptchan_is_encrypted(&cl->cryptchan));
    return 0;
}
