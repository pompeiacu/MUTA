#ifndef MUTA_SERVER_LOGIN_SERVER_H
#define MUTA_SERVER_LOGIN_SERVER_H

/* Connection handling to the login server */

#include "../../shared/types.h"

int
ls_init();

void
ls_destroy();

int
ls_start();

void
ls_stop();

bool32
ls_is_connected();

void
ls_update();

int
ls_send_player_select_shard_result(uint64 account_id, uint32 login_session_id,
    int result);

#endif /* MUTA_SERVER_LOGIN_SERVER_H */
