#include "maps.h"
#include "../../shared/muta_map_format.h"
#include "../../shared/common_utils.h"

typedef struct spawn_file_pair_t    spawn_file_pair_t;
typedef spawn_file_pair_t           spawn_file_pair_darr_t;
typedef map_data_t                  map_data_darr_t;

struct spawn_file_pair_t
{
    dchar *name;
    dchar *path;
};

static muta_map_db_t    _map_db;
static const char       *_map_db_path = "muta-data/common/maps.mapdb";
static const char       *_spawn_files_path = "muta-data/server/spawn_files.cfg";
static spawn_file_pair_darr_t *_spawn_file_pairs; /* Name, file path */
static map_data_darr_t  *_map_datas;

static void
_parse_spawn_files_txt_callback(void *ctx, const char *opt, const char *val);

int
maps_init()
{
    int ret = 0;

    if (muta_map_db_load(&_map_db, _map_db_path))
    {
        printf("Failed to read maps from %s.\n", _map_db_path);
        return 1;
    }

    /* Read spawn files */
    if (parse_cfg_file(_spawn_files_path, _parse_spawn_files_txt_callback, 0))
    {
        printf("Failed to read spawns from %s.\n", _spawn_files_path);
        ret = 1;
        goto out;
    }

    uint32 num_spawn_file_pairs = darr_num(_spawn_file_pairs);

    for (uint32 i = 0; i < _map_db.header.num_entries; ++i)
    {
        muta_map_db_entry_t *entry = &_map_db.entries[i];

        map_data_t map_data;
        map_data.id                 = entry->id;
        map_data.name               = entry->name;
        map_data.spawn_file_path    = 0;

        for (uint32 j = 0; j < num_spawn_file_pairs; ++j)
        {
            if (!streq(_spawn_file_pairs[i].name, map_data.name))
                continue;
            map_data.spawn_file_path = _spawn_file_pairs[i].path;
            break;
        }

        muta_map_file_t map_file;
        if (muta_map_file_load(&map_file, entry->path))
        {
            printf("Error loading map file %s from path %s!\n", map_data.name,
               entry->path);
            continue;
        }

        map_data.tiled_width    = muta_map_file_tw(&map_file);
        map_data.tiled_height   = muta_map_file_th(&map_file);
        darr_push(_map_datas, map_data);
    }

    out:
        if (ret)
            maps_destroy();
        return ret;
}

void
maps_destroy()
{
    muta_map_db_destroy(&_map_db);
    uint32 num_file_pairs = darr_num(_spawn_file_pairs);
    for (uint32 i = 0; i < num_file_pairs; ++i)
    {
        dstr_free(&_spawn_file_pairs[i].name);
        dstr_free(&_spawn_file_pairs[i].path);
    }
    darr_free(_spawn_file_pairs);
    darr_free(_map_datas);
}

map_data_t *
maps_get_by_name(const char *map_name)
{
    uint32 num_map_datas = darr_num(_map_datas);
    for (uint32 i = 0; i < num_map_datas; ++i)
        if (streq(map_name, _map_datas[i].name))
            return &_map_datas[i];
    return 0;
}

map_data_t *
maps_get_by_id(uint32 map_id)
{
    uint32 num_map_datas = darr_num(_map_datas);
    for (uint32 i = 0; i < num_map_datas; ++i)
        if (map_id == _map_datas[i].id)
            return &_map_datas[i];
    return 0;
}

static void
_parse_spawn_files_txt_callback(void *ctx, const char *opt, const char *val)
{
    spawn_file_pair_t pair;
    pair.name = dstr_create(opt);
    pair.path = dstr_create(val);
    darr_push(_spawn_file_pairs, pair);
    printf("Spawn file for map %s: %s\n", opt, val);
}
