Pre 26. April 2018

- Properly tracking when the UI was clicked to prevent the click from being
  passed on forward
- Fallout 1 -like shaders near walls
- Camera turning
- Logging utility

Packetwriter wishlist // Kuvis Apr 11 2018

- Variable arrays appear to use the same type for their 'len' type as the type
  they contain. So a uint64 array uses a uint64 for arr_len. Instead, they
  should use a uint32 by default.
  If a maximum number of items is defined for the array, it should compute
  what size the len type should be to be able to hold the max amount (so for
  example, the maximum is set to 512, so the len variable's type must be a
  uint16).

- New packetwriter feature: sets. Sometimes you have a struct of arrays that
  are all the same length. For example, if you have N player characters you
  need to serialize, you want a set of N players, something like this:

  struct characters:
  {
      uint32  characters_num;
      uint64  *character_ids;
      uint8   *character_races;
  };

  This way, we don't need a separate len variable for each one of the arrays.
  Sets could be always be variable length for now (although fixed size sets
  would also be nice).

  Maybe the user would need to declare a struct in the .mp file and then the
  packetwriter could create a new vararr-type for the struct?

- When the packetwriter creates the line "bbuf_reserve(N)", it
  should put N in brackets and cast it to int due to Microsoft
  compiler warnings.

- Comments: maybe, if a line starts with '#', it's read as a comment? Maybe
  you could even place comments at the end of lines to tell what a variable is
  about?

- Allow code strings and arithmetic in variable array and string maximum
  lengths. So that you can for example do this (where the latter is the
  maximum array length: uint8*: my_array (SOME_CONSTANT * 4)

- Ignore whitespace where it doesn't matter since the printf format is
  currently very string. This can be done with fscanf for example by using the
  pattern 

- Add support for floats

After 26. April 2018
- Logging utility for server and client.
- Character creation improvements: show animations in selection and creation,
  etc.
- Client options configuration interface. Graphics options, controls, etc.
- Making the map file system pack map chunks into a single file where they are
  read in blocks.
- Procedural map generation to create the starting point for a world map.

