# MUTA
Multi-User Time-Wasting Activity

Copyright: The MUTA team, 2017

## Cloning the repository
This project uses git-lfs for large file storage. To clone the repo with the
assets, you must install this git extension first.


## Game data
Some of the game data files (assets, scripts), are not provided. These may need
to be replaced by content of your own making. Tools for map editing, animating
and some other things are part of the client (currently accessible after
pressing F1 in the main menu).

## Building
Note: this process may or may not be up to date depending on the time of day,
the phase of the Moon and the alignment of the stars.

Supported compilers: GCC (GNU/Linux), MSVC (Windows)

### GNU/Linux

#### Requirements:
* gcc
* GNU make
* CMake (because of some dependencies, during first build. Sorry! Not required
  if you don't want to build db-server.)
* Internet connection during first build

```
$ cd MUTA
$ ./configure
$ make depend
$ make
```

### Windows

#### Requirements
* Visual Studio 13 or later
* nmake, cl and git commands set up in the PATH environment variable (cl must be the 32 bit version)
* Internet connection during first build
Configure options: x86 or x64. Configured for x64 by default.

```
cd MUTA
configure.bat
nmake
```

### Setting up the database
Install MariaDB in your system's preferred way (we use version 10.2), then run
the database init script in the sql folder:

`mysql -u root -e "SOURCE create.sql;"`


### Running
#### Client

Press F1 in the login screen to reveal dev tools including the various editors.

##### GNU/Linux
```
$ cd MUTA/client/rundir
$ ./run.sh
```

##### Windows
Run muta_win32.exe in MUTA/client/rundir.

#### Server
mysqld must be running in the background.

##### GNU/Linux
* Find out your myslqd socket path:
`mysql_config --socket`

Modify MUTA/db_server/build/config.cfg so that the parameter "mysql socket path"
is set correctly.

```
$ cd MUTA/server/rundir
$ ./run.sh --daemon
```

If successful, the processes muta_server, muta_worldd and muta_db should now
be running in the background.

##### Windows
Run run.bat in MUTA/server/rundir.
