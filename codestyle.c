The .c extension of this file is just for syntax highlighting.

> TABS

Four spaces wide - spaces, not tabs.

Example:

if (this)
{
    that();
};

###############################################################################

> DATATYPES

- Names are written completely in lower case.
- Words in names can be separeted with an underscore.
- All datatypes are postfixed with "_t".
- If a datatype is not for single-time use only, it's typedef should appear at
  the top of the compile unit's header (.h file), or at the top of the .c file
  in case of an internal datatype.
- If a struct or union has multiple members, the names of said variables are
  aligned with tabs.

Example:

/* External datatype that will be used by other files: */

typedef struct data_type_t data_type_t;

struct data_type_t
{
    int             member_a;
    unsigned int    member_b;
};

/* Internal datatype that isn't meant for external use (for example, only used
 * inside a .c file) doesn't necessarily need an early typedef/declaration: */

typedef struct
{
    int     x;
    float   y;
} internal_data_type_t;

###############################################################################

> ROW WIDTH

- Maximum row width is 80 characters.
- Exceptions can be made if they're sensibe, but if possible, even longer lines
  should be split up for readability's sake.

Example:

int some_variable = 80080 + some_other_variable * compute_this(20, 45) / 20 + \
    760 - 90 + compute_that(45, 20);

###############################################################################

> VARIABLES

- Variable names are written completely in lower case.
- Words in names can be separated with an underscore.
- If you want to emphasize that a variable is for internal use only, it can be
  prefixed with an underscore.

Example:

int some_variable;
data_type_t some_other_variable = {0};

/* An internal variable only used inside a single compilation unit */
static int _some_internal_variable;

###############################################################################

> FUNCTIONS

- Function names are written completely in lower case.
- Words in names can be separated with an underscore.
- The return type of the function is on a separate line before the name.
- Internal functions (statics) only used inside a single compilation unit
  should generally be prefixed with an underscore.
- If a function can fail and the fail could happen even in release mode,
  it should probably return a status (for example, an int), so that the error
  can be handled.
- Think carefully of the function name. Even if it ends up being long, it
  should be somewhat clear to others, too, what the function might do when
  called.
- Most functions should be declared before their definition. If they're
  external functions, declarations can reside in the .h file. Internal functions
  are declared at the top of the .c file. An exception can be made with small
  static functions which are used like macros or inside only one or a couple of
  functions.
- If a function returns something to a pointer given to it as a paremeter,
  its good practice to prefix the parameter with "ret_".

Example:

/* External function */

int
do_something(int num, char *str);

/* Internal function */

static float
_compute_this(float v, float c);

/* A function that returns something to a pointer */

void
do_something_but_return_ptr(int *ret_val, int arg);

###############################################################################


> MACROS

- All macros should be named in caps, except if the macro is to specifically
  parade as something else (for example, a function).
- If a macro represents a constant value, it doesn't necessarily need
  parenthesis at the end.
- If the macro contains logic, such as non-constant arithmetic, it should
  probably have parenthesis at the end.

Example:

#define CONSTANT_MACRO          5000
#define ANOTHER_CONSTANT_MACRO  (CONSTANT_MACRO + 100)
#define NON_CONSTANT_MACRO()    (some_function() * 20)

/* A macro parading as a function */

#define sb_malloc(args) malloc(args)

###############################################################################

> POINTERS

- When declaring pointers, the star comes right before the variable name.
- When casting to a pointer, the start can come right after the type name
  (to not make lines overly wide).

Example:

int *some_pointer;

char *some_casted_pointer = (char*)some_pointer;

###############################################################################

> COMMENTS

- If the comment is concerns a specific line, the comment is placed under said
  line, or at the end of the line if the line's width will stay below 80 columns
  after the comment.
- For multi-line comments, a star is placed at the beginning of each line,
  aligning with the first star.
- Use common sense: if you think the code may be looked at by other people and
  comments may help them to understand it, use them. If the code is
  self-explanatory, don't comment it.

Example:

int buffer_offset; /* We have to save the buffer position before proceeding */

int
do_something(int v);
/* do_something(): this multiline comment could tell us for example what the
 * function above returns and what possible side-effects it might have. */

###############################################################################

> ELSE CLAUSES

- If an else clause is preceded by a line with a single closing curly brace on
  it, the else is appended to that line.

Example:

if (this)
{
    /* ... */
} else
{
    /* ... */
}

###############################################################################

> SWITCH CASES
Cases inside switch statements are not indented.

Example:

switch (val)
{
case 1:
    ...
    break;
case 2:
    ...
    break;
}

###############################################################################
