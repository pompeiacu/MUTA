@echo off

set ERRORLEVEL=0

where nmake
echo %ERRORLEVEL%
if ERRORLEVEL 1 goto bad_nmake

where git
if ERRORLEVEL 1 goto bad_git

::Print help
if "%~1"=="-h" goto print_help
if "%~1"=="--help" goto print_help

::Check architecture
set arch=%1
if "%arch%"=="x86" goto do_configure
if "%arch%"=="x64" goto do_configure
if not "%arch%"=="" goto bad_arch
set arch=x64

:do_configure
    echo Configuring MUTA for architecture %arch%...

    nmake clean
    nmake depend

    echo Configuring client...
    cd client && call configure.bat %arch%
    cd ..
    if ERRORLEVEL 1 goto out

    echo Configuring server...
    cd server && call configure.bat %arch%
    cd ..
    if ERRORLEVEL 1 goto out

    echo Configuring db-server...
    cd db-server && call configure.bat %arch%
    cd ..
    if ERRORLEVEL 1 goto out

    echo Configuring worldd...
    cd worldd && call configure.bat %arch%
    cd ..
    if ERRORLEVEL 1 goto out

    echo Configuring proxy...
    cd proxy && call configure.bat %arch%
    cd ..
    if ERRORLEVEL 1 goto out

    echo Configuring login-server...
    cd login-server && call configure.bat %arch%
    cd ..
    if ERRORLEVEL 1 goto out

    goto success

:bad_arch
    echo %0: error, architecture must be x86 or x64, was %arch%
    goto out

:print_help
    echo Usage: %0 [architecture]
    echo Valid architectures: x86, x64
    echo Example: %0 x64
    goto out

:bad_nmake
    echo nmake not found!
    echo Make sure vcvarsall.bat was executed prior to calling this script.
    goto out

:bad_git
    echo git not found!
    echo Make sure git was added to path prior to calling this script.
    goto out

:success
    echo %0: configured for architecture %arch%

:out
