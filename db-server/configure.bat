:: This script assumes ..\configure.bat has already been called

@echo off

set ERRORLEVEL=0
set base_dir=%cd%

::Print help
if "%~1"=="-h" goto print_help
if "%~1"=="--help" goto print_help

::Check architecture
set arch=%1
if %arch%==x86 goto do_configure
if %arch%==x64 goto do_configure
if not %arch%=="" goto bad_arch
set arch=x64

:do_configure
    cd ..
    call replace_arch.bat %base_dir%\Makefile %arch%
    cd %base_dir%

    if ERRORLEVEL 1 goto out

    nmake clean
    rmdir /S /Q build\data
    rmdir /S /Q build\muta-data
    if exist build\config.cfg del build\config.cfg
    if exist build\libsodium.dll del build\libsodium.dll
    if exist build\sys_accs.txt del build\sys_accs.txt

    if not exist build mkdir build

    cd build
    if exist ..\..\data        mklink /D /J data ..\..\data 
    if exist ..\..\MUTA-Data   mklink /D /J muta-data ..\..\MUTA-Data 
    cd ..

    copy ..\libs\windows\lib\%arch%\libsodium.dll build\libsodium.dll
    if ERRORLEVEL 1 goto bad_dll_path

    copy ..\libs\windows\lib\%arch%\libmariadb.dll build\libmariadb.dll
    if ERRORLEVEL 1 goto bad_dll_path

    copy default_config.cfg build\config.cfg
    copy default_sys_accs.txt  build\sys_accs.txt

    goto out

:bad_arch
    echo %~dp0%0: error, architecture must be x86 or x64
    SET ERRORLEVEL=1
    goto out

:bad_dll_path
    echo %~dp0%0: a dll file was not found!
    goto out

:print_help
    echo Usage: %0 [architecture]
    echo Valid architectures: x86, x64
    echo Example: %0 x64
    goto out

:out
