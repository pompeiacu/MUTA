#ifndef MUTA_DB_H
#define MUTA_DB_H

#include "../../shared/types.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/common_defs.h"

#ifdef _WIN32
    #include <my_global.h>
    #include <mysql.h>
    #include <errmsg.h>
#else
    #include <mariadb/mysql.h>
    #include <mariadb/errmsg.h>
#endif

typedef struct db_character_props_t db_character_props_t;

enum db_conn_state_t
{
    DBCONN_DISCONNECTED,
    DBCONN_CONNECTING,
    DBCONN_CONNECTED
};

enum db_err_code_t
{
    DB_ERR_OK                   = 0,
    DB_ERR_ACC_CREATE_EXISTS    = -1,
    DB_ERR_ACC_CREATE_ERROR     = -2,
    DB_ERR_ACC_LOGIN_WRONG_PW   = -3,
    DB_ERR_ACC_LOGIN_ALREADY_IN = -4,
    DB_ERR_ACC_NO_EXISTS        = -5,
    DB_ERR_ENTRY_NO_EXISTS      = -6,
    DB_ERR_DISCONNECTED         = -7,
    DB_ERR_NOT_OK               = -8,
    DB_ERR_ENTRY_NOT_UNIQUE     = -9,
    DB_ERR_UNKNOWN              = -10
};

struct db_character_props_t
{
    uint64      id;
    uint64      account_id;
    int32       race;
    int32       sex;
    uint32      instance_id;
    uint32      map_id;
    world_pos_t position;
    char        name[MAX_CHARACTER_NAME_LEN + 1];
};

int
db_init(const char *sock_path);

int
db_connect(const char *ip, const char *name, const char *pw);

int
db_conn_state();

#define db_ok() (db_conn_state() == DBCONN_CONNECTED)

bool32
db_account_exists(const char *name);

bool32
db_account_id_exists(uint64 id);

int
db_create_account(const char *name, const char *pw, int pw_len);

int
db_login_account(const char *name, const char *pw, int pw_len);
/* Both strings must be zero terminated. */

int
db_get_account_id(const char *name, uint64 *ret_id);

int
db_get_character_ids_of_account(uint64 account_id,
    uint64 ret_ids[MAX_CHARACTERS_PER_ACC]);
/* Return value is the number of characters */

int
db_create_character(db_character_props_t *in_out_cp);
/* Character id is ignored */

int
db_update_char_pos(uint64 char_id, inst_guid_t inst, int32 x, int32 y, int8 z);

int
db_query_account_characters(uint64 id, uint32 max_characters,
    MYSQL_RES **ret_res);

#endif /* MUTA_DB_H */
