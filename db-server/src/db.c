#define HAVE_UINT64 1

#include <inttypes.h>
#include "db.h"
#include "../../shared/ksys.h"
#include "../../shared/common_utils.h"
#include "../../shared/crypt.h"

static bool32           _db_initialized;
static MYSQL            _db;
static int              _conn_state = DBCONN_DISCONNECTED;
static thread_t         _conn_thread;
static bool32           _conn_thread_created;
static dynamic_bbuf_t   _arg_buf;
static char             *_conn_ip, *_conn_name, *_conn_pw;
static char             _mysql_sock_path[256];
const char              *_mysql_sock_path_ptr;

static thread_ret_t
_db_conn_callback(void *args);

static int
_db_mysql_query(MYSQL *db, const char *str, ...);

int
db_init(const char *sock_path)
{
    if (_db_initialized) return 1;

    if (dynamic_bbuf_init(&_arg_buf, 512))
        return 1;

    int err = 0;

    MYSQL *db = mysql_init(&_db);
    if (!db) {err = 2; goto end;}

    my_bool yes = 1;
    mysql_options(&_db, MYSQL_OPT_RECONNECT, &yes);

    _mysql_sock_path_ptr = 0;

#ifndef _WIN32
    if(sock_path && strlen(sock_path) > 0)
    {
        strcpy(_mysql_sock_path, sock_path);
        _mysql_sock_path_ptr = _mysql_sock_path;
    } else
    {
        int r = 0;

        if (!system("mysql_config --socket > my_sock_path.tmp") ||
            !system("mariadb_config --socket > my_sock_path.tmp"))
        {
            FILE *f = fopen("my_sock_path.tmp", "r");
            if (!f)
            {
                r = 2;
                goto sock_path_out;
            }
            fgets(_mysql_sock_path, sizeof(_mysql_sock_path), f);
            str_strip_trailing_spaces(_mysql_sock_path);
            system("rm my_sock_path.tmp");
            _mysql_sock_path_ptr = _mysql_sock_path;
        } else
            r = 1;

        sock_path_out:
        if (r)
            printf("Socket path not defined in config and mysql_config and "
                "mariadb_config failed!\n");
    }
#else
#endif

    printf("%s: mysql socket path set to %s.\n", __func__, _mysql_sock_path);

    _conn_thread_created    = 0;
    _db_initialized         = 1;

    end:
    {
        if (err)
            dynamic_bbuf_destroy(&_arg_buf);
        return err;
    }
}

int
db_connect(const char *ip, const char *name, const char *pw)
{
    if (!_db_initialized)                   return 1;
    if (_conn_state == DBCONN_CONNECTED)    return 2;

    if (_conn_thread_created)
    {
        thread_join(&_conn_thread);
        _conn_thread_created = 0;
    }

    _conn_state = DBCONN_CONNECTING;

    BBUF_CLEAR(&_arg_buf);

    _conn_ip = (char*)BBUF_CUR_PTR(&_arg_buf);
    int len = (int)strlen(ip);
    dynamic_bbuf_write_bytes(&_arg_buf, ip, len + 1);

    _conn_name = (char*)BBUF_CUR_PTR(&_arg_buf);
    len = (int)strlen(name);
    dynamic_bbuf_write_bytes(&_arg_buf, name, len + 1);

    _conn_pw = (char*)BBUF_CUR_PTR(&_arg_buf);
    len = (int)strlen(pw);
    dynamic_bbuf_write_bytes(&_arg_buf, pw, len + 1);

    if (thread_create(&_conn_thread, _db_conn_callback, 0))
        {_conn_state = DBCONN_DISCONNECTED; return 3;}

    _conn_thread_created = 1;
    return 0;
}

int
db_conn_state()
    {return _conn_state;}

int
db_create_account(const char *name, const char *pw, int pw_len)
{
    if (!db_ok()) return DB_ERR_DISCONNECTED;

    if (db_account_exists(name))
        return DB_ERR_ACC_CREATE_EXISTS;

    char pw_hash[CRYPT_PW_HASH_SZ];
    if (crypt_storable_pw_hash(pw, pw_len, pw_hash, CRYPT_PW_STRENGTH_MEDIUM))
        return DB_ERR_ACC_CREATE_ERROR;

    const char *qstr = "INSERT INTO accounts (name, pw) VALUES('%s', '%s')";
    if (_db_mysql_query(&_db, qstr, name, pw_hash))
        return DB_ERR_ACC_CREATE_ERROR;
    return 0;
}

bool32
db_account_exists(const char *name)
{
    if (!db_ok()) return DB_ERR_DISCONNECTED;
    if (_db_mysql_query(&_db, "SELECT name FROM accounts WHERE name = %s",
        name))
        return 0;
    MYSQL_RES *res = mysql_store_result(&_db);
    if (!res) return 0;
    int ret = mysql_fetch_row(res) ? 1 : 0;
    mysql_free_result(res);
    return ret;
}

bool32
db_account_id_exists(uint64 id)
{
    if (!db_ok()) return DB_ERR_DISCONNECTED;
    if (_db_mysql_query(&_db, "SELECT id FROM accounts WHERE id = %llu", id))
        return 0;
    mysql_free_result(mysql_store_result(&_db));
    return 1;
}

int
db_login_account(const char *name, const char *pw, int pw_len)
{
    if (!db_ok())
        return DB_ERR_DISCONNECTED;

    int         err;
    int         ret     = 0;
    MYSQL_RES   *res    = 0;

    const char *q = "SELECT id, name, pw FROM accounts WHERE name = '%s'";

    if (_db_mysql_query(&_db, q, name))
        {ret = DB_ERR_ACC_NO_EXISTS; err = 1; goto out;}

    res = mysql_store_result(&_db);
    if (!res)
        {ret = DB_ERR_ACC_NO_EXISTS; err = 2; goto out;}

    MYSQL_ROW row = mysql_fetch_row(res);
    for (; row; row = mysql_fetch_row(res))
    {
        if (!streq(row[1], name)) continue;
        if (!crypt_test_stored_pw_hash(pw, pw_len, row[2]))
        {
            ret = DB_ERR_ACC_LOGIN_WRONG_PW;
            err = 3;
            goto out;
        }
        break;
    }

    if (!row)
        {ret = DB_ERR_ACC_NO_EXISTS; err = 4; goto out;}

    out:
        if (ret)
            DEBUG_PRINTFF("error %d\n", err);
        else
        {
            q = "UPDATE accounts SET last_login = CURRENT_TIMESTAMP WHERE id = "
                "'%" PRIu64 "'", str_to_uint64(row[0]);
            _db_mysql_query(&_db, q, row[0]);
        }
        if (res)
            mysql_free_result(res);
        return ret;
}

int
db_get_account_id(const char *name, uint64 *ret_id)
{
    if (!db_ok()) return DB_ERR_DISCONNECTED;
    const char *qs = "SELECT id FROM accounts WHERE name = '%s'";
    if (_db_mysql_query(&_db, qs, name)) return DB_ERR_ACC_NO_EXISTS;

    MYSQL_RES *res = mysql_store_result(&_db);
    if (!res) return DB_ERR_DISCONNECTED;

    int ret;

    MYSQL_ROW row = mysql_fetch_row(res);
    if (row)
        {*ret_id = str_to_uint64(row[0]); ret = 0;}
    else
        ret = DB_ERR_NOT_OK;

    mysql_free_result(res);
    return ret;
}

int
db_create_character(db_character_props_t *cp)
{
    if (!db_account_id_exists(cp->account_id))
        return DB_ERR_ACC_NO_EXISTS;
    if (!db_ok())
        return DB_ERR_DISCONNECTED;
    const char *qs = "INSERT INTO characters "
        "(account_id, name, race, sex, map_id, instance_id, pos_x, pos_y, pos_z)"
        "VALUES ('%" PRIu64 "', '%s', %u, %u, %u, %u, %d, %d, %d)";
    if (_db_mysql_query(&_db, qs, cp->account_id, cp->name, (uint32)cp->race,
        (uint32)cp->sex, cp->map_id, cp->instance_id, cp->position.x,
        cp->position.y, (int32)cp->position.z))
    {
        DEBUG_PRINTFF("%s\n", mysql_error(&_db));
        return DB_ERR_ENTRY_NOT_UNIQUE;
    }
    DEBUG_PRINTFF("created %" PRIu64 ", %s, %d, %d, %hhd\n", cp->account_id,
        cp->name, cp->position.x, cp->position.y, cp->position.z);
    mysql_free_result(mysql_store_result(&_db));

    if (_db_mysql_query(&_db, "SELECT id FROM characters WHERE account_id = "
        "%" PRIu64 " and name = '%s'", cp->account_id, cp->name))
        return DB_ERR_UNKNOWN;

    MYSQL_RES *res = mysql_store_result(&_db);
    if (!res)
        return DB_ERR_UNKNOWN;

    MYSQL_ROW row = mysql_fetch_row(res);
    if (!row)
        return DB_ERR_UNKNOWN;
    cp->id = str_to_uint64(row[0]);
    mysql_free_result(res);
    return 0;
}

int
db_get_character_ids_of_account(uint64 account_id,
    uint64 ret_ids[MAX_CHARACTERS_PER_ACC])
{
    if (!db_ok()) return DB_ERR_DISCONNECTED;
    const char *qs = "SELECT id FROM characters WHERE account_id = %llu";
    if (_db_mysql_query(&_db, qs, account_id)) return 0;

    int num = 0;

    MYSQL_RES *res = mysql_store_result(&_db);
    if (!res) return 0;

    for (MYSQL_ROW row = mysql_fetch_row(res); row; row = mysql_fetch_row(res))
    {
        ret_ids[num++] = str_to_uint64(row[0]);
        if (num == MAX_CHARACTERS_PER_ACC) break;
    }

    mysql_free_result(res);
    return num;
}

int
db_update_char_pos(uint64 char_id, inst_guid_t inst, int32 x, int32 y, int8 z)
{
    char *qstr = "UPDATE characters SET pos_x = %d, "
        "pos_y = %d, pos_z = %d WHERE id = %llu";
    if (_db_mysql_query(&_db, qstr, x, y, (int)z, char_id))
    {
        printf("%s: err %s.\n", __func__, mysql_error(&_db));
        return DB_ERR_ENTRY_NO_EXISTS;
    }
    DEBUG_PRINTFF("saving %" PRIu64 " , %d, %d, %d.\n", char_id, x, y,
        (int)z);
    mysql_free_result(mysql_store_result(&_db));
    return 0;
}

int
db_query_account_characters(uint64 id, uint32 max_characters,
    MYSQL_RES **ret_res)
{
    char *qstr = "SELECT name, id, race, sex, map_id, instance_id, pos_x, "
        "pos_y, pos_z FROM characters WHERE account_id = %llu LIMIT %u";
    int r;
    if ((r = _db_mysql_query(&_db, qstr, (lluint)id, max_characters)))
    {
        DEBUG_PRINTFF("err %s.\n", mysql_error(&_db));
        return DB_ERR_ENTRY_NO_EXISTS;
    }
    MYSQL_RES *res = mysql_store_result(&_db);
    if (!res)
        return 1;
    *ret_res = res;
    return 0;
}

static thread_ret_t
_db_conn_callback(void *args)
{
    (void)args;

    int flags = CLIENT_MULTI_STATEMENTS;
    if (!mysql_real_connect(&_db, _conn_ip, _conn_name, _conn_pw,
        "muta_accounts", 0, _mysql_sock_path_ptr, flags))
    {
        _conn_state = DBCONN_DISCONNECTED;
        puts(mysql_error(&_db));
        return 0;
    }
    puts("Successfully connected to db.");
    _conn_state = DBCONN_CONNECTED;
    return 0;
}

static int
_db_mysql_query(MYSQL *db, const char *str, ...)
{
    int         len;
    char        buf[512];
    const int   buf_sz      = 512;
    char        *ustr       = buf;
    char        *heap_alloc = 0;

    va_list args;
    va_start(args, str);
    len = vsnprintf(ustr, buf_sz - 1, str, args);
    va_end(args);

    if (len > buf_sz)
    {
        int alloc_sz = len + 4;
        heap_alloc = malloc(alloc_sz);
        if (!heap_alloc) return 1;
        ustr = heap_alloc;

        va_list nargs;
        va_start(nargs, str);
        vsnprintf(ustr, alloc_sz, str, nargs);
        va_end(nargs);
    }

    int ret = mysql_query(db, ustr);
    if (ret) DEBUG_PRINTF("%s: mysql error: %s (%d)\n", __func__,
        mysql_error(&_db), ret);
    if (heap_alloc)
        free(heap_alloc);
    return ret;
}
