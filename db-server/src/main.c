#include <stdio.h>
#include "core.h"
#include "../../shared/common_defs.h"
#include "../../shared/ksys.h"
#include "../../shared/common_utils.h"

static const char *_title = "MUTA db server version " MUTA_VERSION_STR;

int main(int argc, char **argv)
{
    char    *cfg_fp     = 0;
    bool32  daemonize   = 0;

    for (int i = 1; i < argc; ++i)
    {
        if (streq(argv[i], "-d") || streq(argv[i], "--daemon"))
            daemonize = 1; else
        if ((streq(argv[i], "-u") || streq(argv[i], "--user")) && argc >= i + 3)
        {
            if (!core_store_sys_user(argv[i + 1], argv[i + 2]))
                printf("Stored system user name %s.", argv[i + 1]);
            else
                return 1;
            i += 2;
            return 0;
        } else
        if (streq(argv[i], "-h") || streq(argv[i], "--help"))
        {
            puts("-d Run in daemon mode.");
            puts("-u Add new system user. Example: ./muta_db -u "
                "\"username\" \"password\"");
            return 0;
        } else
        if ((streq(argv[i], "-c") || streq(argv[i], "--config"))
        && argc >= i + 2)
        {
            cfg_fp = argv[++i];
            printf("Config file specified: %s\n", cfg_fp);
        } else
            {printf("Invalid parameter: %s", argv[i]); return 2;}
    }

    if (daemonize && muta_daemonize())
        {puts("Daemon mode unsupported."); return 1;}

    puts(_title);

    int ret         = 0;
    int init_res    = core_init(cfg_fp);

    if (init_res)
    {
        printf("Initialization error %d.\n", init_res);
        ret = 2;
        goto end;
    }

    puts("Initialization finished - running.");

    for (;!core_update(););

    end:;
    return ret;
}
