#include "shards.h"
#include "common.h"
#include "event.h"
#include "clients.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/crypt.h"
#include "../../shared/netpoll.h"
#include "../../shared/login_packets.h"

#define SHARD_IN_BUF_SIZE   MUTA_MTU
#define SHARD_OUT_BUF_SIZE  MUTA_MTU * 8
#define IS_SHARD(ptr_) \
    ((shard_t*)(ptr_) >= _shards && (shard_t*)(ptr_) < _shards + MAX_SHARDS)
#define SHARD_INDEX(ptr_) ((uint32)((shard_t*)(ptr_) - _shards))

typedef struct shard_t shard_t;

struct shard_t
{
    socket_t socket;
    struct
    {
        uint connected:1;
        uint online_for_players:1;
        uint started_crypt:1;
        uint authed:1;
        uint will_flush:1;
    } flags;
    struct
    {
        uint8   memory[SHARD_IN_BUF_SIZE];
        uint32  num_bytes;
    } in_buf;
    struct
    {
        uint8   memory[SHARD_OUT_BUF_SIZE];
        uint32  num_bytes;
    } out_buf;
    cryptchan_t cryptchan;
};

static shard_t      _shards[MAX_SHARDS];
static shard_t      *_flush_shards[MAX_SHARDS];
static int          _num_flush_shards;
static socket_t     _listen_socket;
static netpoll_t    _netpoll;
static thread_t     _thread;
static bool32       _running;
static segpool_t    _segpool;
static mutex_t      _segpool_mutex;

static thread_ret_t
_main(void *args);

static void
_allocate_messages_for_events(event_t *new_events, int num_new_events);

static void
_check_netpoll_events(netpoll_event_t *events, int num_events);

static void
_disconnect_shard(shard_t *shard, bool32 del_from_netpoll);

static int
_read_shard_packet(shard_t *shard);
/* Return values:
 * < 0 if shard should be disconnected,
 * >= 0 to indicate the amount of bytes left in the in_buf */

static int
_flush_shard(shard_t *shard);

static bbuf_t
_send_msg_to_shard(shard_t *shard, uint32 num_bytes);

static int
_handle_lmsg_pub_key(shard_t *shard, lmsg_pub_key_t *s);

static int
_handle_lmsg_stream_header(shard_t *shard, lmsg_stream_header_t *s);

static int
_handle_tlmsg_login_request(shard_t *shard, tlmsg_login_request_t *s);

static int
_handle_tlmsg_player_select_shard_result(shard_t *shard,
    tlmsg_player_select_shard_result_t *s);

static int
_handle_tlmsg_online_for_players(shard_t *shard);

static int
_handle_tlmsg_offline_for_players(shard_t *shard);

static int
_handle_tlmsg_player_select_shard_result(shard_t *shard,
    tlmsg_player_select_shard_result_t *s);

int
shards_init()
{
    int     ret     = 0;
    uint16  port    = DEFAULT_SHARD_TO_LOGIN_PORT;
    _listen_socket = net_tcp_ipv4_listen_sock(DEFAULT_SHARD_TO_LOGIN_PORT, 5);
    if (_listen_socket == KSYS_INVALID_SOCKET)
        {ret = 1; goto fail;}
    memset(_shards, 0, sizeof(_shards));
    if (thread_init(&_thread))
        {ret = 2; goto fail;}
    if (netpoll_init(&_netpoll))
        {ret = 3; goto fail;}
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    LOG("Listening to shards on port %u.", port);
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
shards_destroy()
{
}

int
shards_start()
{
    int ret = 0;
    _running = 1;
    if (thread_create(&_thread, _main, 0))
        {ret = 1; goto fail;}
    netpoll_event_t event;
    event.events    = NETPOLL_READ;
    event.data.ptr  = &_listen_socket;
    if (netpoll_add(&_netpoll, _listen_socket, &event))
        {ret = 2; goto fail;}
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return 0;
}

uint32
shards_get_index(const char *name)
{
    for (uint32 i = 0; i < com_config.shards.num; ++i)
        if (streq(com_config.shards.items[i].name, name))
            return i;
    return MAX_SHARDS;
}

const char *
shards_get_name(uint32 index)
{
    muta_assert(index < com_config.shards.num);
    return com_config.shards.items[index].name;
}

addr_t
shards_get_address(uint32 index)
{
    muta_assert(index < com_config.shards.num);
    return com_config.shards.items[index].address;
}

bool32
shards_exists(const char *name)
{
    for (uint32 i = 0; i < com_config.shards.num; ++i)
        if (streq(com_config.shards.items[i].name, name))
            return 1;
    return 0;
}

bool32
shards_is_online(uint32 index)
{
    muta_assert(index < com_config.shards.num);
    shard_t *shard = &_shards[index];
    return shard->flags.connected && shard->flags.online_for_players;
}

int
shards_select_for_player(const char *account_name, uint64 account_id,
    uint32 login_session_id, uint8 *token, uint32 ip, uint32 shard_index)
{
    muta_assert(shard_index < com_config.shards.num);
    shard_t *shard = &_shards[shard_index];
    flmsg_player_select_shard_t l_msg;
    l_msg.account_name          = account_name;
    l_msg.account_name_len      = (uint8)strlen(account_name);
    l_msg.account_id            = account_id;
    l_msg.login_session_id      = login_session_id;
    l_msg.ip                    = ip;
    memcpy(l_msg.token, token, AUTH_TOKEN_SZ);
    uint8   buf[LMSGTSZ + CRYPT_MSG_ADDITIONAL_BYTES +
        FLMSG_PLAYER_SELECT_SHARD_MAX_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    flmsg_player_select_shard_write_var_encrypted(&bb, &shard->cryptchan,
        &l_msg);
    if (net_send_all(shard->socket, bb.mem, bb.num_bytes) <= 0)
    {
        _disconnect_shard(shard, 1);
        return 1;
    }
    LOG("Sent player's select message to shard %s.",
        com_config.shards.items[shard_index].name);
    return 0;
}

void
shards_cancel_select_for_player(uint32 shard_index, uint64 account_id,
    uint32 login_session_id)
{
    muta_assert(shard_index < com_config.shards.num);
    shard_t *shard = &_shards[shard_index];
    if (!shard->flags.connected || !shard->flags.online_for_players)
        return;
    flmsg_player_cancel_select_shard_t s;
    s.account_id        = account_id;
    s.login_session_id  = login_session_id;
    uint8   buf[LMSGTSZ + FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    flmsg_player_cancel_select_shard_write(&bb, &s);
    if (net_send_all(shard->socket, bb.mem, bb.num_bytes) <= 0)
        _disconnect_shard(shard, 1);
}

void
shards_accept(accept_shard_event_t *event)
{
    shard_t *shard = &_shards[event->shard];
    if (shard->flags.connected)
        _disconnect_shard(shard, 1);
    net_disable_nagle(event->socket);
    memset(&shard->flags, 0, sizeof(shard->flags));
    memset(&shard->cryptchan, 0, sizeof(cryptchan_t));
    shard->flags.connected          = 1;
    shard->socket                   = event->socket;
    shard->in_buf.num_bytes         = 0;
    shard->out_buf.num_bytes        = 0;
    netpoll_event_t netpoll_event;
    netpoll_event.events    = NETPOLL_READ;
    netpoll_event.data.ptr  = shard;
    if (netpoll_add(&_netpoll, shard->socket, &netpoll_event))
    {
        LOGF("Failed to add shard to netpoll!");
        _disconnect_shard(shard, 0);
        return;
    }
    LOG("Accepted new shard connection.");
}

void
shards_read(read_shard_event_t *event)
{
    shard_t *shard = &_shards[event->shard];
    if (!shard->flags.connected)
    {
        DEBUG_LOGF("Attempted to read shard no longer connected!");
        goto out;
    }
    int num_bytes = event->num_bytes;
    if (num_bytes <= 0)
    {
        _disconnect_shard(shard, 0);
        return;
    }
    int num_moved = 0;
    while (num_moved < num_bytes)
    {
        int max_bytes   = SHARD_IN_BUF_SIZE - shard->in_buf.num_bytes;
        int num_to_move = MIN(num_bytes - num_moved, max_bytes);
        memcpy(shard->in_buf.memory + shard->in_buf.num_bytes,
            (uint8*)event->memory + num_moved, num_to_move);
        shard->in_buf.num_bytes += num_to_move;
        int num_left = _read_shard_packet(shard);
        if (num_left < 0)
        {
            _disconnect_shard(shard, 1);
            goto out;
        }
        memmove(shard->in_buf.memory,
            shard->in_buf.memory + shard->in_buf.num_bytes - num_left,
            num_left);
        shard->in_buf.num_bytes = num_left;
        num_moved += num_to_move;
    }
    out:
        if (event->num_bytes <= 0)
            return;
        mutex_lock(&_segpool_mutex);
        segpool_free(&_segpool, event->memory);
        mutex_unlock(&_segpool_mutex);
}

void
shards_flush()
{
    int num_to_flush = _num_flush_shards;
    for (int i = 0; i < num_to_flush; ++i)
    {
        shard_t *shard = _flush_shards[i];
        shard->flags.will_flush = 0;
        if (shard->socket == KSYS_INVALID_SOCKET)
            continue;
        if (!_flush_shard(shard))
            continue;
        _disconnect_shard(shard, 1);
        i--;
    }
    _num_flush_shards = 0;
}

static thread_ret_t
_main(void *args)
{
    netpoll_event_t events[64];
    int             num_events;
    while (_running)
    {
        num_events = netpoll_wait(&_netpoll, events, 64, 5000);
        _check_netpoll_events(events, num_events);
    }
    return 0;
}

static void
_allocate_messages_for_events(event_t *new_events, int num_new_events)
{
    mutex_lock(&_segpool_mutex);
    event_t *ne;
    void    *memory;
    for (int i = 0; i < num_new_events; ++i)
    {
        ne = &new_events[i];
        if (ne->type != EVENT_READ_SHARD || ne->read_shard.num_bytes <= 0)
            continue;
        memory = segpool_malloc(&_segpool, ne->read_shard.num_bytes);
        memcpy(memory, ne->read_shard.memory, ne->read_shard.num_bytes);
        ne->read_shard.memory = memory;
    }
    mutex_unlock(&_segpool_mutex);
}

static void
_check_netpoll_events(netpoll_event_t *events, int num_events)
{
    uint8           buf[8 * MUTA_MTU];
    event_t         new_events[8];
    int             num_new_events  = 0;
    int             buf_offset      = 0;
    int             num_reads       = 0;
    for (int i = 0; i < num_events; ++i)
    {
        if (!(events[i].events & (NETPOLL_READ | NETPOLL_HUP)))
            continue;
        buf_offset = 0;
        /*-- Accept a new shard --*/
        if (events[i].data.ptr == &_listen_socket)
        {
            addr_t      addr;
            socket_t    s = net_accept(_listen_socket, &addr);
            if (s == KSYS_INVALID_SOCKET)
                continue;
            uint32 index = MAX_SHARDS;
            for (uint32 i = 0; i < com_config.shards.num; ++i)
            {
                if (addr.ip != com_config.shards.items[i].address.ip)
                    continue;
                index = i;
                break;
            }
            if (index == MAX_SHARDS)
                continue;
            event_t *e = &new_events[num_new_events++];
            e->type                 = EVENT_ACCEPT_SHARD;
            e->accept_shard.type    = EVENT_ACCEPT_SHARD;
            e->accept_shard.shard   = index;
            e->accept_shard.socket  = s;
        } else
        /*-- Read a shard --*/
        if (IS_SHARD(events[i].data.ptr))
        {
            read_shard_event_t *ne = &new_events[num_new_events++].read_shard;
            shard_t *shard = events[i].data.ptr;
            ne->type    = EVENT_READ_SHARD;
            ne->shard   = SHARD_INDEX(events[i].data.ptr);
            ne->num_bytes = net_recv(shard->socket, buf + buf_offset, MUTA_MTU);
            ne->memory = buf + buf_offset;
            if (ne->num_bytes > 0)
            {
                buf_offset += ne->num_bytes;
                num_reads++;
            } else
                netpoll_del(&_netpoll, shard->socket);
        } else
            muta_assert(0);
        if (num_new_events < 8)
            continue;
        if (num_reads)
        {
            _allocate_messages_for_events(new_events, num_new_events);
            num_reads = 0;
        }
        event_push(com_event_buf, new_events, num_new_events);
    }
    if (num_reads)
        _allocate_messages_for_events(new_events, num_new_events);
    event_push(com_event_buf, new_events, num_new_events);
}

static void
_disconnect_shard(shard_t *shard, bool32 del_from_netpoll)
{
    muta_assert(shard->flags.connected);
    LOG("Disconnecting shard.");
    shard->flags.connected = 0;
    if (del_from_netpoll)
        netpoll_del(&_netpoll, shard->socket);
    net_shutdown_sock(shard->socket, SOCKSD_BOTH);
    if (shard->flags.online_for_players)
    {
        cl_on_shard_status_changed(SHARD_INDEX(shard), 0);
        shard->flags.online_for_players = 0;
    }
    if (!shard->flags.will_flush)
        return;
    for (int i = 0; i < _num_flush_shards; ++i)
    {
        if (_flush_shards[i] != shard)
            continue;
        _flush_shards[i] = _flush_shards[--_num_flush_shards];
        break;
    }
}

static int
_read_shard_packet(shard_t *shard)
{
    bbuf_t bb = BBUF_INITIALIZER(shard->in_buf.memory, shard->in_buf.num_bytes);
    int         incomplete  = 0;
    int         dc          = 0;
    lmsg_type_t msg_type;
    while (BBUF_FREE_SPACE(&bb) >= LMSGTSZ && !dc && !incomplete)
    {
        BBUF_READ(&bb, lmsg_type_t, &msg_type);
        switch (msg_type)
        {
        case LMSG_PUB_KEY:
        {
            DEBUG_PUTS("LMSG_PUB_KEY");
            lmsg_pub_key_t s;
            incomplete = lmsg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_lmsg_pub_key(shard, &s);
        }
            break;
        case LMSG_STREAM_HEADER:
        {
            DEBUG_PUTS("LMSG_STREAM_HEADER");
            lmsg_stream_header_t s;
            incomplete = lmsg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_lmsg_stream_header(shard, &s);
        }
            break;
        case TLMSG_LOGIN_REQUEST:
        {
            DEBUG_PUTS("TLMSG_LOGIN_REQUEST");
            tlmsg_login_request_t s;
            incomplete = tlmsg_login_request_read_var_encrypted(&bb,
                &shard->cryptchan, &s);
            if (!incomplete)
                dc = _handle_tlmsg_login_request(shard, &s);
        }
            break;
        case TLMSG_ONLINE_FOR_PLAYERS:
            DEBUG_PUTS("TLMSG_ONLINE_FOR_PLAYERS");
            dc = _handle_tlmsg_online_for_players(shard);
            break;
        case TLMSG_OFFLINE_FOR_PLAYERS:
            DEBUG_PUTS("TLMSG_OFFLINE_FOR_PLAYERS");
            dc = _handle_tlmsg_offline_for_players(shard);
            break;
        case TLMSG_PLAYER_SELECT_SHARD_RESULT:
        {
            DEBUG_PUTS("TLMSG_PLAYER_SELECT_SHARD_RESULT");
            tlmsg_player_select_shard_result_t s;
            incomplete = tlmsg_player_select_shard_result_read_const_encrypted(
                &bb, &shard->cryptchan, &s);
            if (!incomplete)
                dc = _handle_tlmsg_player_select_shard_result(shard, &s);
        }
            break;
        default:
            dc = 1;
            break;
        }
    }
    if (dc || incomplete < 0)
    {
        LOGF("msg_type %u, dc: %d, incomplete: %d", msg_type, dc, incomplete);
        return -1;
    }
    return BBUF_FREE_SPACE(&bb);
}

static int
_flush_shard(shard_t *shard)
{
    uint32 num_sent = 0;
    while (num_sent < shard->out_buf.num_bytes)
    {
        uint32 num_to_send = MIN(shard->out_buf.num_bytes - num_sent, MUTA_MTU);
        if (net_send_all(shard->socket,
            shard->out_buf.memory + num_sent, num_to_send)
            <= 0)
            return 1;
        num_sent += num_to_send;
    }
    shard->out_buf.num_bytes -= num_sent;
    return 0;
}

static bbuf_t
_send_msg_to_shard(shard_t *shard, uint32 num_bytes)
{
    bbuf_t ret = {0};
    int req_bytes = LMSGTSZ + num_bytes;
    if (shard->out_buf.num_bytes + req_bytes >= SHARD_OUT_BUF_SIZE &&
        _flush_shard(shard))
        return ret;
    ret.mem         = shard->out_buf.memory + shard->out_buf.num_bytes;
    ret.max_bytes   = req_bytes;
    if (!shard->flags.will_flush)
    {
        _flush_shards[_num_flush_shards++]  = shard;
        shard->flags.will_flush             = 1;
    }
    return ret;
}

static int
_handle_lmsg_pub_key(shard_t *shard, lmsg_pub_key_t *s)
{
    if (shard->flags.started_crypt)
        return 1;
    if (cryptchan_is_encrypted(&shard->cryptchan))
        return 2;
    if (cryptchan_init(&shard->cryptchan, 0))
        return 3;
    lmsg_pub_key_t          p_msg;
    lmsg_stream_header_t    h_msg;
    if (cryptchan_sv_store_pub_key(&shard->cryptchan, s->key, h_msg.header))
        return 4;
    memcpy(p_msg.key, shard->cryptchan.pk, CRYPTCHAN_PUB_KEY_SZ);
    uint8   buf[2 * LMSGTSZ + LMSG_STREAM_HEADER_SZ + LMSG_PUB_KEY_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    lmsg_pub_key_write(&bb, &p_msg);
    lmsg_stream_header_write(&bb, &h_msg);
    if (net_send_all(shard->socket, bb.mem, bb.num_bytes) <= 0)
        return 5;
    shard->flags.started_crypt = 1;
    return 0;
}

static int
_handle_lmsg_stream_header(shard_t *shard, lmsg_stream_header_t *s)
    {return cryptchan_store_stream_header(&shard->cryptchan, s->header);}

static int
_handle_tlmsg_login_request(shard_t *shard, tlmsg_login_request_t *s)
{
    if (shard->flags.authed)
        return 1;
    const char *my_name = shards_get_name(SHARD_INDEX(shard));
    uint32 shard_name_len = (uint32)strlen(my_name);
    if (shard_name_len != s->shard_name_len ||
        memcmp(s->shard_name, my_name, shard_name_len))
    {
        LOG("Shard authentication failed: wrong shard name.");
        return 2;
    }
    uint32 password_len = (uint32)strlen(com_config.shards_password);
    if (password_len != s->password_len ||
        memcmp(s->password, com_config.shards_password, password_len))
    {
        LOG("Shard authentication failed: wrong password.");
        return 3;
    }
    shard->flags.authed = 1;
    flmsg_login_result_t r_msg;
    r_msg.result = 0;
    uint8   buf[LMSGTSZ + FLMSG_LOGIN_RESULT_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    flmsg_login_result_write(&bb, &r_msg);
    if (net_send_all(shard->socket, bb.mem, bb.num_bytes) <= 0)
        return 4;
    LOG("Authenticated a shard.");
    return 0;
}

static int
_handle_tlmsg_online_for_players(shard_t *shard)
{
    if (!shard->flags.authed)
        return 1;
    if (shard->flags.online_for_players)
        return 0;
    shard->flags.online_for_players = 1;
    cl_on_shard_status_changed(SHARD_INDEX(shard), 1);
    return 0;
}

static int
_handle_tlmsg_offline_for_players(shard_t *shard)
{
    if (!shard->flags.authed)
        return 1;
    if (!shard->flags.online_for_players)
        return 0;
    shard->flags.online_for_players = 0;
    cl_on_shard_status_changed(SHARD_INDEX(shard), 0);
    return 0;
}

static int
_handle_tlmsg_player_select_shard_result(shard_t *shard,
    tlmsg_player_select_shard_result_t *s)
{
    if (!shard->flags.authed)
        return 1;
    int r = cl_finish_select_shard_attempt(s->account_id, s->login_session_id,
        s->result);
    if (r)
    {
        DEBUG_LOGF("cl_finish_select_shard_attempt() failed with code %d.", r);
        return 0;
    }
    /* Failed! */
    flmsg_player_cancel_select_shard_t c_msg;
    c_msg.account_id        = s->account_id;
    c_msg.login_session_id  = s->login_session_id;
    bbuf_t bb = _send_msg_to_shard(shard, FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ);
    if (!bb.max_bytes)
        return 2;
    flmsg_player_cancel_select_shard_write(&bb, &c_msg);
    return 0;
}
