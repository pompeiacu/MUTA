#ifndef MUTA_LOGIN_DB_H
#define MUTA_LOGIN_DB_H

#include "../../shared/types.h"

int
db_init();

void
db_destroy();

int
db_start();

void
db_stop();

/* Queries are asynchronous. Events will be pushed to the main thread's event
 * buffer as queries finish. */

int
db_query_account_login(uint32 login_session_id, const char *name, int name_len,
    uint32 name_hash, const char *password, int password_len);

#endif /* MUTA_LOGIN_DB_H */
