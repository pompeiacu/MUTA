#!/bin/bash
# Run with git bash on Windows

git lfs install --skip-smudge
git clone https://gitlab.com/Partanen/MUTA-Assets
git clone https://gitlab.com/Partanen/MUTA-Data
cd MUTA-Assets && git lfs pull
cd ../MUTA-Data && git lfs pull
git lfs install --force
