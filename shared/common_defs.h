#ifndef MUTA_COMMON_DEFS_H
#define MUTA_COMMON_DEFS_H

#include "types.h"

#define MUTA_VERSION_STR            "0.7.1"
#define MUTA_VERSION_ARRAY          {0, 7, 1}

#define MUTA_MTU                    1472
#define MIN_CHARACTER_NAME_LEN      2
#define MAX_CHARACTER_NAME_LEN      16
#define MIN_PW_LEN                  6
#define MAX_PW_LEN                  64
#define MIN_ACC_NAME_LEN            2
#define MAX_ACC_NAME_LEN            16
#define DEFAULT_MAX_PF_REQUEST_FREQ 250
#define DEFAULT_LOGIN_PORT          3489
#define DEFAULT_CLIENT_PORT         3490
#define MAX_CHARACTERS_PER_ACC      8
#define MAX_CHAT_MSG_LEN            255
#define MAX_RACE_NAME_LEN           16
#define CLIENT_LOGIN_PROOF_KEY_LEN  128
#define MIN_SHARD_NAME_LEN          1
#define MAX_SHARD_NAME_LEN          16
#define AUTH_TOKEN_SZ               64

typedef struct pc_props_t pc_props_t;

enum common_error_codes
{
    MUTA_ERR_OUT_OF_MEMORY = 112,
    MUTA_ERR_OPERATING_SYSTEM
};

enum character_creation_fail_reasons
{
    CREATE_CHARACTER_FAIL_DB_BUSY = 1,
    CREATE_CHARACTER_FAIL_EXISTS,
    CREATE_CHARACTER_FAIL_UNKNOWN
};

enum shard_select_error
{
    SHARD_SELECT_ERROR_UNKNOWN = 1,
    SHARD_SELECT_ERROR_FULL
};

static inline const char *
character_creation_fail_to_str(int err)
{
    if (err == 0)
        return "no error";
    if (err == CREATE_CHARACTER_FAIL_DB_BUSY)
        return "database is busy";
    if (err == CREATE_CHARACTER_FAIL_EXISTS)
        return "name taken";
    return "unknown error";
}

struct pc_props_t /* Player character properties */
{
    char    name[MAX_CHARACTER_NAME_LEN + 1];
    int32   x;
    int32   y;
    int8    z;
    uint8   race;
    uint8   sex;
};
/* Player character props */

enum chat_msg_types
{
    CHAT_MSG_GLOBAL_BROADCAST
};

#endif /* MUTA_COMMON_DEFS_H */
