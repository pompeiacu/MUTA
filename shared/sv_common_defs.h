#ifndef MUTA_SV_COMMON_DEFS_H
#define MUTA_SV_COMMON_DEFS_H

#include "types.h"
#include "common_defs.h"

#define MAP_CHUNK_W                 128
#define MAP_CHUNK_T                 64
#define MAX_TILE_Z                  (MAP_CHUNK_T - 1)
#define TILES_PER_MAP_CHUNK         (MAP_CHUNK_W * MAP_CHUNK_W * MAP_CHUNK_T)
#define MAP_CHUNK_BYTE_SZ           (TILES_PER_MAP_CHUNK * sizeof(tile_t))
#define DEFAULT_WORLD_PORT          3492
#define DEFAULT_DB_PORT             3493
#define HASHED_PW_LEN               128
#define MAX_PROXY_PW_LEN            254
#define DEFAULT_PROXY_PORT          3982
#define DEFAULT_SHARD_TO_LOGIN_PORT 3494
#define MAX_LOGIN_SERVER_PW_LEN     64

typedef struct sv_stored_pc_t sv_stored_pc_t;

enum dbsv_error_codes
{
    DBSV_ERR_OK = 0,
    DBSV_ERR_DISCONNECTED,
    DBSV_ERR_NOT_FOUND,
    DBSV_ERR_ENTRY_NOT_UNIQUE,
    DBSV_ERR_VERSION, /* Database version is bad (column missing etc.) */
    DBSV_ERR_UNKNOWN
};

enum worldd_error_codes
{
    WORLDD_ERR_MAP_NOT_FOUND,
    WORLDD_ERR_INTERNAL
};

enum gm_levels
{
    GM_LEVEL_NONE = 0,
    GM_LEVEL1,
    GM_LEVEL2,
    GM_LEVEL_ADMIN
};

struct sv_stored_pc_t
{
    uint64      id;
    pc_props_t  props;
};

enum sys_login_result_t
{
    SYS_LOGIN_OK = 0,
    SYS_LOGIN_REJECTED,
    NUM_SYS_LOGIN_RESULTS
};

#endif /* MUTA_SV_COMMON_DEFS_H */
