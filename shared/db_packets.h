typedef uint8 dbmsg_type_t;
#define DBMSGTSZ sizeof(dbmsg_type_t)
#define MAX_NAME_LEN 64
#define MAX_MSG_LEN 256
#define WRITE_DBMSG_TYPE(mem, val) \
    *(dbmsg_type_t*)(mem) = (dbmsg_type_t)(val); \
    (mem) = (uint8*)(mem) + sizeof(dbmsg_type_t);
enum muta_dbmsg_types
{
    DBMSG_PUB_KEY = 0,
    DBMSG_STREAM_HEADER,
    DBMSG_LOGIN,
    DBMSG_LOGIN_RESULT,

    NUM_MUTA_DBMSG_TYPES
};

#if NUM_MUTA_DBMSG_TYPES > 255
#   error too many MUTA_DBMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(dbmsg, DBMSGTSZ, WRITE_DBMSG_TYPE);

enum muta_fdbmsg_types
{
    FDBMSG_REPLY_ACCOUNT_EXISTS = NUM_MUTA_DBMSG_TYPES,
    FDBMSG_REPLY_LOGIN_ACCOUNT,
    FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID,
    FDBMSG_REPLY_CREATE_CHARACTER,
    FDBMSG_CONFIRM_SET_CHARACTER_POSITION,
    FDBMSG_REPLY_ACCOUNT_CHARACTERS,

    NUM_MUTA_FDBMSG_TYPES
};

#if NUM_MUTA_FDBMSG_TYPES > 255
#   error too many MUTA_FDBMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(fdbmsg, DBMSGTSZ, WRITE_DBMSG_TYPE);

enum muta_tdbmsg_types
{
    TDBMSG_QUERY_ACCOUNT_EXISTS = NUM_MUTA_DBMSG_TYPES,
    TDBMSG_QUERY_LOGIN_ACCOUNT,
    TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID,
    TDBMSG_INSERT_CREATE_CHARACTER,
    TDBMSG_UPDATE_CHARACTER_POSITION,
    TDBMSG_QUERY_ACCOUNT_CHARACTERS,

    NUM_MUTA_TDBMSG_TYPES
};

#if NUM_MUTA_TDBMSG_TYPES > 255
#   error too many MUTA_TDBMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(tdbmsg, DBMSGTSZ, WRITE_DBMSG_TYPE);

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} dbmsg_pub_key_t;

#define DBMSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
dbmsg_pub_key_write(byte_buf_t *buf, dbmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + DBMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, DBMSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
dbmsg_pub_key_read(byte_buf_t *buf, dbmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} dbmsg_stream_header_t;

#define DBMSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
dbmsg_stream_header_write(byte_buf_t *buf, dbmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + DBMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, DBMSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
dbmsg_stream_header_read(byte_buf_t *buf, dbmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint16 pw_len;
    uint8 client_type;
    const char *name;
    const char *pw;
} dbmsg_login_t;

#define DBMSG_LOGIN_SZ (sizeof(uint8) + sizeof(uint16) + sizeof(uint8))

#define DBMSG_LOGIN_COMPUTE_SZ(name_len, pw_len) \
    (DBMSG_LOGIN_SZ + (name_len) + (pw_len))

#define DBMSG_LOGIN_MAX_SZ \
    (DBMSG_LOGIN_COMPUTE_SZ(32, 512))

static inline int
dbmsg_login_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    dbmsg_login_t *s)
{
    int sz = DBMSG_LOGIN_COMPUTE_SZ(s->name_len, s->pw_len);
    if (sz > DBMSG_LOGIN_MAX_SZ)
        return -1;

    uint8 *mem = prep_dbmsg_write_var_encrypted(buf, DBMSG_LOGIN, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT16(mem, s->pw_len);
    WRITE_UINT8(mem, s->client_type);
    WRITE_STR(mem, s->name, s->name_len);
    WRITE_STR(mem, s->pw, s->pw_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
dbmsg_login_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    dbmsg_login_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, DBMSG_LOGIN_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->name_len);
    READ_UINT16(mem, &s->pw_len);

    if (DBMSG_LOGIN_COMPUTE_SZ(s->name_len, s->pw_len) >
        DBMSG_LOGIN_MAX_SZ)
        return -1;

    if (sz < DBMSG_LOGIN_COMPUTE_SZ(s->name_len, s->pw_len)) return -1;

    READ_UINT8(mem, &s->client_type);
    READ_VARCHAR(mem, s->name, s->name_len);
    READ_VARCHAR(mem, s->pw, s->pw_len);

    return 0;
}

typedef struct
{
    uint8 code;
} dbmsg_login_result_t;

#define DBMSG_LOGIN_RESULT_SZ (sizeof(uint8))

static inline int
dbmsg_login_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    dbmsg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + DBMSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, DBMSG_LOGIN_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->code);

    return cryptchan_encrypt(cc, dst, src, DBMSG_LOGIN_RESULT_SZ);
}

static inline int
dbmsg_login_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    dbmsg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        DBMSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        DBMSG_LOGIN_RESULT_SZ))
        return -1;
    READ_UINT8(mem, &s->code);

    return 0;
}

typedef struct
{
    uint32 query_id;
    uint8 exists;
} fdbmsg_reply_account_exists_t;

#define FDBMSG_REPLY_ACCOUNT_EXISTS_SZ (sizeof(uint32) + sizeof(uint8))

static inline int
fdbmsg_reply_account_exists_write(byte_buf_t *buf, fdbmsg_reply_account_exists_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + FDBMSG_REPLY_ACCOUNT_EXISTS_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, FDBMSG_REPLY_ACCOUNT_EXISTS);
    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT8(mem, s->exists);

    return 0;
}

static inline int
fdbmsg_reply_account_exists_read(byte_buf_t *buf, fdbmsg_reply_account_exists_t *s)
{
    uint8 *mem = bbuf_reserve(buf, FDBMSG_REPLY_ACCOUNT_EXISTS_SZ);
    if (!mem) return 1;
    READ_UINT32(mem, &s->query_id);
    READ_UINT8(mem, &s->exists);

    return 0;
}

typedef struct
{
    uint32 query_id;
    uint64 account_id;
    uint8 result;
} fdbmsg_reply_login_account_t;

#define FDBMSG_REPLY_LOGIN_ACCOUNT_SZ (sizeof(uint32) + sizeof(uint64) + sizeof(uint8))

static inline int
fdbmsg_reply_login_account_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fdbmsg_reply_login_account_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + FDBMSG_REPLY_LOGIN_ACCOUNT_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, FDBMSG_REPLY_LOGIN_ACCOUNT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT64(mem, s->account_id);
    WRITE_UINT8(mem, s->result);

    return cryptchan_encrypt(cc, dst, src, FDBMSG_REPLY_LOGIN_ACCOUNT_SZ);
}

static inline int
fdbmsg_reply_login_account_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fdbmsg_reply_login_account_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        FDBMSG_REPLY_LOGIN_ACCOUNT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        FDBMSG_REPLY_LOGIN_ACCOUNT_SZ))
        return -1;
    READ_UINT32(mem, &s->query_id);
    READ_UINT64(mem, &s->account_id);
    READ_UINT8(mem, &s->result);

    return 0;
}

typedef struct
{
    uint32 query_id;
    uint64 character_id;
    uint8 error;
} fdbmsg_reply_account_first_character_id_t;

#define FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID_SZ (sizeof(uint32) + sizeof(uint64) + sizeof(uint8))

static inline int
fdbmsg_reply_account_first_character_id_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fdbmsg_reply_account_first_character_id_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT64(mem, s->character_id);
    WRITE_UINT8(mem, s->error);

    return cryptchan_encrypt(cc, dst, src, FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
}

static inline int
fdbmsg_reply_account_first_character_id_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fdbmsg_reply_account_first_character_id_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID_SZ))
        return -1;
    READ_UINT32(mem, &s->query_id);
    READ_UINT64(mem, &s->character_id);
    READ_UINT8(mem, &s->error);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint32 query_id;
    uint8 error;
    uint64 id;
    uint8 race;
    uint8 sex;
    uint32 map_id;
    uint32 instance_id;
    int32 x;
    int32 y;
    int8 z;
    const char *name;
} fdbmsg_reply_create_character_t;

#define FDBMSG_REPLY_CREATE_CHARACTER_SZ (sizeof(uint8) + sizeof(uint32) + sizeof(uint8) + sizeof(uint64) + sizeof(uint8) + sizeof(uint8) + sizeof(uint32) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8))

#define FDBMSG_REPLY_CREATE_CHARACTER_COMPUTE_SZ(name_len) \
    (FDBMSG_REPLY_CREATE_CHARACTER_SZ + (name_len))

#define FDBMSG_REPLY_CREATE_CHARACTER_MAX_SZ \
    (FDBMSG_REPLY_CREATE_CHARACTER_COMPUTE_SZ(MAX_CHARACTER_NAME_LEN))

static inline int
fdbmsg_reply_create_character_write(byte_buf_t *buf, fdbmsg_reply_create_character_t *s)
{
    int sz = FDBMSG_REPLY_CREATE_CHARACTER_COMPUTE_SZ(s->name_len);
    if (sz > FDBMSG_REPLY_CREATE_CHARACTER_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        FDBMSG_REPLY_CREATE_CHARACTER_COMPUTE_SZ(s->name_len));
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, FDBMSG_REPLY_CREATE_CHARACTER);
    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT8(mem, s->error);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->race);
    WRITE_UINT8(mem, s->sex);
    WRITE_UINT32(mem, s->map_id);
    WRITE_UINT32(mem, s->instance_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_STR(mem, s->name, s->name_len);

    return 0;
}

static inline int
fdbmsg_reply_create_character_read(byte_buf_t *buf, fdbmsg_reply_create_character_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < FDBMSG_REPLY_CREATE_CHARACTER_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);

    if (FDBMSG_REPLY_CREATE_CHARACTER_COMPUTE_SZ(s->name_len) >
        FDBMSG_REPLY_CREATE_CHARACTER_MAX_SZ)
        return -1;

    int req_sz = FDBMSG_REPLY_CREATE_CHARACTER_COMPUTE_SZ(s->name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT32(mem, &s->query_id);
    READ_UINT8(mem, &s->error);
    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->race);
    READ_UINT8(mem, &s->sex);
    READ_UINT32(mem, &s->map_id);
    READ_UINT32(mem, &s->instance_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint32 query_id;
} fdbmsg_confirm_set_character_position_t;

#define FDBMSG_CONFIRM_SET_CHARACTER_POSITION_SZ (sizeof(uint32))

static inline int
fdbmsg_confirm_set_character_position_write(byte_buf_t *buf, fdbmsg_confirm_set_character_position_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + FDBMSG_CONFIRM_SET_CHARACTER_POSITION_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, FDBMSG_CONFIRM_SET_CHARACTER_POSITION);
    WRITE_UINT32(mem, s->query_id);

    return 0;
}

static inline int
fdbmsg_confirm_set_character_position_read(byte_buf_t *buf, fdbmsg_confirm_set_character_position_t *s)
{
    uint8 *mem = bbuf_reserve(buf, FDBMSG_CONFIRM_SET_CHARACTER_POSITION_SZ);
    if (!mem) return 1;
    READ_UINT32(mem, &s->query_id);

    return 0;
}

typedef struct
{
    uint8 names_len;
    uint64 ids_len;
    uint8 races_len;
    uint8 sexes_len;
    uint32 map_ids_len;
    uint32 instance_ids_len;
    uint8 name_indices_len;
    int32 xs_len;
    int32 ys_len;
    int8 zs_len;
    uint32 query_id;
    const char *names;
    uint64 *ids;
    uint8 *races;
    uint8 *sexes;
    uint32 *map_ids;
    uint32 *instance_ids;
    uint8 *name_indices;
    int32 *xs;
    int32 *ys;
    int8 *zs;
} fdbmsg_reply_account_characters_t;

#define FDBMSG_REPLY_ACCOUNT_CHARACTERS_SZ (sizeof(uint8) + sizeof(uint64) + sizeof(uint8) + sizeof(uint8) + sizeof(uint32) + sizeof(uint32) + sizeof(uint8) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint32))

#define FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(names_len, ids_len, races_len, sexes_len, map_ids_len, instance_ids_len, name_indices_len, xs_len, ys_len, zs_len) \
    (FDBMSG_REPLY_ACCOUNT_CHARACTERS_SZ + (names_len) + (ids_len * sizeof(uint64)) + (races_len * sizeof(uint8)) + (sexes_len * sizeof(uint8)) + (map_ids_len * sizeof(uint32)) + (instance_ids_len * sizeof(uint32)) + (name_indices_len * sizeof(uint8)) + (xs_len * sizeof(int32)) + (ys_len * sizeof(int32)) + (zs_len * sizeof(int8)))

#define FDBMSG_REPLY_ACCOUNT_CHARACTERS_MAX_SZ \
    (FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(136, 136, 136, 136, 136, 136, 136, 136, 136, 136))

static inline int
fdbmsg_reply_account_characters_write(byte_buf_t *buf, fdbmsg_reply_account_characters_t *s)
{
    int sz = FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->map_ids_len, s->instance_ids_len, s->name_indices_len, s->xs_len, s->ys_len, s->zs_len);
    if (sz > FDBMSG_REPLY_ACCOUNT_CHARACTERS_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->map_ids_len, s->instance_ids_len, s->name_indices_len, s->xs_len, s->ys_len, s->zs_len));
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, FDBMSG_REPLY_ACCOUNT_CHARACTERS);
    WRITE_UINT8(mem, s->names_len);
    WRITE_UINT64(mem, s->ids_len);
    WRITE_UINT8(mem, s->races_len);
    WRITE_UINT8(mem, s->sexes_len);
    WRITE_UINT32(mem, s->map_ids_len);
    WRITE_UINT32(mem, s->instance_ids_len);
    WRITE_UINT8(mem, s->name_indices_len);
    WRITE_INT32(mem, s->xs_len);
    WRITE_INT32(mem, s->ys_len);
    WRITE_INT8(mem, s->zs_len);
    WRITE_UINT32(mem, s->query_id);
    WRITE_STR(mem, s->names, s->names_len);
    WRITE_UINT64_VARARR(mem, s->ids, s->ids_len);
    WRITE_UINT8_VARARR(mem, s->races, s->races_len);
    WRITE_UINT8_VARARR(mem, s->sexes, s->sexes_len);
    WRITE_UINT32_VARARR(mem, s->map_ids, s->map_ids_len);
    WRITE_UINT32_VARARR(mem, s->instance_ids, s->instance_ids_len);
    WRITE_UINT8_VARARR(mem, s->name_indices, s->name_indices_len);
    WRITE_INT32_VARARR(mem, s->xs, s->xs_len);
    WRITE_INT32_VARARR(mem, s->ys, s->ys_len);
    WRITE_INT8_VARARR(mem, s->zs, s->zs_len);

    return 0;
}

static inline int
fdbmsg_reply_account_characters_read(byte_buf_t *buf, fdbmsg_reply_account_characters_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < FDBMSG_REPLY_ACCOUNT_CHARACTERS_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->names_len);
    READ_UINT64(mem, &s->ids_len);
    READ_UINT8(mem, &s->races_len);
    READ_UINT8(mem, &s->sexes_len);
    READ_UINT32(mem, &s->map_ids_len);
    READ_UINT32(mem, &s->instance_ids_len);
    READ_UINT8(mem, &s->name_indices_len);
    READ_INT32(mem, &s->xs_len);
    READ_INT32(mem, &s->ys_len);
    READ_INT8(mem, &s->zs_len);

    if (FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->map_ids_len, s->instance_ids_len, s->name_indices_len, s->xs_len, s->ys_len, s->zs_len) >
        FDBMSG_REPLY_ACCOUNT_CHARACTERS_MAX_SZ)
        return -1;

    int req_sz = FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->map_ids_len, s->instance_ids_len, s->name_indices_len, s->xs_len, s->ys_len, s->zs_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT32(mem, &s->query_id);
    READ_VARCHAR(mem, s->names, s->names_len);
    READ_UINT64_VARARR(mem, s->ids, s->ids_len);
    READ_UINT8_VARARR(mem, s->races, s->races_len);
    READ_UINT8_VARARR(mem, s->sexes, s->sexes_len);
    READ_UINT32_VARARR(mem, s->map_ids, s->map_ids_len);
    READ_UINT32_VARARR(mem, s->instance_ids, s->instance_ids_len);
    READ_UINT8_VARARR(mem, s->name_indices, s->name_indices_len);
    READ_INT32_VARARR(mem, s->xs, s->xs_len);
    READ_INT32_VARARR(mem, s->ys, s->ys_len);
    READ_INT8_VARARR(mem, s->zs, s->zs_len);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint32 query_id;
    const char *name;
} tdbmsg_query_account_exists_t;

#define TDBMSG_QUERY_ACCOUNT_EXISTS_SZ (sizeof(uint8) + sizeof(uint32))

#define TDBMSG_QUERY_ACCOUNT_EXISTS_COMPUTE_SZ(name_len) \
    (TDBMSG_QUERY_ACCOUNT_EXISTS_SZ + (name_len))

#define TDBMSG_QUERY_ACCOUNT_EXISTS_MAX_SZ \
    (TDBMSG_QUERY_ACCOUNT_EXISTS_COMPUTE_SZ(MAX_ACC_NAME_LEN))

static inline int
tdbmsg_query_account_exists_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_account_exists_t *s)
{
    int sz = TDBMSG_QUERY_ACCOUNT_EXISTS_COMPUTE_SZ(s->name_len);
    if (sz > TDBMSG_QUERY_ACCOUNT_EXISTS_MAX_SZ)
        return -1;

    uint8 *mem = prep_tdbmsg_write_var_encrypted(buf, TDBMSG_QUERY_ACCOUNT_EXISTS, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT32(mem, s->query_id);
    WRITE_STR(mem, s->name, s->name_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
tdbmsg_query_account_exists_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_account_exists_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, TDBMSG_QUERY_ACCOUNT_EXISTS_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->name_len);

    if (TDBMSG_QUERY_ACCOUNT_EXISTS_COMPUTE_SZ(s->name_len) >
        TDBMSG_QUERY_ACCOUNT_EXISTS_MAX_SZ)
        return -1;

    if (sz < TDBMSG_QUERY_ACCOUNT_EXISTS_COMPUTE_SZ(s->name_len)) return -1;

    READ_UINT32(mem, &s->query_id);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint8 pw_len;
    uint8 create_if_not_exists;
    uint32 query_id;
    const char *name;
    const char *pw;
} tdbmsg_query_login_account_t;

#define TDBMSG_QUERY_LOGIN_ACCOUNT_SZ (sizeof(uint8) + sizeof(uint8) + sizeof(uint8) + sizeof(uint32))

#define TDBMSG_QUERY_LOGIN_ACCOUNT_COMPUTE_SZ(name_len, pw_len) \
    (TDBMSG_QUERY_LOGIN_ACCOUNT_SZ + (name_len) + (pw_len))

#define TDBMSG_QUERY_LOGIN_ACCOUNT_MAX_SZ \
    (TDBMSG_QUERY_LOGIN_ACCOUNT_COMPUTE_SZ(MAX_ACC_NAME_LEN, MAX_PW_LEN))

static inline int
tdbmsg_query_login_account_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_login_account_t *s)
{
    int sz = TDBMSG_QUERY_LOGIN_ACCOUNT_COMPUTE_SZ(s->name_len, s->pw_len);
    if (sz > TDBMSG_QUERY_LOGIN_ACCOUNT_MAX_SZ)
        return -1;

    uint8 *mem = prep_tdbmsg_write_var_encrypted(buf, TDBMSG_QUERY_LOGIN_ACCOUNT, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT8(mem, s->pw_len);
    WRITE_UINT8(mem, s->create_if_not_exists);
    WRITE_UINT32(mem, s->query_id);
    WRITE_STR(mem, s->name, s->name_len);
    WRITE_STR(mem, s->pw, s->pw_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
tdbmsg_query_login_account_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_login_account_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, TDBMSG_QUERY_LOGIN_ACCOUNT_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->name_len);
    READ_UINT8(mem, &s->pw_len);

    if (TDBMSG_QUERY_LOGIN_ACCOUNT_COMPUTE_SZ(s->name_len, s->pw_len) >
        TDBMSG_QUERY_LOGIN_ACCOUNT_MAX_SZ)
        return -1;

    if (sz < TDBMSG_QUERY_LOGIN_ACCOUNT_COMPUTE_SZ(s->name_len, s->pw_len)) return -1;

    READ_UINT8(mem, &s->create_if_not_exists);
    READ_UINT32(mem, &s->query_id);
    READ_VARCHAR(mem, s->name, s->name_len);
    READ_VARCHAR(mem, s->pw, s->pw_len);

    return 0;
}

typedef struct
{
    uint32 query_id;
    uint64 account_id;
} tdbmsg_query_account_first_character_id_t;

#define TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID_SZ (sizeof(uint32) + sizeof(uint64))

static inline int
tdbmsg_query_account_first_character_id_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_account_first_character_id_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT64(mem, s->account_id);

    return cryptchan_encrypt(cc, dst, src, TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
}

static inline int
tdbmsg_query_account_first_character_id_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_account_first_character_id_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID_SZ))
        return -1;
    READ_UINT32(mem, &s->query_id);
    READ_UINT64(mem, &s->account_id);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint32 query_id;
    uint64 acc_id;
    uint32 map_id;
    uint32 instance_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 race;
    uint8 sex;
    const char *name;
} tdbmsg_insert_create_character_t;

#define TDBMSG_INSERT_CREATE_CHARACTER_SZ (sizeof(uint8) + sizeof(uint32) + sizeof(uint64) + sizeof(uint32) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8) + sizeof(uint8))

#define TDBMSG_INSERT_CREATE_CHARACTER_COMPUTE_SZ(name_len) \
    (TDBMSG_INSERT_CREATE_CHARACTER_SZ + (name_len))

#define TDBMSG_INSERT_CREATE_CHARACTER_MAX_SZ \
    (TDBMSG_INSERT_CREATE_CHARACTER_COMPUTE_SZ(MAX_CHARACTER_NAME_LEN))

static inline int
tdbmsg_insert_create_character_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_insert_create_character_t *s)
{
    int sz = TDBMSG_INSERT_CREATE_CHARACTER_COMPUTE_SZ(s->name_len);
    if (sz > TDBMSG_INSERT_CREATE_CHARACTER_MAX_SZ)
        return -1;

    uint8 *mem = prep_tdbmsg_write_var_encrypted(buf, TDBMSG_INSERT_CREATE_CHARACTER, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT64(mem, s->acc_id);
    WRITE_UINT32(mem, s->map_id);
    WRITE_UINT32(mem, s->instance_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->race);
    WRITE_UINT8(mem, s->sex);
    WRITE_STR(mem, s->name, s->name_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
tdbmsg_insert_create_character_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_insert_create_character_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, TDBMSG_INSERT_CREATE_CHARACTER_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->name_len);

    if (TDBMSG_INSERT_CREATE_CHARACTER_COMPUTE_SZ(s->name_len) >
        TDBMSG_INSERT_CREATE_CHARACTER_MAX_SZ)
        return -1;

    if (sz < TDBMSG_INSERT_CREATE_CHARACTER_COMPUTE_SZ(s->name_len)) return -1;

    READ_UINT32(mem, &s->query_id);
    READ_UINT64(mem, &s->acc_id);
    READ_UINT32(mem, &s->map_id);
    READ_UINT32(mem, &s->instance_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->race);
    READ_UINT8(mem, &s->sex);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint32 query_id;
    uint64 character_id;
    uint32 x;
    uint32 y;
    int8 z;
} tdbmsg_update_character_position_t;

#define TDBMSG_UPDATE_CHARACTER_POSITION_SZ (sizeof(uint32) + sizeof(uint64) + sizeof(uint32) + sizeof(uint32) + sizeof(int8))

static inline int
tdbmsg_update_character_position_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_update_character_position_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + TDBMSG_UPDATE_CHARACTER_POSITION_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, TDBMSG_UPDATE_CHARACTER_POSITION);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT64(mem, s->character_id);
    WRITE_UINT32(mem, s->x);
    WRITE_UINT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return cryptchan_encrypt(cc, dst, src, TDBMSG_UPDATE_CHARACTER_POSITION_SZ);
}

static inline int
tdbmsg_update_character_position_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_update_character_position_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        TDBMSG_UPDATE_CHARACTER_POSITION_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        TDBMSG_UPDATE_CHARACTER_POSITION_SZ))
        return -1;
    READ_UINT32(mem, &s->query_id);
    READ_UINT64(mem, &s->character_id);
    READ_UINT32(mem, &s->x);
    READ_UINT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint32 query_id;
    uint64 account_id;
    uint8 max;
} tdbmsg_query_account_characters_t;

#define TDBMSG_QUERY_ACCOUNT_CHARACTERS_SZ (sizeof(uint32) + sizeof(uint64) + sizeof(uint8))

static inline int
tdbmsg_query_account_characters_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_account_characters_t *s)
{
    uint8 *mem = bbuf_reserve(buf, DBMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + TDBMSG_QUERY_ACCOUNT_CHARACTERS_SZ);
    if (!mem) return 1;
    WRITE_DBMSG_TYPE(mem, TDBMSG_QUERY_ACCOUNT_CHARACTERS);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->query_id);
    WRITE_UINT64(mem, s->account_id);
    WRITE_UINT8(mem, s->max);

    return cryptchan_encrypt(cc, dst, src, TDBMSG_QUERY_ACCOUNT_CHARACTERS_SZ);
}

static inline int
tdbmsg_query_account_characters_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tdbmsg_query_account_characters_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        TDBMSG_QUERY_ACCOUNT_CHARACTERS_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        TDBMSG_QUERY_ACCOUNT_CHARACTERS_SZ))
        return -1;
    READ_UINT32(mem, &s->query_id);
    READ_UINT64(mem, &s->account_id);
    READ_UINT8(mem, &s->max);

    return 0;
}

