#include "world_common.h"
#include "common_utils.h"
#include "containers.h"
#include "muta_map_format.h"

#define WC_DEFAULT_PLAYER_WALK_SPEED 127

enum ramp_t ramp_up_table[NUM_ISODIRS] =
{
    RAMP_NORTH, /* ISODIR_NORTH */
    RAMP_NONE,  /* ISODIR_NORTH_EAST */
    RAMP_EAST,  /* ISODIR_EAST */
    RAMP_NONE,  /* ISODIR_SOUTH_EAST */
    RAMP_SOUTH, /* ISODIR_SOUTH */
    RAMP_NONE,  /* ISODIR_SOUTH_WEST */
    RAMP_WEST,  /* ISODIR_WEST */
    RAMP_NONE   /* ISODIR_NORTH_WEST */
};
/* Required directions of movement per each ramp */

enum ramp_t ramp_down_table[NUM_ISODIRS] =
{
    RAMP_SOUTH, /* ISODIR_NORTH */
    RAMP_NONE,  /* ISODIR_NORTH_EAST */
    RAMP_WEST,  /* ISODIR_EAST */
    RAMP_NONE,  /* ISODIR_SOUTH_EAST */
    RAMP_NORTH, /* ISODIR_SOUTH */
    RAMP_NONE,  /* ISODIR_SOUTH_WEST */
    RAMP_EAST,  /* ISODIR_WEST */
    RAMP_NONE   /* ISODIR_NORTH_WEST */
};
/* Required directions of movement per each ramp */

DYNAMIC_HASH_TABLE_DEFINITION(uint32_creature_table, creature_def_t*, uint32,
    uint32, HASH_FROM_NUM, 1);
DYNAMIC_HASH_TABLE_DEFINITION(str_creature_table, creature_def_t*,
    const char *, uint32, fnv_hash32_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(id_static_obj_def_table, static_obj_def_t,
    uint32, uint32, HASH_FROM_NUM, 1);
DYNAMIC_HASH_TABLE_DEFINITION(id_dynamic_obj_def_table, dynamic_obj_def_t,
    uint32, uint32, HASH_FROM_NUM, 1);
DYNAMIC_HASH_TABLE_DEFINITION(id_player_race_def_table, player_race_def_t,
    player_race_id_t, uint32, HASH_FROM_NUM, 1);

tile_def_t                          *tile_defs;
tile_gfx_def_t                      *tile_gfx_defs;
uint32                              tile_defs_num;

player_race_id_darr_t               *wc_player_race_ids;

/*-- Creature definitions */
static obj_pool_t                   _creature_pool;
static uint32_creature_table_t      _id_num_creature_table;
static str_creature_table_t         _id_str_creature_table;
static creature_def_t               **_all_creatures;


static id_static_obj_def_table_t    _static_obj_defs;
static id_dynamic_obj_def_table_t   _dynamic_obj_defs;
static id_player_race_def_table_t   _player_race_defs;

static bool32 _tile_defs_initialized;
static bool32 _creature_defs_initialized;
static bool32 _player_race_defs_initialized;
static bool32 _static_obj_defs_initialized;
static bool32 _dynamic_obj_defs_initialized;

static void
_wc_read_static_obj_def_count_cb(void *ctx, const char *opt, const char *val);

static void
_wc_read_static_obj_def_parms_cb(void *ctx, const char *opt, const char *val);

bool32 wc_tile_defs_initialized() {return _tile_defs_initialized;}
bool32 wc_creature_defs_initialized() {return _creature_defs_initialized;}
bool32 wc_player_race_defs_initialized() {return _player_race_defs_initialized;}
bool32 wc_static_obj_defs_initialized() {return _static_obj_defs_initialized;}
bool32 wc_dynamic_obj_defs_initialized() {return _dynamic_obj_defs_initialized;}

bool32
wc_all_defs_initialized()
{
    return _tile_defs_initialized && _player_race_defs_initialized &&
        _static_obj_defs_initialized && _dynamic_obj_defs_initialized &&
        _creature_defs_initialized;
}

int
wc_load_tile_defs(const char *fp, bool32 use_gfx)
{
    DEBUG_PRINTF("%s: path %s\n", __func__, fp);

    int ret = 0;
    FILE *f = fopen(fp, "r");
    if (!f) {ret = 1; goto cleanup;}

    const int   line_len        = 256;
    uint        highest_index   = 0;
    char        line[256];
    uint        index;

    /* Count the amount of tile types we have */
    while (fgets(line, line_len, f))
    {
        if (sscanf(line, "[%u]", &index) != 1)  continue;
        if (index >= MAX_TILE_TYPES)            continue;
        if (index > highest_index) highest_index = index;
    }

    /* Add one for the invisible collider */
    uint32 num_defs = highest_index + 2;

    tile_defs = calloc(num_defs, sizeof(tile_def_t));
    if (!tile_defs)
        {ret = 2; goto cleanup;}

    if (use_gfx)
    {
        tile_gfx_defs = calloc(num_defs, sizeof(tile_gfx_def_t));
        if (!tile_gfx_defs)
            {ret = 3; goto cleanup;}
    }

    tile_defs_num = num_defs;
    rewind(f);

    /* The empty tile */
    tile_defs[0].passthrough = 1;

    /* Read the actual definitions */
    tile_def_t      *def        = 0;
    tile_gfx_def_t  *gfx_def    = 0;
    char        tile_name[256];
    int         passthrough;
    int         ramp;

    while (fgets(line, line_len, f))
    {
        if (sscanf(line, "[%d]", &index) == 1)
        {
            if (index == 0)
                DEBUG_PRINTF("%s: error, 0 is a reserved tile type.\n",
                    __func__);
            def = &tile_defs[index];
            if (use_gfx) gfx_def = &tile_gfx_defs[index];
        } else
        if (!def) continue;

        if (sscanf(line, "NAME: %s", tile_name) == 1)
        {
            char *n = set_dynamic_str(def->name, tile_name);
            if (n) def->name = n;
        } else
        if (sscanf(line, "PASSTHROUGH: %d", &passthrough) == 1)
            def->passthrough = passthrough ? 1 : 0;
        else if (sscanf(line, "RAMP: %d", &ramp) == 1)
            def->ramp = ramp;

        if (!use_gfx) continue;

        int c[4];
        if (sscanf(line, "CLIP: %d, %d, %d, %d", &c[0], &c[1], &c[2], &c[3])
            == 4)
        {
            for (int i = 0; i < 4; ++i) gfx_def->clip[i] = (float)c[i];
        } else
        if (sscanf(line, "OFFSET X: %d", &c[0]) == 1)
            gfx_def->ox = (float)c[0];
        else if (sscanf(line, "OFFSET Y: %d", &c[0]) == 1)
            gfx_def->oy = (float)c[0];
    }

    /* Invisible collider */
    def                 = &tile_defs[highest_index];
    def->passthrough    = 1;
    def->name           = create_dynamic_str("Invisible Collider");
    if (!def->name) {ret = 4; goto cleanup;}

    cleanup:;
        if (ret)
        {
            free(tile_defs);
            free(tile_gfx_defs);
            tile_defs_num = 0;
        } else
            _tile_defs_initialized = 1;
        printf("%s: loaded %u tile defs.\n", __func__, tile_defs_num);
        safe_fclose(f);
        return ret;
}

void
wc_destroy_tile_defs()
{
    free(tile_defs);
    tile_defs               = 0;
    tile_defs_num           = 0;
    _tile_defs_initialized  = 0;
}

void
wc_destroy_id_num_creature_table()
{
    uint32 num_creatures = darr_num(_all_creatures);
    for (uint32 i = 0; i < num_creatures; ++i)
    {
        creature_def_t *def = _all_creatures[i];
        dstr_free(&def->id_str);
        dstr_free(&def->name);
        dstr_free(&def->description);
        dstr_free(&def->script_name);
        switch (def->gfx_type)
        {
        case WC_GFX_TEXTURE:
            dstr_free(&def->gfx_data.tex_name);
        }
    }
    obj_pool_destroy(&_creature_pool);
    uint32_creature_table_destroy(&_id_num_creature_table);
    str_creature_table_destroy(&_id_str_creature_table);
}

static int
_player_race_defs_on_def(void *ctx, const char *def_name, const char *val)
{
    if (!streq(def_name, "race"))
        return 1;
    if (!str_is_int(val))
    {
        printf("Error: race ids must be numeric in player_races.def.\n");
        return 2;
    }
    uint32 id = str_to_uint32(val);
    if (id_player_race_def_table_exists(&_player_race_defs, id))
    {
        printf("Error: player race id %u is not unique!\n", id);
        return 3;
    }
    player_race_def_t **def = ctx;
    *def = id_player_race_def_table_einsert_empty_by_hash(&_player_race_defs,
        id);
    memset(*def, 0, sizeof(player_race_def_t));
    (*def)->id = id;
    darr_push(wc_player_race_ids, id);
    return 0;
}

static int
_player_race_defs_on_opt(void *ctx, const char *opt, const char *val)
{
    player_race_def_t **def = ctx;
    if (!*def)
        return 1;
    if (streq(opt, "name"))
        dstr_set(&(*def)->name, val);
    else if (streq(opt, "description"))
        dstr_set(&(*def)->description, val);
    else if (streq(opt, "ae_set"))
        dstr_set(&(*def)->ae_set, val);
    else
        return 2;
    return 0;
}

int
wc_load_player_race_defs(const char *fp)
{
    id_player_race_def_table_init(&_player_race_defs, 16);
    player_race_def_t *def = 0;
    int r = parse_def_file(fp, _player_race_defs_on_def,
        _player_race_defs_on_opt, &def);
    if (!r)
        _player_race_defs_initialized = 1;
    return 0;
}

void
wc_destroy_player_race_defs()
{
    uint                num_bs  = (uint)_player_race_defs.num_buckets;
    uint                bsz     = (uint)id_player_race_def_table_bucket_sz();
    uint                i, j;
    player_race_def_t    *def;
    for (i = 0; i < num_bs; ++i)
    {
        for (j = 0; j < bsz; ++j)
        {
            def = id_player_race_def_table_get_from_bucket_at(
                &_player_race_defs, i, j);
            if (!def)
                continue;
            free_dynamic_str(def->name);
            free_dynamic_str(def->description);
            free_dynamic_str(def->anim_idle);
            free_dynamic_str(def->anim_walk);
        }
    }
    id_player_race_def_table_destroy(&_player_race_defs);
    _player_race_defs_initialized = 0;
    darr_free(wc_player_race_ids);
}

int
wc_load_static_obj_defs(const char *fp)
{
    uint num = 0;
    if (parse_cfg_file(fp, _wc_read_static_obj_def_count_cb, &num))
        return 1;
    if (id_static_obj_def_table_init(&_static_obj_defs, num))
        return 2;
    creature_def_t *def = 0;
    parse_cfg_file(fp, _wc_read_static_obj_def_parms_cb, &def);
    _static_obj_defs_initialized = 1;
    DEBUG_PRINTF("%s: loaded %u static obj defs.\n", __func__, num);
    return 0;
}

void
wc_destroy_static_obj_defs()
{
    uint                num_bs  = (uint)_static_obj_defs.num_buckets;
    uint                bsz     = (uint)id_static_obj_def_table_bucket_sz();
    uint                i, j;
    static_obj_def_t    *def;
    for (i = 0; i < num_bs; ++i)
    {
        for (j = 0; j < bsz; ++j)
        {
            def = id_static_obj_def_table_get_from_bucket_at(&_static_obj_defs,
                i, j);
            if (!def) continue;
            free_dynamic_str(def->name);
            free_dynamic_str(def->examine);

            switch (def->gfx_type)
            {
                case WC_GFX_SPRITESHEET:
                    free_dynamic_str(def->gfx_data.spritesheet.path);
                    break;
                default:
                    FIXME();
            }
        }
    }
    id_static_obj_def_table_destroy(&_static_obj_defs);
    _static_obj_defs_initialized = 0;
}

static int
_dynamic_obj_on_def(void *ctx, const char *def_name, const char *val)
{
    if (!streq(def_name, "dynamic_obj"))
        return 1;

    uint32 id = fnv_hash32_from_str(val);
    if (id_dynamic_obj_def_table_exists(&_dynamic_obj_defs, id))
    {
        printf("Error: id collision for '%s' in dynamic obj defs.\n", val);
        return 2;
    }
    dynamic_obj_def_t **def = ctx;
    *def = id_dynamic_obj_def_table_einsert_empty(&_dynamic_obj_defs, id);
    return 0;
}

static int
_dynamic_obj_on_opt(void *ctx, const char *opt, const char *val)
{
    dynamic_obj_def_t **def = ctx;
    if (!*def)
        return 1;
    if (streq(opt, "name"))
        dstr_set(&(*def)->name, val);
    return 0;
}

int
wc_load_dynamic_obj_defs(const char *fp)
{
    id_dynamic_obj_def_table_einit(&_dynamic_obj_defs, 256);
    dynamic_obj_def_t *def = 0;
    int r = parse_def_file(fp, _dynamic_obj_on_def, _dynamic_obj_on_opt, &def);
    if (r)
        printf("Error loading dynamic object definitions, code %d.\n", r);
    else
        _dynamic_obj_defs_initialized = 1;
    return r;
}

void
wc_destroy_dynamic_obj_defs()
{
    uint                num_bs  = (uint)_dynamic_obj_defs.num_buckets;
    uint                bsz     = (uint)id_dynamic_obj_def_table_bucket_sz();
    uint                i, j;
    dynamic_obj_def_t   *def;
    for (i = 0; i < num_bs; ++i)
    {
        for (j = 0; j < bsz; ++j)
        {
            def = id_dynamic_obj_def_table_get_from_bucket_at(
                &_dynamic_obj_defs, i, j);
            if (!def) continue;
            free_dynamic_str(def->name);
        }
    }
    id_dynamic_obj_def_table_destroy(&_dynamic_obj_defs);
    _dynamic_obj_defs_initialized = 0;
}

creature_def_t *
wc_get_creature_def(uint32 type_id)
{
    creature_def_t **ret = uint32_creature_table_get_ptr_by_hash(
        &_id_num_creature_table, type_id);
    if (ret)
        return *ret;
    return 0;

}

creature_def_t *
wc_get_creature_def_by_str_id(const char *name)
{
    creature_def_t **ret = str_creature_table_get_ptr(&_id_str_creature_table,
        name);
    if (ret)
        return *ret;
    return 0;
}

static_obj_def_t *
wc_get_static_obj_def(static_obj_type_id_t type_id)
    {return id_static_obj_def_table_get_ptr(&_static_obj_defs, type_id);}

dynamic_obj_def_t *
wc_get_dynamic_obj_def(dynamic_obj_type_id_t type_id)
    {return id_dynamic_obj_def_table_get_ptr(&_dynamic_obj_defs, type_id);}

player_race_def_t *
wc_get_player_race_def(player_race_id_t type_id)
    {return id_player_race_def_table_get_ptr(&_player_race_defs, type_id);}

bool32
wc_chunk_file_tiles_ok(muta_chunk_file_t *ch)
{
    tile_t *tiles   = ch->tiles;
    if (!tiles) return 0;
    uint32 num = MUTA_CHUNK_W * MUTA_CHUNK_W * MUTA_CHUNK_T;
    for (uint32 i = 0; i < num; ++i)
        if (tiles[i] >= tile_defs_num) return 0;
    return 1;
}

bool32
wc_is_player_race_name_legal(const char *name)
{
    static char syms[] =
    {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
        'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4',
        '5', '6', '7', '8', '9', '0', '\''
    };
    return str_contains_only_symbols(name, syms, sizeof(syms));
}

bool32
str_contains_only_symbols(const char *str, const char *symbols,
    int num_symbols);

static void
_wc_read_static_obj_def_count_cb(void *ctx, const char *opt, const char *val)
    {if (streq(opt, "id")) (*(uint*)ctx)++;}

static void
_wc_read_static_obj_def_parms_cb(void *ctx, const char *opt, const char *val)
{
    static_obj_def_t *def = *(static_obj_def_t**)(ctx);

    if (streq(opt, "id"))
    {
        uint32 id = str_to_uint32(val);
        def = wc_get_static_obj_def(id);
        if (!def) def = id_static_obj_def_table_insert_empty(&_static_obj_defs,
            id);
        if (def) memset(def, 0, sizeof(static_obj_def_t));
        *(static_obj_def_t**)ctx = def;
        return;
    } else if (!def) return;

    if (streq(opt, "name"))
    {
        if (def->name) return;
        def->name = create_dynamic_str(val);
    } else
    if (streq(opt, "examine"))
    {
        if (def->examine) return;
        def->examine = create_dynamic_str(val);
    } else
    if (streq(opt, "spritesheet"))
    {
        if (def->gfx_type != WC_GFX_NONE) return;
        char    p[256];
        int     c;
        int     x, y;
        float   sx, sy;
        float   rot;
        int     flip;
        if (sscanf(val, "%s [%d] (ox:%d, oy:%d, sx:%f, sy:%f, rot:%f, flip:%d)",
            p, &c, &x, &y, &sx, &sy, &rot, &flip) != 8)
            return;
        def->gfx_type = WC_GFX_SPRITESHEET;
        def->gfx_data.spritesheet.path  = create_dynamic_str(p);
        def->gfx_data.spritesheet.clip  = c;
        def->gfx_data.spritesheet.ox    = x;
        def->gfx_data.spritesheet.oy    = y;
        def->gfx_data.spritesheet.sx    = sx;
        def->gfx_data.spritesheet.sy    = sy;
        def->gfx_data.spritesheet.rot   = rot;
        def->gfx_data.spritesheet.flip  = (uint8)(flip % 4);
    } else
    if (streq(opt, "passthrough"))
    {
        int v = atoi(val);
        def->passthrough = v ? 1 : 0;
    }
}

const char *
wc_iso_dir_to_str(int dir)
{
    switch (dir)
    {
        case ISODIR_NORTH:      return "north";
        case ISODIR_NORTH_EAST: return "north east";
        case ISODIR_EAST:       return "east";
        case ISODIR_SOUTH_EAST: return "south east";
        case ISODIR_SOUTH:      return "south";
        case ISODIR_SOUTH_WEST: return "south west";
        case ISODIR_WEST:       return "west";
        case ISODIR_NORTH_WEST: return "north west";
        default:                return "invalid";
    }
}

static int
_get_eq_value(char *line, char **ret_value, char *opt_name)
{
    int opt_len = (int)strlen(opt_name);
    if (strncmp(line, opt_name, opt_len))
        return 1;
    char *value = line + opt_len;
    while (*value != '=' && *value != '\n' && *value)
        value++;
    if (*value != '=')
        return 2;
    str_strip_trailing_spaces(++value);
    _darr_head(line)->num = (uint32)strlen(line);
    *ret_value = value;
    return 0;
}

int
wc_load_creature_defs(const char *fp)
{
    #define FMT_ERR_IF(v_, ret_) \
    if ((v_)) \
    { \
        fmt_error = 1; \
        ret = ret_; \
        goto out; \
    } \

    int             ret         = 0;
    creature_def_t  *def        = 0;
    char            *line       = 0;
    bool32          fmt_error   = 0;
    int             c;

    uint32 num_res = 256;
    obj_pool_init(&_creature_pool, num_res, sizeof(creature_def_t));
    uint32_creature_table_einit(&_id_num_creature_table, num_res);
    str_creature_table_einit(&_id_str_creature_table, num_res);
    darr_reserve(_all_creatures, num_res);

    FILE *f = fopen(fp, "r");
    if (!f)
    {
        printf("Failed to open creature definition file %s.\n", fp);
        return 1;
    }

    darr_reserve(line, 256);

    for (;;)
    {
        darr_clear(line);

        /* Find the next non-whitespace */
        while ((c = fgetc(f)) != EOF && (c == ' ' || c == '\n'));
        if (c == EOF)
            break;

        fseek(f, ftell(f) - 1, SEEK_SET);

        /* Read the line into a dynamic buffer */
        while ((c = fgetc(f)) != EOF && c != '\n')
           darr_push(line, (char)c);
        darr_push(line, 0); /* Zero-terminate */

        str_strip_trailing_spaces(line);
        _darr_head(line)->num = (uint32)strlen(line) + 1;

        if (line[0] == '#') /* Line is a comment */
            continue;

        /* Line begins a new creature definition? */
        if (!strncmp(line, "creature", 8))
        {
            char *id = line + 8;;
            while (*id && *id != ':' && *id != '\n')
                id++;
            if (*id == ':')
                id++;
            else if (!*id || *id == '\n')
            {
                fmt_error   = 1;
                ret         = 3;
                goto out;
            }
            str_strip_trailing_spaces(id);
            _darr_head(line)->num = (uint32)strlen(line) + 1;

            FMT_ERR_IF(!str_is_ascii(id), 4);

            DEBUG_PRINTFF("Creature id: %s\n", id);
            uint32 id_num = fnv_hash32_from_str(id);

            /* Insert creature into containers */
            def = obj_pool_reserve(&_creature_pool);
            str_creature_table_einsert_by_hash(&_id_str_creature_table, id_num,
                def);
            uint32_creature_table_einsert(&_id_num_creature_table, id_num, def);
            darr_push(_all_creatures, def);

            memset(def, 0, sizeof(creature_def_t));
            def->id_str = dstr_create(id);
            def->id_num = id_num;

            continue;
        }

        /* File doesn't begin with a creature definition */
        if (!def)
            {ret = 2; goto out;}

        char *value = 0;

        if (!_get_eq_value(line, &value, "name"))
        {
            FMT_ERR_IF(!str_is_ascii(value), 5);
            FMT_ERR_IF(def->name, 6);
            def->name = dstr_create(value);
            DEBUG_PRINTFF("name: %s\n", value);
            continue;
        }

        if (!_get_eq_value(line, &value, "sex"))
        {
            float val;
            FMT_ERR_IF(sscanf(value, "%f", &val) != 1, 7);
            def->sex = CLAMP(val, 0.f, 1.f);
            DEBUG_PRINTFF("sex: %f\n", val);
            continue;
        }

        if (!_get_eq_value(line, &value, "attackable"))
        {
            bool32 val;
            FMT_ERR_IF(str_to_bool(value, &val), 8);
            def->flags.attackable = (uint)val;
            DEBUG_PRINTFF("attackable: %s\n", value);
            continue;
        }

        if (!_get_eq_value(line, &value, "aggressive"))
        {
            bool32 val;
            FMT_ERR_IF(str_to_bool(value, &val), 9)
            DEBUG_PRINTFF("aggressive: %s\n", value);
            continue;
        }

        if (!_get_eq_value(line, &value, "texture_name"))
        {
            FMT_ERR_IF(def->gfx_type != WC_GFX_NONE, 10);
            def->gfx_type           = WC_GFX_TEXTURE;
            def->gfx_data.tex_name  = dstr_create(value);
            continue;
        }

        if (!_get_eq_value(line, &value, "ae_set"))
        {
            FMT_ERR_IF(def->gfx_type != WC_GFX_NONE, 10);
            def->gfx_type   = WC_GFX_AE_SET;
            def->gfx_data.ae_set_name = dstr_create(value);
            continue;
        }

        if (!_get_eq_value(line, &value, "script"))
        {
            FMT_ERR_IF(def->script_name, 11);
            def->script_name = dstr_create(value);
            continue;
        }

        if (!_get_eq_value(line, &value, "description"))
        {
            def->description = value;
            continue;
        }

        fmt_error   = 1;
        ret         = 11;
        goto out;
    }

    out:
        if (fmt_error)
            printf("Format error in creature definition file %s. Line "
                "contents: '%s', error %d.\n", fp, line, ret);
        if (ret)
            printf("Errors parsing creature definition file %s, code %d.\n",
                fp, ret);
        else
            _creature_defs_initialized = 1;
        fclose(f);
        darr_free(line);
        return ret;

    #undef FMT_ERR_IF
}
