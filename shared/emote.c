#include <string.h>
#include "emote.h"

typedef struct emote_t emote_t;

struct emote_t
{
    const char *name;
    const char *targeted_str;
    const char *non_targeted_str;
    const char *me_targeted_str;
    const char *me_non_targeted_str;
};

emote_t _emotes[] =
{
    {
        "poke",
        "%p pokes %t.",
        "%p pokes the air.",
        "You poke %t.",
        "You poke the air"
    },

    {
        "laugh",
        "%p laughs at %t.",
        "%p laughs.",
        "You laugh at %t",
        "You laugh."
    }
};

#define NUM_EMOTES (sizeof(_emotes) / sizeof(emote_t))

bool32      emote_is_id_valid(emote_id_t id) {return id < NUM_EMOTES;}
emote_id_t  emote_invalid_id() {return NUM_EMOTES;}
uint        emote_num_emotes() {return NUM_EMOTES;}

emote_id_t
emote_get_id_by_name(const char *name)
{
    for (int i = 0; i < NUM_EMOTES; ++i)
        if (!strcmp(_emotes[i].name, name)) return (emote_id_t)i;
    return NUM_EMOTES;
}

const char *
emote_get_name_by_id(emote_id_t id)
    {return id < NUM_EMOTES ? _emotes[id].name : 0;}

const char *
emote_get_targeted_str(emote_id_t id)
    {return id < NUM_EMOTES ? _emotes[id].targeted_str : 0;}

const char *
emote_get_non_targeted_str(emote_id_t id)
    {return id < NUM_EMOTES ? _emotes[id].non_targeted_str : 0;}

const char *
emote_get_me_targeted_str(emote_id_t id)
    {return id < NUM_EMOTES ? _emotes[id].me_targeted_str : 0;}

const char *
emote_get_me_non_targeted_str(emote_id_t id)
    {return id < NUM_EMOTES ? _emotes[id].me_non_targeted_str : 0;}
