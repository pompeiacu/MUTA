#include "netqueue.h"
#include "common_utils.h" /* For muta_assert() */

typedef struct net_queue_t   net_queue_t;

static void
_net_wait_callback(void *args);

#if defined(__linux__)

#include <sys/epoll.h>
#include <sys/eventfd.h>

#define NH_IN_QUEUE (1 << 0)

int
net_queue_init(net_queue_t *q, int num_threads, net_read_callback_t read_cb,
    net_accept_callback_t accept_cb)
{
    if (!read_cb)           return NETQERR_INVALID_PARAM;
    if (!accept_cb)         return NETQERR_INVALID_PARAM;
    if (num_threads < 1)    return NETQERR_INVALID_PARAM;

    memset(q, 0, sizeof(net_queue_t));
    q->epoll_fd         = -1;
    q->read_callback    = read_cb;
    q->accept_callback  = accept_cb;

    int ret         = 0;
    int num_ths_r   = num_threads ? num_threads : get_num_physical_cpus();

    if (kth_pool_init(&q->tp, num_ths_r, 16))
        {ret = NETQERR_NOMEM; goto cleanup;}

    q->epoll_fd = epoll_create(1);

    if (q->epoll_fd < 0)
        {ret = NETQERR_OS; goto cleanup;}

    cleanup:
    if (ret) net_queue_destroy(q);
    return ret;
}

int
net_queue_run(net_queue_t *q)
{
    if (q->running) return NETQERR_INVALID_CONDITION;

    int ret = 0;

    if (kth_pool_run(&q->tp))
        {ret = NETQERR_OS; goto cleanup;}

    q->running = 1;

    int num_ths = q->tp.threads_num;

    for (int i = 0; i < num_ths; ++i)
    {
        if (kth_pool_add_job(&q->tp, _net_wait_callback, q))
            {ret = NETQERR_OS; goto cleanup;}
    }

    cleanup:
    if (ret) q->running = 0;
    return ret;
}

int
net_queue_shutdown(net_queue_t *q)
{
    if (!q->running) return NETQERR_INVALID_CONDITION;

    int event_fd = eventfd(0, EFD_CLOEXEC|EFD_NONBLOCK);
    if (event_fd < 0) return NETQERR_OS;

    q->running = 0;

    struct epoll_event ev;
    ev.events = EPOLLIN | EPOLLET;

    if (epoll_ctl(q->epoll_fd, EPOLL_CTL_ADD, event_fd, &ev) < 0)
    {
        close(event_fd);
        return NETQERR_OS;
    }

    kth_pool_shutdown(&q->tp, 0);
    return 0;
}

int
net_queue_destroy(net_queue_t *q)
{
    if (q->running) return NETQERR_INVALID_CONDITION;
    close(q->epoll_fd);
    kth_pool_shutdown(&q->tp, 0);
    kth_pool_destroy(&q->tp);
    return 0;
}

int
net_queue_add(net_queue_t *q, net_handle_t *h, void *buf, int buf_len)
{
    struct epoll_event ev;
    ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLHUP | EPOLLONESHOT;
    ev.data.ptr = (void*)h;

    if (h->op == NETQOP_READ)
    {
        h->buf      = buf;
        h->buf_len  = buf_len;
    }

    /* Set operation depending on if we were already in the queue or not */
    int epoll_op;

    if (h->flags & NH_IN_QUEUE)
        epoll_op = EPOLL_CTL_MOD;
    else
    {
        h->flags |= NH_IN_QUEUE;
        epoll_op = EPOLL_CTL_ADD;
    }

    int r = epoll_ctl(q->epoll_fd, epoll_op, h->fd, &ev);
    return !r ? 0 : NETQERR_OS;
}

int
net_handle_init(net_handle_t *h, socket_t fd, int op, int32 user_id,
    void *user_ptr)
{
    if (fd == KSYS_INVALID_SOCKET) return NETQERR_INVALID_PARAM;
    if (op != NETQOP_LISTEN && op != NETQOP_READ) return NETQERR_INVALID_PARAM;
    memset(h, 0, sizeof(net_handle_t));
    h->fd               = fd;
    h->user_data.id     = user_id;
    h->user_data.ptr    = user_ptr;
    h->op               = op;
    h->flags            = 0;
    return 0;
}

void
net_handle_close(net_handle_t *h)
{
    net_shutdown_sock(h->fd, SOCKSD_BOTH);
    h->fd = KSYS_INVALID_SOCKET;
}

int
net_handle_read(net_handle_t *h, char *buf, int max_bytes)
    {return recv(h->fd, buf, max_bytes, MSG_DONTWAIT);}

static void
_net_wait_callback(void *args)
{
    net_queue_t *q = (net_queue_t*)args;

    int                     epoll_fd    = q->epoll_fd;;
    net_read_callback_t     rcb         = q->read_callback;
    net_accept_callback_t   acb         = q->accept_callback;

    struct epoll_event  events[64];
    int                 i, num_evs;
    net_handle_t        *h;
    socket_t            in_fd;
    addr_t              in_addr;
    int                 num_bytes;

    while (q->running)
    {
        num_evs = epoll_wait(epoll_fd, events, 64, -1);
        if (!q->running) break;

        for (i = 0; i < num_evs; ++i)
        {
            h = (net_handle_t*)events[i].data.ptr;
            if (h->fd == KSYS_INVALID_SOCKET) continue;

            if (h->op == NETQOP_READ)
            {
                if (events[i].events & (EPOLLRDHUP | EPOLLHUP | EPOLLERR))
                    num_bytes = 0;
                else if (events[i].events & EPOLLIN)
                    num_bytes = recv(h->fd, h->buf, h->buf_len, MSG_DONTWAIT);
                else
                {
                    struct epoll_event ev;
                    ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLHUP | EPOLLONESHOT;
                    ev.data.ptr = (void*)h;
                    epoll_ctl(q->epoll_fd, EPOLL_CTL_MOD, h->fd, &ev);
                }
                rcb(h, num_bytes);
            } else
            if (h->op == NETQOP_LISTEN)
            {
                if (events[i].events & (EPOLLRDHUP | EPOLLHUP | EPOLLERR))
                {
                    acb(h, in_fd, in_addr, NETQERR_OS);
                } else if (events[i].events & EPOLLIN)
                {
                    in_fd = net_accept(h->fd, &in_addr);
                    acb(h, in_fd, in_addr, 0);
                } else
                {
                    struct epoll_event ev;
                    ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLHUP | EPOLLONESHOT;
                    ev.data.ptr = (void*)h;
                    epoll_ctl(q->epoll_fd, EPOLL_CTL_MOD, h->fd, &ev);
                }
            } else {
                DEBUG_PRINTFF("net handle operation was invalid!\n");
                muta_assert(0);
            }
        }
    }
}

#elif defined(_WIN32)

#include <mswsock.h>

#define NET_QUEUE_MEM_POOL_MUTEX_INITIALIZED (1 << 0)
#define NET_QUEUE_ADDR_SZ (sizeof(struct sockaddr_in) + 16)

int
net_queue_init(net_queue_t *q, int num_threads,
    net_read_callback_t read_cb,
    net_accept_callback_t accept_cb)
{
    if (!read_cb)           return NETQERR_INVALID_PARAM;
    if (!accept_cb)         return NETQERR_INVALID_PARAM;
    if (num_threads < 0)    return NETQERR_INVALID_PARAM;

    memset(q, 0, sizeof(net_queue_t));

    int ret         = 0;
    int num_ths_r   = num_threads ? num_threads : get_num_physical_cpus();

    if (kth_pool_init(&q->tp,  num_ths_r, 16))
        {ret = NETQERR_NOMEM; goto cleanup;}

    q->port = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, (ULONG_PTR)q, 0);
    if (!q->port)
        {ret = NETQERR_OS; goto cleanup;}

    q->read_callback    = read_cb;
    q->accept_callback  = accept_cb;

    mutex_init(&q->mem_block_pool_mtx);
    q->flags |= NET_QUEUE_MEM_POOL_MUTEX_INITIALIZED;

    if (net_qmblock_pool_init(&q->mem_block_pool, 256, 8))
        {ret = NETQERR_NOMEM; goto cleanup;}

    cleanup:
    if (ret) net_queue_destroy(q);
    return ret;
}

int
net_queue_run(net_queue_t *q)
{
    if (q->running)
        return NETQERR_INVALID_CONDITION;

    int ret = 0;

    q->running = 1;

    if (kth_pool_run(&q->tp))
        {ret = NETQERR_OS; goto cleanup;}

    int num_ths = q->tp.threads_num;

    for (int i = 0; i < num_ths; ++i)
    {
        if (kth_pool_add_job(&q->tp, _net_wait_callback, q))
            {ret = NETQERR_OS; goto cleanup;}
    }

    cleanup:
    if (ret) q->running = 0;
    return ret;
}

int
net_queue_shutdown(net_queue_t *q)
{
    if (!q->running) return NETQERR_INVALID_CONDITION;
    q->running = 0;
    BOOL r = PostQueuedCompletionStatus(q->port, 0, 0, 0);
    kth_pool_shutdown(&q->tp, 0);
    return r ? 0 : NETQERR_OS;
}

int
net_queue_destroy(net_queue_t *q)
{
    if (q->running) return NETQERR_INVALID_CONDITION;
    CloseHandle(q->port);
    kth_pool_shutdown(&q->tp, 0);
    kth_pool_destroy(&q->tp);
    net_qmblock_pool_destroy(&q->mem_block_pool);
    if (q->flags & NET_QUEUE_MEM_POOL_MUTEX_INITIALIZED)
        mutex_destroy(&q->mem_block_pool_mtx);
    return 0;
}

static int
_net_queue_async_accept(net_queue_t *q, net_handle_t *h)
{
    char *start = (char*)h->op_data.accept->mem;
    char *mem   = start;

    socket_t *in_sock = (socket_t*)mem;
    mem += sizeof(socket_t);

    DWORD *num_bytes = (DWORD*)mem;
    mem += sizeof(DWORD);

    *in_sock = net_tcp_ipv4_sock();

    if (*in_sock == KSYS_INVALID_SOCKET)
        return NETQERR_OS;

    BOOL r = AcceptEx(h->fd, *in_sock, mem, 0, NET_QUEUE_ADDR_SZ,
        NET_QUEUE_ADDR_SZ, num_bytes, &h->ol);

    return (r || WSAGetLastError() == ERROR_IO_PENDING) ? 0 : NETQERR_OS;
}

int
net_queue_add(net_queue_t *q, net_handle_t *h, void *buf, int buf_len)
{
    /* If not registered yet, register with the queue */
    if (!h->queue)
    {
        if (h->op == NETQOP_LISTEN)
        {
            mutex_lock(&q->mem_block_pool_mtx);
            h->op_data.accept = net_qmblock_pool_reserve(&q->mem_block_pool);
            mutex_unlock(&q->mem_block_pool_mtx);
            if (!h->op_data.accept) return NETQERR_NOMEM;
        }

        if (!CreateIoCompletionPort((HANDLE)h->fd, q->port, (ULONG_PTR)h, 0))
        {
            muta_assert(0);
            if (h->op == NETQOP_LISTEN)
            {
                mutex_lock(&q->mem_block_pool_mtx);
                net_qmblock_pool_free(&q->mem_block_pool, h->op_data.accept);
                mutex_unlock(&q->mem_block_pool_mtx);
                h->op_data.accept = 0;
            }
            return NETQERR_OS;
        }
        h->queue = q;
    }

    if (h->op == NETQOP_READ)
    {
        h->op_data.read = buf;
        WSABUF wbuf;
        wbuf.len = buf_len;
        wbuf.buf = buf;
        DWORD flags = 0;
        if (WSARecv(h->fd, &wbuf, 1, 0, &flags, (LPOVERLAPPED)&h->ol, 0)
        && WSAGetLastError() != WSA_IO_PENDING)
            return NETQERR_OS;
    } else
    if (h->op == NETQOP_LISTEN)
    {
        if (_net_queue_async_accept(q, h)
        && WSAGetLastError() != WSA_IO_PENDING)
            return NETQERR_OS;
    } else muta_assert(0);
    return 0;
}

int
net_handle_init(net_handle_t *h, socket_t fd, int op, int32 user_id,
    void *user_ptr)
{
    if (fd == KSYS_INVALID_SOCKET) return NETQERR_INVALID_PARAM;
    if (op != NETQOP_READ && op != NETQOP_LISTEN) return NETQERR_INVALID_PARAM;
    memset(h, 0, sizeof(net_handle_t));
    h->ol.hEvent = WSACreateEvent();
    if (h->ol.hEvent == WSA_INVALID_EVENT) return NETQERR_OS;
    h->fd               = fd;
    h->user_data.ptr    = user_ptr;
    h->user_data.id     = user_id;
    h->op               = op;
    h->flags            = 0;
    return 0;
}

static void
_net_wait_callback(void *args)
{
    net_queue_t *q = args;

    net_read_callback_t     rcb = q->read_callback;
    net_accept_callback_t   acb = q->accept_callback;

    DWORD           num_bytes;
    ULONG_PTR       key;
    OVERLAPPED      *ret_ol;
    net_handle_t    *h;
    DWORD           flags = 0;
    char            *mem;
    socket_t        in_sock;
    struct sockaddr *in_sock_addr_loc;
    INT             in_sock_addr_loc_len;
    struct sockaddr *in_sock_addr_rem;
    INT             in_sock_addr_rem_len;
    addr_t          in_addr;

    while (q->running)
    {
        BOOL r = GetQueuedCompletionStatus(q->port, &num_bytes, &key,
            &ret_ol, INFINITE);

        if (!q->running || !r || !ret_ol)
            continue;

        h = (net_handle_t*)key;

        if (h->fd == KSYS_INVALID_SOCKET)
        {
            DEBUG_PRINTF("%s: a net_handle's socket was invalid (op: %d)\n",
                __func__, (int)h->op);
            continue;
        }

        if (h->op == NETQOP_READ)
        {
            if (h->fd != KSYS_INVALID_SOCKET)
                rcb(h, num_bytes);
        } else
        if (h->op == NETQOP_LISTEN)
        {
            mem     = h->op_data.accept->mem;
            in_sock = *(socket_t*)h->op_data.accept->mem;
            mem += sizeof(socket_t) + sizeof(DWORD);

            GetAcceptExSockaddrs(mem, 0, NET_QUEUE_ADDR_SZ,
                NET_QUEUE_ADDR_SZ, &in_sock_addr_loc,
                &in_sock_addr_loc_len, &in_sock_addr_rem,
                &in_sock_addr_rem_len);

            addr_from_sockaddr(&in_addr, in_sock_addr_rem);
            acb(h, in_sock, in_addr, 0);
        } else
            muta_assert(0);
    }
}

void
net_handle_close(net_handle_t *h)
{
    closesocket(h->fd);
    net_queue_t *q = h->queue;
    if (q && h->op == NETQOP_LISTEN)
    {
        mutex_lock(&q->mem_block_pool_mtx);
        net_qmblock_pool_free(&q->mem_block_pool, h->op_data.accept);
        mutex_unlock(&q->mem_block_pool_mtx);
    }
    h->fd = KSYS_INVALID_SOCKET;
}

#endif /* __linux__, _WIN32 */
