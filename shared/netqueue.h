#ifndef MUTA_NET_QUEUE_H
#define MUTA_NET_QUEUE_H

/* A unified interface to Linux' and Windows' more  advanced socket polling
 * APIs, epoll and I/O Completion Ports. */

#include "net.h"
#include "kthp.h"

enum net_queue_ops
{
    NETQOP_LISTEN = 1,
    NETQOP_READ
};

enum net_queue_errors
{
    NETQERR_NOMEM = 200,
    NETQERR_INVALID_PARAM,
    NETQERR_OS,
    NETQERR_INVALID_CONDITION
};

/* These structures are different depending on the operating system */
typedef struct net_queue_t      net_queue_t;
typedef struct net_handle_t     net_handle_t;

/* Callback types. */
typedef void (*net_read_callback_t)(net_handle_t *h, int num_bytes);
typedef void (*net_accept_callback_t)(net_handle_t *h, socket_t fd,
    addr_t addr, int err);

/* LINUX */
#if defined(__linux__)

struct net_queue_t
{
    int                     running;
    kth_pool_t              tp;
    int                     epoll_fd;
    net_read_callback_t     read_callback;
    net_accept_callback_t   accept_callback;
};

struct net_handle_t
{
    socket_t    fd;
    struct
    {
        void *ptr;
        int32 id;
    } user_data;
    void        *buf;
    int         buf_len;
    uint8       op;
    uint8       flags;
};

/* WINDOWS */
#elif defined(_WIN32)
#include "containers.h"

#define NET_QUEUE_MEM_BLOCK_SZ 256

typedef struct net_qmblock_t net_qmblock_t;

struct net_qmblock_t
{
    net_qmblock_t   *next;
    char            mem[NET_QUEUE_MEM_BLOCK_SZ];
};

DYNAMIC_OBJ_POOL_DEFINITION(net_qmblock_t, net_qmblock_pool, 16);

struct net_queue_t
{
    int                     running;
    HANDLE                  port;
    net_read_callback_t     read_callback;
    net_accept_callback_t   accept_callback;
    kth_pool_t              tp;
    net_qmblock_pool_t      mem_block_pool;
    mutex_t                 mem_block_pool_mtx;
    char                    flags;
};

struct net_handle_t
{
    OVERLAPPED      ol;
    net_queue_t     *queue;
    union
    {
        net_qmblock_t   *accept;
        void            *read;
    } op_data;
    struct
    {
        int32 id;
        void *ptr;
    } user_data;
    socket_t        fd;
    uint8           op;
    uint8           flags;
};

#endif

int
net_queue_init(net_queue_t *q, int num_threads, net_read_callback_t read_cb,
    net_accept_callback_t accept_cb);

int
net_queue_destroy(net_queue_t *q);

int
net_queue_run(net_queue_t *q);

int
net_queue_shutdown(net_queue_t *q);

int
net_queue_add(net_queue_t *q, net_handle_t *h, void *buf, int buf_sz);
/* The address of the handle struct must stay constant. */

int
net_handle_init(net_handle_t *h, socket_t fd, int op, int32 user_id,
    void *user_data);

void
net_handle_close(net_handle_t *h);
/* Closes a handle and the file descriptor, unassociating the handle with
 * any queue it's contained in */

#endif /* MUTA_NET_QUEUE_H */
