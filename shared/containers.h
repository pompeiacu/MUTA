#ifndef MUTA_GENERIC_CONTAINERS_H
#define MUTA_GENERIC_CONTAINERS_H

#include "common_utils.h" /* For muta_panic() */

/* A hashtable that will resize automatically should it fill up.  Because of
 * this, memory addresses to contained items are not constant.
 *
 * NOTES:
 * - bucket_sz must be <= 8 */

#define DYNAMIC_HASH_TABLE_DEFINITION(table_name, type, hash_input_type, \
    hash_type, hash_func, bucket_sz) \
\
typedef struct table_name##_item_t      table_name##_item_t; \
typedef struct table_name##_bucket_t    table_name##_bucket_t; \
typedef struct table_name##_t           table_name##_t; \
\
struct table_name##_item_t \
{ \
    type        val; \
    hash_type   hash; \
}; \
\
struct table_name##_bucket_t \
{ \
    table_name##_item_t items[bucket_sz]; \
    uint8               res_flags; \
}; \
\
struct table_name##_t \
{ \
    table_name##_bucket_t   *buckets; \
    size_t                  num_buckets; \
    size_t                  num_items; \
}; \
\
static inline int \
table_name##_init(table_name##_t *t, size_t sz); \
\
static inline void \
table_name##_einit(table_name##_t *t, size_t sz); \
\
static inline void \
table_name##_destroy(table_name##_t *t); \
\
static inline int \
table_name##_resize(table_name##_t *t, size_t new_num_buckets); \
/* A return value of < 0 indicates out of memory. > 0 indicates having run \
 * out of slots while rearranging the internal containers. */ \
\
static inline type * \
table_name##_insert_empty_by_hash(table_name##_t *t, hash_type hash); \
\
static inline type * \
table_name##_einsert_empty_by_hash(table_name##_t *t, hash_type hash); \
\
static inline int \
table_name##_insert_by_hash(table_name##_t *t, hash_type hash, type val); \
\
static inline void \
table_name##_einsert_by_hash(table_name##_t *t, hash_type hash, type val); \
\
static inline type * \
table_name##_insert_empty(table_name##_t *t, hash_input_type hi); \
\
static inline type * \
table_name##_einsert_empty(table_name##_t *t, hash_input_type hi); \
\
static inline int \
table_name##_insert(table_name##_t *t, hash_input_type hi, type val); \
\
static inline void \
table_name##_einsert(table_name##_t *t, hash_input_type hi, type val); \
\
static inline void \
table_name##_erase(table_name##_t *t, hash_input_type hi); \
\
static inline type * \
table_name##_get_ptr_by_hash(table_name##_t *t, hash_type hash); \
\
static inline type * \
table_name##_get_ptr(table_name##_t *t, hash_input_type hi); \
\
static inline type \
table_name##_get_by_hash(table_name##_t *t, hash_type hash); \
\
static inline type \
table_name##_get(table_name##_t *t, hash_input_type hi); \
\
static inline type \
table_name##_pop_by_hash(table_name##_t *t, hash_type h); \
\
static inline type \
table_name##_pop(table_name##_t *t, hash_input_type hi); \
\
static inline int \
table_name##_try_pop_by_hash(table_name##_t *t, hash_type hash, type *ret); \
\
static inline int \
table_name##_try_pop(table_name##_t *t, hash_input_type hi, type *ret); \
\
static inline int \
table_name##_exists(table_name##_t *t, hash_input_type hi); \
\
static inline void \
table_name##_clear(table_name##_t *t); \
\
static inline size_t \
table_name##_bucket_sz(); \
\
static inline type * \
table_name##_get_from_bucket_at(table_name##_t *t, size_t bucket_index, \
    size_t item_index); \
\
static inline int \
table_name##_init(table_name##_t *t, size_t num_buckets) \
{ \
    table_name##_t tmp = {0}; \
    tmp.buckets = calloc(num_buckets, sizeof(table_name##_bucket_t)); \
    if (!tmp.buckets) return 1; \
    tmp.num_buckets = num_buckets; \
    tmp.num_items   = 0; \
    *t = tmp; \
    return 0; \
} \
\
static inline void \
table_name##_einit(table_name##_t *t, size_t num_buckets) \
    {if (table_name##_init(t, num_buckets)) muta_panic(1, "Container error!");} \
\
static inline void \
table_name##_destroy(table_name##_t *t) \
{ \
    free(t->buckets); \
    memset(t, 0, sizeof(table_name##_t)); \
} \
\
static inline int \
table_name##_resize(table_name##_t *t, size_t new_num_buckets) \
{ \
    table_name##_t nt; \
    if (table_name##_init(&nt, new_num_buckets)) \
        return 1; \
\
    size_t  old_max = t->num_buckets; \
    int     j; \
\
    for (size_t i = 0; i < old_max; ++i) \
    { \
        for (j = 0; j < bucket_sz; ++j) \
        { \
            if (!(t->buckets[i].res_flags & (1 << j))) \
                continue; \
            if (table_name##_insert_by_hash(&nt, t->buckets[i].items[j].hash, \
                t->buckets[i].items[j].val)) \
                {table_name##_destroy(&nt); return 2;} \
        } \
    } \
\
    free(t->buckets); \
    *t = nt; \
    return 0; \
} \
\
static inline type * \
table_name##_insert_empty_by_hash(table_name##_t *t, hash_type hash) \
{ \
    if (!t->num_buckets && table_name##_init(t, 8)) \
        return 0; \
    size_t max_bs   = t->num_buckets; \
    size_t index    = hash % max_bs; \
\
    table_name##_bucket_t   *b = &t->buckets[index]; \
\
    size_t max_items    = max_bs * bucket_sz; \
    size_t prcnt        = 100 * t->num_items / max_items; \
\
    /* If more than 70% of the table is filled, enlarge. */ \
    if (prcnt >= 70) \
    { \
        size_t req_max_bs = max_bs + 16; \
        size_t new_max_bs = max_bs * 2; \
        if (new_max_bs <= req_max_bs) \
            new_max_bs = req_max_bs; \
        if (!table_name##_resize(t, new_max_bs)) \
        { \
            max_bs  = t->num_buckets; \
            index   = hash % max_bs; \
            b       = &t->buckets[index]; \
        } \
    } \
\
    type *slot = 0; \
\
    for (;;) \
    { \
        for (int i = 0; i < bucket_sz; ++i) \
            if (b->res_flags & (1 << i) && b->items[i].hash == hash) \
                muta_panic(1, "Container error!"); \
        for (int i = 0; i < bucket_sz; ++i) \
        { \
            table_name##_item_t *item = &b->items[i]; \
            if (b->res_flags & (1 << i)) \
                continue; \
            slot            = &item->val; \
            item->hash      = hash; \
            b->res_flags |= (1 << i); \
            t->num_items++; \
            break; \
        } \
        if (slot) break; \
        if (table_name##_resize(t, t->num_buckets * 2)) \
            return 0; \
        index   = hash % t->num_buckets; \
        b       = &t->buckets[index]; \
    } \
\
    return slot; \
} \
\
static inline type * \
table_name##_einsert_empty_by_hash(table_name##_t *t, hash_type hash) \
{ \
    type *ret = table_name##_insert_empty_by_hash(t, hash); \
    if (!ret) \
        muta_panic(1, "Container error!"); \
    return ret; \
} \
\
static inline int \
table_name##_insert_by_hash(table_name##_t *t, hash_type hash, type val) \
{ \
    type *sv = table_name##_insert_empty_by_hash(t, hash); \
    if (sv) {*sv = val; return 0;} \
    return 1; \
} \
\
static inline void \
table_name##_einsert_by_hash(table_name##_t *t, hash_type hash, type val) \
{ \
    if (table_name##_insert_by_hash(t, hash, val)) \
        muta_panic(1, "Container error!"); \
} \
\
static inline type * \
table_name##_insert_empty(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_insert_empty_by_hash(t, hash); \
} \
static inline type * \
table_name##_einsert_empty(table_name##_t *t, hash_input_type hi) \
{ \
    type *ret = table_name##_insert_empty(t, hi); \
    if (!ret) muta_panic(1, "Container error!"); \
    return ret; \
} \
\
static inline int \
table_name##_insert(table_name##_t *t, hash_input_type hi, type val) \
{ \
    type *sv = table_name##_insert_empty(t, hi); \
    if (sv) {*sv = val; return 0;} \
    return 1; \
} \
\
static inline void \
table_name##_einsert(table_name##_t *t, hash_input_type hi, type val) \
{ \
    int r = table_name##_insert(t, hi, val); \
    if (r) muta_panic(1, "Container error!"); \
} \
\
static inline void \
table_name##_erase_by_hash(table_name##_t *t, hash_type hash) \
{ \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8 res_flags = b->res_flags; \
    for (int i = 0; i < bucket_sz; ++i) \
    { \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
            {b->res_flags &= ~(1 << i); t->num_items--; return;} \
    } \
} \
\
static inline void \
table_name##_erase(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    table_name##_erase_by_hash(t, hash); \
} \
\
static inline type * \
table_name##_get_ptr_by_hash(table_name##_t *t, hash_type hash) \
{ \
    if (!t->num_buckets) return 0; \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8 res_flags = b->res_flags; \
    for (int i = 0; i < bucket_sz; ++i) \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
            return &b->items[i].val; \
    return 0; \
} \
static inline type * \
table_name##_get_ptr(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_get_ptr_by_hash(t, hash); \
} \
\
static inline type \
table_name##_get_by_hash(table_name##_t *t, hash_type hash) \
{ \
    type ret; \
    type *ptr = table_name##_get_ptr_by_hash(t, hash); \
    if (ptr) \
        ret = *ptr; \
    else \
        memset(&ret, 0, sizeof(ret)); \
    return ret; \
} \
\
static inline type \
table_name##_get(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_get_by_hash(t, hash); \
} \
static inline type \
table_name##_pop_by_hash(table_name##_t *t, hash_type hash) \
{ \
    type ret; \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8   res_flags   = b->res_flags; \
    bool32  found       = 0; \
    for (int i = 0; i < bucket_sz; ++i) \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
        { \
            ret = b->items[i].val; \
            res_flags &= ~(1 << i); \
            b->res_flags = res_flags; \
            t->num_items--; \
            found = 1; \
            break; \
        } \
    if (!found) \
        memset(&ret, 0, sizeof(ret)); \
    return ret; \
} \
\
static inline type \
table_name##_pop(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_pop_by_hash(t, hash); \
} \
\
static inline int \
table_name##_try_pop_by_hash(table_name##_t *t, hash_type hash, type *ret) \
{ \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8 res_flags = b->res_flags; \
    for (int i = 0; i < bucket_sz; ++i) \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
        { \
            *ret = b->items[i].val; \
            res_flags &= ~(1 << i); \
            b->res_flags = res_flags; \
            t->num_items--; \
            return 0; \
        } \
    return 1; \
} \
\
static inline int \
table_name##_try_pop(table_name##_t *t, hash_input_type hi, type *ret) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_try_pop_by_hash(t, hash, ret); \
} \
\
static inline int \
table_name##_exists(table_name##_t *t, hash_input_type hi) \
    {return table_name##_get_ptr(t, hi) ? 1 : 0;} \
\
static inline void \
table_name##_clear(table_name##_t *t) \
{ \
    memset(t->buckets, 0, t->num_buckets * sizeof(table_name##_bucket_t)); \
    t->num_items = 0; \
} \
static inline size_t \
table_name##_bucket_sz() \
    {return bucket_sz;} \
\
static inline type * \
table_name##_get_from_bucket_at(table_name##_t *t, size_t bucket_index, \
    size_t item_index) \
{ \
    if (bucket_index >= t->num_buckets) return 0; \
    if (item_index >= bucket_sz)        return 0; \
    table_name##_bucket_t *b = &t->buckets[bucket_index]; \
    return b->res_flags & bucket_index ? &b->items[item_index].val : 0; \
}

/* Name must not contain the _t postfix, cmd_type does */
#define DOUBLE_VAR_CMD_BUF_DEFINITION(name, cmd_type) \
\
typedef struct name##_arr_t name##_arr_t; \
typedef struct name##_t     name##_t; \
\
struct name##_arr_t \
{ \
    cmd_type        *darr; \
    dynamic_bbuf_t  bbuf; \
}; \
\
struct name##_t \
{ \
    name##_arr_t    wbuf, rbuf; \
    mutex_t         mtx; \
}; \
\
static inline int \
name##_init(name##_t *buf, int num, int mem); \
\
static inline void \
name##_destroy(name##_t *buf); \
\
static inline int \
name##_push(name##_t *buf, cmd_type *cmd); \
\
static inline int \
name##_push_data(name##_t *buf, cmd_type *cmd, const void *data, \
    int num_bytes); \
\
static inline cmd_type *\
name##_begin_push(name##_t *buf); /* Push empty, but leave lock on */ \
\
static inline void \
name##_finalize_push(name##_t *buf); \
/* Finalize a push from begin_push. Don't call if begin_push failed. */ \
\
static inline void * \
name##_reserve_data(name##_t *buf, int num_bytes); \
/* This assumes begin_push was called previously, but the push was not \
 * finalized. */ \
\
static inline void \
name##_cancel(name##_t *buf); \
\
static inline cmd_type * \
name##_swap(name##_t *buf, uint32 *num); \
\
static inline void * \
name##_cmd_data(name##_t *buf, cmd_type *cmd); \
\
static inline void \
name##_clear_read(name##_t *buf); \
\
static inline int \
name##_init(name##_t *buf, int num, int mem) \
{ \
    muta_assert(num >= 0 && mem >= 0); \
    memset(buf, 0, sizeof(name##_t)); \
    int ret = 0; \
    darr_reserve(buf->wbuf.darr, (uint32)num); \
    darr_reserve(buf->rbuf.darr, (uint32)num); \
    if (dynamic_bbuf_init(&buf->wbuf.bbuf, mem))    {ret = 3; goto failure;} \
    if (dynamic_bbuf_init(&buf->rbuf.bbuf, mem))    {ret = 4; goto failure;} \
    mutex_init(&buf->mtx); \
    return ret; \
\
    failure: \
        name##_destroy(buf); \
        return ret; \
} \
\
static inline void \
name##_destroy(name##_t *buf) \
{ \
    darr_free(buf->wbuf.darr); \
    darr_free(buf->rbuf.darr); \
    dynamic_bbuf_destroy(&buf->wbuf.bbuf); \
    dynamic_bbuf_destroy(&buf->rbuf.bbuf); \
    mutex_destroy(&buf->mtx); \
    memset(buf, 0, sizeof(name##_t)); \
} \
\
static inline int \
name##_push(name##_t *buf, cmd_type *cmd) \
{ \
    int ret = 0; \
    mutex_lock(&buf->mtx); \
    cmd_type *cc = darr_push_empty(buf->wbuf.darr); \
    if (cc) \
    { \
        *cc             = *cmd; \
        cc->num_bytes   = 0; \
    } else \
        ret = 1; \
    mutex_unlock(&buf->mtx); \
    return ret; \
} \
\
static inline int \
name##_push_data(name##_t *buf, cmd_type *cmd, const void *data, \
    int num_bytes) \
{ \
    mutex_lock(&buf->mtx); \
\
    int         ret     = 0; \
    uint32      index   = darr_num(buf->wbuf.darr); \
    cmd_type    *cc     = darr_push_empty(buf->wbuf.darr); \
\
    if (cc) \
    { \
        *cc             = *cmd; \
        cc->offset      = buf->wbuf.bbuf.num_bytes; \
        cc->num_bytes   = num_bytes; \
        if (dynamic_bbuf_write_bytes(&buf->wbuf.bbuf, data, num_bytes)) \
            {darr_erase(buf->wbuf.darr, index); ret = 2;} \
    } else \
        ret = 1; \
\
    mutex_unlock(&buf->mtx); \
    return ret; \
} \
static inline cmd_type * \
name##_begin_push(name##_t *buf) \
{ \
    mutex_lock(&buf->mtx); \
    cmd_type *cc = darr_push_empty(buf->wbuf.darr); \
    if (cc) cc->num_bytes = 0; else mutex_unlock(&buf->mtx); \
    return cc; \
} \
\
static inline void \
name##_finalize_push(name##_t *buf) \
    {mutex_unlock(&buf->mtx);} \
\
static inline void * \
name##_reserve_data(name##_t *buf, int num_bytes) \
    {return bbuf_reserve(&buf->wbuf.bbuf, num_bytes);} \
\
static inline void \
name##_cancel(name##_t *buf) \
    {_darr_head(buf->wbuf.darr)->num--;} \
\
static inline cmd_type * \
name##_swap(name##_t *buf, uint32 *num) \
{ \
    mutex_lock(&buf->mtx); \
    name##_arr_t tmp    = buf->wbuf; \
    buf->wbuf           = buf->rbuf; \
    buf->rbuf           = tmp; \
    mutex_unlock(&buf->mtx); \
    *num = darr_num(buf->rbuf.darr); \
    return buf->rbuf.darr; \
} \
\
static inline void * \
name##_cmd_data(name##_t *buf, cmd_type *cmd) \
    {return buf->rbuf.bbuf.mem + cmd->offset;} \
\
static inline void \
name##_clear_read(name##_t *buf) \
{ \
    darr_clear(buf->rbuf.darr); \
    BBUF_CLEAR(&buf->rbuf.bbuf); \
}


/* An object pool that will not grow nor shrink dynamically. type requires the
 * _t postfix, pool_name must not have it. */

#define STATIC_OBJ_POOL_DEFINITION(type, pool_name) \
typedef struct pool_name##_t pool_name##_t; \
struct pool_name##_t \
{ \
    type    *items, *res, *free; \
    int     max; \
}; \
\
static inline int \
pool_name##_init(pool_name##_t *p, type *mem, int max); \
\
static inline void \
pool_name##_clear(pool_name##_t *p); \
\
static inline int \
pool_name##_init(pool_name##_t *p, type *mem, int max) \
{ \
    if (max <= 0)   return 1; \
    if (!mem)       return 2; \
    for (int i = 0; i < max - 1; ++i) \
        mem[i].next = &mem[i] + 1; \
    mem[max - 1].next   = 0; \
    p->items            = mem; \
    p->free             = mem; \
    p->res              = 0; \
    p->max              = max; \
    return 0; \
} \
\
static inline void \
pool_name##_clear(pool_name##_t *p) \
{ \
    int num     = p->max - 1; \
    type *mem   = p->items; \
    for (int i = 0; i < num; ++i) \
        mem[i].next = &mem[i] + 1; \
    mem[num].next   = 0; \
    p->res          = 0; \
} \
\
static inline type * \
pool_name##_reserve(pool_name##_t *p) \
{ \
    type *ret = p->free; \
    if (!ret) return 0; \
    p->free     = ret->next; \
    ret->next   = p->res; \
    p->res      = ret; \
    return ret; \
} \
\
static inline void \
pool_name##_free(pool_name##_t *p, type *item) \
{ \
    if (!item) return; \
    for (type **oi = &p->res; *oi; oi = &(*oi)->next) \
    { \
        if (*oi != item) continue; \
        *oi = item->next; \
        break; \
    } \
    item->next  = p->free; \
    p->free     = item; \
}

/* A single-linked list of objects whose memory addresses will not change even
 * if the internal array is resized.
 * The type contained must have a "next" pointer as a member.
 * new_arr_sz is the number of new objects allocated if space is run out of */
#define DYNAMIC_OBJ_POOL_DEFINITION(type, pool_name, new_arr_sz) \
\
typedef struct pool_name##_t pool_name##_t; \
\
struct pool_name##_t \
{ \
    type    **item_arrs; \
    int     num_item_arrs; \
    int     max_item_arrs; \
    int     first_arr_sz; \
    type    *free, *res; \
}; \
\
static inline int \
pool_name##_init(pool_name##_t *p, int num, int num_arrs); \
\
static inline int \
pool_name##_init(pool_name##_t *p, int num, int num_arrs); \
\
static inline void \
pool_name##_destroy(pool_name##_t *p); \
\
static inline void \
pool_name##_clear(pool_name##_t *p); \
\
static inline type * \
pool_name##_first(pool_name##_t *p) \
    {return p->res;} \
\
static inline int \
pool_name##_init(pool_name##_t *p, int num, int num_arrs) \
{ \
    if (num <= 0)       return 1; \
    if (num_arrs <= 0)  return 2; \
\
    pool_name##_t tmp = {0}; \
\
    tmp.item_arrs = calloc(num_arrs, sizeof(type*)); \
    if (!tmp.item_arrs) return 3; \
\
    int ret = 0; \
\
    tmp.max_item_arrs   = num_arrs; \
    tmp.item_arrs[0]    = malloc(num * sizeof(type)); \
    tmp.first_arr_sz    = num; \
\
    if (!tmp.item_arrs[0]) \
    { \
        ret = 4; \
        goto cleanup; \
    } \
\
    tmp.num_item_arrs   = 1; \
    type *arr           = tmp.item_arrs[0]; \
\
    for (int i = 0; i < num - 1; ++i) \
        arr[i].next = &arr[i] + 1; \
\
    arr[num - 1].next   = 0; \
    tmp.free            = arr; \
\
    cleanup: \
\
    if (ret == 0) \
        *p = tmp; \
    else \
        pool_name##_destroy(&tmp); \
\
    return ret; \
} \
\
static inline void \
pool_name##_einit(pool_name##_t *p, int num, int num_arrs) \
    {if (pool_name##_init(p, num, num_arrs)) muta_panic(1, "Container error!");} \
\
static inline void \
pool_name##_destroy(pool_name##_t *p) \
{ \
    for (int i = 0; i < p->num_item_arrs; ++i) \
        free(p->item_arrs[i]); \
    free(p->item_arrs); \
    memset(p, 0, sizeof(pool_name##_t)); \
} \
\
static inline void \
pool_name##_clear(pool_name##_t *p) \
{ \
    int i, j, num_items; \
    type *arr, *last; \
    type **last_ptr = &p->free; \
 \
    for (i = 0; i < p->num_item_arrs; ++i) \
    { \
        arr         = p->item_arrs[i]; \
        num_items   = i == 0 ? p->first_arr_sz : new_arr_sz; \
 \
        for (j = 0; j < num_items; ++j) \
            arr[j].next = &arr[j] + 1; \
 \
        last        = &arr[num_items - 1]; \
        last->next  = 0; \
        *last_ptr   = arr; \
        last_ptr    = &last->next; \
    } \
    p->res = 0; \
} \
\
static inline type * \
pool_name##_reserve(pool_name##_t *p) \
{ \
    /* Allocate more */ \
    if (!p->free) \
    { \
        if (p->num_item_arrs == p->max_item_arrs) \
        { \
            int max_arrs = p->max_item_arrs + 4; \
            type **arrs = realloc(p->item_arrs, max_arrs * sizeof(type*)); \
            if (!arrs) return 0; \
\
            p->item_arrs        = arrs; \
            p->max_item_arrs    = max_arrs; \
        } \
\
        type *arr = malloc(new_arr_sz * sizeof(type)); \
        if (!arr) return 0; \
\
        p->item_arrs[p->num_item_arrs++] = arr; \
\
        for (int i = 0; i < new_arr_sz; ++i) \
            arr[i].next = &arr[i] + 1; \
\
        arr[new_arr_sz - 1].next    = 0; \
        p->free                     = arr; \
    } \
\
    type *ret   = p->free; \
    p->free     = ret->next; \
    ret->next   = p->res; \
    p->res      = ret; \
    return ret; \
} \
\
static inline type * \
pool_name##_ereserve(pool_name##_t *p) \
{ \
    type *ret = pool_name##_reserve(p); \
    if (!ret) muta_panic(1, "Container error!"); \
    return ret; \
} \
\
static inline void \
pool_name##_free(pool_name##_t *p, type *item) \
{ \
    if (!item) return; \
    for (type **oi = &p->res; *oi; oi = &(*oi)->next) \
    { \
        if (*oi != item) continue; \
        *oi = item->next; \
        break; \
    } \
    item->next  = p->free; \
    p->free     = item; \
}

#endif /* MUTA_GENERIC_CONTAINERS_H */
