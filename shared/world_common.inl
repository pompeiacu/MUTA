static inline enum iso_dir_t
vec2_to_iso_dir(int dx, int dy);
/* The values of the vector passed to this function can be in any range -
 * they will be clamped automatically */

static inline enum iso_dir_t
vec2_to_iso_dir(int dx, int dy)
{
    static enum iso_dir_t table[9] =
    {
        /* x, y */
        ISODIR_NORTH_WEST, ISODIR_NORTH,   ISODIR_NORTH_EAST,
        ISODIR_WEST,       INVALID_ISODIR, ISODIR_EAST,
        ISODIR_SOUTH_WEST, ISODIR_SOUTH,   ISODIR_SOUTH_EAST
    };

    int fdx = CLAMP(dx, -1, 1) + 1;
    int fdy = CLAMP(dy, -1, 1) + 1;

    return table[fdy * 3 + fdx];
}

static inline int
iso_dir_to_vec2(int dir, int *ret_x, int *ret_y)
{
    switch (dir)
    {
        case ISODIR_NORTH:      *ret_x =  0; *ret_y = -1; break;
        case ISODIR_NORTH_EAST: *ret_x =  1; *ret_y = -1; break;
        case ISODIR_EAST:       *ret_x =  1; *ret_y =  0; break;
        case ISODIR_SOUTH_EAST: *ret_x =  1; *ret_y =  1; break;
        case ISODIR_SOUTH:      *ret_x =  0; *ret_y =  1; break;
        case ISODIR_SOUTH_WEST: *ret_x = -1; *ret_y =  1; break;
        case ISODIR_WEST:       *ret_x = -1; *ret_y =  0; break;
        case ISODIR_NORTH_WEST: *ret_x = -1; *ret_y = -1; break;
        default:                *ret_x =  0; *ret_y =  0; return 1;
    }
    return 0;
}
