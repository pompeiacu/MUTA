#ifndef MUTA_TYPES_H
#define MUTA_TYPES_H

#include <stdint.h>

typedef unsigned char           uchar;
typedef unsigned short          ushort;
typedef unsigned int            uint;
typedef int8_t                  int8;
typedef uint8_t                 uint8;
typedef int32_t                 int32;
typedef uint32_t                uint32;
typedef int32_t                 bool32;
typedef int8_t                  bool8;
typedef int16_t                 int16;
typedef uint16_t                uint16;
typedef int64_t                 int64;
typedef uint64_t                uint64;
typedef unsigned int            tex_id_t;   /* Renderer texture id */
typedef unsigned int            au_buf_t;   /* Audio buffer identifier */
typedef uint16_t                msg_sz_t;   /* Part of an encrypted message */
typedef long int                lint;
typedef long long int           llint;
typedef long unsigned int       luint;
typedef long long unsigned int  lluint;
typedef uint16                  tile_t;
typedef uint32                  static_obj_type_id_t;
typedef static_obj_type_id_t    sobj_type_id_t;
typedef uint32                  dynamic_obj_type_id_t;
typedef dynamic_obj_type_id_t   dobj_type_id_t;
typedef uint32                  creature_type_id_t;
typedef uint64                  player_guid_t; /* For characters */
typedef uint64                  creature_guid_t;
typedef uint64                  dynamic_obj_guid_t;
typedef dynamic_obj_guid_t      dobj_guid_t;
typedef uint32                  map_guid_t;
typedef uint64                  instance_guid_t;
typedef instance_guid_t         inst_guid_t;
typedef struct world_pos_t      world_pos_t;
typedef struct world_pos_t      wpos_t;
typedef char                    dchar; /* Dynamic string */
typedef uint8                   player_race_id_t;
typedef player_race_id_t        player_race_id_darr_t;
typedef uint32                  map_id_t;
typedef uint16                  emote_id_t;
/* Types to mark certain pointers as dynamic arrays. */
typedef float                   float_darr_t;
typedef int                     int_darr_t;
typedef void*                   void_ptr_darr_t;
typedef uint32                  uint32_darr_t;
typedef uint64                  uint64_darr_t;

struct world_pos_t
{
    int32   x;
    int32   y;
    int8    z;
};

#define MUTA_LIL_ENDIAN 1
#define MUTA_BIG_ENDIAN 2
#define MUTA_ENDIANNESS MUTA_LIL_ENDIAN

#endif
