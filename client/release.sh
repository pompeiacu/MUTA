./configure

make clean
make all-opt

rm -rf release
mkdir release

echo hostname = 127.0.0.1 > release/server_info.cfg

cp "default_config.cfg" "release/config.cfg"
cp "rundir/muta" "release"
cp ../libs/linux/lib/libSDL2-2.0.so* release
cp ../libs/linux/lib/libsndio.so* release
cp ../libs/linux/lib/libsodium.so* release
cp ../libs/linux/lib/libopenal.so* release

cp ../COPYING release

cp -r ../paperwork release
cp -r ../assets release
cp -r ../MUTA-Assets release/muta-assets
cp -r ../MUTA-Data release/muta-data

echo #!/bin/bash > release/run.sh
echo LD_LIBRARY_PATH="." ./muta >> release/run.sh

chmod +x release/run.sh
chmod +x release/muta

rm -rf release/*~
rm -rf release/*.swp
rm -rf release/*.swpo
