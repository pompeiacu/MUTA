#include "gamestate.h"
#include "world.h"
#include "world_internal.h"
#include "render.h"
#include "../../shared/emote.h"
#include "lui.h"
#include "hotkey.h"

#define MAX_CHAT_ENTRIES_PER_FRAME  32

typedef struct chat_entry_t chat_entry_t;

struct chat_entry_t
{
    int     type;
    char    sender[MAX_CHARACTER_NAME_LEN + 1];
    char    msg[MAX_CHAT_MSG_LEN + 1];
};

static bool32       _in_session;
static const char   *_unk_name_str = "Unknown";
static world_t      _world;
static entity_t     *_player_entity;

static struct
{
    chat_entry_t    entries[MAX_CHAT_ENTRIES_PER_FRAME];
    uint32          num_entries;
    uint32          first_index;
} _chat_cache; /* Cleared every frame */

static uint8 _special_keys[NUM_GS_SPECIAL_KEYS];

#include "gamepackets.c" /* Included here for the static variables. */

static void
_wev_despawn_entity(world_t *w, void *data, world_event_t *ev);

static bool32
_check_mouse_world_interaction();

static void
_click_to_move(int px_x, int px_y);

static int
_drag_to_move();

static void
_move_in_dir(int dir);

static int
_send_find_path_msg_to_sv(int32 x, int32 y, int8 z);

static int
_send_chat_msg_to_sv(const char *msg);

static int
_get_mod_key_mask();

static void
_execute_hk_action(hk_action_callback_t *a);

static void _capi_move_north() {_move_in_dir(ISODIR_NORTH);}
static void _capi_move_south() {_move_in_dir(ISODIR_SOUTH);}
static void _capi_move_west() {_move_in_dir(ISODIR_WEST);}
static void _capi_move_east() {_move_in_dir(ISODIR_EAST);}

void
gs_init()
{
    hk_init();

    /*-- Default key bindings --*/
    hk_action_callback_t cb;
    cb.type = HK_ACTION_CALLBACK_C;

    /*-- Bind default keys --*/
    cb.c.callback = _capi_move_north;
    hk_new_action("Move North", cb, HK_PRESS_REPEAT);
    cb.c.callback = _capi_move_south;
    hk_new_action("Move South", cb, HK_PRESS_REPEAT);
    cb.c.callback = _capi_move_west;
    hk_new_action("Move West", cb, HK_PRESS_REPEAT);
    cb.c.callback = _capi_move_east;
    hk_new_action("Move East", cb, HK_PRESS_REPEAT);
    cb.c.callback = lui_reload;
    hk_new_action("Reload UI", cb, HK_PRESS_UP);

    hk_bind_key("Move North", 0, hk_str_to_keycode("W"), 0);
    hk_bind_key("Move South", 0, hk_str_to_keycode("S"), 0);
    hk_bind_key("Move West", 0, hk_str_to_keycode("A"), 0);
    hk_bind_key("Move East", 0, hk_str_to_keycode("D"), 0);
    hk_bind_key("Reload UI", 0, hk_str_to_keycode("F5"), 0);

    world_init(&_world, 0, 0, 1024);
    world_register_event_listener(&_world, WEV_DESPAWN_ENTITY, 0,
        _wev_despawn_entity);

    lui_init();
}

void
gs_destroy()
{
    world_destroy(&_world);
    lui_destroy();
    hk_destroy();
}

bool32
gs_initialized()
{
    return wc_all_defs_initialized();
}

int
gs_begin_session(uint32 map_id, player_guid_t id, const char *name,
    player_race_id_t race, int32 x, int32 y, int8 z, int dir)
{
    muta_assert(!_in_session);
    lui_reload();
    if (world_load_map_by_id(&_world, map_id))
        return 1;
    entity_t *e = world_spawn_player(&_world, id, name, x, y, z, dir, race, 0);
    _player_entity  = e;
    _in_session     = 1;
    memset(_special_keys, 0, sizeof(_special_keys));
    return 0;
}

void
gs_end_session()
    {_in_session = 0;}

bool32
gs_in_session()
    {return _in_session;}

void
gs_update_and_render(double dt, int x, int y, int w, int h)
{
    muta_assert(_in_session);

    uint32          num_chat_entries    = _chat_cache.num_entries;
    uint32          first_index         = _chat_cache.first_index;
    chat_entry_t    *chat_entry;
    for (uint32 i = 0; i < num_chat_entries; ++i)
    {
        uint32 index = (first_index + i) % MAX_CHAT_ENTRIES_PER_FRAME;
        chat_entry = &_chat_cache.entries[index];
        lui_event_t lui_event;
        lui_event.type = LUI_EVENT_CHAT_LOG_ENTRY;
        lui_event.chat_log_entry.msg_type   = chat_entry->type;
        lui_event.chat_log_entry.sender     = chat_entry->sender;
        lui_event.chat_log_entry.msg        = chat_entry->msg;
        lui_post_event(&lui_event);
    }
    _chat_cache.first_index = 0;
    _chat_cache.num_entries = 0;

    if (!lui_is_grabbing_keyboard())
    {
        uint32      num_hka;
        hk_action_t **hka = hk_get_repeat_actions(&num_hka);
        for (uint32 i = 0; i < num_hka; ++i)
        {
            for (int j = 0; j < 2; ++j)
            {
                if (hka[i]->keys[j].keycode < 0)
                    continue;
                SDL_Scancode sc = SDL_GetScancodeFromKey(
                    hka[i]->keys[j].keycode);
                if (!keyboard_state[sc])
                    continue;
                _execute_hk_action(&hka[i]->callback);
                j = 2;
            }
        }
    }

    float sx = w ? (float)w / RESOLUTION_W : 0.f;
    float sy = h ? (float)h / RESOLUTION_H : 0.f;

    world_render_props_t    rp      = {0};
    int                     dp[6]   = {0};

    rp.screen_area[0] = x - (int)((float)(tile_px_w * 3) * sx * 0.5f) -
        (int)((float)(tile_px_w / 2) * sx);
    rp.screen_area[1] = y - (int)((float)(tile_px_h * 3) * sy * 0.5f);
    rp.screen_area[2] = w + (int)((float)(tile_px_w * 3) * sx);
    rp.screen_area[3] = h + (int)((float)(tile_px_h * 3) * sy);

    rp.scissor[0]     = x;
    rp.scissor[1]     = y;
    rp.scissor[2]     = w;
    rp.scissor[3]     = h;

    rp.coord_space[0] = 0;
    rp.coord_space[1] = 0;
    rp.coord_space[2] = RESOLUTION_W + tile_px_w * 3;
    rp.coord_space[3] = RESOLUTION_H + tile_px_h * 3;

    int     tx, ty, tz;
    float   bx, by;

    /* Figure out the tiled width and height of the rendered area */
    int ptw = rp.coord_space[2] / MAX(tile_px_w, 2)     * 2 + 3;
    int pth = rp.coord_space[3] / MAX(tile_top_px_h, 2) * 2 + 3;
    int tw  = MAX(ptw, pth);

    /* Focus the camera so that the center is on the tile the player inhabits */
    entity_t        *pe = _player_entity;
    mobility_data_t *md = entity_get_mobility_data(pe);

    /* Params used to make tiles transparent in given position and area */
    dp[0] = pe->x;
    dp[1] = pe->y;
    dp[2] = pe->z;
    dp[3] = 5;
    dp[4] = 5;
    dp[5] = 0;

    for (int i = 1; i < MAP_CHUNK_T - pe->z; ++i)
    {
        if (!world_get_tile_def(&_world, pe->x, pe->y,
            pe->z + i)->passthrough)
        {
            /* Drawing inside houses/caves etc */
            dp[2] = pe->z + 1; // Rendering starts one tile higher
            dp[3] = 37;        // Area width/height 37
            dp[4] = 37;
            /* Why the fuck are these hardcoded??? */
            dp[5] = 1;         // 'Tiles above player' flag
        }
    }

    #define NUM_VISIBLE_LAYERS 14

    tx = pe->x - tw / 2;
    ty = pe->y - tw / 2;
    tz = pe->z - NUM_VISIBLE_LAYERS / 2;

    if (md && md->walk_timer > 0.f)
    {
        int     tdx     = md->last_x - pe->x;
        int     tdy     = md->last_y - pe->y;
        float   t_px_hw = (float)(tile_px_w / 2);
        float   t_px_hh = (float)(tile_top_px_h / 2);
        float   dx      = tdx * t_px_hw - tdy * t_px_hw;
        float   dy      = tdy * t_px_hh + tdx * t_px_hh;
        float prcnt = md->walk_timer / WC_WALK_SPEED_TO_SEC(md->walk_speed);
        rp.screen_area[0] -= (int)(sx * dx * prcnt);
        rp.screen_area[1] -= (int)(sy * dy * prcnt);
    }

    /* Determine where the first tile should be drawn */
    bx = (float)rp.coord_space[2] * 0.5f;
    by = -(float)(tw * tile_top_px_h / 4) + \
        (float)(NUM_VISIBLE_LAYERS * tile_top_px_h / 2);

    rp.tx = tx;
    rp.ty = ty;
    rp.tz = tz;
    rp.tw = tw;
    rp.th = tw;
    rp.tt = NUM_VISIBLE_LAYERS;
    rp.bx = bx;
    rp.by = by;
    rp.st = tz;

    world_update(&_world, &rp, dt);

#if 0
    if (BUTTON_DOWN(BUTTON_BIT(BUTTON_LEFT)) &&
        BUTTON_DOWN(BUTTON_BIT(BUTTON_RIGHT)))
        _drag_to_move();
#endif

    int     hilight_tile_pos[3];
    bool32  do_hilight = world_get_solid_tile_by_px_pos(&_world, mouse_state.x,
        mouse_state.y, hilight_tile_pos);

    r_render_world(&_world, &rp, dp, do_hilight ? hilight_tile_pos : 0);
    lui_update_and_render(dt);
}

void
gs_mouse_up(int button, int x, int y)
{
}

void
gs_mouse_down(int button, int x, int y)
{
    if (!_check_mouse_world_interaction())
        return;
    if (button == BUTTON_RIGHT)
        _click_to_move(x, y);
}

static void
_on_key_event(int event_type, int key, int lui_event_type)
{
    if (key == KEYCODE(LSHIFT) || key == KEYCODE(RSHIFT) ||
        key == KEYCODE(LCTRL)  || key == KEYCODE(RCTRL) ||
        key == KEYCODE(LALT)   || key == KEYCODE(RALT))
        return;
    int mods = _get_mod_key_mask();
    if (!lui_is_grabbing_keyboard())
    {
        hk_action_t *a = hk_get_action(key, mods);
        if (a && a->press_event_type == event_type)
            _execute_hk_action(&a->callback);
    }
    lui_event_t lui_event;
    lui_event.type          = lui_event_type;
    if (!hk_key_combo_to_str(key, mods, lui_event.key.key_combo_name))
        lui_post_event(&lui_event);
}

void
gs_key_down(int key, bool32 repeat)
    {_on_key_event(HK_PRESS_DOWN, key, LUI_EVENT_KEY_DOWN);}

void
gs_key_up(int key)
{
    if (key == KEYCODE(RETURN) && _special_keys[GS_SPECIAL_KEY_ENTER])
        _special_keys[GS_SPECIAL_KEY_ENTER] = 0;
    else
        _on_key_event(HK_PRESS_UP, key, LUI_EVENT_KEY_UP);
}

int
gs_send_chat_msg_to_sv(const char *msg)
    {return _send_chat_msg_to_sv(msg);}

void
gs_flag_special_key_executed(int special_key)
{
    muta_assert(special_key >= 0 && special_key < NUM_GS_SPECIAL_KEYS);
    int scancode;
    switch (special_key)
    {
    case GS_SPECIAL_KEY_ENTER:
        scancode = SDL_SCANCODE_RETURN;
        break;
    }
    if (IS_KEY_PRESSED(scancode))
        _special_keys[special_key] = 1;
}

static void
_wev_despawn_entity(world_t *w, void *data, world_event_t *ev)
{
    if (ev->d.despawn_entity == _player_entity)
        _player_entity = 0;
}

static void
_click_to_move(int x, int y)
{
    int tp[3];
    if (!world_get_solid_tile_by_px_pos(&_world, x, y, tp))
        return;
    tp[2] += 1;
    if (!world_can_player_enter_tile(&_world, tp[0], tp[1], tp[2]))
        return;
    _send_find_path_msg_to_sv(tp[0], tp[1], tp[2]);
}

static int
_drag_to_move()
{
    entity_t *e = _player_entity;
    if (!e)
        return 0;
    int px, py;
    if (!r_compute_px_pos_from_tiled_pos(&_world.render_props, e->x, e->y,
        e->z - 1, &px, &py))
        return 5;
    float sx = (float)_world.render_props.screen_area[2] /
        (float)_world.render_props.coord_space[2];
    float sy = (float)_world.render_props.screen_area[3] /
        (float)_world.render_props.coord_space[3];
    float s_tw = sx * (float)(tile_px_w / 2);
    float s_th = sy * (float)(tile_top_px_h / 2);
    px += (int)s_tw / 2;
    py += (int)s_th / 2;
    int dx = mouse_state.x - px;
    int dy = mouse_state.y - py;
    if (ABS(dx) < s_tw && ABS(dy) < s_th)
        return 6;
    double r        = atan2((float)dx, (float)dy) + PI;
    double deg22    = 0.3926990817f;
    int dir;
    if (r <= deg22)
        dir = ISODIR_NORTH_WEST;
    else if (r < deg22 * 3)
        dir = ISODIR_WEST;
    else if (r < deg22 * 5)
        dir = ISODIR_SOUTH_WEST;
    else if (r < deg22 * 7)
        dir = ISODIR_SOUTH;
    else if (r < deg22 * 9)
        dir = ISODIR_SOUTH_EAST;
    else if (r < deg22 * 11)
        dir = ISODIR_EAST;
    else if (r < deg22 * 13)
        dir = ISODIR_NORTH_EAST;
    else if (r < deg22 * 15)
        dir = ISODIR_NORTH;
    else
        dir = ISODIR_NORTH_WEST;

    bbuf_t *bb = core_send_msg_to_sv(CLMSG_PLAYER_MOVE_SZ);
    if (!bb)
        return 1;
    clmsg_player_move_t s;
    s.dir = (uint8)dir;
    clmsg_player_move_write(bb, &s);
    set_player_dir(e, dir);
    return 0;
}

static void
_move_in_dir(int dir)
{
    mobility_data_t *md = entity_get_mobility_data(_player_entity);
    if (md->walk_timer > md->walk_speed / 2)
        return;
    bbuf_t *bb = core_send_msg_to_sv(CLMSG_PLAYER_MOVE_SZ);
    if (!bb)
        return;
    clmsg_player_move_t s;
    s.dir = dir;
    clmsg_player_move_write(bb, &s);
}

static bool32
_check_mouse_world_interaction()
{
    return 1;
}

static int
_send_find_path_msg_to_sv(int32 x, int32 y, int8 z)
{
    byte_buf_t *bb = core_send_msg_to_sv(CLMSG_FIND_PATH_SZ);
    if (!bb)
        return 3;
    clmsg_find_path_t s;
    s.x = x;
    s.y = y;
    s.z = z;
    clmsg_find_path_write(bb, &s);
    return 0;
}

static int
_send_chat_msg_to_sv(const char *msg)
{
    if (msg[0] == '/')
    {
        int r = core_execute_slash_cmd_from_chat(msg);
        if (r <= 0)
            return r;
    }
    int msg_len = (int)strlen(msg);
    if (!msg_len)
        return 0;
    if (msg_len > MAX_CHAT_MSG_LEN)
        return 1;
    byte_buf_t *bb = core_send_msg_to_sv(CLMSG_CHAT_MSG_COMPUTE_SZ(msg_len));
    if (!bb)
        return 2;
    clmsg_chat_msg_t s;
    s.msg_len = (uint8)msg_len;
    s.msg = msg;
    clmsg_chat_msg_write(bb, &s);
    return 0;
}

static int
_get_mod_key_mask()
{
    int mods = 0;
    mods |= (keyboard_state[KEY_SCANCODE(LSHIFT)] ? HK_MOD_SHIFT : 0);
    mods |= (keyboard_state[KEY_SCANCODE(RSHIFT)] ? HK_MOD_SHIFT : 0);
    mods |= (keyboard_state[KEY_SCANCODE(LCTRL)] ? HK_MOD_CTRL : 0);
    mods |= (keyboard_state[KEY_SCANCODE(RCTRL)] ? HK_MOD_CTRL : 0);
    mods |= (keyboard_state[KEY_SCANCODE(LALT)] ? HK_MOD_ALT : 0);
    mods |= (keyboard_state[KEY_SCANCODE(RALT)] ? HK_MOD_ALT : 0);
    return mods;
}

static void
_execute_hk_action(hk_action_callback_t *a)
{
    if (a->type == HK_ACTION_CALLBACK_LUA)
    {
        lui_event_t lui_event;
        lui_event.type                      = LUI_EVENT_KEY_CALLBACK;
        lui_event.key_callback.callback_id  = a->lua.id;
        lui_post_event(&lui_event);
    } else
    if (a->type == HK_ACTION_CALLBACK_C && a->c.callback)
        a->c.callback();
}
