#include "../../shared/glad.h"
#include <math.h>
#include "core.h"
#include "render.h"
#include "gui.h"
#include "bitmap.h"
#include "assets.h"
#include "world_internal.h"
#include "../../shared/world_common.inl"

#define GLS_ACTIVE_TEX_INDEX()  (gl_state.active_texture - GL_TEXTURE0)
#define MAX_TEXTURE_UNITS       16

#if SB_USE_MAPPED_VBOS
    #define SB_BUFFER_USAGE GL_STREAM_DRAW
#else
    #define SB_BUFFER_USAGE GL_DYNAMIC_DRAW
#endif

typedef struct spritebatch_t        spritebatch_t;
typedef struct w_draw_cmd_data_t    w_draw_cmd_data_t;
typedef struct w_draw_cmd_t         w_draw_cmd_t;
typedef w_draw_cmd_t                w_draw_cmd_darr_t;

struct sb_cmd_t
{
    int     num_repeats;
    uint32  tex;
};

struct spritebatch_t
{
    int         began;
    uint32      vbo;
    uint32      ebo;
    int         vbo_offset;
    float       *mapped_vbo;
    sb_cmd_t    *cmds;
    int         max_cmds;
    int         buf_size;
    int         num_cmds;
    int         repeat_count;
    int         num_floats;
    int         max_floats; /* Saved for possible space checks */
    float       coord_space[4];
    uint32      shader;
    int         index_type;
};

struct w_draw_cmd_data_t
{
    tex_t   *tex;
    float   clip[4];
    float   x, y, z;
    float   scale[2];
    float   rot;
    uint8   flip;
    int8    tz_offset;
};

struct w_draw_cmd_t
{
    w_draw_cmd_data_t   data;
    uint32              next;
};

int R_CLEAR_COLOR_BIT = GL_COLOR_BUFFER_BIT;
int R_CLEAR_DEPTH_BIT = GL_DEPTH_BUFFER_BIT;

static SDL_GLContext    _gl_context;
static uint32           _bound_tex2ds[MAX_TEXTURE_UNITS];
static const uint8      _sb_col_white[4] = {255, 255, 255, 255};
static GLuint           _gui_shader;
static tex_t            _white_dummy_tex;
static GLuint           _gui_vbo;
static GLuint           _gui_vbo_size;
static GLuint           _gui_ebo;
static spritebatch_t    _sb;
static GLuint           _sb_opaq_shader;
static GLuint           _sb_blend_shader;

/* World draw commands. These globals are used by the world render functions
 * and they assume everything happens from a single thread, one scene at a
 * time */
static w_draw_cmd_darr_t    *_w_draw_cmds;
static uint32               *w_draw_cmd_table;
static uint32               w_draw_cmd_table_sz;

static struct gl_state_t
{
    int     active_texture;
    uint32  bound_vbo;
    uint32  bound_ebo;
    uint32  bound_program;
    int     blend_sfactor;
    int     blend_dfactor;
    bool32  blend_enabled;
    bool32  depth_test_enabled;
    bool32  scissor_enabled;
    uint32  *bound_tex2ds;
    int32   viewport[4];
    int32   scissor[4];
} gl_state;

/* TODO: allocate via alloca(), or make sure doesn't overflow */
#define MAX_TILE_SHADOW_COLORS 14
static uint8 _tile_shadow_colors[MAX_TILE_SHADOW_COLORS][4] =
{
    {  96,  96,  96, 255 },
    { 128, 128, 128, 255 },
    { 160, 160, 160, 255 },
    { 192, 192, 192, 255 },
    { 224, 224, 224, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 },
    { 224, 224, 224, 255 },
    { 192, 192, 192, 255 },
    { 160, 160, 160, 255 },
    { 128, 128, 128, 255 },
    { 96, 96, 96, 255}
};

static uint8 _tile_shadow_col_trans[MAX_TILE_SHADOW_COLORS][4] =
{
    {  96,  96,  96, 255 / 5 },
    { 128, 128, 128, 255 / 5 },
    { 160, 160, 160, 255 / 5 },
    { 192, 192, 192, 255 / 5 },
    { 224, 224, 224, 255 / 5 },
    { 255, 255, 255, 255 / 5 },
    { 255, 255, 255, 255 / 5 },
    { 255, 255, 255, 255 / 5 },
    { 224, 224, 224, 255 / 5 },
    { 192, 192, 192, 255 / 5 },
    { 160, 160, 160, 255 / 5 },
    { 128, 128, 128, 255 / 5 },
    { 96, 96, 96, 255}
};

static uint8 _editor_tile_shadow_colors[14][4] =
{
    {  70,  70,  70, 255 },
    {  80,  80,  80, 255 },
    { 100, 100, 100, 255 },
    { 120, 120, 120, 255 },
    { 140, 140, 140, 255 },
    { 160, 160, 160, 255 },
    { 180, 180, 180, 255 },
    { 200, 200, 200, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 },
    { 255, 255, 255, 255 }
};

/* The cmd_table is used as a 3D grid in to which linked lists of entity sprites
 * can be placed each frame, so that each one of the sprites is associated
 * with a single tile position. When each tile is drawn, the appopriate position
 * can be checked from the table.  */

/* Extern */
tex_t               *tile_sheet_tex;
img_t               *tile_sheet_img;
int                 tile_px_w, tile_px_h, tile_top_px_h;

static inline GLenum
_img_pxf_to_gl_enum(enum img_pxf_t pxf);

static void
_init_gl_state();

static inline void
_bind_gui_vao();

static inline void
_bind_sb_vao();

static void
_r_render_map_inner(map_t *map,
    int srt_col, int srt_row, int srt_lay,
    int end_col,   int end_row,   int end_lay, int shadow_start,
    float bx, float by, int draw_params[6], int *highlight_tile);

static void
_read_draw_param_data(bool32 *draw_normal, bool32 *draw_trans, 
    int px, int py, int pz, int pw, int ph, int pflag, int x, int y, int lay);

static int
_push_w_draw_cmd(w_draw_cmd_data_t cmd, int aw, int ah, int x, int y, int z);

static inline void
_r_render_w_draw_cmd(w_draw_cmd_t *cmd, int k);

static inline void
gls_use_program(GLuint program);

static inline void
gls_bind_vbo(GLuint vbo);

static inline void
gls_bind_ebo(GLuint ebo);

static inline void
gls_active_texture(GLenum tex);

static inline void
gls_enable_blend();

static inline void
gls_disable_blend();

static inline void
gls_blend_func(GLenum sfactor, GLenum dfactor);

static inline void
gls_enable_depth_test();

static inline void
gls_disable_depth_test();

static inline void
gls_enable_scissor();

static inline void
gls_bind_tex2d(GLuint tex);

static inline void
gls_bind_tex2d_if_not_bound(GLuint tex);

void
print_gl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        puts("GL errors found! Printing them out...");
        printf("%i\n", error);

        for (error = glGetError(); error != GL_NO_ERROR; error = glGetError())
            printf("%i\n", error);

        puts("Printed all errors!");
    }
}

GLuint
shader_from_strs(const char *vtx_code, const char *frg_code,
    char *err_log, int err_log_len)
{
    if (!vtx_code || !frg_code) return 0;

    GLuint vtx_sh = glCreateShader(GL_VERTEX_SHADER);
    GLuint frg_sh = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vtx_sh, 1, &vtx_code, 0);
    glShaderSource(frg_sh, 1, &frg_code, 0);
    glCompileShader(vtx_sh);
    glCompileShader(frg_sh);

    GLint success;

    glGetShaderiv(vtx_sh, GL_COMPILE_STATUS, &success);

    if (!success)
    {
        if (err_log) glGetShaderInfoLog(vtx_sh, err_log_len, 0, err_log);
        return 0;
    }

    glGetShaderiv(frg_sh, GL_COMPILE_STATUS, &success);

    if (!success)
    {
        if (err_log) glGetShaderInfoLog(frg_sh, err_log_len, 0, err_log);
        return 0;
    }

    GLuint prog = glCreateProgram();

    glAttachShader(prog, vtx_sh);
    glAttachShader(prog, frg_sh);

    glBindAttribLocation(prog, SHADER_ATTR_LOC_POS, "position");
    glBindAttribLocation(prog, SHADER_ATTR_LOC_UV,  "in_uv");
    glBindAttribLocation(prog, SHADER_ATTR_LOC_COL, "in_color");

    glLinkProgram(prog);
    glDeleteShader(vtx_sh);
    glDeleteShader(frg_sh);

    glGetProgramiv(prog, GL_LINK_STATUS, &success);

    if (!success)
    {
        if (err_log) glGetProgramInfoLog(prog, err_log_len, 0, err_log);
        return 0;
    }

    return prog;
}

GLuint
shader_from_files(const char *vert_path, const char *frag_path,
    char *err_log, int err_log_len)
{
    dchar *vert = load_text_file_to_dstr(vert_path);
    dchar *frag = load_text_file_to_dstr(frag_path);
    GLuint ret = shader_from_strs(vert, frag, err_log, err_log_len);
    free(vert);
    free(frag);
    return ret;
}

int
init_graphics_api(window_t *win)
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    _gl_context = SDL_GL_CreateContext(win->sdl_win);
    if (!gladLoadGL())
        return 3;
    return 0;
}

int
tex_from_mem(tex_t *tex, uint8 *pixels, int w, int h, int pxf)
{
    return tex_from_mem_ext(tex, pixels, w, h, pxf, TEX_FILTER_LINEAR,
        TEX_FILTER_NEAREST, TEX_WRAP_REPEAT);
}

int
tex_from_mem_ext(tex_t *tex, uint8 *pixels, int w, int h,
    int pxf,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping
    )
{
    if (!tex) return 1;
    GLenum min_filt  = min_filter == TEX_FILTER_LINEAR  ? GL_LINEAR  : GL_NEAREST;
    GLenum mag_filt  = mag_filter == TEX_FILTER_NEAREST ? GL_NEAREST : GL_LINEAR;
    GLenum wrap_type = 0;

    switch (wrapping)
    {
        default:
        case TEX_WRAP_REPEAT:          wrap_type = GL_REPEAT;          break;
        case TEX_WRAP_MIRROR_REPEAT:   wrap_type = GL_MIRRORED_REPEAT; break;
        case TEX_WRAP_CLAMP_TO_EDGE:   wrap_type = GL_CLAMP_TO_EDGE;   break;
        case TEX_WRAP_CLAMP_TO_BORDER: wrap_type = GL_CLAMP_TO_BORDER; break;
    };

    glGenTextures(1, &tex->id);
    glActiveTexture(GL_TEXTURE0);
    gls_bind_tex2d(tex->id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_type);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_type);

    GLenum px_format = _img_pxf_to_gl_enum(pxf);

    glTexImage2D(GL_TEXTURE_2D, 0, px_format, w, h, 0, px_format,
        GL_UNSIGNED_BYTE, pixels);
    gls_bind_tex2d(0);

    tex->w = (GLfloat)w;
    tex->h = (GLfloat)h;

    return 0;
}

int
tex_from_img(tex_t *tex, img_t *img)
{
    if (!img) return 1;

    int res = tex_from_mem(tex, img->pixels, img->w, img->h, img->pxf);
    if (res != 0) return (res + 1);

    return 0;
}

int
tex_from_img_ext(tex_t *tex, img_t *img, int min_filter, int mag_filter, int wrapping)
{
    if (!img) return 1;
    int res = tex_from_mem_ext(tex, img->pixels, img->w, img->h, img->pxf,
        min_filter, mag_filter, wrapping);
    return res ? res + 1 : 0;
}

void
tex_free(tex_t *tex)
{
    if (!tex) return;
    glDeleteTextures(1, &tex->id);
    memset(tex, 0, sizeof(tex_t));
}

int
sfont_from_ttf(sfont_t *sfont, ttf_t *ttf)
{
    return sfont_from_ttf_ext(sfont, ttf, TEX_FILTER_LINEAR, TEX_FILTER_LINEAR,
        TEX_WRAP_REPEAT);
}

int
sfont_from_ttf_ext(sfont_t *sfont, ttf_t *ttf,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping)
{
    if (!sfont || !ttf)
        return 1;

    sfont_glyph_t *glyphs = malloc(ttf->num_glyphs * sizeof(sfont_glyph_t));

    if (!glyphs)
        return 2;

    if (tex_from_mem_ext(&sfont->tex, ttf->bitmap, ttf->bitmap_w, ttf->bitmap_h,
        IMG_RGBA, min_filter, mag_filter, wrapping) != 0)
    {
        free(glyphs);
        return 3;
    }

    sfont_glyph_t *g;
    int tmp_advance, tmp_left_side_bearing;

    for (int i = 0; i < ttf->num_glyphs; ++i)
    {
        stbtt_GetGlyphHMetrics(&ttf->info, i, &tmp_advance,
            &tmp_left_side_bearing);

        g                       = &glyphs[i];
        g->clip[0]              = ttf->glyphs[i].x0;
        g->clip[1]              = ttf->glyphs[i].y0;
        g->clip[2]              = ttf->glyphs[i].x1;
        g->clip[3]              = ttf->glyphs[i].y1;
        g->advance              = ROUNDF(ttf->glyphs[i].xadvance);
        g->left_side_bearing    = (float)tmp_left_side_bearing;
        g->x_off                = ROUNDF((float)ttf->glyphs[i].xoff2);
        g->y_off                = ROUNDF((float)ttf->glyphs[i].yoff2);
    }

    sfont->glyphs       = glyphs;
    sfont->num_glyphs   = ttf->num_glyphs;
    sfont->v_advance    = ROUNDF(ttf->scale * ((float)ttf->ascent - \
        (float)ttf->descent + (float)ttf->linegap));

    return 0;
}

void
sfont_free(sfont_t *sfont)
{
    if (!sfont) return;
    free(sfont->glyphs);
    tex_free(&sfont->tex);
    memset(sfont, 0, sizeof(sfont_t));
}

int
sb_init(int max_sprites, char *err_log, int err_log_len)
{
    if (err_log) err_log[0] = 0;

    if (max_sprites <= 0)
        return 1;

    _sb.cmds = malloc(max_sprites * sizeof(sb_cmd_t));
    if (!_sb.cmds) return 2;

    /* Create the default shader */
    const char *vtx_code =
        "#version 100\n"
        "precision lowp float;"
        "attribute vec3 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "void main(void)"
        "{"
            "gl_Position    = projection * vec4(position, 1.0);"
            "uv             = in_uv;"
            "color          = in_color;"
        "}";

    const char *frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec4 color;"
        "varying vec2 uv;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "vec4 frag_color = texture2D(tex, uv) * color;"
            "if (frag_color.w == 0.0) discard;"
            "gl_FragColor = frag_color;"
        "}";

    _sb_opaq_shader = shader_from_strs(vtx_code, frg_code, err_log,
        err_log_len);

    if (!_sb_opaq_shader)
        return 3;

    /* Create the defaut blended shader */
    frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
        "   gl_FragColor = texture2D(tex, uv) * color;"
        "}";

    _sb_blend_shader = shader_from_strs(vtx_code, frg_code, err_log,
        err_log_len);

    if (!_sb_blend_shader)
        return 4;

    /* Gen buffers */
    glGenBuffers(1, &_sb.vbo);
    gls_bind_vbo(_sb.vbo);
    glBufferData(GL_ARRAY_BUFFER, max_sprites * SIZE_OF_SPRITE, 0,
        SB_BUFFER_USAGE);

#if !SB_USE_MAPPED_VBOS
    void *vbo_mem = malloc(max_sprites * SIZE_OF_SPRITE);
    if (!vbo_mem) return 5;

    _sb.mapped_vbo = (GLfloat*)vbo_mem;
#endif

    GLint loc_position  = glGetAttribLocation(_sb_opaq_shader, "position");
    if (loc_position < 0) return 6;
    GLint loc_tex_coord = glGetAttribLocation(_sb_opaq_shader, "in_uv");
    if (loc_tex_coord < 0) return 7;

    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), (const GLvoid *)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        6 * sizeof(GLfloat), (const GLvoid *)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);

    /* Element array buffer */
    glGenBuffers(1, &_sb.ebo);
    gls_bind_ebo(_sb.ebo);

    GLsizei single_index_size;

    int max_verts = max_sprites * 6;

    if (max_verts <= 256)
    {
        single_index_size = sizeof(GLubyte);
        _sb.index_type = GL_UNSIGNED_BYTE;
    } else
    if (max_verts <= 0xFFFF)
    {
        single_index_size = sizeof(GLushort);
        _sb.index_type = GL_UNSIGNED_SHORT;
    } else
    {
        single_index_size = sizeof(GLuint);
        _sb.index_type = GL_UNSIGNED_INT;
    }

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_verts * single_index_size, 0,
        SB_BUFFER_USAGE);

    void *ebo_data = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

    switch (_sb.index_type)
    {
        case GL_UNSIGNED_BYTE:
        {
            GLubyte indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLubyte i = 0; i < (GLubyte)max_sprites; ++i)
                for (GLubyte j = 0; j < 6; ++j)
                    ((GLubyte*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        case GL_UNSIGNED_SHORT:
        {
            GLushort indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLushort i = 0; i < (GLushort)max_sprites; ++i)
                for (GLushort j = 0; j < 6; ++j)
                    ((GLushort*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        case GL_UNSIGNED_INT:
        {
            GLuint indices[6] = {0, 1, 2, 2, 3, 1};

            for (GLuint i = 0; i < (GLuint)max_sprites; ++i)
                for (GLuint j = 0; j < 6; ++j)
                    ((GLuint*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        default:
            assert(0);
    }

    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

    gls_bind_ebo(0);
    gls_bind_vbo(0);

    _sb.max_cmds    = max_sprites;
    _sb.max_floats  = max_sprites * SB_NUM_FLOATS_PER_QUAD;
    _sb.buf_size    = max_sprites * SIZE_OF_SPRITE;

    return 0;
}

void
sb_destroy()
{
    glDeleteBuffers(1, &_sb.vbo);
    glDeleteBuffers(1, &_sb.ebo);
    free(_sb.cmds);
#if !SB_USE_MAPPED_VBOS
    free(_sb.mapped_vbo);
#endif
    glDeleteProgram(_sb_opaq_shader);
    glDeleteProgram(_sb_blend_shader);
    glDeleteProgram(_gui_shader);
    memset(&_sb, 0, sizeof(spritebatch_t));
}

int
sb_begin(GLuint shader, float *coord_space)
{
    assert(!_sb.began);
    _sb.began = 1;

    _sb.num_cmds         = 0;
    _sb.repeat_count     = 0;

    _sb.cmds[0].num_repeats = 0;
    _sb.num_floats          = 0;

    if (coord_space)
    {
        _sb.coord_space[0] = coord_space[0];
        _sb.coord_space[1] = coord_space[1];
        _sb.coord_space[2] = coord_space[2];
        _sb.coord_space[3] = coord_space[3];
    } else
    {
        GLint vp[4];
        glGetIntegerv(GL_VIEWPORT, vp);
        _sb.coord_space[0] = 0.f;
        _sb.coord_space[1] = 0.f;
        _sb.coord_space[2] = (GLfloat)vp[2];
        _sb.coord_space[3] = (GLfloat)vp[3];
    }

    _sb.shader = shader ? shader : _sb_opaq_shader;

#if SB_USE_MAPPED_VBOS
    gls_bind_vbo(_sb.vbo);
    _sb.mapped_vbo = (GLfloat*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
#endif

    return 0;
}

#if SB_USE_SAFE_LIMITS
#   define SB_CHECK_SPACE_FOR_ONE() \
        if (_sb.num_floats >= _sb.max_floats) \
            return;
#else
#   define SB_CHECK_SPACE_FOR_ONE()
#endif

#define SB_WRITE_SPRITE_VERTS(tx, vbo_mem, start_index, clip, x, y, z, w, h, \
    col) \
{ \
    (vbo_mem)[(start_index) +  0] = (x); \
    (vbo_mem)[(start_index) +  1] = (y); \
    (vbo_mem)[(start_index) +  2] = (z); \
    (vbo_mem)[(start_index) +  6] = (x) + (w); \
    (vbo_mem)[(start_index) +  7] = (y); \
    (vbo_mem)[(start_index) +  8] = (z); \
    (vbo_mem)[(start_index) + 12] = (x); \
    (vbo_mem)[(start_index) + 13] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 14] = (z); \
    (vbo_mem)[(start_index) + 18] = (x) + (w); \
    (vbo_mem)[(start_index) + 19] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 20] = (z); \
    (vbo_mem)[(start_index) +  3] = (clip)[0] / (tx)->w; \
    (vbo_mem)[(start_index) +  4] = (clip)[1] / (tx)->h; \
    (vbo_mem)[(start_index) +  9] = (clip)[2] / (tx)->w; \
    (vbo_mem)[(start_index) + 10] = (clip)[1] / (tx)->h; \
    (vbo_mem)[(start_index) + 15] = (clip)[0] / (tx)->w; \
    (vbo_mem)[(start_index) + 16] = (clip)[3] / (tx)->h; \
    (vbo_mem)[(start_index) + 21] = (clip)[2] / (tx)->w; \
    (vbo_mem)[(start_index) + 22] = (clip)[3] / (tx)->h; \
    int csi_ = (start_index) * 4; \
    ((GLubyte*)(vbo_mem))[csi_ + 20] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 21] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 22] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 23] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 44] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 45] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 46] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 47] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 68] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 69] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 70] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 71] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 92] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 93] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 94] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 95] = (col)[3]; \
}

#define SB_WRITE_FLIPPED_SPRITE_VERTS(tex, vbo_mem, start_index, clip, x, y, \
    z, w, h, col, flip) \
{ \
    (vbo_mem)[(start_index) +  0] = (x); \
    (vbo_mem)[(start_index) +  1] = (y); \
    (vbo_mem)[(start_index) +  2] = (z); \
    (vbo_mem)[(start_index) +  6] = (x) + (w); \
    (vbo_mem)[(start_index) +  7] = (y); \
    (vbo_mem)[(start_index) +  8] = (z); \
    (vbo_mem)[(start_index) + 12] = (x); \
    (vbo_mem)[(start_index) + 13] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 14] = (z); \
    (vbo_mem)[(start_index) + 18] = (x) + (w); \
    (vbo_mem)[(start_index) + 19] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 20] = (z); \
    switch (flip) \
    { \
        case SB_FLIP_NONE: \
            (vbo_mem)[(start_index) +  3] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) +  4] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) +  9] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 16] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[3] / (tex)->h; \
            break; \
        case SB_FLIP_H: \
            (vbo_mem)[(start_index) +  3] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) +  4] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) +  9] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) + 16] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[3] / (tex)->h; \
            break; \
        case SB_FLIP_V: \
            (vbo_mem)[(start_index) +  3] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) +  4] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) +  9] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) + 15] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 16] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[1] / (tex)->h; \
            break; \
        case SB_FLIP_BOTH: \
            (vbo_mem)[(start_index) +  3] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) +  4] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) +  9] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) + 16] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[1] / (tex)->h; \
            break; \
    } \
    int csi_ = (start_index) * 4; \
    ((GLubyte*)(vbo_mem))[csi_ + 20] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 21] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 22] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 23] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 44] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 45] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 46] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 47] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 68] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 69] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 70] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 71] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 92] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 93] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 94] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 95] = (col)[3]; \
}

#define SB_ROTATE_SPRITE(vbo_mem, first_index, angle, ox, oy) \
{ \
    float tx, ty, rx, ry; \
    for (int i = 0; i < 4; ++i) \
    { \
        tx = (vbo_mem)[(first_index) + (i * 6) + 0] - (ox); \
        ty = (vbo_mem)[(first_index) + (i * 6) + 1] - (oy); \
 \
        rx = tx * cosf((angle)) - ty * sinf((angle)); \
        ry = tx * sinf((angle)) + ty * cosf((angle)); \
 \
        (vbo_mem)[(first_index) + (i * 6) + 0] = rx + (ox); \
        (vbo_mem)[(first_index) + (i * 6) + 1] = ry + (oy); \
    } \
}

#define SB_WRITE_SPRITE_CMD(tx) \
{ \
    sb_cmd_t *cmd; \
 \
    if (_sb.num_cmds != 0) \
    { \
        cmd = &_sb.cmds[_sb.num_cmds - 1]; \
 \
        if (cmd->tex == (tx)->id) \
        { \
            ++cmd->num_repeats; \
        } else \
        { \
            cmd = &_sb.cmds[_sb.num_cmds]; \
            cmd->tex = (tx)->id; \
            cmd->num_repeats = 1; \
            ++_sb.num_cmds; \
        } \
    } else \
    { \
        cmd = &_sb.cmds[0]; \
        cmd->num_repeats = 1; \
        cmd->tex = (tx)->id; \
        ++_sb.num_cmds; \
    } \
 \
    _sb.num_floats += SB_NUM_FLOATS_PER_QUAD; \
}

void
sb_sprite(tex_t *tex, float *clip, float x, float y, float z)
{
    if (_sb.num_floats >= _sb.max_floats)
        DEBUG_PRINTF("VITTU EBIIIN\n");
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_WRITE_SPRITE_CMD(tex);
}

#define SB_SPRITE(tx, clip, x, y, z) \
    float w = clip[2] - clip[0]; \
    float h = clip[3] - clip[1]; \
    SB_WRITE_SPRITE_VERTS(tx, _sb.mapped_vbo, _sb.num_floats, clip, x, y, \
        z, w, h, _sb_col_white); \
    SB_WRITE_SPRITE_CMD(tx);
/* Inlined version of the above function */

void
sb_sprite_s(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_c(tex_t *tex, float *clip, float x, float y, float z, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_r(tex_t *tex, float *clip, float x, float y, float z,
    float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_f(tex_t *tex, float *clip, float x, float y, float z,
    enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, _sb_col_white, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_sc(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_sr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_sf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip,
        x, y, z, w, h, _sb_col_white, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_cr(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_cf(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, col, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_scr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_scf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip,
        x, y, z, w, h, col, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void
sb_sprite_scrf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float origin_x, float origin_y,
    enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, col, flip);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

#define SB_PREP_SPRITE_CMD_FOR_TEXT(cmd_ptr, tex) \
{ \
    if (_sb.num_cmds != 0) \
    { \
        cmd_ptr = &_sb.cmds[_sb.num_cmds - 1]; \
 \
        if (cmd_ptr->tex != (tex)->id) \
        { \
            (cmd_ptr) = &_sb.cmds[_sb.num_cmds]; \
            (cmd_ptr)->tex = (tex)->id; \
            (cmd_ptr)->num_repeats = 0; \
            ++_sb.num_cmds; \
        } \
    } \
    else \
    { \
        cmd_ptr = &_sb.cmds[0]; \
        cmd_ptr->tex = tex->id; \
        cmd_ptr->num_repeats = 0; \
        ++_sb.num_cmds; \
    } \
}

#define SB_WRITE_TEXT(sf, txt, x, y, z)

void
sb_text(sfont_t *sf, const char *txt, float x, float y, float z)
{
    SB_CHECK_SPACE_FOR_ONE();

    if (!txt || !txt[0] || !sfont_is_loaded(sf)) return;

    tex_t *tex = &sf->tex;
    sfont_glyph_t *g;
    float w, h, rx, ry;
    float px = x;
    float py = y;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);

    for (const char *c = txt; *c; ++c)
    {
        SB_CHECK_SPACE_FOR_ONE();

        if (*c == '\n')
        {
            px = x;
            py += sf->v_advance;
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = g->clip[2] - g->clip[0];
        h   = g->clip[3] - g->clip[1];
        rx  = px;
        ry  = (py - h + g->y_off);

        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, _sb_col_white);

        px += g->advance;
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void
sb_text_sc(sfont_t *sf, const char *txt, float x, float y, float z,
    float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();

    if (!txt || !txt[0] || !sfont_is_loaded(sf)) return;

    tex_t *tex = &sf->tex;
    sfont_glyph_t *g;
    float w, h, rx, ry;
    float px = x;
    float py = y;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);

    for (const char *c = txt; *c; ++c)
    {
        SB_CHECK_SPACE_FOR_ONE();

        if (*c == '\n')
        {
            px = x;
            py += (sf->v_advance * sy);
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = (g->clip[2] - g->clip[0]) * sx;
        h   = (g->clip[3] - g->clip[1]) * sy;
        rx  = px;
        ry  = (py - h + g->y_off);

        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, col);

        px += (g->advance * sx);
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void
sb_text_wrap(sfont_t *sf, const char *txt, float x, float y, float z, float wrap)
{
    SB_CHECK_SPACE_FOR_ONE();

    if (!txt || !txt[0] || !sfont_is_loaded(sf)) return;

    tex_t *tex = &sf->tex;
    sfont_glyph_t *g;
    float w, h, rx, ry;
    float px    = x;
    float py    = y;
    float max_w = x + wrap;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);

    for (const char *c = txt; *c; ++c)
    {
        SB_CHECK_SPACE_FOR_ONE();

        if (*c == '\n')
        {
            px = x;
            py += sf->v_advance;
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = g->clip[2] - g->clip[0];
        h   = g->clip[3] - g->clip[1];

        if (c != txt && px + g->advance > max_w )
        {
            px = x;
            py += sf->v_advance;
        }

        rx  = px;
        ry  = (py - h + g->y_off);

        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, _sb_col_white);

        px += g->advance;
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

#define SB_WRITE_WRAPPED_STRING_VERTS_S(sb, sf, txt, x, y, z, wrap, sx, sy, col) \
{ \
    sfont_glyph_t *g; \
    float w, h, rx, ry; \
    float px    = x; \
    float py    = y; \
    float max_w = x + wrap; \
    float x_adv; \
    float y_adv = sf->v_advance * sx; \
\
    for (const char *c = txt; *c; ++c) \
    { \
        SB_CHECK_SPACE_FOR_ONE(); \
\
        if (*c == '\n') \
        { \
            px = x; \
            py += y_adv; \
            continue; \
        } \
 \
        g       = SFONT_GLYPH(sf, (int)*c); \
        w       = (g->clip[2] - g->clip[0]) * sx; \
        h       = (g->clip[3] - g->clip[1]) * sy; \
        x_adv   = g->advance * sx; \
 \
        if (c != txt && px + x_adv > max_w ) \
        { \
            px = x; \
            py += y_adv; \
        } \
 \
        rx  = px; \
        ry  = (py - h + (sy * g->y_off)); \
 \
        SB_WRITE_SPRITE_VERTS(tex, sb.mapped_vbo, sb.num_floats, g->clip, rx, \
            ry, z, w, h, col); \
 \
        px += x_adv; \
        ++cmd->num_repeats; \
        sb.num_floats += SB_NUM_FLOATS_PER_QUAD; \
    } \
}


void
sb_text_wrap_s(sfont_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy)
{
    SB_CHECK_SPACE_FOR_ONE(); \

    if (!txt || !txt[0] || !sfont_is_loaded(sf)) return;

    tex_t *tex = &sf->tex;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    SB_WRITE_WRAPPED_STRING_VERTS_S(_sb, sf, txt, x, y, z, wrap, sx, sy,
        _sb_col_white);
}

void
sb_text_wrap_sc(sfont_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE(); \

    if (!txt || !txt[0] || !sfont_is_loaded(sf)) return;

    tex_t *tex = &sf->tex;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    SB_WRITE_WRAPPED_STRING_VERTS_S(_sb, sf, txt, x, y, z, wrap, sx, sy, col);
}

int
sb_end()
{
    assert(_sb.began);
    _sb.began = 0;

#if SB_USE_MAPPED_VBOS
    glUnmapBuffer(GL_ARRAY_BUFFER);
#else
    gls_bind_vbo(_sb.vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, _sb.num_floats * sizeof(GLfloat),
        _sb.mapped_vbo);
#endif

    gls_bind_ebo(_sb.ebo);
    _bind_sb_vao();

    const float *cs = _sb.coord_space;

    GLfloat projection[4][4] =
    {
        {2.0f / (cs[2] - cs[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (cs[1] - cs[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (cs[2] + cs[0]) / (cs[2] - cs[0]),
         - (cs[1] + cs[3]) / (cs[1] - cs[3]),
         0.0f, 1.0f}
    };

    if (gl_state.bound_program != _sb.shader)
        gls_use_program(_sb.shader);
    gls_active_texture(GL_TEXTURE0);

    glUniformMatrix4fv(glGetUniformLocation(_sb_opaq_shader, "projection"),
        1, GL_FALSE, &projection[0][0]);

    GLsizei index_size;

    switch (_sb.index_type)
    {
        case GL_UNSIGNED_BYTE:  index_size = sizeof(GLubyte);   break;
        case GL_UNSIGNED_SHORT: index_size = sizeof(GLushort);  break;
        case GL_UNSIGNED_INT:   index_size = sizeof(GLuint);    break;
        default: assert(0); break;
    }

    sb_cmd_t            *cmd;
    size_t              offset      = 0;
    const GLenum        index_type  = _sb.index_type;
    int                 num_cmds    = _sb.num_cmds;

    for (int i = 0; i < num_cmds; ++i)
    {
        cmd = &_sb.cmds[i];
        gls_bind_tex2d_if_not_bound(cmd->tex);
        glDrawElements(GL_TRIANGLES, cmd->num_repeats * 6, index_type,
            (const GLvoid*)offset);
        offset += cmd->num_repeats * 6 * index_size;
    }

#if SB_USE_MAPPED_VBOS
    /* Orphan the buffer */
    glBufferData(GL_ARRAY_BUFFER, _sb.max_floats * sizeof(GLfloat), 0,
        SB_BUFFER_USAGE);
#endif

    gls_bind_vbo(0);
    gls_bind_ebo(0);
    return 0;
}

int
r_init(window_t *win)
{
    char        err_log[512];
    const int   err_log_len = 512;

    if (init_graphics_api(win) != 0)
        return 1;

    _init_gl_state();

    if (sb_init(game_cfg.sprite_batch_size, err_log, err_log_len) != 0)
    {
        puts(err_log);
        return 4;
    }

    /* Init the gui rendering stuff */
    {
    const char *vtx_code =
        "#version 100\n"
        "precision lowp float;"
        "attribute vec2 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "void main(void)"
        "{"
            "gl_Position = projection * vec4(position, 1.0, 1.0);"
            "uv = in_uv;"
            "color = in_color;"
        "}";

    const char *frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec4 color;"
        "varying vec2 uv;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "gl_FragColor = texture2D(tex, uv) * color;"
        "}";

    _gui_shader = shader_from_strs(vtx_code, frg_code, 0, 0);

    if (!_gui_shader)
        return 5;
    }

    /* Create the while dummy texture */
    uint8 white_tex_px[16];

    for (int i = 0; i < 16; ++i)
        white_tex_px[i] = 255;

    if (tex_from_mem(&_white_dummy_tex, white_tex_px, 2, 2, IMG_RGBA))
        return 6;

    /* Create the GUI rendering buffers */
    const uint num_quads = 16384; /* TODO: fix the hardcoded value */
    DEBUG_PRINTF("%s: gui num quads is hardcoded FIXME\n", __func__);
    const uint num_verts = num_quads * 6;
    _gui_vbo_size = num_verts * GUI_VERT_SIZE;

    glGenBuffers(1, &_gui_vbo);
    glGenBuffers(1, &_gui_ebo);

    /* VBO */
    gls_bind_vbo(_gui_vbo);
    glBufferData(GL_ARRAY_BUFFER, _gui_vbo_size, 0, GL_DYNAMIC_DRAW);

    _bind_gui_vao();

    /* EBO */
    gls_bind_ebo(_gui_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_verts / 4 * 6 * sizeof(GLushort),
        0, GL_STREAM_DRAW);
    GLushort *ebo_data = (GLushort*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER,
        GL_WRITE_ONLY);

    GLushort indices[6] = {0, 1, 2, 2, 3, 1};
    for (GLushort i = 0; i < num_quads; ++i)
        for (GLushort j = 0; j < 6; ++j)
            ebo_data[i * 6 + j] = indices[j] + i * 4;

    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    gls_bind_ebo(0);
    gls_bind_vbo(0);

    /* Allocate some space for world (entity) sprites */
    darr_reserve(_w_draw_cmds, 256);

    r_set_vsync(1);
    return 0;
}

int
r_destroy()
{
    sb_destroy();
    tex_free(&_white_dummy_tex);
    glDeleteBuffers(1, &_gui_vbo);
    glDeleteBuffers(1, &_gui_ebo);
    darr_free(_w_draw_cmds);
    free(w_draw_cmd_table);
    SDL_GL_DeleteContext(_gl_context);
    return 0;
}

void
r_render_gui(gui_draw_list_t *lists, int num_lists)
{
    gui_draw_cmd_t  *cmd;
    gui_draw_list_t *list;

    if (gl_state.depth_test_enabled)
        gls_disable_depth_test();

    int prev_vp[4], prev_scis[4];
    r_get_viewport(&prev_vp[0], &prev_vp[1], &prev_vp[2], &prev_vp[3]);
    r_get_scissor(&prev_scis[0], &prev_scis[1], &prev_scis[2], &prev_scis[3]);

    int tar_vp[4];
    core_compute_target_viewport(tar_vp);
    r_viewport(tar_vp[0], tar_vp[1], tar_vp[2], tar_vp[3]);
    r_scissor(tar_vp[0], tar_vp[1], tar_vp[2], tar_vp[3]);

    gls_use_program(_gui_shader);

    GLfloat cs[4] = {0.f, 0.f, (float)RESOLUTION_W, (float)RESOLUTION_H};

    GLfloat projection[4][4] =
    {
        {2.0f / (cs[2] - cs[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (cs[1] - cs[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (cs[2] + cs[0]) / (cs[2] - cs[0]),
         - (cs[1] + cs[3]) / (cs[1] - cs[3]),
         0.0f, 1.0f}
    };

    glUniformMatrix4fv(glGetUniformLocation(_gui_shader, "projection"),
        1, GL_FALSE, &projection[0][0]);

    gls_bind_vbo(_gui_vbo);
    gls_bind_ebo(_gui_ebo);
    _bind_gui_vao();

    gls_active_texture(GL_TEXTURE0);
    gls_bind_tex2d(_white_dummy_tex.id);
    GLuint next_tex;

    int     i, j, k, num_cmds;
    size_t  offset;
    GLint   num_elements;
    int     vp[4];
    int     last_raw_scissor[4] = {0};
    float   sc                  = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.00000001f);
    for (i = 0; i < num_lists; ++i)
    {
        /* TODO: resize buf if necessary */
        list        = &lists[i];
        offset      = 0;
        num_cmds    = list->num_cmds;
        glBufferSubData(GL_ARRAY_BUFFER, 0,
            list->num_verts * GUI_VERT_SIZE, list->verts);

        for (j = 0; j < num_cmds; ++j)
        {
            cmd = &list->cmds[j];
            for (k = 0; k < 4; ++k)
            {
                if (cmd->scissor[k] == last_raw_scissor[k])
                    continue;
                memcpy(last_raw_scissor, cmd->scissor, 4 * sizeof(int));
                r_scissor((int)((float)last_raw_scissor[0] * sc) + vp[0],
                    (int)((float)last_raw_scissor[1] * sc) + vp[1],
                    (int)((float)last_raw_scissor[2] * sc),
                    (int)((float)last_raw_scissor[3] * sc));
                break;
            }
            next_tex = cmd->tex ? cmd->tex : _white_dummy_tex.id;
            num_elements = cmd->num_verts / 4 * 6;
            gls_bind_tex2d_if_not_bound(next_tex);
            glDrawElements(GL_TRIANGLES, num_elements, GL_UNSIGNED_SHORT,
                (const GLvoid*)(offset));
            offset += num_elements * sizeof(GLushort);
        }
    }
    glBufferData(GL_ARRAY_BUFFER, _gui_vbo_size, 0, GL_DYNAMIC_DRAW);
    gls_bind_ebo(0);
    gls_bind_vbo(0);
    gls_use_program(0);
    r_viewport(prev_vp[0], prev_vp[1], prev_vp[2], prev_vp[3]);
    r_scissor(prev_scis[0], prev_scis[1], prev_scis[2], prev_scis[3]);
}

int
r_set_tile_props(tex_t *tex, img_t *img, int tile_w, int tile_h, int tile_top_h)
{
	int ret = 0;
    tile_sheet_tex  = tex;
    tile_sheet_img  = img;
    tile_px_w       = tile_w;
    tile_px_h       = tile_h;
    tile_top_px_h   = tile_top_h;
    return ret;
}

static void
_compute_map_render_cs_and_vp(int *screen_area, int *coord_space,
    float *ret_cs, int *ret_vp, int *ret_prev_vp)
{
    for (int i = 0; i < 4; ++i)
        ret_prev_vp[i] = (int)gl_state.viewport[i];

    if (coord_space)
    {
        for (int i = 0; i < 4; ++i)
            ret_cs[i] = (float)coord_space[i];
    } else
    if (screen_area)
    {
        ret_cs[0] = 0.f;
        ret_cs[1] = 0.f;
        ret_cs[2] = (float)screen_area[2];
        ret_cs[3] = (float)screen_area[3];
    } else
    {
        ret_cs[0] = 0.f;
        ret_cs[1] = 0.f;
        ret_cs[2] = (float)RESOLUTION_W;
        ret_cs[3] = (float)RESOLUTION_H;
    }

    if (screen_area)
    {
        for (int i = 0; i < 4; ++i)
            ret_vp[i] = screen_area[i];
    } else
    {
        ret_vp[0] = 0;
        ret_vp[1] = 0;
        ret_vp[2] = main_window.w;
        ret_vp[3] = main_window.h;
    }
}

static void
_r_render_map_inner(map_t *map,
    int srt_col, int srt_row, int srt_lay,
    int end_col, int end_row, int end_lay,
    int shadow_start, float bx, float by, 
    int draw_params[6], int *highlight_tile)
{
    float   tx          = 0.f;
    float   ty          = 0.f;
    int     area_w      = end_col - srt_col;
    int     area_h      = end_row - srt_row;
    int     f_srt_col   = MAX(srt_col, 0);
    int     f_srt_row   = MAX(srt_row, 0);
    int     f_srt_lay   = MAX(srt_lay, 0);

    tile_t          tile;
    chunk_cache_t   *cache;
    w_draw_cmd_t    *cmd;

    int             lay, i, j, k, cmd_index;
    int             dy, dx, temp, x, y;
    uint32          cmd_rptr;

    bool32 use_p   =  draw_params == 0 ? 0 : 1;
    int px = 0, py = 0, pz = 0, pw = 0, ph = 0;
    int pflag = 0;

    if (use_p)
    {
        px    = draw_params[0];
        py    = draw_params[1];
        pz    = draw_params[2];
        pw    = draw_params[3];
        ph    = draw_params[4];
        pflag = draw_params[5];
    }

    if (highlight_tile != 0 &&
        highlight_tile[0] >= srt_col    &&
        highlight_tile[0] <  end_col    &&
        highlight_tile[1] >= srt_row    &&
        highlight_tile[1] <  end_row    &&
        highlight_tile[2] >= srt_lay    &&
        highlight_tile[2] <  end_lay)
    {
        int rtx = highlight_tile[0] - srt_col;
        int rty = highlight_tile[1] - srt_row;
        int rtz = highlight_tile[2] - srt_lay - 1;

        w_draw_cmd_data_t cmd;
        cmd.tex     = tile_sheet_tex;
        cmd.clip[0] = tile_gfx_defs[1].clip[0];
        cmd.clip[1] = tile_gfx_defs[1].clip[1];
        cmd.clip[2] = tile_gfx_defs[1].clip[2];
        cmd.clip[3] = tile_gfx_defs[1].clip[3];

        float ox = bx + tile_gfx_defs[1].ox;
        float oy = by + tile_gfx_defs[1].oy;
        cmd.x           = R_COMPUTE_ISO_OBJ_PX_X(rtx, rty, ox);
        cmd.y           = R_COMPUTE_ISO_OBJ_PX_Y(rtx, rty, rtz, oy);
        cmd.z           = 0.f;
        cmd.scale[0]    = 1.f;
        cmd.scale[1]    = 1.f;
        cmd.rot         = 0.f;
        cmd.flip        = 0;
        cmd.tz_offset   = 0;

        _push_w_draw_cmd(cmd, end_col - srt_col, end_row - srt_row, rtx, rty,
            rtz + 1);
    }

    for (temp = 0; temp < area_w + area_h; ++temp)
    {
        for (dy = temp, dx = 0; dy >= 0; --dy, ++dx)
        {
            if (!(dy < area_w && dx < area_h))
                continue;

            y = dy + f_srt_col;
            x = dx + f_srt_row;
            i = y - srt_col;
            j = x - srt_row;

            if (y < f_srt_col || y >= end_col) continue;
            if (x < f_srt_row || x >= end_row) continue;

            cache = GET_CACHE_OF_TILE_FAST(map, y, x);

            for (lay = f_srt_lay; lay < end_lay; ++lay)
            {
                k = lay - srt_lay;

                tile = GET_CACHE_TILE_FAST(cache, y, x, lay);

                tx = bx + (float)(i * tile_px_w / 2) - \
                    (float)(j * tile_px_w / 2) +
                    tile_gfx_defs[tile].ox;
                ty = by + (float)(i * tile_px_w / 4) + \
                    (float)(j * tile_px_h / 4) - \
                    (float)(k * tile_px_h / 2) + \
                    tile_gfx_defs[tile].oy;

                if (tile)
                {
                    bool32 draw_normal = 1;
                    bool32 draw_trans  = 0;

                    /* Is the tile within the defined box of undrawn
                     * tiles? */
                    if (use_p)
                        _read_draw_param_data(&draw_normal, &draw_trans,
                            px, py, pz, pw, ph, pflag, x, y, lay);

                    int shadow_index = MAX_TILE_SHADOW_COLORS - 1 - k;

                    if (draw_normal)
                        sb_sprite_c(tile_sheet_tex,
                        tile_gfx_defs[tile].clip, tx, ty, 0.f,
                        _tile_shadow_colors[shadow_index]);
                    else if (!pflag || draw_trans)
                        sb_sprite_c(tile_sheet_tex,
                        tile_gfx_defs[tile].clip, tx, ty, 0.f,
                        _tile_shadow_col_trans[shadow_index]);
                }

                cmd_index = k * (area_w * area_h) + j * area_w + i;
                for (cmd_rptr = w_draw_cmd_table[cmd_index];
                     cmd_rptr != 0xFFFFFFFF;
                     cmd_rptr = cmd->next)
                {
                    cmd = &_w_draw_cmds[cmd_rptr];
                    _r_render_w_draw_cmd(cmd, k);
                }
            }
        }
    }
}

static int
_fill_w_draw_cmd_table(world_t *world,
    int srt_col, int srt_row, int srt_lay,
    int end_col, int end_row, int end_lay,
    float bx, float by, int *vp, float *cs)
{
    int area_w = end_col - srt_col;
    int area_h = end_row - srt_row;
    int area_t = end_lay - srt_lay;

    /* Make sure the table has enough space */
    uint32 req_tsz = area_w * area_h * area_t;
    if (w_draw_cmd_table_sz < req_tsz)
    {
        uint32 *t = realloc(w_draw_cmd_table, req_tsz * sizeof(uint32));
        if (!t) return 2;
        w_draw_cmd_table       = t;
        w_draw_cmd_table_sz    = req_tsz;
    }

    /* All bits on means no command */
    memset(w_draw_cmd_table, 0xFF, w_draw_cmd_table_sz * sizeof(uint32));
    darr_clear(_w_draw_cmds);

    /* Push entities to the draw cmd list */
    uint32 num_cmps = darr_num(world->cmp_renderable_pool);
    cmp_renderable_pool_item_t  *cmps = world->cmp_renderable_pool;
    cmp_renderable_t            *cmp;

    for (uint32 i = 0; i < num_cmps; ++i)
    {
        cmp = &cmps[i].cmp;
        if (!cmp->tex)          continue;
        if (cmp->tx <  srt_col) continue;
        if (cmp->tx >= end_col) continue;
        if (cmp->ty <  srt_row) continue;
        if (cmp->ty >= end_row) continue;
        if (cmp->tz <  srt_lay) continue;
        if (cmp->tz >= end_lay) continue;

        w_draw_cmd_data_t cmd;
        cmd.tex         = cmp->tex;
        for (int j = 0; j < 4; ++j)
            cmd.clip[j] = (float)cmp->clip[j];
        cmd.x           = cmp->x;
        cmd.y           = cmp->y;
        cmd.z           = 0.f;
        cmd.scale[0]    = cmp->scale[0];
        cmd.scale[1]    = cmp->scale[1];
        cmd.rot         = cmp->rot;
        cmd.flip        = cmp->flip;
        cmd.tz_offset   = 0;

        /* If the direction of travel is upwards, draw order must be changed */
        int fx = cmp->tx - srt_col;
        int fy = cmp->ty - srt_row;
        int fz = cmp->tz - srt_lay;
        switch (cmp->travel_dir)
        {
            case ISODIR_NORTH:      fy += 1;            break;
            case ISODIR_NORTH_WEST: fx += 1; fy += 1;   break;
            case ISODIR_WEST:       fx += 1;            break;
            case ISODIR_NORTH_EAST:
            {
                fy += 1;
                fz = MAX(fz - 1, 0);
                cmd.tz_offset = 1;
            }
                break;
        }
        _push_w_draw_cmd(cmd, area_w, area_h, fx, fy, fz);
    }
    return 0;
}

int
r_render_editor_world(world_t *world, world_render_props_t *rp,
    bool32 grid_toggle, int *selection_loc, int selected_tiletype,
    int vert_render_height)
{
    if (!world) return 1;

    map_t *map = &world->map;

    int     prev_scis[4];
    int     prev_vp[4];
    float   cs[4];
    int     sa[4];

    _compute_map_render_cs_and_vp(rp->screen_area, rp->coord_space, cs, sa,
        prev_vp);

    r_get_scissor(&prev_scis[0], &prev_scis[1], &prev_scis[2], &prev_scis[3]);
    r_viewport(sa[0], sa[1], sa[2], sa[3]);
    r_scissor(rp->scissor[0], rp->scissor[1], rp->scissor[2], rp->scissor[3]);
    r_color(0.2f, 0.3f, 0.8f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    if (gl_state.depth_test_enabled)
        gls_disable_depth_test();

    int num_cols = MIN(rp->tw, MAP_CHUNK_W);
    int num_rows = MIN(rp->th, MAP_CHUNK_W);
    int num_lays = MIN(rp->tt + vert_render_height, MAP_CHUNK_T);

    int srt_col = rp->tx;
    int srt_row = rp->ty;
    int srt_lay = rp->tz;

    int end_col = CLAMP(rp->tx + num_cols, srt_col, MAP_TILED_W(map));
    int end_row = CLAMP(rp->ty + num_rows, srt_row, MAP_TILED_H(map));
    int end_lay = CLAMP(rp->tz + num_lays, srt_lay, MAP_CHUNK_T);

    int f_srt_col   = MAX(srt_col, 0);
    int f_srt_row   = MAX(srt_row, 0);
    int f_srt_lay   = MAX(srt_lay, 0);

    _fill_w_draw_cmd_table(world,
        srt_col, srt_row, srt_lay,
        end_col, end_row, end_lay,
        rp->bx, rp->by, sa, cs);

    if (selection_loc != 0)
    {
        int rtx = selection_loc[0] - srt_col;
        int rty = selection_loc[1] - srt_row;
        int rtz = selection_loc[2] - srt_lay;

        w_draw_cmd_data_t cmd;
        cmd.tex     = tile_sheet_tex;
        cmd.clip[0] = tile_gfx_defs[selected_tiletype].clip[0];
        cmd.clip[1] = tile_gfx_defs[selected_tiletype].clip[1];
        cmd.clip[2] = tile_gfx_defs[selected_tiletype].clip[2];
        cmd.clip[3] = tile_gfx_defs[selected_tiletype].clip[3];
        cmd.x = rp->bx + (float)(rtx * tile_px_w / 2) - (float)(rty *
            tile_px_w / 2) + tile_gfx_defs[selected_tiletype].ox;
        cmd.y = rp->by + (float)(rtx * tile_px_h / 4) + (float)(rty *
            tile_px_h / 4) - (rtz  * tile_px_w / 2) +
            tile_gfx_defs[selected_tiletype].oy;
        cmd.scale[0]    = 1.f;
        cmd.scale[1]    = 1.f;
        cmd.z           = 0.f;
        cmd.rot         = 0.f;
        cmd.flip        = 0;
        cmd.tz_offset   = -1;

        _push_w_draw_cmd(cmd, end_col - srt_col, end_row - srt_row, rtx, rty,
            rtz);
    }

    sb_begin(0, cs);

    float tx        = 0.f;
    float ty        = 0.f;

    int final_area_w = end_col - srt_col;
    int final_area_h = end_row - srt_row;

    tile_t          tile;
    chunk_cache_t   *cache;
    w_draw_cmd_t    *cmd;

    int     lay, i, j, k, cmd_index;
    int     dy, dx, temp, x, y;
    uint32  cmd_rptr;

    for (temp = 0; temp < final_area_w + final_area_h; ++temp)
    {
        for (dy = temp, dx = 0; dy >= 0; --dy, ++dx)
        {
            if (dy < final_area_w && dx < final_area_h)
            {
                y = dy + f_srt_col;
                x = dx + f_srt_row;
                i = y - srt_col;
                j = x - srt_row;

                if (y < f_srt_col || y >= end_col) continue;
                if (x < f_srt_row || x >= end_row) continue;

                cache = GET_CACHE_OF_TILE_FAST(map, y, x);

                for (lay = f_srt_lay; lay < end_lay; ++lay)
                {
                    k       = lay - srt_lay;
                    tile    = GET_CACHE_TILE_FAST(cache, y, x, lay);

                    tx = rp->bx + (float)(i * tile_px_w / 2) - \
                        (float)(j * tile_px_w / 2) +
                        tile_gfx_defs[tile].ox;
                    ty = rp->by + (float)(i * tile_px_w / 4) + \
                        (float)(j * tile_px_h / 4) - \
                        (float)(k * tile_px_h / 2) + \
                        tile_gfx_defs[tile].oy;

                    if (tile != 0)
                    {
                        sb_sprite_c(tile_sheet_tex, tile_gfx_defs[tile].clip,
                            tx, ty, 0.f, _editor_tile_shadow_colors[CLAMP(k + 1, 0, 13)]);
                    } else
                    if (grid_toggle)
                    {
                        sb_sprite(tile_sheet_tex, tile_gfx_defs[0].clip,
                            tx, ty, 0.f);
                    }

                    cmd_index = k * (final_area_w * final_area_h) +
                        j * final_area_w + i;

                    /* Render non-tile objects on top of this tile */
                    for (cmd_rptr = w_draw_cmd_table[cmd_index];
                         cmd_rptr != 0xFFFFFFFF;
                         cmd_rptr = cmd->next)
                    {
                        cmd = &_w_draw_cmds[cmd_rptr];
                        _r_render_w_draw_cmd(cmd, k);
                    }
                }
            }
        }
    }
    sb_end();
    r_viewport(prev_vp[0], prev_vp[1], prev_vp[2], prev_vp[3]);
    r_scissor(prev_scis[0], prev_scis[1] , prev_scis[2], prev_scis[3]);
    return 0;
}


int
r_render_world(world_t *world, world_render_props_t *rp, int draw_params[6], 
    int *highlight_tile)
{
    if (!world) return 1;

    map_t *map = &world->map;

    int     prev_scis[4];
    int     prev_vp[4];
    float   cs[4];
    int     sa[4];

    _compute_map_render_cs_and_vp(rp->screen_area, rp->coord_space, cs, sa, prev_vp);

    r_get_scissor(&prev_scis[0], &prev_scis[1], &prev_scis[2], &prev_scis[3]);
    r_viewport(sa[0], sa[1], sa[2], sa[3]);
    r_scissor(rp->scissor[0], rp->scissor[1], rp->scissor[2], rp->scissor[3]);
    r_color(0.2f, 0.3f, 0.8f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    if (gl_state.depth_test_enabled)
        gls_disable_depth_test();

    int num_cols = MIN(rp->tw, MAP_CHUNK_W);
    int num_rows = MIN(rp->th, MAP_CHUNK_W);
    int num_lays = MIN(rp->tt, MAP_CHUNK_T);

    int srt_col = rp->tx;//, 0, MAP_TILED_W(map) - lower_col);
    int srt_row = rp->ty;//, 0, MAP_TILED_W(map) - lower_row);
    int srt_lay = rp->tz;//, 0, MAP_CHUNK_T - num_lays);
    int end_col = CLAMP(rp->tx + num_cols, srt_col, MAP_TILED_W(map));
    int end_row = CLAMP(rp->ty + num_rows, srt_row, MAP_TILED_H(map));
    int end_lay = CLAMP(rp->tz + num_lays, srt_lay, MAP_CHUNK_T);

    _fill_w_draw_cmd_table(world,
        srt_col, srt_row, srt_lay,
        end_col,   end_row,   end_lay,
        rp->bx, rp->by, sa, cs);

    sb_begin(_sb_blend_shader, cs);

    /* Render the map */

    _r_render_map_inner(map, srt_col, srt_row, srt_lay, end_col, end_row,
        end_lay, rp->st, rp->bx, rp->by, draw_params, highlight_tile);

    sb_end();

    r_viewport(prev_vp[0], prev_vp[1], prev_vp[2], prev_vp[3]);
    r_scissor(prev_scis[0], prev_scis[1] , prev_scis[2], prev_scis[3]);
    return 0;
}

void
r_compute_tiled_pos_from_px_pos(world_render_props_t *rp, int px_x, int px_y,
    int *ret_x, int *ret_y)
{
    float scale_x = (float)rp->screen_area[2] / (float)rp->coord_space[2];
    float scale_y = (float)rp->screen_area[3] / (float)rp->coord_space[3];

    float s_tile_px_w = (scale_x * (float)tile_px_w);
    float s_tile_px_h = (scale_y * (float)tile_px_h);

    float t_px_hw = s_tile_px_w / 2;
    float t_px_hh = s_tile_px_h / 2;

    float offset_x = scale_x * rp->bx + (float)rp->screen_area[0];
    float offset_y = scale_y * rp->by + (float)rp->screen_area[1];

    int sx = (int)(px_x - offset_x - t_px_hw);
    int sy = (int)(px_y - (int)offset_y + t_px_hh);

    int untrans_x = (int)(((s_tile_px_w * sy) + (t_px_hh * sx)) /
        (s_tile_px_w * s_tile_px_h / 2));
    int untrans_y = (int)(((s_tile_px_w * sy) - (t_px_hh * sx)) /
        (s_tile_px_w * s_tile_px_h / 2));

    *ret_x = untrans_x + rp->tx + rp->tt - 2;
    *ret_y = untrans_y + rp->ty + rp->tt - 2;
}

bool32
r_compute_px_pos_from_tiled_pos(world_render_props_t *rp,
    int tx, int ty, int tz, int *ret_x, int *ret_y)
{
    float sx = (float)rp->screen_area[2] / (float)rp->coord_space[2];
    float sy = (float)rp->screen_area[3] / (float)rp->coord_space[3];

    float s_tile_px_w     = sx * tile_px_w;
    float s_tile_px_h     = sy * tile_px_h;

    int i = tx - rp->tx;
    int j = ty - rp->ty;
    int k = tz - rp->tz;

    *ret_x = (int)(sy * rp->bx + (float)rp->screen_area[0] +
        (i * s_tile_px_w / 2 - \
        j * s_tile_px_h  / 2));

    *ret_y = (int)(sx * rp->by + (float)rp->screen_area[1] + \
        (i * s_tile_px_w / 4 + \
        j * s_tile_px_h  / 4 - \
        k * s_tile_px_h / 2));

    return tx >= rp->tx && tx < rp->tx + rp->tw
        && ty >= rp->ty && ty < rp->ty + rp->th
        && tz >= rp->tz && tz < rp->tz + rp->tt;
}

void
r_color(float r, float g, float b, float a)
    {glClearColor(r, g, b, a);}

void
r_clear(int mask)
    {glClear(mask);}

void
r_scissor(int x, int y, int w, int h)
{
    glScissor(x, main_window.h - h - y, w, h);
    gl_state.scissor[0] = x;
    gl_state.scissor[1] = y;
    gl_state.scissor[2] = w;
    gl_state.scissor[3] = h;
}

void
r_get_scissor(int *x, int *y, int *w, int *h)
{
    *x = gl_state.scissor[0];
    *y = gl_state.scissor[1];
    *w = gl_state.scissor[2];
    *h = gl_state.scissor[3];
}

void
r_viewport(int x, int y, int w, int h)
{
    glViewport(x, main_window.h - h - y, w, h);
    gl_state.viewport[0] = x;
    gl_state.viewport[1] = y;
    gl_state.viewport[2] = w;
    gl_state.viewport[3] = h;
}

void
r_get_viewport(int *x, int *y, int *w, int *h)
{
    *x = gl_state.viewport[0];
    *y = gl_state.viewport[1];
    *w = gl_state.viewport[2];
    *h = gl_state.viewport[3];
}

void
r_set_vsync(bool32 opt)
    {SDL_GL_SetSwapInterval(opt);}

bool32
r_get_vsync()
    {return SDL_GL_GetSwapInterval();}

void
r_swap_buffers(window_t *win)
    {SDL_GL_SwapWindow(win->sdl_win);}

static inline GLenum
_img_pxf_to_gl_enum(enum img_pxf_t pxf)
{
    switch (pxf)
    {
        case IMG_RGB:   return GL_RGB;
        case IMG_RGBA:  return GL_RGBA;
        case IMG_ALPHA: return GL_ALPHA;
        default: muta_assert(0); return 0;
    }
}

static void
_init_gl_state()
{
    gl_state.bound_tex2ds = _bound_tex2ds;
    gls_active_texture(GL_TEXTURE0);
    gls_enable_blend();
    gls_blend_func(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    gls_disable_depth_test();
    gls_enable_scissor();
    glGetIntegerv(GL_VIEWPORT, gl_state.viewport);
    glViewport(0, 0, main_window.w, main_window.h);
    glScissor(0, 0, main_window.w, main_window.h);
    gl_state.viewport[0]    = 0;
    gl_state.viewport[1]    = 0;
    gl_state.viewport[2]    = main_window.w;
    gl_state.viewport[3]    = main_window.h;
    gl_state.scissor[0]     = 0;
    gl_state.scissor[1]     = 0;
    gl_state.scissor[2]     = main_window.w;
    gl_state.scissor[3]     = main_window.h;
}

static inline void
_bind_gui_vao()
{
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 2, GL_FLOAT, GL_FALSE,
        5 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        5 * sizeof(GLfloat), (GLvoid*)(4 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}

static inline void
_bind_sb_vao()
{
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), (const GLvoid *)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        6 * sizeof(GLfloat), (const GLvoid *)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}

static void
_read_draw_param_data(bool32 *draw_normal, bool32 *draw_trans, 
    int px, int py, int pz, int pw, int ph, int pflag, int x, int y, int lay)
{
    *draw_normal = 1;
    *draw_trans  = 0;

    if (pflag == 0)
    {
        if (pz < lay && x >= py && y >= px)
            if (ABS(px - y) <= pw)
                if (ABS(py - x) <= ph)
                    *draw_normal = 0;
    }
    else if (pflag == 1)
    {
        if (x >= py && y >= px)
        {
            if (pz - 1 < lay)
                if (ABS(px - y) <= 4)
                    if (ABS(py - x) <= 4)
                    {
                        *draw_normal = 0;
                        *draw_trans = 1;
                    }
        }
        if (pz < lay)
        {
            if (ABS(px - y) <= pw)
                if (ABS(py - x) <= ph)
                {
                    *draw_normal = 0;
                    *draw_trans = 0;
                }
        }
    }
}

static int
_push_w_draw_cmd(w_draw_cmd_data_t d, int aw, int ah, int x, int y, int z)
{
    if (!tex_is_loaded(d.tex)) return 0;
    int i = z * (aw * ah) + y * aw + x;
    if (i < 0 || (uint)i >= w_draw_cmd_table_sz) return 1;
    uint32 num = darr_num(_w_draw_cmds);
    w_draw_cmd_t *cmd = darr_push_empty(_w_draw_cmds);
    if (!cmd)
        return 2;
    cmd->data           = d;
    cmd->next           = w_draw_cmd_table[i];
    w_draw_cmd_table[i] = num;
    return 0;
}

static inline void
_r_render_w_draw_cmd(w_draw_cmd_t *cmd, int k)
{
    int ak = k + cmd->data.tz_offset + 1;
    if (cmd->data.rot == 0.f)
    {
        sb_sprite_scf(cmd->data.tex, cmd->data.clip,
            cmd->data.x, cmd->data.y, cmd->data.z,
            cmd->data.scale[0], cmd->data.scale[1],
            _editor_tile_shadow_colors[CLAMP(ak, 0, 13)],
            cmd->data.flip);
    } else
    {
        sb_sprite_scrf(cmd->data.tex, cmd->data.clip,
            cmd->data.x, cmd->data.y, cmd->data.z,
            cmd->data.scale[0], cmd->data.scale[1],
            _editor_tile_shadow_colors[ CLAMP(ak, 0, 13)],
            cmd->data.rot, 0.f, 0.f, cmd->data.flip);
    }
}

static inline void
gls_use_program(GLuint program)
{
    glUseProgram(program);
    gl_state.bound_program = program;
}

static inline void
gls_bind_vbo(GLuint vbo)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    gl_state.bound_vbo = vbo;
}

static inline void
gls_bind_ebo(GLuint ebo)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    gl_state.bound_ebo = ebo;
}

static inline void
gls_bind_tex2d(GLuint tex)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    gl_state.bound_tex2ds[GLS_ACTIVE_TEX_INDEX()] = tex;
}

static inline void
gls_bind_tex2d_if_not_bound(GLuint tex)
{
    if (gl_state.bound_tex2ds[GLS_ACTIVE_TEX_INDEX()] == tex) return;
    gls_bind_tex2d(tex);
}

static inline void
gls_active_texture(GLenum tex)
{
    glActiveTexture(tex);
    gl_state.active_texture = tex;
}

static inline void
gls_enable_blend()
{
    glEnable(GL_BLEND);
    gl_state.blend_enabled = 1;
}

static inline void
gls_disable_blend()
{
    glDisable(GL_BLEND);
    gl_state.blend_enabled = 0;
}

static inline void
gls_blend_func(GLenum sfactor, GLenum dfactor)
{
    glBlendFunc(sfactor, dfactor);
    gl_state.blend_sfactor = sfactor;
    gl_state.blend_dfactor = dfactor;
}

static inline void
gls_enable_depth_test()
{
    glEnable(GL_DEPTH_TEST);
    gl_state.depth_test_enabled = 1;
}

static inline void
gls_disable_depth_test()
{
    glDisable(GL_DEPTH_TEST);
    gl_state.depth_test_enabled = 0;
}

static inline void
gls_enable_scissor()
{
    glEnable(GL_SCISSOR_TEST);
    gl_state.scissor_enabled = 1;
}

static inline void
gls_disable_scissor()
{
    glDisable(GL_SCISSOR_TEST);
    gl_state.scissor_enabled = 0;
}


// Ye olde tile rendering (client)
/*
for (col = f_srt_col; col < end_col; ++col)
{
    i = col - srt_col;

    for (row = f_srt_row; row < end_row; ++row)
    {
        j = row - srt_row;

        cache = GET_CACHE_OF_TILE_FAST(map, col, row);

        for (lay = f_srt_lay; lay < end_lay; ++lay)
        {
            ++penis;
            k = lay - srt_lay;

            tile = GET_CACHE_TILE_FAST(cache, col, row, lay);

            if (tile != 0)
            {
                tx = bx + (float)(i * tile_px_w / 2) - \
                    (float)(j * tile_px_w / 2) +
                    tile_gfx_defs[tile].offset_x;
                ty = by + (float)(i * tile_px_w / 4) + \
                    (float)(j * tile_px_h / 4) - \
                    (float)(k * tile_px_h / 2) + \
                    tile_gfx_defs[tile].offset_y;

                sb_sprite_c(&tile_sheet_tex, tile_gfx_defs[tile].clip,
                    tx, ty, 0.f,
                    _tile_shadow_colors[MAX_TILE_SHADOW_COLORS - 1 - k]);
            }

            cmd_index = k * (area_w * area_h) + j * area_w + i;
            for (cmd = w_draw_cmd_table[cmd_index];
                cmd;
                cmd = cmd->next)
            {
                sb_sprite_c(cmd->tex, cmd->clip,
                    cmd->x, cmd->y, cmd->z, _tile_shadow_colors[k]);
            }
        }
    }
}
*/
// Ye olde tile rendering (editor)
/*
for (col = MAX(srt_col, 0); col < end_col; ++col)
{
    i = col - srt_col;

    for (row = MAX(srt_row, 0); row < end_row; ++row)
    {
        j = row - srt_row;

        cache = GET_CACHE_OF_TILE_FAST(map, col, row);

        for (lay = MAX(srt_lay, 0); lay < end_lay; ++lay)
        {
            k = lay - srt_lay;

            tile = GET_CACHE_TILE_FAST(cache, col, row, lay);

            tx = rp->bx + (float)(i * tile_px_w / 2) - \
                (float)(j * tile_px_w / 2) +
                tile_gfx_defs[tile].offset_x;
            ty = rp->by + (float)(i * tile_px_h / 4) + \
                (float)(j * tile_px_h / 2 / 2) - \
                (float)(k * (tile_px_w / 2)) + tile_gfx_defs[tile].offset_y;

            if (tile != 0)
            {
                sb_sprite_c(&tile_sheet_tex, tile_gfx_defs[tile].clip, tx,
                    ty, 0.f, _editor_tile_shadow_colors[k + 1]);
            }
            else if (grid_toggle)
            {
                sb_sprite(&tile_sheet_tex, tile_gfx_defs[0].clip, tx,
                    ty, 0.f);
            }

            cmd_index = k * (final_area_w * final_area_h) + j * final_area_w + i;

            for (cmd = w_draw_cmd_table[cmd_index]; cmd; cmd = cmd->next)
                sb_sprite_c(cmd->tex, cmd->clip,
                    cmd->x, cmd->y, cmd->z, _editor_tile_shadow_colors[k + 1]);
        }
    }
}
*/
