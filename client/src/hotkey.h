#ifndef MUTA_HOTKEY_H
#define MUTA_HOTKEY_H

#include "../../shared/types.h"

typedef struct hk_lua_callback_t        hk_lua_callback_t;
typedef struct hk_c_callback_t          hk_c_callback_t;
typedef union hk_action_callback_t      hk_action_callback_t;
typedef struct hk_key_mod_pair_t        hk_key_mod_pair_t;
typedef struct hk_action_t hk_action_t;

#define HK_MAX_ACTION_NAME_LEN      31
#define HK_MAX_KEY_COMBO_NAME_LEN   31

enum hk_action_callback_types
{
    HK_ACTION_CALLBACK_LUA,
    HK_ACTION_CALLBACK_C
};

enum hk_modifiers
{
    HK_MOD_SHIFT    = (1 << 0),
    HK_MOD_CTRL     = (1 << 1),
    HK_MOD_ALT      = (1 << 2)
};

enum hk_press_event_types
{
    HK_PRESS_DOWN = 0,
    HK_PRESS_UP,
    HK_PRESS_REPEAT,
    NUM_HK_PRESS_EVENT_TYPES
};

struct hk_lua_callback_t
{
    int     type;
    uint32  id;
};

struct hk_c_callback_t
{
    int     type;
    void    (*callback)();
};

union hk_action_callback_t
{
    int                 type;
    hk_lua_callback_t   lua;
    hk_c_callback_t     c;
};

struct hk_key_mod_pair_t
{
    int keycode;
    int mods;
};

struct hk_action_t
{
    char                    name[HK_MAX_ACTION_NAME_LEN + 1];
    hk_key_mod_pair_t       keys[2];
    hk_action_callback_t    callback;
    int                     press_event_type; /* Key up, down or repeat */
};

int
hk_init();

void
hk_destroy();

int
hk_reload();

int
hk_new_action(const char *name, hk_action_callback_t callback,
    int press_event_type);
/* Failure may happen if name is longer than HK_MAX_ACTION_NAME_LEN */

int
hk_str_to_keycode(const char *str);

const char *
hk_key_enum_to_str(int key_enum);

int
hk_bind_key(const char *action_name, int index, int keycode, int mods);
/* Index must be 0 or 1. mods is a bitmask of modifier keys. */

int
hk_bind_key_from_str(const char *action_name, int index, const char *key);

hk_action_t *
hk_get_action(int key_enum, int mods);
/* If a key is not found with the given modifiers but a key with no modifiers
 * is found instead, the latter will be returned. */

hk_action_t **
hk_get_actions(uint32 *ret_num);

hk_action_t *
hk_get_action_by_name(const char *name);

hk_action_t **
hk_get_repeat_actions(uint32 *ret_num);

int
hk_str_to_key_combo(const char *str, int *ret_keycode, int *ret_mods);
/* Any paremeter is allowed to be null. Format must follow the convention:
 * Q
 * SHIFT-V
 * CTRL-SHIFT-5
 * The order of modifiers does not matter. */

int
hk_key_combo_to_str(int keycode, int mods,
    char ret_buf[HK_MAX_KEY_COMBO_NAME_LEN + 1]);
/* Writes "None" if keycode is -1. */

#endif /* MUTA_HOTKEY_H */
