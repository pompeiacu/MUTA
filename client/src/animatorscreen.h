#ifndef MUTA_ANIMATOR_H
#define MUTA_ANIMATOR_H

#include "../../shared/types.h"

void
animator_screen_update(double dt);

void
animator_screen_open();

void
animator_screen_close();

void
animator_screen_keydown(int key, bool32 is_repeat);

void
animator_screen_text_input(const char *text);

void
animator_screen_mousebuttonup(uint8 button, int x, int y);

void
animator_screen_mousebuttondown(uint8 button, int x, int y);

void
animator_screen_mousewheel(int x, int y);

void
animator_screen_file_drop(const char *path);

#endif /* MUTA_ANIMATOR_H */