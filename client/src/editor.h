#ifndef MUTA_EDITOR_H
#define MUTA_EDITOR_H

#include "../../shared/types.h"

enum editor_state_t
{
    EDITOR_STATE_EDITING = 0,
    EDITOR_STATE_LOADING,
    EDITOR_STATE_SAVING,
    EDITOR_STATE_PAUSED
};

enum editor_mode_t
{
    EDITOR_MODE_TILEMODE = 0,
    EDITOR_MODE_ENTITYMODE
};

enum editor_entitytype_t
{
    EDITOR_ENTTYPE_STATOBJ = 1
};

void
editor_update(double dt);

void
editor_open();

void
editor_close();

void
editor_text_input(const char *text);

void
editor_keydown(int key, bool32 is_repeat);

void
editor_mousebuttondown(uint8 button, int x, int y);

void
editor_mousewheel(int x, int y);

#endif /* MUTA_EDITOR_H */
