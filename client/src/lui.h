#ifndef MUTA_LUI_H
#define MUTA_LUI_H

#include "../../shared/types.h"
#include "hotkey.h"

/* Forward declaration(s) */
typedef struct hk_action_t hk_action_t;

/* Types defined here */
typedef struct lui_update_event_t               lui_update_event_t;
typedef struct lui_app_window_event_t           lui_app_window_event_t;
typedef struct lui_key_callback_event_t         lui_key_callback_event_t;
typedef struct lui_chat_log_event_t             lui_chat_log_event_t;
typedef struct lui_new_hotkey_action_event_t    lui_new_hotkey_action_event_t;
typedef struct lui_key_event_t                  lui_key_event_t;
typedef struct lui_key_bind_changed_event_t     lui_key_bind_changed_event_t;
typedef struct lui_chat_log_entry_event_t       lui_chat_log_entry_event_t;
typedef union lui_event_t                       lui_event_t;

enum lui_events
{
    LUI_EVENT_UPDATE = 0,
    LUI_EVENT_APP_WINDOW_SIZE,
    LUI_EVENT_KEY_CALLBACK,
    LUI_EVENT_NEW_HOTKEY_ACTION,
    LUI_EVENT_KEY_DOWN,
    LUI_EVENT_KEY_UP,
    LUI_EVENT_KEY_BIND_CHANGED,
    LUI_EVENT_CHAT_LOG_ENTRY,

    LUI_NUM_EVENTS
};

struct lui_update_event_t
{
    int     type;
    double  delta;
};

struct lui_app_window_event_t
{
    int type;
    int w, h;
};

struct lui_key_callback_event_t
{
    int     type;
    uint32  callback_id;
};

struct lui_chat_log_event_t
{
    int type;
};

struct lui_new_hotkey_action_event_t
{
    int         type;
    hk_action_t *action;
};

struct lui_key_event_t
{
    int         type;
    char        key_combo_name[HK_MAX_KEY_COMBO_NAME_LEN + 1];
};

struct lui_key_bind_changed_event_t
{
    int         type;
    int         key_index;
    hk_action_t *action;
};

struct lui_chat_log_entry_event_t
{
    int         type;
    int         msg_type;
    const char  *sender;
    const char  *msg;
};

union lui_event_t
{
    int                             type;
    lui_update_event_t              update;
    lui_app_window_event_t          app_window;
    lui_key_callback_event_t        key_callback;
    lui_chat_log_event_t            chat_log;
    lui_new_hotkey_action_event_t   new_hotkey_action;
    lui_key_event_t                 key;
    lui_key_bind_changed_event_t    key_bind_changed;
    lui_chat_log_entry_event_t      chat_log_entry;
};

int
lui_init();

void
lui_destroy();

void
lui_reload();

void
lui_update_and_render(double dt);

void
lui_post_event(lui_event_t *event);

bool32
lui_is_grabbing_keyboard();

#endif /* MUTA_LUI_H */
