#ifndef MUTA_GAME_STATE_H
#define MUTA_GAME_STATE_H

/* gamestate.h: held state for an on-going online session (world, UI, etc.) */

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct byte_buf_t   bbuf_t;

enum gs_packet_actions
{
    GS_PACKET_OK,
    GS_PACKET_INCOMPLETE,
    GS_PACKET_DISCONNECT,
    GS_PACKET_UNDEFINED_MSG
};

enum gs_special_keys
{
    GS_SPECIAL_KEY_ENTER = 0,
    NUM_GS_SPECIAL_KEYS
};

void
gs_init();

void
gs_destroy();

bool32
gs_initialized();

int
gs_begin_session(uint32 map_id, player_guid_t id, const char *name,
    player_race_id_t race, int32 x, int32 y, int8 z, int dir);

void
gs_end_session();

bool32
gs_in_session();

void
gs_update_and_render(double dt, int x, int y, int w, int h);

void
gs_mouse_up(int button, int x, int y);

void
gs_mouse_down(int button, int x, int y);

void
gs_key_down(int key, bool32 repeat);

void
gs_key_up(int key);

int
gs_send_chat_msg_to_sv(const char *msg);

void
gs_flag_special_key_executed(int special_key);

int
gs_read_packet(int msg_type, bbuf_t *bb);

#endif /* MUTA_GAME_STATE_H */
