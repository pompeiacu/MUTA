#include "editor.h"
#include "core.h"
#include "render.h"
#include "gui.h"
#include "world_internal.h"
#include "assets.h"
#include "world.h"
#include "../../shared/muta_map_format.h"

#define NUM_INITIAL_ENTS 4096
#define TIMESTEP 0.06666f

world_t         editor_world;

typedef struct _pix_color_t _pix_color_t;
typedef _pix_color_t        _pix_color_darr_t;

struct _pix_color_t
{
    uint8 _color[4];
};

enum editor_state_t      _editor_state;
enum editor_mode_t       _editor_mode;
enum iso_dir_t           _ent_init_dir;
enum editor_entitytype_t _editor_enttype;

entity_handle_t _entity_handles[NUM_INITIAL_ENTS] = {0};

static bool32   _editor_map_initialized = 0;
static bool32   _grid_toggle = 0;
static bool32   _mousebutton_left_pressed = 0;
static bool32   _show_minimap = 0;
static bool32   _mouse_scroll_toggle = 0;
static bool32   _cur_layer_toggle;
static bool32   _tile_selection_win_toggle;
static bool32   _help_window_toggle;
static int      _vp_x;
static int      _vp_y;
static int      _tile_view_x;
static int      _tile_view_y;
static int      _tile_view_z;
static int      _currently_selected_layer;
static int      _num_visible_layers;
static int      _zoom;
static int      _scroll_speed;
static int      _max_input_length = 30;
static int      _max_layers_to_render = 8;
static int      _input_buffer_offset = 0;
static int      _selected_tile_x;
static int      _selected_tile_y;
static int      _num_tiles_to_render;
static uint     _selected_tiletype;
static uint     _selected_entitytype;
static char     _input_buffer[256] = { 0 };
static char     _map_name[96] = { 0 };
static char     _map_path[128] = { 0 };
static char     _chunk_name[128] = { 0 };
static int      _entity_handle_offset;
static uint32   _running_entity_id;
static int      _map_x, _map_y, _prev_map_x, _prev_map_y;
static int      _world_w = 3;
static uint8    _bitmap[(MAP_CHUNK_W * 3) * (MAP_CHUNK_W * 3) * 4];
uint8			_quad_col[4];

static tex_t    _minimap_texture;
static int      _minimap_tex_x_pos, _minimap_tex_y_pos;
static int      _minimap_win_w, _minimap_win_h;
static int      _minimap_click_coord_x, _minimap_click_coord_y;
static int      _minimap_loc_indic_x, _minimap_loc_indic_y;
static int      _tile_selector;
static int      _draw_pointer;
static int      _selection_loc[3];
static uint8    _color[4];
static uint32   win_help_id;
static uint32   win_minimap_id;
static world_render_props_t     _r_props;
double          _accum;
int             testx, testy;
int             _offset_x, _offset_y;
int             _vert_render_height;
float           bx = 0.0f;
float           by = 0.0f;

static _pix_color_darr_t *_pix_color;

static int
_save_map();

static int
_init_editor();

static int
_load_editor();

static void
_fill_layer(map_t *map, uint8 tiletype, int offset_x, int offset_y);

static void
_render_scene();

static void
_set_map_position();

static void
_get_minimap_texture();

//static void
//_create_minimap_texture(map_t *map);

static void
_scroll_with_mouse();

static void
_minimap_draw_rect(int x, int y, int w, int h, uint8 col[4]);

static void
_update_minimap(int x, int y, int map_x, int map_y, uint8 col[4]);

static void
_set_pixel_color(uint tiletype);

static void
_load_color_data();

static void
_tile_selection_win();

static void
_help_window();

void
editor_update(double dt)
{
    gui_begin();
    gui_font(&sf_default);

    r_viewport(0, 0, main_window.w, main_window.h);
    r_scissor(0, 0, main_window.w, main_window.h);
    r_color(0.5f, 0.5f, 0.5f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    gui_origin(GUI_TOP_LEFT);

    static bool32 printed = 0;

    if (!printed)
    {
        DEBUG_PRINTF("tiletex id: %u\n\n", tile_sheet_tex->id);
        printed = 1;
    }

    if (!_editor_map_initialized)
    {
        if (gui_button("<", 120, 100, 20, 20, 0) && _world_w > 3)
            --_world_w;
        if (gui_button(">", 200, 100, 20, 20, 0))
            ++_world_w;

        char buf[256];

        stbsp_sprintf(buf,
            "EDITOR MAP WIDTH & HEIGHT = %d x %d", _world_w, _world_w);
        gui_text(buf, 0, 120, 32);

        if (gui_button("NEW MAP", 120, 200, 80, 40, 0))
        {
            int buflen = (int)strlen(_input_buffer);
            if (buflen > 0)
            {
            if (_init_editor() != 0)
                printf("_init_editor FAILED!\n");
            } else
                printf("map name can't be empty!\n");
        }
        if (gui_button("LOAD MAP", 120, 400, 80, 30, 0))
            _load_editor();


        if ((_editor_state == EDITOR_STATE_LOADING ||
            _editor_state == EDITOR_STATE_SAVING) && _input_buffer_offset == 0)
            gui_text("PLEASE INPUT A NAME FOR YOUR MAP:",
                0, 120, 500);
        else
            gui_text(_input_buffer, 0, 120, 500);
    }
    else
    {
        world_update(&editor_world, &_r_props, dt);

        _render_scene();

        r_compute_tiled_pos_from_px_pos(&_r_props,
            mouse_state.x, mouse_state.y,
            &_selection_loc[0], &_selection_loc[1]);
        _selection_loc[2] = _currently_selected_layer;

        if (!(_selected_tile_x < 0 || _selected_tile_y < 0))
            _draw_pointer = 1;

        char gui_text_string[256];

        gui_font(&sf_default);

        gui_quad(0, 0, RESOLUTION_W, 80, _quad_col);
        gui_quad(0, 80, tile_px_h + 32, RESOLUTION_H / 3, _quad_col);

        gui_text("EDITOR MODE\nMouse Left: Place tiles/Entities\nMouse Right: Erase",
            0, RESOLUTION_W / 2, 4);
        sprintf(gui_text_string,
            "Visible tiles X %d, Y %d, Z %d\n(W, A, S, D, Up, Down)",
            _tile_view_x + _num_tiles_to_render,
            _tile_view_y + _num_tiles_to_render, _currently_selected_layer);
        gui_text(gui_text_string, 0, 200, 4);

        sprintf(gui_text_string, "Entities in world: %d / %d",
            _entity_handle_offset, NUM_INITIAL_ENTS);
        gui_text(gui_text_string, 0, 400, 4);

        if (_editor_mode == EDITOR_MODE_TILEMODE)
        {
            sprintf(gui_text_string, "%d", _selected_tiletype);
            gui_text("(E, T) MODE: TILEMODE:", 0, 16, 4);
        }
        else if (_editor_mode == EDITOR_MODE_ENTITYMODE)
        {
            sprintf(gui_text_string, "%d", _selected_entitytype);
            gui_text("(E, T) MODE: ENTITYMODE:", 0, 16, 4);
        }

        gui_text("(1) STATIC_OBJECTS", 0, 16, 20);

        if (_editor_mode == EDITOR_MODE_TILEMODE)
            gui_texture(tile_sheet_tex, tile_gfx_defs[_selected_tiletype].clip, 16, 64);

        gui_text(gui_text_string, 0, 150, 4);
        sprintf(gui_text_string, "(+, -) Scroll speed x%d", _scroll_speed);
        gui_text(gui_text_string, 0, 16, 220);

        sprintf(gui_text_string, "Direction: %d", _ent_init_dir);
        gui_text(gui_text_string, 0, 16, 354);

        gui_origin(GUI_TOP_RIGHT);
        gui_text("THE EDITOR IS ERROR PRONE! SAVE OFTEN!", 0, 5, 4);
        gui_origin(GUI_TOP_LEFT);

        if (gui_button("(G) Toggle grid", 16, 280, 80, 30, 0))
            _grid_toggle = _grid_toggle ? 0 : 1;
        if (gui_button("(Z, X) Turn clockwise", 16, 320, 100, 30, 0))
        {
            if (_ent_init_dir != ISODIR_NORTH_WEST)
                ++_ent_init_dir;
            else
                _ent_init_dir = ISODIR_NORTH;
        }
        if (gui_button("SAVE MAP", RESOLUTION_W - 96, 32, 80, 30, 0))
            _save_map();
        if (gui_button("(P) Fill layer", 16, 360, 90, 20, 0))
            _fill_layer(&editor_world.map, _selected_tiletype, 0, 0);
        if (gui_button("(zero) Erase layer", 16, 398, 90, 20, 0))
            _fill_layer(&editor_world.map, 0, 0, 0);
    }

    if (_show_minimap)
    {
        uint8  blue[4] = { 10,  62, 179, 255 };
        gui_origin(GUI_TOP_LEFT);
        uint8 col[4] = { 0, 0, 0, 255 };
        uint8 col2[4] = { 200, 170, 51, 255 };
        int os = 4;
        gui_quad(RESOLUTION_W - _minimap_win_w - (os * 4),
            RESOLUTION_H - _minimap_win_h - (os * 4),
            _minimap_win_w + os * 4, _minimap_win_h + os * 4, col2);

        gui_quad(RESOLUTION_W - _minimap_win_w - (os * 3),
            RESOLUTION_H - _minimap_win_h - (os * 3),
            _minimap_win_w + os * 2, _minimap_win_h + os * 2, col);

        win_minimap_id = gui_begin_empty_win("minimap",
            RESOLUTION_W - _minimap_win_w - (os * 2),
            RESOLUTION_H - _minimap_win_h - (os * 2),
            _minimap_win_w, _minimap_win_h, 0);


        _minimap_loc_indic_x = _tile_view_x - 12 - _minimap_tex_x_pos - (testx * MAP_CHUNK_W);
        _minimap_loc_indic_y = _tile_view_y - 11 - _minimap_tex_y_pos - (testy * MAP_CHUNK_W);

        gui_texture(&_minimap_texture, 0,
            -_minimap_tex_x_pos, -_minimap_tex_y_pos);
        _minimap_draw_rect(_minimap_loc_indic_x, _minimap_loc_indic_y, 30, 30, blue);

        if (_tile_view_x > (int)(_minimap_win_w / 2))
        {
            _minimap_tex_x_pos = _tile_view_x - (int)(_minimap_win_w / 2) - (testx * MAP_CHUNK_W);
            _minimap_tex_x_pos = CLAMP(_minimap_tex_x_pos, 0,
                MAP_CHUNK_W * _world_w - _minimap_win_w - (testx * MAP_CHUNK_W));
        }
        else
            _minimap_tex_x_pos = 0;

        if (_tile_view_y > (int)(_minimap_win_h / 2))
        {
            _minimap_tex_y_pos = _tile_view_y - (int)(_minimap_win_h / 2) - (testy * MAP_CHUNK_W);
            _minimap_tex_y_pos = CLAMP(_minimap_tex_y_pos, 0,
                MAP_CHUNK_W * _world_w - _minimap_win_h - (testy * MAP_CHUNK_W));
        }
        else
            _minimap_tex_y_pos = 0;

        gui_end_win();
    }

    if (_help_window_toggle)
        _help_window();

    if (_mouse_scroll_toggle)
    {
        _accum += dt;
        while (_accum >= TIMESTEP)
        {
            _scroll_with_mouse();
            _accum -= TIMESTEP;
        }
    }

    if (_tile_selection_win_toggle)
    {
        _tile_selection_win();
    }

    gui_end();

    if (BUTTON_DOWN_NOW(BUTTON_BIT(BUTTON_LEFT)) && _editor_map_initialized)
    {
        if (!gui_is_a_button_pressed() && !gui_is_any_element_pressed())
        {
            r_compute_tiled_pos_from_px_pos(&_r_props,
                mouse_state.x, mouse_state.y,
                &_selected_tile_x, &_selected_tile_y);
            if (_editor_mode == EDITOR_MODE_TILEMODE)
            {
                tile_t *tile_ptr;
                tile_ptr = map_get_tile_ptr(&editor_world.map, _selected_tile_x,
                    _selected_tile_y, _currently_selected_layer);
                if (tile_ptr != 0)
                {
                    *tile_ptr = _selected_tiletype;
                    _set_pixel_color(_selected_tiletype);
                    _update_minimap(_selected_tile_x, _selected_tile_y, testx, testy, _color);
                }
            }
            else if (_editor_mode == EDITOR_MODE_ENTITYMODE &&
                !_mousebutton_left_pressed &&
                _entity_handle_offset < NUM_INITIAL_ENTS)
            {
                _mousebutton_left_pressed = 1;
                entity_t *e = 0;

                switch (_editor_enttype)
                {
                    case ENT_TYPE_STATIC_OBJ:
                    {
                        e = world_spawn_static_object(&editor_world, _selected_entitytype,
                            _selected_tile_x, _selected_tile_y, _currently_selected_layer, 0);
                        if (!e) printf("Failed to create Static object.\nCheck if entity ID is supported.\n");
                    }break;
                    default: FIXME(); break;
                }
                if (e) ++_running_entity_id;
            }
        }

        if (gui_is_any_element_pressed() && !_mousebutton_left_pressed)
        {
            if (gui_get_active_win_id() == win_minimap_id)
            {
                gui_clicked_coord_inside_win(&_minimap_click_coord_x,
                    &_minimap_click_coord_y);
                _tile_view_x = _minimap_click_coord_x + _minimap_tex_x_pos -
                    _num_tiles_to_render / 2 + (testx * MAP_CHUNK_W) + _offset_x;
                _tile_view_y = _minimap_click_coord_y + _minimap_tex_y_pos -
                    _num_tiles_to_render / 2 + (testy * MAP_CHUNK_W) + _offset_y;
                _set_map_position();
            }

            if (_tile_view_x < _num_tiles_to_render / 2)
                _tile_view_x = 0;
            else if (_tile_view_x > MAP_CHUNK_W * _world_w - _num_tiles_to_render)
                _tile_view_x = MAP_CHUNK_W * _world_w - _num_tiles_to_render;

            if (_tile_view_y < _num_tiles_to_render / 2)
                _tile_view_y = 0;
            else if (_tile_view_y > MAP_CHUNK_W * _world_w - _num_tiles_to_render)
                _tile_view_y = MAP_CHUNK_W * _world_w - _num_tiles_to_render;
            _mousebutton_left_pressed = 1;
        }
    }
    else if (BUTTON_DOWN(BUTTON_BIT(BUTTON_RIGHT)) && _editor_map_initialized)
    {
        if (!gui_is_a_button_pressed() && !gui_is_any_element_pressed())
        {
            r_compute_tiled_pos_from_px_pos(&_r_props,
                mouse_state.x, mouse_state.y,
                &_selected_tile_x, &_selected_tile_y);

            if (_editor_mode == EDITOR_MODE_TILEMODE)
            {
                tile_t *tile_ptr;
                tile_ptr = map_get_tile_ptr(&editor_world.map,
                    _selected_tile_x, _selected_tile_y,
                    _currently_selected_layer);
                if (tile_ptr != 0)
                {
                    *tile_ptr = 0;
                    _get_minimap_texture();
                }
            }
            else if (_editor_mode == EDITOR_MODE_ENTITYMODE)
            {
                entity_t *e = 0;
                e = world_get_entity_by_window_coords(&editor_world,
                    mouse_state.x, mouse_state.y);
                if (e) despawn_static_obj(e);
            }
        }
    }

    if (!BUTTON_DOWN(BUTTON_BIT(BUTTON_LEFT)))
    {
        if (_mousebutton_left_pressed)
            _mousebutton_left_pressed = 0;
    }

    r_swap_buffers(&main_window);
}

void
editor_open()
{
    _editor_map_initialized = 0;
    _editor_state = EDITOR_STATE_SAVING;
    _editor_mode = EDITOR_MODE_TILEMODE;
    _editor_enttype = ENT_TYPE_STATIC_OBJ;
    _vp_x = 0;
    _vp_y = 0;
    _tile_view_x = 0;
    _tile_view_y = 0;
    _tile_view_z = 0;
    _selected_tile_x = 0;
    _selected_tile_y = 0;
    _num_tiles_to_render = 16;
    _zoom = 0;
    _selected_tiletype = 1;
    _selected_entitytype = 1;
    _scroll_speed = 1;
    _num_visible_layers = 1;
    _currently_selected_layer = 0;
    _entity_handle_offset = 0;
    _running_entity_id = 0;
    _map_x = 0;
    _map_y = 0;
    _prev_map_x = 0;
    _prev_map_y = 0;
    _ent_init_dir = ISODIR_NORTH;
    _currently_selected_layer += 32;
    _currently_selected_layer = CLAMP(_currently_selected_layer, 0,
        MAP_CHUNK_T - 1);
    _minimap_tex_x_pos = 0;
    _minimap_tex_y_pos = 0;
    _minimap_win_w = 250;
    _minimap_win_h = 200;
    testx = 0;
    testy = 0;
    _offset_x = 5;
    _offset_y = 4;
    _cur_layer_toggle = 0;
    _show_minimap = 0;
    _tile_selection_win_toggle = 0;
    _tile_selector = 1;
    _draw_pointer = 0;

    if (_currently_selected_layer > _max_layers_to_render - 1)
    {
        _tile_view_z += 25;
        _tile_view_z = CLAMP(_tile_view_z, 0,
            MAP_CHUNK_T - _num_visible_layers);
    }
    _num_visible_layers += 32;
    _num_visible_layers = CLAMP(_num_visible_layers, 1, _max_layers_to_render);

    _color[0] = 255;
    _color[1] = 255;
    _color[2] = 255;
    _color[3] = 255;

    _quad_col[0] = 0;
    _quad_col[1] = 0;
    _quad_col[2] = 0;
    _quad_col[3] = 128;

    _selection_loc[2] = _currently_selected_layer;

    darr_reserve(_pix_color, 256);

    darr_push_empty(_pix_color);
    _load_color_data();
    create_directory("editor");
}

void
editor_close()
{
    for (int i = 0; i < sizeof(_input_buffer); ++i)
        _input_buffer[i] = 0;
    _input_buffer_offset = 0;
    world_clear(&editor_world);
    //(Lommi): Is the world data cleared from memory?

    darr_free(_pix_color);
}

void
editor_text_input(const char *text)
{
    if ((_editor_state == EDITOR_STATE_SAVING ||
        _editor_state == EDITOR_STATE_LOADING)
        && _input_buffer_offset < _max_input_length)
    {
        strncat(_input_buffer, text, sizeof(_input_buffer) - 1);
        ++_input_buffer_offset;
    }
}

void
editor_keydown(int key, bool32 is_repeat)
{
    if (!_editor_map_initialized)
    {
        switch (key)
        {
        case KEYCODE(ESCAPE):
            core_set_screen(&main_menu_screen);
        }
    }
    if (_editor_state == EDITOR_STATE_SAVING ||
        _editor_state == EDITOR_STATE_LOADING)
    {
        switch (key)
        {
        case KEYCODE(BACKSPACE):
        {
            if (_input_buffer_offset > 0)
            {
                _input_buffer[_input_buffer_offset - 1] = 0;
                --_input_buffer_offset;
            }
        }
        break;
        }
    }
    else if (_editor_state == EDITOR_STATE_EDITING)
    {
        switch (key)
        {
        case KEYCODE(ESCAPE):
        {
            core_set_screen(&main_menu_screen);
        }
        break;
        case KEYCODE(a):
        {
            _tile_view_x -= _scroll_speed;
            _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
                _num_tiles_to_render);
            _set_map_position();

        }
        break;
        case KEYCODE(d):
        {
            _tile_view_x += _scroll_speed;
            _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
                _num_tiles_to_render);
            _set_map_position();
        }
        break;
        case KEYCODE(w):
        {
            _tile_view_y -= _scroll_speed;
            _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
                _num_tiles_to_render);
            _set_map_position();
        }
        break;
        case KEYCODE(s):
        {
            _tile_view_y += _scroll_speed;
            _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
                _num_tiles_to_render);
            _set_map_position();
        }
        break;
        case KEYCODE(e):
        {
            _editor_mode = EDITOR_MODE_ENTITYMODE;
        }
        break;
        case KEYCODE(t):
        {
            _editor_mode = EDITOR_MODE_TILEMODE;
        }
        break;
        case KEYCODE(KP_PLUS):
        {
            _scroll_speed = _scroll_speed * 2;
            _scroll_speed = CLAMP(_scroll_speed, 1, 32);

        }
        break;
        case KEYCODE(KP_MINUS):
        {
            _scroll_speed -= _scroll_speed / 2;
            _scroll_speed = CLAMP(_scroll_speed, 1, 32);
        }
        break;
        case KEYCODE(KP_8):
        {
            _vert_render_height++;
            if (_vert_render_height > 10)
                _vert_render_height = 10;
        }
        break;
        case KEYCODE(KP_5):
        {
            _vert_render_height--;
            if (_vert_render_height < 0)
                _vert_render_height = 0;
        }
        case KEYCODE(RIGHT):
        {
            if (_num_tiles_to_render + _tile_view_x < MAP_CHUNK_W &&
                _num_tiles_to_render + _tile_view_y < MAP_CHUNK_W)
            {
                _num_tiles_to_render += 1;
                _num_tiles_to_render = CLAMP(_num_tiles_to_render, 1, 32);
            }
        }
        break;
        case KEYCODE(LEFT):
        {
            _num_tiles_to_render -= 1;
            _num_tiles_to_render = CLAMP(_num_tiles_to_render, 1, 32);
        }
        break;
        case KEYCODE(UP):
        {
            _currently_selected_layer += _scroll_speed;
            _currently_selected_layer = 
                CLAMP(_currently_selected_layer, 0, MAP_CHUNK_T - 1);
            if (_currently_selected_layer > _max_layers_to_render - 1)
            {
                _tile_view_z += _scroll_speed;
                _tile_view_z = 
                    CLAMP(_tile_view_z, 0, MAP_CHUNK_T - _num_visible_layers);
            }
            _num_visible_layers += _scroll_speed;
            _num_visible_layers = 
                CLAMP(_num_visible_layers, 1, _max_layers_to_render);
            if(_cur_layer_toggle)
            _get_minimap_texture();
        }
        break;
        case KEYCODE(DOWN):
        {
            _currently_selected_layer -= _scroll_speed;
            _currently_selected_layer = 
                CLAMP(_currently_selected_layer, 0, MAP_CHUNK_T - 1);
            _tile_view_z -= _scroll_speed;
            _tile_view_z = 
                CLAMP(_tile_view_z, 0, MAP_CHUNK_T - _num_visible_layers);
            if (_currently_selected_layer < _max_layers_to_render - 1)
            {
                _num_visible_layers -= _scroll_speed;
                _num_visible_layers = 
                    CLAMP(_num_visible_layers, 1, _max_layers_to_render);
            }
            if (_cur_layer_toggle)
            _get_minimap_texture();
        }
        break;
        case KEYCODE(g):
        {
            _grid_toggle = _grid_toggle ? 0 : 1;
        }
        break;
        case KEYCODE(p):
        {
            _fill_layer(&editor_world.map, _selected_tiletype,
                _offset_x, _offset_y);
        }
        break;
        case KEYCODE(0):
        {
            _fill_layer(&editor_world.map, 0, _offset_x, _offset_y);
        }
        break;
        case KEYCODE(1):
        {
            _editor_enttype = ENT_TYPE_STATIC_OBJ;
        }
        break;
        case KEYCODE(x):
        {
            if (_ent_init_dir != ISODIR_NORTH_WEST)
                ++_ent_init_dir;
            else
                _ent_init_dir = ISODIR_NORTH;
        }
        break;
        case KEYCODE(z):
        {
            if (_ent_init_dir != ISODIR_NORTH)
                --_ent_init_dir;
            else
                _ent_init_dir = ISODIR_NORTH_WEST;
        }
        break;
        case KEYCODE(m):
        {
            _show_minimap = _show_minimap ? 0 : 1;
        }
        break;
        case KEYCODE(l):
        {
            _mouse_scroll_toggle = _mouse_scroll_toggle ? 0 : 1;
        }
        break;
        case KEYCODE(9):
        {
            _cur_layer_toggle = _cur_layer_toggle ? 0 : 1;
            _get_minimap_texture();
        }
        break;
        case KEYCODE(TAB):
        {
            _tile_selection_win_toggle = _tile_selection_win_toggle ? 0 : 1;
        }
        break;
        case KEYCODE(h):
        {
            _help_window_toggle = _help_window_toggle ? 0 : 1;
        }
        }
    }
}

void
editor_mousebuttondown(uint8 button, int x, int y)
{

}

void
editor_mousewheel(int x, int y)
{
    if (!_tile_selection_win_toggle)
    {
        if (_editor_mode == EDITOR_MODE_TILEMODE)
        {
            _selected_tiletype += y;
            if (_selected_tiletype < 1)
                _selected_tiletype = tile_defs_num;
            if (_selected_tiletype > tile_defs_num)
                _selected_tiletype = 1;
        } else
        if (_editor_mode == EDITOR_MODE_ENTITYMODE)
        {
            _selected_entitytype += y;
            if (_editor_enttype == EDITOR_ENTTYPE_STATOBJ)
            {
                DEBUG_PRINTF("%s, %d: IMPLEMENTME\n", __func__, __LINE__);
                /* if (_selected_entitytype < 1)
                    _selected_entitytype = (uint32)creature_defs_max - 1;
                if (_selected_entitytype > (uint32)creature_defs_max - 1)
                    _selected_entitytype = 1; */
            } else
                DEBUG_PRINTF("%s, %d: IMPLEMENTME\n", __func__, __LINE__);
        }
    } else
    {
        if (_editor_mode == EDITOR_MODE_TILEMODE)
        {
            _tile_selector -= y;
            if (_tile_selector < 1)
                _tile_selector = tile_defs_num;
            if (_tile_selector > (int)tile_defs_num)
                _tile_selector = 1;
        }
    }
}

static int
_init_editor()
{
    world_destroy(&editor_world);
    if (world_init(&editor_world, _world_w, _world_w, NUM_INITIAL_ENTS) != 0)
        return 1;

    strcpy(_map_name, _input_buffer);
    if (muta_map_file_init_for_edit(&editor_world.map.file,
        _map_name, 1, _world_w, _world_w) != 0)
        printf("muta_map_file_init_for_edit failed\n");

    snprintf(_map_path, 127, "editor/%s", _map_name);
    if (muta_map_file_set_chunk_path_pattern(&editor_world.map.file,
        _map_path) != 0)
        printf("muta_map_file_set_chunk_path_pattern failed\n");

    map_chunk_t *ch;
    for (int i = 0; i < editor_world.map.w; ++i)
    {
        for (int j = 0; j < editor_world.map.h; ++j)
        {
            ch = &editor_world.map.chunks[j * _world_w + i];
            ch->path = muta_map_file_get_chunk_path(&editor_world.map.file,
                i, j);
            printf("ch->path: %s\n", ch->path);
        }
    }

    muta_chunk_file_t tmp;
    muta_chunk_file_init(&tmp);

    for (int i = 0; i < editor_world.map.w; ++i)
    {
        for (int j = 0; j < editor_world.map.h; ++j)
        {
            stbsp_sprintf(_chunk_name, "%s%d_%d.dat", _map_name, i, j);
            muta_chunk_file_set_name(&tmp, _chunk_name);
            muta_chunk_file_save(&tmp, "editor");
        }
    }

    _fill_layer(&editor_world.map, 1, 0, 0);

    _editor_map_initialized = 1;
    _editor_state = EDITOR_STATE_EDITING;
    _get_minimap_texture();


    return 0;
}

static int
_save_map()
{
    if (muta_map_file_save(&editor_world.map.file, _map_path) != 0)
    {
        chunk_cache_t *cc;
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                cc = editor_world.map.cache_ptrs[j * 3 + i];
                char *n = muta_map_file_get_chunk_name(&editor_world.map.file,
                    editor_world.map.cam_x + i, editor_world.map.cam_y + j);
                if (muta_chunk_file_set_name(&cc->file, n))     return 1;
                if (muta_chunk_file_save(&cc->file, "editor"))  return 2;
                printf("saved: %s\n", n);
            }
        }
        printf("muta_map_file_save failed return: %d\n",
            muta_map_file_save(&editor_world.map.file, _map_path));
        return 3;
    }

    return 0;
}

static int
_load_editor()
{
    char fp[7 + MUTA_MAP_FILE_NAME_LEN + 1] = "editor/";
    strncpy(fp + 7, _input_buffer, MUTA_MAP_FILE_NAME_LEN + 1);

    DEBUG_PRINTF("%s: path %s\n", __func__, fp);

    sprintf(_map_path, "editor/%s", _map_name);
    muta_map_file_set_name(&editor_world.map.file, _map_path);

    if (world_init_from_file(&editor_world, fp) != 0)
    {
        printf("failed to init world from file\n");
        return 1;
    }

    _editor_map_initialized = 1;
    _editor_state = EDITOR_STATE_EDITING;
    _get_minimap_texture();

    printf("map loaded!\n");
    return 0;
}

static void
_fill_layer(map_t *map, uint8 tiletype, int offset_x, int offset_y)
{
    int i, j, x_offset, y_offset;
    int x_limit = _tile_view_x + _num_tiles_to_render;
    int y_limit = _tile_view_y + _num_tiles_to_render;

    for (i = _tile_view_x;
        i < x_limit;
        ++i)
    {
        for (j = _tile_view_y;
            j < y_limit;
            ++j)
        {
            x_offset = i - offset_x;
            y_offset = j - offset_y;
            if (x_offset < 0)
                x_offset = i;
            if (y_offset < 0)
                y_offset = j;
            *map_get_tile_ptr(map, x_offset, y_offset,
                _currently_selected_layer) = tiletype;
        }
    }
    _set_pixel_color(tiletype);
    _get_minimap_texture();
}

static void
_render_scene()
{
    float s = core_compute_target_viewport_scale();

    _r_props.screen_area[2] =
        (int)(s * (float)(RESOLUTION_W + tile_px_w * 3));
    _r_props.screen_area[3] =
        (int)(s * (float)(RESOLUTION_H + tile_px_h * 3));
    _r_props.screen_area[0] = main_window.w / 2 -
        _r_props.screen_area[2] / 2 - (int)(s * (float)(tile_px_w / 2));
    _r_props.screen_area[1] = main_window.h / 2 -
        _r_props.screen_area[3] / 2 - (int)(s * (float)(s * tile_top_px_h));

    _r_props.scissor[0] = (main_window.w - (int)((float)RESOLUTION_W * s)) / 2;
    _r_props.scissor[1] = (main_window.h - (int)((float)RESOLUTION_H * s)) / 2;
    _r_props.scissor[2] = main_window.w - _r_props.scissor[0] * 2;
    _r_props.scissor[3] = main_window.h - _r_props.scissor[1] * 2;

    _r_props.coord_space[0] = 0;
    _r_props.coord_space[1] = 0;
    _r_props.coord_space[2] = RESOLUTION_W + tile_px_w * 3;
    _r_props.coord_space[3] = RESOLUTION_H + tile_px_h * 3;

    int tx, ty, tz;

    /* Figure out the tiled width and height of the rendered area */
    int ptw = _r_props.coord_space[2] / MAX(tile_px_w, 2) * 2 + 3;
    int pth = _r_props.coord_space[3] / MAX(tile_top_px_h, 2) * 2 + 3;
    int tw = MAX(ptw, pth);

    tx = _tile_view_x - tw / 2;
    ty = _tile_view_y - tw / 2;
    tz = _tile_view_z - _num_visible_layers / 2;

    /* Determine where the first tile should be drawn */

    bx = (float)_r_props.coord_space[2] * 0.5f;
    by = -(float)(tw * tile_top_px_h / 4) +
        (float)(_num_visible_layers * tile_top_px_h / 2);

    _r_props.tx = tx;
    _r_props.ty = ty;
    _r_props.tz = _tile_view_z;
    _r_props.tw = tw;
    _r_props.th = tw;
    _r_props.tt = _num_visible_layers;
    _r_props.bx = bx;
    _r_props.by = by;
    _r_props.st = tz;

    r_render_editor_world(&editor_world, &_r_props, _grid_toggle,
        _selection_loc, _selected_tiletype, _vert_render_height);
}

static void
_set_map_position()
{
    _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
        _num_tiles_to_render);
    _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
        _num_tiles_to_render);

    _prev_map_x = _map_x;
    _prev_map_y = _map_y;
    _map_x = _tile_view_x / MAP_CHUNK_W;
    _map_y = _tile_view_y / MAP_CHUNK_W;

    if (_map_x != _prev_map_x || _map_y != _prev_map_y)
    {
        //Optimize by choosing only new caches or by threadpooling
        chunk_cache_t *cc;
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                cc = editor_world.map.cache_ptrs[j * 3 + i];
                char *n = muta_map_file_get_chunk_name(&editor_world.map.file,
                    editor_world.map.cam_x + i, editor_world.map.cam_y + j);
                if (muta_chunk_file_set_name(&cc->file, n))     return;
                if (muta_chunk_file_save(&cc->file, "editor"))  return;
            }
        }
    }

    if (world_set_cam_pos(&editor_world, _map_x - 1, _map_y - 1))
    {
        if (_map_x >= 1)
        {
            if (_map_x != _prev_map_x)
                testx = _map_x - 1;
        }
        if (_map_y >= 1)
        {
            if (_map_y != _prev_map_y)
                testy = _map_y - 1;
        }
        _get_minimap_texture();
    }
}

static void
_get_minimap_texture()
{
    tex_free(&_minimap_texture);
    int i, j, k, h, g, index, layer_limit;

    if (!_cur_layer_toggle)
        layer_limit = MAP_CHUNK_T - 1;
    else
        layer_limit = _currently_selected_layer;

    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            for (k = 0; k < MAP_CHUNK_W; ++k)
            {
                for (h = 0; h < MAP_CHUNK_W; ++h)
                {
                    for (g = layer_limit; g > 0; --g)
                    {
                        tile_t tile = GET_CHUNK_TILE(
                            editor_world.map.cache_ptrs[j * 3 + i], k, h, g);

                        index = ((i * MAP_CHUNK_W * 4) +
                            h * 4 * MAP_CHUNK_W * 3) +
                            ((j * MAP_CHUNK_W * 4) *
                                MAP_CHUNK_W * 3 + k * 4);

                        if (tile != 0)
                        {
                            int col_index = tile;
                            _bitmap[index + 0] =
                                _pix_color[col_index]._color[0];
                            _bitmap[index + 1] =
                                _pix_color[col_index]._color[1];
                            _bitmap[index + 2] =
                                _pix_color[col_index]._color[2];
                            _bitmap[index + 3] =
                                _pix_color[col_index]._color[3];
                            break;
                        } else
                        {
                            _bitmap[index + 0] = 255;
                            _bitmap[index + 1] = 255;
                            _bitmap[index + 2] = 255;
                            _bitmap[index + 3] = 255;
                        }

                        if (_cur_layer_toggle)
                            break;
                    }
                }
            }
        }
    }
    _update_minimap(_selected_tile_x, _selected_tile_y, testx, testy, _color);
}

static void
_update_minimap(int x, int y, int map_x, int map_y, uint8 col[4])
{
    tex_free(&_minimap_texture);
    int index;
    if (map_x <= 0)
        map_x = 0;
    if (map_y <= 0)
        map_y = 0;
    index = ((y - (map_y * MAP_CHUNK_W)) * 
        MAP_CHUNK_W * 3 + (x - (map_x * MAP_CHUNK_W))) * 4;

    _bitmap[index] = col[0];
    _bitmap[index + 1] = col[1];
    _bitmap[index + 2] = col[2];
    _bitmap[index + 3] = col[3];

    tex_from_mem(&_minimap_texture, _bitmap,
        MAP_CHUNK_W * 3, MAP_CHUNK_W * 3, IMG_RGBA);
}

static void
_scroll_with_mouse()
{
    bool32 left, right, up, down;
    if ((mouse_state.x - _r_props.scissor[0]) <
        (int)(_r_props.scissor[2] * 0.05f))
    {
        left = 1;
    }
    else
        left = 0;
    if ((mouse_state.x - _r_props.scissor[0]) >
        (int)(_r_props.scissor[2] * 0.95f))
    {
        right = 1;
    }
    else
        right = 0;

    if ((mouse_state.y - _r_props.scissor[1]) <
        (int)(_r_props.scissor[3] * 0.05f))
    {
        up = 1;
    }
    else
        up = 0;
    if ((mouse_state.y - _r_props.scissor[1]) >
        (int)(_r_props.scissor[3] * 0.95f))
    {
        down = 1;
    }
    else
        down = 0;

    if (left && !(up || down))
    {
        _tile_view_x -= 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _tile_view_y += 1;
        _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
    if (right && !(up || down))
    {
        _tile_view_x += 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _tile_view_y -= 1;
        _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
    if (up && !(left || right))
    {
        _tile_view_y -= 1;
        _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _tile_view_x -= 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
    if (down && !(left || right))
    {
        _tile_view_y += 1;
        _tile_view_y = CLAMP(_tile_view_y, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _tile_view_x += 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }

    if (left && up)
    {
        _tile_view_x -= 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
    if (right && up)
    {
        _tile_view_y -= 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
    if (left && down)
    {
        _tile_view_y += 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
    if (right && down)
    {
        _tile_view_x += 1;
        _tile_view_x = CLAMP(_tile_view_x, 0, _world_w * MAP_CHUNK_W -
            _num_tiles_to_render);
        _set_map_position();
    }
}

static void
_minimap_draw_rect(int x, int y, int w, int h, uint8 col[4])
{
    gui_quad(x, y, w, 1, col);
    gui_quad(x, y, 1, h, col);
    gui_quad(x, y + h - 1, w, 1, col);
    gui_quad(x + w - 1, y, 1, h, col);
}

static void
_set_pixel_color(uint tiletype)
{
    int index = (int)tiletype;
    if (index != 0)
    {
        _color[0] = _pix_color[index]._color[0];
        _color[1] = _pix_color[index]._color[1];
        _color[2] = _pix_color[index]._color[2];
        _color[3] = _pix_color[index]._color[3];
    } else
    {
        _color[0] = 255;
        _color[1] = 255;
        _color[2] = 255;
        _color[3] = 255;
    }
}

static void
_load_color_data()
{
  if (tile_sheet_img->pixels == 0)
      return;
  uint i;
  int j, k;
  for (i = 1; i < tile_defs_num; ++i)
  {
      int start_x   = (int)tile_gfx_defs[i].clip[0];
      int start_y   = (int)tile_gfx_defs[i].clip[1];
      int end_x     = (int)tile_gfx_defs[i].clip[2];
      int end_y     = (int)tile_gfx_defs[i].clip[3];

      int clip_col_count = 0;

      int col_r = 0;
      int col_g = 0;
      int col_b = 0;

      for (j = start_x; j < end_x; ++j)
      {
          if (j < tile_sheet_img->w)
          {
              for (k = start_y; k < end_y; ++k)
              {
                  if (k < tile_sheet_img->h)
                  {
                      uint pixel_index = k * 4 * tile_sheet_img->w + j * 4;

                      if (tile_sheet_img->pixels[pixel_index + 3] == 0)
                          continue;

                      col_r += tile_sheet_img->pixels[pixel_index + 0];
                      col_g += tile_sheet_img->pixels[pixel_index + 1];
                      col_b += tile_sheet_img->pixels[pixel_index + 2];
                      ++clip_col_count;

                      if (clip_col_count > 0)
                      {
                          darr_reserve(_pix_color, i + 1);
                          _pix_color[i]._color[0] = col_r / clip_col_count;
                          _pix_color[i]._color[1] = col_g / clip_col_count;
                          _pix_color[i]._color[2] = col_b / clip_col_count;
                          _pix_color[i]._color[3] = 255;
                      }
                  }
              }
          }
      }
  }
}

static void
_tile_selection_win()
{
    int tiles;
    int i, j;
    int rows = 2;
    int columns = 5;
    int offset = 32;

    gui_quad(RESOLUTION_W - rows * tile_px_w - offset - 16, 118, rows * 
        tile_px_w + offset * 2, tile_px_h * columns + offset * 3, _quad_col);

    for (i = 0; i < columns; ++i)
    {
        for (j = 0; j < rows; ++j)
        {
            tiles = _tile_selector + i + columns * j;
            if (tiles > (int)tile_defs_num)
                tiles -= (int)tile_defs_num;

            if (gui_invisible_button("lol", RESOLUTION_W - (rows * tile_px_w)
                - offset + tile_px_w * j + (j * offset / 2), 
                128 + tile_px_w * i + (i * offset / 2), tile_px_w,
                tile_px_w, 0))
            {
                _selected_tiletype = tiles;
            }
            gui_texture(tile_sheet_tex, tile_gfx_defs[tiles].clip,
                RESOLUTION_W - (rows * tile_px_w) - 
                offset + tile_px_w * j + (j * offset / 2), 
                128 + tile_px_w * i + (i * offset / 2));
        }
    }
}

static void
_help_window()
{
    gui_origin(GUI_CENTER_CENTER);

    win_help_id = gui_begin_win("HELP", 0, 0, 640, 640, 0);
    gui_origin(GUI_TOP_CENTER);
    gui_text("HOTKEYS are: ", 0, 0, 0);

    gui_origin(GUI_TOP_LEFT);
    gui_text("  Escape: Shuts down editor (does not save the map!)", 0, 0, 20);
    gui_text("  WASD: scrolls the editor world. (right, left, up, down) ",
        0, 0, 60);
    gui_text("  Keyboard + / -: Increases editor scroll speed ", 0, 0, 100);
    gui_text("  Right / left arrows: Changes size of fill- and clear area ",
        0, 0, 140);
    gui_text("  Up / Down arrows: Changes the current active layer (up goes"
        "up, down goes down) ", 0, 0, 180);
    gui_text("  Keypad 8 / 5: Change how many layers are visible (edited "
        "layer doesn't change!) ", 0, 0, 220);
    gui_text("  G: Shows and hides the grid ", 0, 0, 260);
    gui_text("  P: Fill layer (makes x*x square with currently active "
        "tiletype)", 0, 0, 280);
    gui_text("  0: Clear layer (clears x*x square with currently active "
        "tiletype)", 0, 0, 320);
    gui_text("  M: Show and hides the minimap", 0, 0, 360);
    gui_text("  L: Toggle map scrolling with mouse (sides of the screen)",
        0, 0, 400);
    gui_text("  TAB: Open/hides tile selection window. While open, mouse "
        "scroll let you scroll through tiles", 0, 0, 440);
    gui_text("  Mouse scroll: Without selection window active, scrolls "
        "through tiletypes", 0, 0, 480);
    gui_text("  H: Toggles help window", 0, 0, 520);
    gui_end_win();
}
