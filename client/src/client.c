#include "client.h"
#include "../../shared/common_utils.h"
#include "../../shared/crypt.h"
#include "../../shared/net.h"
#include "../../shared/common_defs.h"
#include "../../shared/packets.h"

#define IN_BUF_SZ (8 * MUTA_MTU)

typedef struct cl_event_t cl_event_t;

enum cl_event
{
    CL_EVENT_DISCONNECT
};

struct cl_event_t
{
    union
    {
        int type;
    };
};

static thread_t         _thread;
static cryptchan_t      _cryptchan;
static int32            _running; /* Atomic */
static socket_t         _login_socket = KSYS_INVALID_SOCKET;
static socket_t         _shard_socket = KSYS_INVALID_SOCKET;
static int              _connection_status;
static addr_t           _login_server_address;
static blocking_queue_t _msg_queue; /* Recvd data is pushed here */
static blocking_queue_t _event_queue;

static struct
{
    uint8   mem[IN_BUF_SZ];
    uint32  num_bytes;
} _in_buf;

static thread_ret_t
_main(void *args);

static void
_read_server_info_callback(void *ctx, const char *opt, const char *val);

static int
_read_packet();

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s);

int
cl_init()
{
    int ret = 0;
    blocking_queue_init(&_msg_queue, IN_BUF_SZ, 1);
    blocking_queue_init(&_event_queue, 8, sizeof(cl_event_t));
    if (thread_init(&_thread))
        {ret = 1; goto fail;}
    return ret;
    fail:
        DEBUG_PRINTFF("failed, error %d.\n", ret);
        return ret;
}

void
cl_destroy()
{
}

int
cl_connect()
{
    int ret = 0;
    muta_assert(!_running);
    bool32 found_address = 0;
    _login_socket = KSYS_INVALID_SOCKET;
    _shard_socket = KSYS_INVALID_SOCKET;
    if (parse_cfg_file("server_info.cfg", _read_server_info_callback,
        &found_address))
    {
        DEBUG_PRINTF("No server_info.cfg found, unable to connect.\n");
        ret = 1;
        goto fail;
    }
    cryptchan_init(&_cryptchan, 0);
    blocking_queue_clear(&_msg_queue);
    _in_buf.num_bytes   = 0;
    if ((_login_socket = net_tcp_ipv4_sock()) == KSYS_INVALID_SOCKET)
        {ret = 2; goto fail;}
    if (_shard_socket = net_tcp_ipv4_sock() == KSYS_INVALID_SOCKET)
        {ret = 3; goto fail;}
    _running = 1;
    if (thread_create(&_thread, _main, 0))
        {ret = 4; goto fail;}
    return 0;
    fail:
        close_socket(_login_socket);
        close_socket(_shard_socket);
        _running            = 0;
        _connection_status  = CONNSTATUS_DISCONNECTED;
        _login_socket       = KSYS_INVALID_SOCKET;
        _shard_socket       = KSYS_INVALID_SOCKET;
        DEBUG_PRINTFF("failed with code %d.\n", ret);
}

void
cl_disconnect()
{
    DEBUG_PRINTF("Disconnecting from server...\n");
    muta_assert(_running);
    interlocked_increment_int32(&_running);
    blocking_queue_clear(&_msg_queue);
    net_close_blocking_sock(_login_socket);
    net_close_blocking_sock(_shard_socket);
    _connection_status  = CONNSTATUS_DISCONNECTED;
    _login_socket       = KSYS_INVALID_SOCKET;
    _shard_socket       = KSYS_INVALID_SOCKET;
}

void
cl_update_read()
{
    if (_connection_status == CONNSTATUS_DISCONNECTED)
        return;
    uint32 num_bytes = blocking_queue_pop_num(&_msg_queue,
        IN_BUF_SZ - _in_buf.num_bytes, _in_buf.mem + _in_buf.num_bytes);
    if (_read_packet())
        cl_disconnect();
}

void
cl_update_write()
{
}

bbuf_t
cl_send(int num_bytes)
{
}

static thread_ret_t
_main(void *args)
{
    (void)args;
    uint8   buf[MUTA_MTU];
    bbuf_t  bb;
    for (;;)
    {
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        if (net_connect(_login_socket, _login_server_address))
        {
            DEBUG_PRINTF("Failed to connect to login server, "
                "reconnecting in 2 seconds...");
            sleep_ms(2000);
            continue;
        }
        if (!interlocked_compare_exchange_int32(&_running, -1, -1))
            return 0;
        BBUF_INIT(&bb, buf, sizeof(buf));
        clmsg_pub_key_t k_msg;
        memcpy(k_msg.key, _cryptchan.pk, sizeof(_cryptchan.pk));
        clmsg_pub_key_write(&bb, &k_msg);
        if (net_send_all(_login_socket, bb.mem, bb.num_bytes) <= 0)
        {
            cl_event_t cl_event;
            cl_event.type = CL_EVENT_DISCONNECT;
            blocking_queue_push(&_event_queue, &cl_event);
            return 0;
        }
    }
    return 0;
}

static void
_read_server_info_callback(void *ctx, const char *opt, const char *val)
{
    if (!streq(opt, "hostname"))
        return;
    if (addr_init_from_str(&_login_server_address, val, DEFAULT_LOGIN_PORT))
        return;
    *(bool32*)ctx = 1;
}

static int
_read_packet()
{
    bbuf_t      bb          = BBUF_INITIALIZER(_in_buf.mem, _in_buf.num_bytes);
    int         dc          = 0;
    bool32      incomplete  = 0;
    msg_type_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= MSGTSZ && !incomplete && !dc)
    {
        BBUF_READ(&bb, msg_type_t, &msg_type);
        switch (msg_type)
        {
        case SVMSG_PUB_KEY:
        {
            svmsg_pub_key_t s;
            incomplete = svmsg_pub_key_read(&bb, &s);
            if (incomplete)
                dc = _handle_svmsg_pub_key(&s);
        }
            break;
#if 0
        case SVMSG_STREAM_HEADER:
        {
            svmsg_stream_header_t s;
            incomplete = svmsg_stream_header_read(&bb, &s);
            if (incomplete)
                dc = _handle_svmsg_stream_header(&s);
        }
            break;
        case SVMSG_CHARACTER_LIST:
        {
            svmsg_character_list_t s;
            incomplete = svmsg_character_list_read(&bb, &s);
            if (incomplete)
                dc = _handle_svmsg_character_list(&s);
        }
            break;
        case SVMSG_CREATE_CHARACTER_FAIL:
        {
            svmsg_create_character_fail_t s;
            incomplete = svmsg_create_character_fail_read(&bb, &s);
            if (incomplete)
                dc = _handle_svmsg_create_character_fail(&s);
        }
            break;
        case SVMSG_NEW_CHARACTER_CREATED:
        {
            svmsg_new_character_created_t s;
            incomplete = svmsg_new_character_created_read(&bb, &s);
            if (incomplete)
                dc = _handle_svmsg_new_character_created(&s);
        }
            break;
#endif
        }
    }
}

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s)
{
    clmsg_stream_header_t h_msg;
    if (cryptchan_cl_store_pub_key(&_cryptchan, s->key, h_msg.header))
        return 1;
    bbuf_t bb = cl_send(CLMSG_STREAM_HEADER_SZ);
    if (bb.max_bytes)
        return 2;
    if (clmsg_stream_header_write(&bb, &h_msg))
        return 3;
    DEBUG_PRINTF("Received public key.\n");
    return 0;
}
