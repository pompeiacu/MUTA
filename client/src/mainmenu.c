#include "core.h"
#include "render.h"
#include "assets.h"
#include "gui.h"
#include "sprite_context.h"
#include "audio.h"
#include "../../shared/acc_utils.h"

#define NAME_BUF_SZ         128
#define PW_BUF_SZ           128
#define PW_DISPLAY_MAX_LEN  14

enum login_fields
{
    LOGINFIELD_NONE = 0,
    LOGINFIELD_NAME,
    LOGINFIELD_PW
};

static sprite_t                 *_bg_sprite             = 0;
static anim_asset_t             *_bg_anim_asset         = 0;
static spritesheet_t            *_login_menu_ss         = 0;
static sound_asset_t            *_login_music           = 0;
static gui_tex_button_style_t   _login_menu_btn_style1;
static gui_tex_button_style_t   _login_menu_btn_style2;
static char                     _name_buf[NAME_BUF_SZ];
static char                     _pw_buf[PW_BUF_SZ];
static int                      _login_field;
static double                   _cursor_timer;
static bool32                   _show_cursor;
static bool32                   _show_login_progress_win;
static bool32                   _show_developer_options = 0;
static bool32                   _show_dc_reason_win;
static sfont_t                  *_sf_fancy;
static gui_button_style_t       _offline_shard_button_style;

static void
_try_connect();

static void
_cancel_connection_attempt();

static void
_draw_login_progress_win();

static void
_draw_shards_win();

static void
_draw_dc_reason_win();

void
main_menu_update(double dt)
{
    r_viewport(0, 0, main_window.w, main_window.h);
    r_scissor(0, 0, main_window.w, main_window.h);
    r_color(0.0f, 0.0f, 0.0f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);
    int scissor[4];
    r_get_scissor(&scissor[0], &scissor[1], &scissor[2], &scissor[3]);
    sprite_context_update(shared_sprite_ctx, dt);
    _cursor_timer += dt; /* Text cursor - change to use gui_text_input() */
    if (_cursor_timer >= 0.2f)
    {
        _show_cursor = _show_cursor ? 0 : 1;
        _cursor_timer = 0.f;
    }
    gui_begin();
    gui_font(_sf_fancy);
    gui_button_style(menu_button_style);
    /*-- Background --*/
    gui_origin(GUI_CENTER_CENTER);
    gui_texture(sprite_get_cur_tex(_bg_sprite), 0, 0, 0);
    /*-- Bottom version text --*/
    gui_origin(GUI_BOTTOM_LEFT);
    gui_textf("MUTA v. %s", 0, 20, 20, MUTA_VERSION_STR);
    /*-- Logo --*/
    gui_origin(GUI_BOTTOM_RIGHT);
    const int middle_btn_w = 166;
    if (gui_button("exit", 5, 5, middle_btn_w, 30, 0))
        core_shutdown();
    if (_show_developer_options)
    {
        if (gui_button("editor", 5, 37, middle_btn_w, 30, 0))
            core_set_screen(&editor_screen);
        if (gui_button("animator", 5, 101, middle_btn_w, 30, 0))
            core_set_screen(&animator_screen);
        if (gui_button("tile_editor", 5, 69, middle_btn_w, 30, 0))
            core_set_screen(&tile_editor_screen);
        if (gui_button("sprite_editor", 5, 133, middle_btn_w, 30, 0))
            core_set_screen(&sprite_editor_screen);
    }
    gui_origin(GUI_TOP_LEFT);
    /*-- Draw the login info entry window --*/
    gui_begin_empty_win("login info", 1069, 794, 272, 280, 0);
    char    tmp_text_buf[128];
    int     b1w = 272;
    int     b1h = 46;
    gui_tex_button_style(&_login_menu_btn_style2);
    gui_begin_empty_win("name", 0, 0, b1w, b1h, 0);
    gui_origin(GUI_CENTER_LEFT);
    if (gui_invisible_button("##name", 0,  0, b1w, b1h, 0))
        _login_field = LOGINFIELD_NAME;
    strcpy(tmp_text_buf, _name_buf);
    if (_show_cursor && _login_field == LOGINFIELD_NAME)
    {
        int len = (int)strlen(tmp_text_buf);
        if (len < MAX_ACC_NAME_LEN)
        {
            tmp_text_buf[len] = '|';
            tmp_text_buf[len + 1] = 0;
        }
    }
    gui_text(tmp_text_buf, 0, 4, 0);
    gui_end_win(); /* name */
    gui_origin(GUI_TOP_LEFT);
    gui_begin_empty_win("pw", 0, 72, b1w, b1h, 0);
    gui_origin(GUI_CENTER_LEFT);
    if (gui_invisible_button("##pw", 0, 0, b1w, b1h, 0))
        _login_field = LOGINFIELD_PW;
    int pw_len      = (int)strlen(_pw_buf);
    int pw_tmp_len  = MIN(pw_len, PW_DISPLAY_MAX_LEN);
    memset(tmp_text_buf, '*', pw_tmp_len);
    tmp_text_buf[pw_tmp_len] = 0;
    if (_show_cursor && _login_field == LOGINFIELD_PW
    && pw_tmp_len < PW_DISPLAY_MAX_LEN)
    {
        tmp_text_buf[pw_tmp_len]        = '|';
        tmp_text_buf[pw_tmp_len + 1]    = 0;
    }
    gui_text(tmp_text_buf, 0, 4, 0);
    gui_end_win(); /* pw */
    gui_origin(GUI_TOP_CENTER);
    if (gui_button("Connect", 0, 128, 128, 32, 0))
        _try_connect();
    gui_end_win(); /* Login info window */
    gui_origin(GUI_CENTER_CENTER);
    switch (c_conn_status)
    {
    case CONNSTATUS_DISCONNECTED:
        if (_show_dc_reason_win && core_dc_reason != DC_REASON_NONE)
            _draw_dc_reason_win();
        break;
    case CONNSTATUS_CONNECTING:
    case CONNSTATUS_HANDSHAKING:
    case CONNSTATUS_CONNECTED:
    case CONNSTATUS_WAITING_FOR_SHARD_LIST:
        _draw_login_progress_win();
        break;
    case CONNSTATUS_SELECTING_SHARD:
    case CONNSTATUS_WAITING_FOR_CHARACTER_LIST:
        _draw_shards_win();
        break;
    }
    gui_end();
    r_swap_buffers(&main_window);
    if (c_conn_status == CONNSTATUS_CONNECTED && c_authed)
        core_set_screen(&character_select_screen);
}

void
main_menu_open()
{
    _sf_fancy = as_get_font("metamorphous16");
    sprite_context_clear(shared_sprite_ctx);
    _bg_sprite = create_sprite(shared_sprite_ctx);
    sprite_set_loop(_bg_sprite, 1);
    _bg_anim_asset = as_claim_anim_asset_by_name("login bg", 1);
    if (_bg_anim_asset)
        sprite_play_anim(_bg_sprite, &_bg_anim_asset->anim);

    /* Button style 1 */
    _login_menu_btn_style1 = gui_create_tex_button_style();
    _login_menu_btn_style1.normal.font  = _sf_fancy;
    _login_menu_btn_style1.hovered.font = _sf_fancy;
    _login_menu_btn_style1.pressed.font = _sf_fancy;

    _offline_shard_button_style = *menu_button_style;
    uint8 color[4];
    memcpy(color, menu_button_style->normal.bg_col, 4);
    memcpy(_offline_shard_button_style.hovered.bg_col, color, 4);
    memcpy(_offline_shard_button_style.pressed.bg_col, color, 4);

    const size_t    cpy_sz = 4 * sizeof(float);
    float           *clip;

    clip = spritesheet_get_clip(_login_menu_ss, 2);
    if (clip)
        memcpy(_login_menu_btn_style1.normal.clip, clip, cpy_sz);
    clip = spritesheet_get_clip(_login_menu_ss, 0);
    if (clip)
        memcpy(_login_menu_btn_style1.hovered.clip, clip, cpy_sz);
    clip = spritesheet_get_clip(_login_menu_ss, 1);
    if (clip)
        memcpy(_login_menu_btn_style1.pressed.clip, clip, cpy_sz);

    /* Button style 2 */
    _login_menu_btn_style2 = gui_create_tex_button_style();

    clip = spritesheet_get_clip(_login_menu_ss, 3);
    if (clip)
    {
        memcpy(_login_menu_btn_style2.normal.clip,  clip, cpy_sz);
        memcpy(_login_menu_btn_style2.hovered.clip, clip, cpy_sz);
        memcpy(_login_menu_btn_style2.pressed.clip, clip, cpy_sz);
    }

    /* Clear login buffers */
    _name_buf[0]    = 0;
    _pw_buf[0]      = 0;
    _cursor_timer   = 0.f;
#ifdef _MUTA_DEBUG
    static bool32 first_open = 1;
    if (first_open && core_auto_login_name && core_auto_login_pw)
    {
        strcpy(_name_buf, core_auto_login_name);
        strcpy(_pw_buf, core_auto_login_pw);
    } else
#endif
    strcpy(_name_buf, game_cfg.account_name);
    _login_field = strlen(_name_buf) == 0 ? LOGINFIELD_NAME : LOGINFIELD_PW;
    _login_music = as_claim_sound_by_name("night1", 1);
    stop_channel(31);
    if (_login_music)
        DEBUG_PRINTFF("_login_music.loading: %u\n", _login_music->loading);
    play_music_asset_on_channel(_login_music, 31, 1.0f, 1.0f, 1);
    _show_login_progress_win    = 0;
    _show_dc_reason_win         = 1;
#ifdef _MUTA_DEBUG
    if (first_open && core_auto_login_name && core_auto_login_pw)
    {
        first_open = 0;
        _try_connect();
    }
#endif
}

void
main_menu_close()
{
    as_unclaim_anim_asset(_bg_anim_asset);
    as_unclaim_sound(_login_music);
}

void
main_menu_text_input(const char *text)
{
    int offset;

    char tmp_buf[256];
    strncpy(tmp_buf, text, 255);

    switch (_login_field)
    {
    case LOGINFIELD_NAME:
    {
        accut_strip_illegal_name_symbols(tmp_buf);
        offset = (int)strlen(_name_buf);
        strncpy(_name_buf + offset, tmp_buf, MAX_ACC_NAME_LEN - offset);
    }
        break;
    case LOGINFIELD_PW:
    {
        accut_strip_illegal_pw_symbols(tmp_buf);
        offset = (int)strlen(_pw_buf);
        strncpy(_pw_buf + offset, tmp_buf, MAX_PW_LEN - offset);
    }
        break;
    default:
        break;
    }
}

void
main_menu_keydown(int key, bool32 is_repeat)
{
    switch (key)
    {
    case KEYCODE(TAB):
    {
        switch (_login_field)
        {
        case LOGINFIELD_NAME:
            _login_field    = LOGINFIELD_PW;
            _show_cursor    = 1;
            _cursor_timer   = 0.f;
            break;
        case LOGINFIELD_PW:
            _login_field    = LOGINFIELD_NAME;
            _show_cursor    = 1;
            _cursor_timer   = 0.f;
            break;
        default:
            _login_field = LOGINFIELD_NAME; break;
        }
    }
        break;
    case KEYCODE(BACKSPACE):
    {
        char    *buf = 0;
        int     limit;
        if (_login_field == LOGINFIELD_NAME)
            {buf = _name_buf; limit = MAX_ACC_NAME_LEN - 1;} else
        if (_login_field == LOGINFIELD_PW)
            {buf = _pw_buf; limit = MAX_PW_LEN - 1;}
        if (buf)
        {
            int len = (int)strlen(buf);
            buf[CLAMP(len - 1, 0, limit)] = 0;
        }
    }
        break;
    case KEYCODE(RETURN):
    {
        if (!(_show_dc_reason_win && core_dc_reason != DC_REASON_NONE)
        && !_show_login_progress_win)
            _try_connect();
        else
        {
            _cancel_connection_attempt();
            _show_dc_reason_win = 0;
        }
    }
        break;
    case KEYCODE(ESCAPE):
    {
        if (is_repeat)
            break;
        if (!_show_dc_reason_win && !_show_login_progress_win)
            core_shutdown();
        else if (c_conn_status == CONNSTATUS_SELECTING_SHARD)
            core_disconnect_from_sv();
        else if (c_conn_status == CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
            core_cancel_select_shard();
        else
        {
            _cancel_connection_attempt();
            _show_dc_reason_win = 0;
        }
    }
        break;
    case KEYCODE(F1):
        _show_developer_options = _show_developer_options ? 0 : 1;
        break;
    }
}

void
main_menu_os_quit()
    {core_shutdown();}

static void
_try_connect()
{
    _show_login_progress_win = 1;
    int res = core_connect_to_sv(_name_buf, _pw_buf);
    if (res != 0)
    {
        char buf[128];
        sock_err_str(buf, 128);
        DEBUG_PRINTF("Failed to connect, code %d: %s.\n", res, buf);
    } else
        strncpy(game_cfg.account_name, _name_buf, MAX_ACC_NAME_LEN);
}

static void
_cancel_connection_attempt()
{
    core_disconnect_from_sv();
    _show_login_progress_win = 0;
}

static void
_draw_login_progress_win()
{
    gui_origin(GUI_CENTER_CENTER);
    gui_win_style(menu_window_style);
    gui_begin_win("##Login progress", 0, 0, 256, 128, 0);
    const char *s;
    switch (c_conn_status)
    {
        case CONNSTATUS_DISCONNECTED:   s = "Disconnected"; break;
        case CONNSTATUS_CONNECTING:     s = "Connecting";   break;
        case CONNSTATUS_HANDSHAKING:    s = "Handshaking";  break;
        case CONNSTATUS_CONNECTED:      s = "Connected";    break;
        case CONNSTATUS_WAITING_FOR_SHARD_LIST:
            s = "Waiting for shard list";
            break;
        default:
            break;
    }
    gui_text(s, 0, 0, 0);
    if (gui_button("Cancel", 0, 40, 64, 32, 0))
    {
        _cancel_connection_attempt();
        _show_dc_reason_win = 0;
    }
    gui_end_win();
}

static void
_draw_shards_win()
{
    gui_win_style(menu_window_style);
    gui_begin_win("Shards", 0, 0, 800, 600, 0);
    gui_origin(GUI_TOP_LEFT);
    gui_text("Shard selection", 0, 6, 2);
    uint32              num_shards = core_num_shards;
    shard_info_t        *si;
    gui_button_style_t  *s;
    for (uint32 i = 0; i < num_shards; ++i)
    {
        si  = &core_shards[i];
        s   = si->online ? menu_button_style : &_offline_shard_button_style;
        gui_button_style(s);
        if (gui_button(si->name, 8, 24 + i * 26, 715, 26, 0) && si->online)
            core_select_shard(i);
        gui_text(si->online ? "Online" : "Offline", 0, 728, 20 + i * 26 + 4);
    }
    if (c_conn_status == CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
    {
        gui_origin(GUI_CENTER_CENTER);
        gui_begin_win("##Shard Connect", 0, 0, 250, 180, 0);
        gui_textf("Connecting to %s...", 0, 5, 5,
            core_shards[core_selected_shard].name);
        gui_origin(GUI_BOTTOM_CENTER);
        if (gui_button("Cancel##Shard Connect", 0, 5, 100, 26, 0))
            core_cancel_select_shard();
        gui_end_win();
    }
    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Cancel##Shard Selection", 4, 4, 100, 26, 0) &&
        c_conn_status != CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
        core_disconnect_from_sv();
    gui_end_win();
    gui_set_active_win("Shards");
}

static void
_draw_dc_reason_win()
{
    gui_begin_empty_win("dc frame", 0, 0, RESOLUTION_W, RESOLUTION_H, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_win_style(menu_window_style);
    gui_begin_win("##Disconnect reason", 0, 0, 256, 128, 0);
    gui_text(core_dc_reason_str, 0, 0, 0);
    gui_text("Disconnected:", 0, 0, -gui_get_last_text_h());
    if (gui_button("OK", 0, 40, 64, 32, 0))
        _show_dc_reason_win = 0;
    gui_end_win();
    gui_end_win();
    gui_set_active_win("dc_frame");
}
