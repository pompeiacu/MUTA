#ifndef WORLD_INTERNAL_H
#define WORLD_INTERNAL_H

#include "world.h"

#define MAP_SIDE_SZ     (MAP_CHUNK_W * MAP_CHUNK_T)
#define MAP_CHUNK_SZ    (MAP_CHUNK_W * MAP_CHUNK_W * MAP_CHUNK_T)
#define INVALID_ENTITY_COMPONENT_INDEX  0xFFFFFFFF
#define MAP_TILED_W(map)                ((map)->w * MAP_CHUNK_W)
#define MAP_TILED_H(map)                ((map)->h * MAP_CHUNK_W)
#define COMPUTE_TILE_INDEX(x, y, z) \
    (((x) % MAP_CHUNK_W) * MAP_SIDE_SZ + ((y) % MAP_CHUNK_W) * MAP_CHUNK_T + \
    ((z) % MAP_CHUNK_T))
#define GET_MAP_CHUNK(map, x, y) ((map)->chunks[(y) * (map)->w + (x)])
#define IS_CHUNK_LOADED(map, cx, cy) \
    ((cx) >= (map)->cam_x && (cx) < map->cam_x + 3 && \
     (cy) >= (map)->cam_y && (cy) < map->cam_y + 3)
#define COMPUTE_CHUNK_COORDS(map, chunk, ret_x, ret_y) \
    *(ret_x) = (int)((chunk) - (map)->chunks) % (map)->w; \
    *(ret_y) = (int)((chunk) - (map)->chunks) / (map)->w;
#define COMPUTE_CACHE_PTR_COORDS(map, cache, ret_x, ret_y) \
    *(ret_x) = (int)((cache) - (map)->cache_ptrs) % 3; \
    *(ret_y) = (int)((cache) - (map)->cache_ptrs) / 3;
#define GET_MAP_CHUNK_OF_TILE(map, tile_x, tile_y) \
    ((map)->chunks[((tile_y) / MAP_CHUNK_W) * (map)->w + \
    ((tile_x) / MAP_CHUNK_W)])
#define GET_CHUNK_TILE(ch, x, y, z) \
    (ch)->file.tiles[x * (MAP_SIDE_SZ) + y * MAP_CHUNK_T + z]
#define GET_CACHE_OF_TILE_FAST(map, x, y) \
    map->cache_ptrs[(y / MAP_CHUNK_W - MIN(map->cam_y, y / MAP_CHUNK_W)) % 3 \
        * 3 + ((x / MAP_CHUNK_W - MIN(map->cam_x, x / MAP_CHUNK_W)) % 3)]
#define GET_CACHE_TILE_FAST(ch, x, y, z) \
    (ch)->file.tiles[((x) % MAP_CHUNK_W) * MAP_SIDE_SZ + ((y) % MAP_CHUNK_W) * \
        MAP_CHUNK_T + ((z) % MAP_CHUNK_T)]
#define ENTITY_HAS_COMPONENT(e, c_index) \
    ((e)->cmps[c_index] != INVALID_ENTITY_COMPONENT_INDEX)
#define ENTITY_OK(e) ((e)->flags & ENT_RESERVED)

/* Define the functions for removing and adding an entity component.
 * If calling attach_func evaluates to non-zero, attachment will fail. */
#define ENT_CMP_DEF(type, name, index, rem_func, attach_check_func, \
    attach_init_func) \
name##_item_t \
*name##_get_items(name##_t *p, uint32 *ret_num) \
    {*ret_num = darr_num(*p); return *p;} \
\
int \
entity_attach_##type(entity_t *e, type##_t *cmp) \
{ \
    type##_t *acmp = entity_attach_empty_##type(e); \
    if (!acmp) \
        return 1; \
    *acmp = *cmp; \
    return 0; \
} \
\
type##_t * \
entity_attach_empty_##type(entity_t *e) \
{ \
    if (e->cmps[index] != INVALID_ENTITY_COMPONENT_INDEX) \
        return entity_get_##type(e); \
    int r = attach_check_func(e); \
    if (r) \
    { \
        DEBUG_PRINTF("%s: attach_check_func() returned %d\n", __func__, r); \
        return 0; \
    } \
    world_t         *w      = e->w; \
    uint32          i       = darr_num(w->name); \
    name##_item_t   *item   = darr_push_empty(w->name); \
    if (item) memset(item, 0, sizeof(type##_t)); \
    item->e         = e; \
    e->cmps[index]  = i; \
    attach_init_func(&item->cmp, e); \
    return &item->cmp; \
} \
\
void \
entity_remove_##type(entity_t *e) \
{ \
    uint32 i = e->cmps[index]; \
    if (i == INVALID_ENTITY_COMPONENT_INDEX) return; \
    world_t         *w      = e->w; \
    name##_item_t   *item   = &w->name[i]; \
    name##_item_t   *last   = &w->name[darr_num(w->name) - 1]; \
    rem_func(e, item, last); \
    last->e->cmps[index] = i; \
    *item = *last; \
    darr_head(w->name)->num--; \
    e->cmps[index] = INVALID_ENTITY_COMPONENT_INDEX; \
} \
\
type##_t * \
entity_get_##type(entity_t *e) \
{ \
    if (e->cmps[index] == INVALID_ENTITY_COMPONENT_INDEX) return 0; \
    muta_assert(e->w); \
    muta_assert(e->w->name); \
    return &e->w->name[e->cmps[index]].cmp; \
} \
\
bool32 \
entity_has_##type(entity_t *e) \
    {return e->cmps[index] != INVALID_ENTITY_COMPONENT_INDEX;} \
\
entity_t * \
type##_get_entity(type##_t *c) \
{ \
    name##_item_t *item = (name##_item_t*)((char*)c - \
        offsetof(name##_item_t, cmp)); \
   return item->e; \
}

/* Handled components */
#define ENT_HANDLED_CMP_DEF(htype, utype, name, cmp_index, rem_func) \
htype##_t \
*entity_attach_##utype(entity_t *e) \
{ \
    if (e->hcmps[cmp_index]) return (htype##_t*)e->hcmps[cmp_index]; \
    world_t *w = e->w; \
 \
    uint32 i = darr_num(w->name.updatables); \
    utype##_cont_t *u = darr_push_empty(w->name.updatables); \
    if (!u) \
        return 0; \
 \
    htype##_cont_t *h = htype##_cont_pool_reserve(&w->name.handles); \
    if (!h) {darr_erase_last(w->name.updatables); return 0;} \
 \
    h->e        = e; \
    h->uindex   = i; \
    memset(&h->h, 0, sizeof(htype##_t)); \
 \
    u->h = h; \
    memset(&u->u, 0, sizeof(utype##_t)); \
 \
    e->hcmps[cmp_index] = h; \
    return &h->h; \
} \
\
utype##_t \
*entity_get_##utype(entity_t *e) \
{ \
    htype##_cont_t *h = e->hcmps[cmp_index]; \
    if (!h) return 0; \
    return &e->w->name.updatables.items[h->uindex].u; \
} \
\
utype##_t * \
htype##_get_##utype(htype##_t *h) \
{ \
    htype##_cont_t *hc = \
        (htype##_cont_t*)((char*)h - offsetof(htype##_cont_t, h)); \
    world_t *w = hc->e->w; \
    return &w->name.updatables[hc->uindex].u; \
} \
void \
entity_remove_##utype(entity_t *e) \
{ \
    htype##_cont_t *hc = e->hcmps[cmp_index]; \
    if (!hc) return; \
    world_t *w  = e->w; \
    uint32 i    = hc->uindex; \
    uint32 li   = darr_num(w->name.updatables) - 1; \
\
    utype##_cont_t *u   = &w->name.updatables[i]; \
    utype##_cont_t *lu  = &w->name.updatables[li]; \
    htype##_cont_t *ohc = lu->h; \
\
    if (hc != ohc) {rem_func(e, &hc->h, &u->u, &ohc->h, &lu->u);} \
\
    *u          = w->name.updatables[li]; \
    hc->uindex  = i; \
    darr_erase_last(w->name.updatables); \
    e->hcmps[cmp_index] = 0; \
} \
entity_t \
*utype##_get_entity(utype##_t *u) \
{ \
    utype##_cont_t *uc = \
        (utype##_cont_t*)((char*)u - offsetof(utype##_cont_t, u)); \
    return uc->h->e; \
} \
entity_t \
*htype##_get_entity(htype##_t *h) \
{ \
    htype##_cont_t *hc = \
        (htype##_cont_t*)((char*)h - offsetof(htype##_cont_t, h)); \
    return hc->e; \
}

#define ENT_STATIC_CMP_DEF(type, name, cmp_index, rem_func, attach_check_func, \
    attach_init_func) \
\
type##_t * \
entity_attach_empty_##type(entity_t *e) \
{ \
    name##_item_t *item = e->hcmps[cmp_index]; \
    if (item) return &item->cmp; \
    if (attach_check_func(e)) return 0; \
    world_t *w = e->w; \
\
    item = name##_reserve(&w->name); \
    if (!item) return 0; \
\
    item->e = e; \
    memset(&item->cmp, 0, sizeof(type##_t)); \
    e->hcmps[cmp_index] = &item->cmp; \
    attach_init_func(&item->cmp, e); \
\
    return &item->cmp; \
} \
\
void \
entity_remove_##type(entity_t *e) \
{ \
    name##_item_t *item = e->hcmps[cmp_index]; \
    if (!item) return; \
    rem_func(e, &item->cmp); \
    name##_free(&e->w->name, item); \
    e->hcmps[cmp_index] = 0; \
} \
\
type##_t * \
entity_get_##type(entity_t *e) \
{ \
    name##_item_t *item = e->hcmps[cmp_index]; \
    if (!item) return 0; \
    return &item->cmp; \
} \
\
entity_t * \
type##_get_entity(type##_t *cmp) \
{ \
    name##_item_t *item = \
        (name##_item_t*)((char*)cmp - offsetof(name##_item_t, cmp)); \
    return item->e; \
}



static inline tile_t
map_get_tile_type(map_t *map, int x, int y, int z);

#define world_get_tile_type(w, x, y, z) \
    map_get_tile_type(&(w)->map, (x), (y), (z))

#define world_get_tile_def( w, x, y, z) \
    map_get_tile_def( &(w)->map, (x), (y), (z))

static inline tile_t
map_get_tile_type(map_t *map, int x, int y, int z)
{
    if (x < 0 || x >= map->tw
    ||  y < 0 || y >= map->th
    ||  z < 0 || z >= MAP_CHUNK_T)
        return 0;

    int cx = x / MAP_CHUNK_W;
    int cy = y / MAP_CHUNK_W;

    if (IS_CHUNK_LOADED(map, cx, cy) && z >= 0 && z < MAP_CHUNK_T)
        return GET_CACHE_TILE_FAST(GET_CACHE_OF_TILE_FAST(map, cx, cy),
            x, y, z);
    return 0;
}

static inline tile_def_t *
map_get_tile_def(map_t *map, int x, int y, int z)
{
    tile_t t = map_get_tile_type(map, x, y, z);
    return t < tile_defs_num ? &tile_defs[t] : 0;
}

static inline chunk_cache_t *
map_get_cache_of_tile(map_t *map, int x, int y)
{
    if (x >= map->cam_tx && x < map->cam_tx + MAP_CHUNK_W * 3
    &&  y >= map->cam_ty && y < map->cam_ty + MAP_CHUNK_W * 3)
    {
        int index = ((y / MAP_CHUNK_W - map->cam_y) % 3) * 3 + \
            ((x / MAP_CHUNK_W - map->cam_x) % 3) % 3;
        return map->cache_ptrs[index];
    }
    return 0;
}

#define world_get_cache_of_tile(w, x, y) \
    (map_get_cache_of_tile(&(w)->map, (x), (y)))

static inline chunk_cache_t *
map_map_get_cache_of_tile_fast(map_t *map, int x, int y)
{
    int dx = x / MAP_CHUNK_W;
    int dy = y / MAP_CHUNK_W;
    return map->cache_ptrs[dy - MIN(map->cam_y, dy) % 3 * 3 + (dx % 3)];
}

static inline tile_t
map_get_tile(map_t *map, int x, int y, int z)
{
    chunk_cache_t *ch = map_get_cache_of_tile(map, x, y);
    if (!ch) return 0;
    int index = COMPUTE_TILE_INDEX(x, y, z);;
    return ch->file.tiles[index];
}

static inline tile_t *
map_get_tile_ptr(map_t *map, int x, int y, int z)
{
    chunk_cache_t *ch = map_get_cache_of_tile(map, x, y);
    if (!ch) return 0;
    int index = COMPUTE_TILE_INDEX(x, y, z);
    return &ch->file.tiles[index];
}

static inline tile_t *
map_get_tiles_of_chunk(map_t *map, map_chunk_t *chunk)
{
    int x, y;
    COMPUTE_CHUNK_COORDS(map, chunk, &x, &y);
    if (!IS_CHUNK_LOADED(map, x, y)) return 0;
    return map->cache_ptrs[(y - map->cam_y) * 3 + (x - map->cam_x)]->file.tiles;
}

#endif /* WORLD_INTERNAL_H */
