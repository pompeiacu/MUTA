/* Connection handling to the server */

#ifndef MUTA_CLIENT_H
#define MUTA_CLIENT_H

#include "../../shared/common_utils.h"

enum connection_status
{
    CONNSTATUS_DISCONNECTED,
    CONNSTATUS_CONNECTING,
    CONNSTATUS_HANDSHAKING,
    CONNSTATUS_WAITING_FOR_SHARD_LIST,
    CONNSTATUS_SELECTING_SHARD,
    CONNSTATUS_WAITING_FOR_CHARACTER_LIST,
    CONNSTATUS_CONNECTED
};

int
cl_init();

void
cl_destroy();

int
cl_connect();
/* Connect to the server defined in server_info.cfg */

void
cl_disconnect();

void
cl_update_read();

void
cl_update_write();

bbuf_t
cl_send(int num_bytes);

#endif /* MUTA_CLIENT_H */
