#ifndef MUTA_CHARACTER_create_SCREEN
#define MUTA_CHARACTER_create_SCREEN

#include "../../shared/types.h"

void
character_create_screen_update(double dt);

void
character_create_screen_open();

void
character_create_screen_close();

void
character_create_screen_text_input(const char *text);

void
character_create_screen_keydown(int key, bool32 is_repeat);

#endif
