#include <time.h>
#include "charactercreatescreen.h"
#include "assets.h"
#include "gui.h"
#include "render.h"
#include "core.h"
#include "render.h"
#include "../../shared/acc_utils.h"

#define MID_WINDOW_W 400
#define MID_WINDOW_H 200

static character_props_t    _character;
static player_race_def_t    **_race_defs;
static player_race_def_t    *_selected_race_def;
static const char           *_err_msg;
static bool32               _started_creating;

enum text_fields
{
    TEXT_FIELD_CHARACTER_NAME
};

static void
_draw_selected_options(int x, int y);

static void
_draw_local_error_window(int x, int y);

static void
_draw_creating_window(int x, int y);

static void
_draw_server_error_window(int x, int y);

static void
_create_character();

void
character_create_screen_update(double dt)
{
    r_viewport(0, 0, main_window.w, main_window.h);
    r_scissor(0, 0, main_window.w, main_window.h);
    r_color(0.0f, 0.0f, 0.0f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    gui_begin();

    gui_font(&sf_fancy);
    gui_button_style(menu_button_style);
    gui_win_style(menu_window_style);

    gui_origin(GUI_TOP_CENTER);
    gui_text("Create new character", 0, 0, 30);

    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Cancel##screen", 10, 10, 256, 64, 0))
        core_set_screen(&character_select_screen);

    gui_origin(GUI_TOP_LEFT);

    int num_race_defs = darr_num(wc_player_race_ids);
    for (int i = 0; i < num_race_defs; ++i)
        if (gui_button(_race_defs[i]->name, 10, 10 + i * 66, 256, 64, 0))
            _character.race = _race_defs[i]->id;

    gui_origin(GUI_BOTTOM_LEFT);
    if (gui_button("Male", 10, 10, 96, 96, 0))
        _character.sex = SEX_MALE;
    if (gui_button("Female", 10 + 96 + 2, 10, 96, 96, 0))
        _character.sex = SEX_FEMALE;


    gui_origin(GUI_CENTER_CENTER);
    _draw_selected_options(0, 0);

    gui_origin(GUI_BOTTOM_CENTER);

    gui_text_input(TEXT_FIELD_CHARACTER_NAME, _character.name, 0, 0, 40, 128,
        16);

    if (gui_button("Create", 0, 10, 96, 24, 0))
        _create_character();

    gui_origin(GUI_CENTER_CENTER);

    if (_err_msg)
        _draw_local_error_window(0, 0);
    else if (core_character_creation_in_progress)
        _draw_creating_window(0, 0);
    else if (_started_creating)
    {
        if (core_character_creation_error)
            _draw_server_error_window(0, 0);
        else
            core_set_screen(&character_select_screen);
    }
    else if (IS_KEY_PRESSED(KEY_SCANCODE(RETURN)))
        _create_character();

    gui_end();
    r_swap_buffers(&main_window);

    if (c_conn_status != CONNSTATUS_CONNECTED)
        core_set_screen(&main_menu_screen);
}

void
character_create_screen_open()
{
    _character.race     = 0; /* Default */
    _selected_race_def  = wc_get_player_race_def(0);
    _character.sex      = rand() % 2;
    _character.name[0]  = 0;
    darr_clear(_race_defs);
    uint32 num_race_defs = darr_num(wc_player_race_ids);
    for (uint32 i = 0; i < num_race_defs; ++i)
        darr_push(_race_defs, wc_get_player_race_def(wc_player_race_ids[i]));
    _started_creating   = 0;
    _err_msg            = 0;
}

void
character_create_screen_close()
{
}

void
character_create_screen_text_input(const char *text)
{
    if (gui_get_active_text_input() != TEXT_FIELD_CHARACTER_NAME)
        return;
    char buf[MAX_CHARACTER_NAME_LEN + 1];
    strncpy(buf, text, MAX_CHARACTER_NAME_LEN);
    for (char *c = buf; *c; ++c)
    {
        if (!accut_check_character_name_symbol(*c))
            continue;
        if (_character.name_len == MAX_CHARACTER_NAME_LEN)
            break;
        _character.name[_character.name_len++]  = *c;
        _character.name[_character.name_len]    = 0;
    }
}

void
character_create_screen_keydown(int key, bool32 is_repeat)
{
    switch (key)
    {
    case KEYCODE(BACKSPACE):
        if (_character.name_len)
            _character.name[--_character.name_len] = 0;
        break;
    }
}

static void
_draw_selected_options(int x, int y)
{
    gui_begin_win("##selectedopts", x, y, 512, 512, 0);
    gui_origin(GUI_TOP_LEFT);
    gui_textf(
        "Race: %s\n"
        "Sex: %s",
        0, 5, 5,
        _selected_race_def ? _selected_race_def->name : "none",
        _character.sex == SEX_MALE ? "male" : "female");
    gui_end_win();
}

static void
_draw_local_error_window(int x, int y)
{
    gui_begin_empty_win("local error layer", 0, 0, RESOLUTION_W, RESOLUTION_H, 0);
    gui_begin_win("##local error win", 0, 0, MID_WINDOW_W, MID_WINDOW_H, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_text(_err_msg, 0, 0, 0);
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Ok##local_error_win", 0, 10, 32, 32, 0))
        _err_msg = 0;
    gui_end_win();
    gui_end_win();
    gui_set_active_win("error layer");
}

static void
_draw_creating_window(int x, int y)
{
    gui_begin_empty_win("creating layer", 0, 0, RESOLUTION_W, RESOLUTION_H, 0);
    gui_begin_win("##creating win", 0, 0, MID_WINDOW_W, MID_WINDOW_H, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_text("Creating character...", 0, 0, 0);
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Cancel##progress", 0, 10, 64, 24, 0))
    {
        core_cancel_character_creation();
        _started_creating = 0;
    }
    gui_end_win();
    gui_end_win();
}

static void
_draw_server_error_window(int x, int y)
{
    gui_begin_empty_win("server error layer", 0, 0, RESOLUTION_W, RESOLUTION_H, 0);
    gui_begin_win("##server error win", 0, 0, MID_WINDOW_W, MID_WINDOW_H, 0);
    gui_origin(GUI_CENTER_CENTER);
    gui_textf("Character creation failed: %s.", 0, 0, 0,
        character_creation_fail_to_str(core_character_creation_error));
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Ok##server_error_win", 0, 10, 64, 24, 0))
    {
        core_cancel_character_creation();
        _started_creating = 0;
    }
    gui_end_win();
    gui_end_win();
}

static void
_create_character()
{
    if (_character.name_len < MIN_CHARACTER_NAME_LEN)
    {
        _err_msg = "Character name is too short.";
        return;
    }
    if (_character.name_len > MAX_CHARACTER_NAME_LEN)
    {
        _err_msg = "Character name is too short.";
        return;
    }
    if (core_send_create_character_msg_to_sv(&_character))
    {
        _err_msg = "Failed to contact server.";
        return;
    }
    _started_creating = 1;
}
