#include <inttypes.h>
#include "../../shared/common_utils.h"
#include "../../shared/packets.h"
#include "core.h"

static int
_handle_svmsg_global_chat_broadcast(svmsg_global_chat_broadcast_t *s);

static int
_handle_svmsg_walk_creature(svmsg_walk_creature_t *s);

static int
_handle_svmsg_walk_player_character(svmsg_walk_player_character_t *s);

static int
_handle_svmsg_new_creature(svmsg_new_creature_t *s);

static int
_handle_svmsg_new_player_character(svmsg_new_player_character_t *s);

static int
_handle_svmsg_remove_creature(svmsg_remove_creature_t *s);

static int
_handle_svmsg_remove_player_character(svmsg_remove_player_character_t *s);

static int
_handle_svmsg_player_character_dir(svmsg_player_character_dir_t *s);

static int
_handle_svmsg_new_dynamic_obj(svmsg_new_dynamic_obj_t *s);

static int
_handle_svmsg_remove_dynamic_obj(svmsg_remove_dynamic_obj_t *s);

static int
_handle_svmsg_player_player_emote(svmsg_player_player_emote_t *s);

static int
_handle_svmsg_update_player_position(svmsg_update_player_position_t *s);

static int
_handle_svmsg_creature_dir(svmsg_creature_dir_t *s);

int
gs_read_packet(int type, bbuf_t *bb)
{
    if (!_in_session)
    {
        DEBUG_PRINTFF("not in session!\n");
        return GS_PACKET_DISCONNECT;
    }

    int         dc          = 0;
    int         incomplete  = 0;

    #define HANDLE_MSG(upper_, lower_) \
        case upper_: \
        { \
            lower_##_t s; \
            incomplete = lower_##_read(bb, &s); \
            if (!incomplete) dc = _handle_##lower_(&s); \
        } \
            break;

    switch (type)
    {
    HANDLE_MSG(SVMSG_GLOBAL_CHAT_BROADCAST, svmsg_global_chat_broadcast);
    HANDLE_MSG(SVMSG_WALK_PLAYER_CHARACTER, svmsg_walk_player_character);
    HANDLE_MSG(SVMSG_WALK_CREATURE, svmsg_walk_creature);
    HANDLE_MSG(SVMSG_NEW_CREATURE, svmsg_new_creature);
    HANDLE_MSG(SVMSG_NEW_PLAYER_CHARACTER, svmsg_new_player_character);
    HANDLE_MSG(SVMSG_REMOVE_CREATURE, svmsg_remove_creature);
    HANDLE_MSG(SVMSG_REMOVE_PLAYER_CHARACTER, svmsg_remove_player_character);
    HANDLE_MSG(SVMSG_PLAYER_CHARACTER_DIR, svmsg_player_character_dir);
    HANDLE_MSG(SVMSG_NEW_DYNAMIC_OBJ, svmsg_new_dynamic_obj);
    HANDLE_MSG(SVMSG_REMOVE_DYNAMIC_OBJ, svmsg_remove_dynamic_obj);
    HANDLE_MSG(SVMSG_PLAYER_PLAYER_EMOTE, svmsg_player_player_emote);
    HANDLE_MSG(SVMSG_UPDATE_PLAYER_POSITION, svmsg_update_player_position);
    HANDLE_MSG(SVMSG_CREATURE_DIR, svmsg_creature_dir);
    default:
        DEBUG_PRINTFF("undefined message %d.\n", (int)type);
        return GS_PACKET_UNDEFINED_MSG;
    }

    if (dc || incomplete < 0)
    {
        DEBUG_PRINTFF("illegal packet: incomplete %d, dc %d.\n", incomplete,
            dc);
        return GS_PACKET_DISCONNECT;
    }
    if (incomplete)
    {
        DEBUG_PRINTFF("incomplete packet.\n");
        return GS_PACKET_INCOMPLETE;
    }

    #undef HANDLE_MSG
    return GS_PACKET_OK;
}

static int
_handle_svmsg_remove_creature(svmsg_remove_creature_t *s)
{
    entity_t *e = world_get_creature_by_id(&_world, s->id);
    if (!e)
    {
        DEBUG_PRINTFF("creature not found!\n");
        return 0;
    }
    world_despawn_creature(e);
    return 0;
}

static int
_handle_svmsg_remove_player_character(svmsg_remove_player_character_t *s)
{
    entity_t *e = world_get_player_by_id(&_world, s->id);
    if (!e)
    {
        DEBUG_PRINTFF("player %" PRIu64 " not found\n", s->id);
        return 0;
    }
    world_despawn_player(&_world, e);
    return 0;
}

static int
_handle_svmsg_player_character_dir(svmsg_player_character_dir_t *s)
    {world_set_player_dir_by_id(&_world, s->id, s->dir); return 0;}

static int
_handle_svmsg_new_creature(svmsg_new_creature_t *s)
{
    entity_t *e = world_get_creature_by_id(&_world, s->id);
    if (e)
        world_despawn_creature(e);
    e = world_spawn_creature(&_world, s->id, s->type_id, s->x, s->y, s->z,
        s->dir, s->flags);
    muta_assert(e);
    return 0;
}

static int
_handle_svmsg_new_player_character(svmsg_new_player_character_t *s)
{
    char buf[256];
    memcpy(buf, s->name, s->name_len);
    buf[s->name_len] = 0;
    DEBUG_PRINTFF("new player %" PRIu64 ", name %s.\n", s->id, buf);
    entity_t *e = world_get_player_by_id(&_world, s->id);
    if (e)
        world_despawn_player_by_id(&_world, s->id);
    e = world_spawn_player(&_world, s->id, buf, s->x, s->y, s->z, s->dir,
        s->type_id, s->sex);
    return 0;
}

static int
_handle_svmsg_walk_creature(svmsg_walk_creature_t *s)
{
    entity_t *e = world_get_creature_by_id(&_world, s->id);
    if (!e)
        return 0;
    entity_walk_to(e, s->x, s->y, s->z);
    return 0;
}

static int
_handle_svmsg_walk_player_character(svmsg_walk_player_character_t *s)
{
    entity_t *e = world_get_player_by_id(&_world, s->id);
    if (!e)
        return 0;
    entity_walk_to(e, s->x, s->y, s->z);
    return 0;
}

static int
_handle_svmsg_global_chat_broadcast(svmsg_global_chat_broadcast_t *s)
{
    DEBUG_PRINTFF("please implement msg validity checks!\n");
    uint32 num_entries  = _chat_cache.num_entries;
    uint32 first_index  = _chat_cache.first_index;
    if (num_entries == MAX_CHAT_ENTRIES_PER_FRAME)
    {
        num_entries--;
        first_index = (first_index + 1) % MAX_CHAT_ENTRIES_PER_FRAME;
    }
    uint32 index = (first_index + num_entries) % MAX_CHAT_ENTRIES_PER_FRAME;
    chat_entry_t *entry = &_chat_cache.entries[index];
    entry->type = CHAT_MSG_GLOBAL_BROADCAST;
    memcpy(entry->sender, s->name, s->name_len);
    memcpy(entry->msg, s->msg, s->msg_len);
    entry->sender[s->name_len]  = 0;
    entry->msg[s->msg_len]      = 0;
    _chat_cache.num_entries     = num_entries + 1;
    _chat_cache.first_index     = first_index;
    return 0;
}

static int
_handle_svmsg_new_dynamic_obj(svmsg_new_dynamic_obj_t *s)
{
    DEBUG_PRINTFF("### NEW DYNAMIC OBJ: type %u, %d.%d.%hd",
        s->type_id, s->x, s->y, s->z);
    return 0;
}

static int
_handle_svmsg_remove_dynamic_obj(svmsg_remove_dynamic_obj_t *s)
{
    IMPLEMENTME();
    return 0;
}

static int
_handle_svmsg_player_player_emote(svmsg_player_player_emote_t *s)
{
    const char *pname = _unk_name_str;
    const char *tname = _unk_name_str;

    entity_t *p = _player_entity;
    entity_t *t = world_get_player_by_id(&_world, s->player_id);
    if (t) pname = t->type_data.player.name;
    if (s->have_target && (t = world_get_player_by_id(&_world, s->target_id)))
        tname = t->type_data.player.name;

    const char *str;

    if (p && s->player_id == p->type_data.player.id) /* User a 'me' form */
    {
        if (s->have_target && t && t->type_data.player.id != s->player_id)
            str = emote_get_me_targeted_str(s->emote_id);
        else
            str = emote_get_me_non_targeted_str(s->emote_id);
    } else
    {
        if (s->have_target && t && t->type_data.player.id != s->player_id)
            str = emote_get_targeted_str(s->emote_id);
        else
            str = emote_get_non_targeted_str(s->emote_id);
    }
    if (!str) return 1; /* Invalid emote */

#if 0
    char *text = stack_alloc(chat_log.line_len);
    char *dst = text;

    for (int i = 0; i < chat_log.line_len - 1 && str[i]; ++i)
    {
        if (str[i] == '%')
        {
            int max_len = chat_log.line_len - (int)(dst - text) - 1;
            if (str[i + 1] == 'p')
            {
                strncpy(dst, pname, max_len);
                dst += strlen(pname);
                i++;
                continue;
            } else
            if (str[i + 1] == 't')
            {
                strncpy(dst, tname, max_len);
                dst += strlen(tname);
                i++;
                continue;
            }
        }
        *(dst++) = str[i];
    }
    *dst = 0;
#endif
    return 0;
}

static int
_handle_svmsg_update_player_position(svmsg_update_player_position_t *s)
{
    entity_t *e = world_get_player_by_id(&_world, s->id);
    if (!e)
    {
        DEBUG_PRINTF("%s: player not found.\n", __func__);
        return 0;
    }
    player_set_pos(e, s->x, s->y, s->z);
    return 0;
}

static int
_handle_svmsg_creature_dir(svmsg_creature_dir_t *s)
{
    entity_t *e = world_get_creature_by_id(&_world, s->id);
    if (!e)
    {
        DEBUG_PRINTFF("creature %" PRIu64 " not found.\n", s->id);
        return 0;
    }
    entity_set_dir(e, s->dir);
    return 0;
}
