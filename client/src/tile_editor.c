#include "tile_editor.h"
#include "core.h"
#include "render.h"
#include "gui.h"
#include "world.h"
#include "assets.h"
#include "../../shared/containers.h"

#define INPUT_FIELD_LIMIT 128

typedef struct editor_tile_t    editor_tile_t;
typedef editor_tile_t           editor_tile_darr_t;

struct editor_tile_t
{
    char            name[64];
    bool32          passthrough;
    enum ramp_t     ramp;
    float           offset_x, offset_y;
    float           clip[4];
};

enum gui_text_input_field_t
{
    CHOP_WIDTH = 0,
    CHOP_HEIGHT,
    CHOP_SPACE,
    CLIP_2,
    CLIP_3,
    CLIP_0,
    CLIP_1,
    NAME,
    TILE_WIDTH,
    TILE_HEIGHT,
    TOP_PIXEL,
    PATH
};

static char		_save_path[256];
static char		_tex_path[256];
static char		_auto_chop_width_buf[INPUT_FIELD_LIMIT]		= { 0 };
static char		_auto_chop_height_buf[INPUT_FIELD_LIMIT]	= { 0 };
static char		_auto_chop_space_buf[INPUT_FIELD_LIMIT]		= { 0 };
static char		_clip2_buf[INPUT_FIELD_LIMIT]				= { 0 };
static char		_clip3_buf[INPUT_FIELD_LIMIT]				= { 0 };
static char		_clip0_buf[INPUT_FIELD_LIMIT]				= { 0 };
static char		_clip1_buf[INPUT_FIELD_LIMIT]				= { 0 };
static char		_name_buf[INPUT_FIELD_LIMIT]				= { 0 };
static char		_tile_width_buf[INPUT_FIELD_LIMIT]			= { 0 };
static char		_tile_height_buf[INPUT_FIELD_LIMIT]			= { 0 };
static char		_tile_top_pixel_buf[INPUT_FIELD_LIMIT]		= { 0 };
static char		_save_path_buf[INPUT_FIELD_LIMIT]			= { 0 };
static char		*_input_buf									= 0;

static int		_input_buf_offset							= 0;
static int		_selected_tile;
static int		_tile_selector;
static int		_draw_tile_id;
static int		_tile_win_x, _tile_win_y;
static int		_rect_x, _rect_y, _rect_w, _rect_h;
static int		_viewport[4];
static int		_chop_width, _chop_height, _chop_spacing;
static int		_manual_clip_win_id;
static int		_click_coord_x, _click_coord_y;
static int		_tile_indic;
static int		_min_chop_value;
static int		_confirm_value;
static int		_tile_width;
static int		_tile_height;
static int		_tile_top_pixel_height;

static float	_coord_space[4];

static uint8	black[4];
static uint32	win_tiles_id;
static bool32	_passthrough_toggle;
static bool32	_auto_chop_win;
static bool32	_manual_clip_win;
static bool32	_can_write;
static bool32	_front_tile_toggle;
static bool32	_middle_tile_toggle;
static bool32	_mouse_left_pressed;
static bool32	_error_message;
static bool32	_text_allowed;
static bool32   _help_window_toggle;
static bool32	_tile_sheet;
static bool32	_horizontal_toggle;
static bool32	_sheet_dragged;
static bool32	_png_not_dragged;

static tex_asset_t *_arrow_pointer;
static tex_asset_t *_tile_sheet_tex;

static editor_tile_darr_t *editor_tile_defs;

static void
_load_tiles();

static const char*
_ramp_to_text(int ramp_id);

static void
_draw_rect(int x, int y, int w, int h, uint8 col[4]);

static int
_read_tile_defs(const char *fp);

static int
_save_tile_defs(const char *fp);

static void
_auto_chop();

static void
_delete_clip();

static void
_help_window();

static void
_error_message_window(char *txt);

static void
_confirm_action(char *txt, void(*yes_answer)());

static void
_main_menu();

static void
_insert_path_to_save();

void
tile_editor_update(double dt)
{
    static double held_timer;

    gui_begin();
    gui_font(&sf_default);

    core_compute_target_viewport(_viewport);
    r_viewport(_viewport[0], _viewport[1], _viewport[2], _viewport[3]);
    r_scissor(_viewport[0], _viewport[1], _viewport[2], _viewport[3]);
    r_color(0.2f, 0.3f, 0.8f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    _coord_space[0] = 0;
    _coord_space[1] = 0;
    _coord_space[2] = RESOLUTION_W;
    _coord_space[3] = RESOLUTION_H;
    sb_begin(0, _coord_space);
    char buf[256];

    if (!_help_window_toggle && _sheet_dragged)
    {
        if (_auto_chop_win)
        {
            /* AUTO CLIPPING */
            gui_begin_win("clipping", 300, RESOLUTION_H - 200, 300, 150, 0);

            gui_text("WIDTH: ", 0, 10, 20);

            if (gui_text_input(CHOP_WIDTH, _auto_chop_width_buf, 0, 50, 20,
                50, 20)) _input_buf = _auto_chop_width_buf;

            gui_text("HEIGHT: ", 0, 10, 64);

            if (gui_text_input(CHOP_HEIGHT, _auto_chop_height_buf, 0, 50, 64,
                50, 20)) _input_buf = _auto_chop_height_buf;

            gui_text("SPACE: ", 0, 10, 108);
            if (gui_text_input(CHOP_SPACE, _auto_chop_space_buf, 0, 50, 108,
                50, 20)) _input_buf = _auto_chop_space_buf;

            if (_horizontal_toggle)
                stbsp_sprintf(buf, "HORIZONTAL WISE");
            else
                stbsp_sprintf(buf, "VERTICAL WISE");

            if (gui_button(buf, 120, 20, 128, 36, 0))
                _horizontal_toggle = _horizontal_toggle ? 0 : 1;

            if (gui_button("CHOP", 120, 70, 128, 36, 0))
            {
                _input_buf = 0;
                _chop_width = atoi(_auto_chop_width_buf);
                _chop_height = atoi(_auto_chop_height_buf);
                _chop_spacing = atoi(_auto_chop_space_buf);

                if (_chop_width >= _min_chop_value &&
                    _chop_height >= _min_chop_value)
                {
                    _auto_chop();
                    _auto_chop_win = 0;
                    _manual_clip_win = 1;
                    _error_message = 0;
                }
                else
                {
                    _error_message = 1;
                }
            }
            gui_end_win();
        }

        gui_origin(GUI_TOP_LEFT);
        
        /* TILE INFORMATION */

        _manual_clip_win_id = gui_begin_empty_win("Tile inffo",
            RESOLUTION_W - 250, 10, 250, 200, 0);

        if (gui_get_active_win_id() == _manual_clip_win_id)
            _can_write = 1;

        gui_text("Tile width: ", 0, 0, 10);
        if (gui_text_input(TILE_WIDTH, _tile_width_buf, 0, 100, 10, 50, 20))
            _input_buf = _tile_width_buf;
        gui_text("Tile height: ", 0, 0, 40);
        if (gui_text_input(TILE_HEIGHT, _tile_height_buf, 0, 100, 40, 50, 20))
            _input_buf = _tile_height_buf;
        gui_text("Tile top pixel height: ", 0, 0, 70);
        if (gui_text_input(TOP_PIXEL, _tile_top_pixel_buf, 0, 100, 70, 50, 20))
            _input_buf = _tile_top_pixel_buf;

        if (gui_button("SET VALUES", 175, 35, 70, 30, 0))
        {
            _tile_width = atoi(_tile_width_buf);
            _tile_height = atoi(_tile_height_buf);
            _tile_top_pixel_height = atoi(_tile_top_pixel_buf);
        }

        gui_end_win();

        stbsp_sprintf(buf, "PASSTHROUGH TOGGLE: %d",
            editor_tile_defs[_selected_tile].passthrough);

        if (gui_button(buf, RESOLUTION_W - 200, 240, 150, 50, 0))
            editor_tile_defs[_selected_tile].passthrough =
            editor_tile_defs[_selected_tile].passthrough ? 0 : 1;

        if ( gui_button(_ramp_to_text(editor_tile_defs[_selected_tile].ramp),
            RESOLUTION_W - 200, 300, 150, 50, 1))
        {
                ++editor_tile_defs[_selected_tile].ramp;
                if (editor_tile_defs[_selected_tile].ramp > 8)
                    editor_tile_defs[_selected_tile].ramp = 0;
        }

        if (gui_button("x+", 180, -65, 25, 25, 0) || (held_timer >= 0.25
            && gui_repeat_invisible_button("x+", 180, -65, 25, 25, 0)))

        if (gui_button("DELETE CLIP", RESOLUTION_W - 200, 680, 100, 50, 0))
        {
            _delete_clip();
        }

        if (gui_button("CREATE NEW CLIP", RESOLUTION_W - 200, 740, 100, 50, 0))
        {
            darr_push_empty(editor_tile_defs);
            _selected_tile = darr_num(editor_tile_defs) - 1;
            _tile_selector = darr_num(editor_tile_defs) - 1;
        }

        if (gui_button("AUTO CHOP", RESOLUTION_W - 200, 800, 100, 50, 0))
        {
            strcpy(_auto_chop_width_buf, "");
            strcpy(_auto_chop_height_buf, "");
            strcpy(_auto_chop_space_buf, "");
            _auto_chop_win = 1;
        }
        if (gui_button("SAVE", RESOLUTION_W - 200, 860, 100, 50, 0))
        {
            _confirm_value = 2;
            strcpy(_save_path_buf, _save_path);
        }
        if (gui_button("EXIT", RESOLUTION_W - 200, 920, 100, 50, 0))
            _confirm_value = 1;

        _load_tiles();

        if (_tile_sheet && !_help_window_toggle)
        {
            gui_origin(GUI_TOP_LEFT);
            win_tiles_id = gui_begin_empty_win("tiles", 260, 0,
                RESOLUTION_W - 560, RESOLUTION_H - 255, 0);
            gui_origin(GUI_TOP_LEFT);

            gui_texture(&_tile_sheet_tex->tex, 0, _tile_win_x, _tile_win_y);

            _rect_x = (int)editor_tile_defs[_selected_tile].clip[0];
            _rect_y = (int)editor_tile_defs[_selected_tile].clip[1];
            _rect_w = (int)editor_tile_defs[_selected_tile].clip[2]
                - _rect_x;
            _rect_h = (int)editor_tile_defs[_selected_tile].clip[3]
                - _rect_y;
            _draw_rect(_rect_x + _tile_win_x, _rect_y + _tile_win_y,
                _rect_w, _rect_h, black);

            if (_error_message)
                _error_message_window(buf);

            if (_confirm_value != 0)
            {
                switch (_confirm_value)
                {
                case 1:
                {
                    _confirm_action("Are you sure you wan't to exit?",
                        _main_menu);
                } break;
                case 2:
                {
                    _insert_path_to_save();
                } break;
                }
            }
            gui_end_win();
        }

        if (!_can_write)
        {
            stbsp_sprintf(buf, "%s",
                editor_tile_defs[_selected_tile].name);
            strcpy(_name_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_tile_defs[_selected_tile].clip[0]);
            strcpy(_clip0_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_tile_defs[_selected_tile].clip[1]);
            strcpy(_clip1_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_tile_defs[_selected_tile].clip[2]);
            strcpy(_clip2_buf, buf);
            stbsp_sprintf(buf, "%d",
                (int)editor_tile_defs[_selected_tile].clip[3]);
            strcpy(_clip3_buf, buf);
            stbsp_sprintf(buf, "%d", _tile_width);
            strcpy(_tile_width_buf, buf);
            stbsp_sprintf(buf, "%d", _tile_height);
            strcpy(_tile_height_buf, buf);
            stbsp_sprintf(buf, "%d", _tile_top_pixel_height);
            strcpy(_tile_top_pixel_buf, buf);
        }

        /* DRAW TILES SO OFFSET CAN BE SET */
        gui_origin(GUI_CENTER_CENTER);
        gui_begin_empty_win("SET OFFSET", 0, 400, 600, 300, 0);

        int x_pos = 130;
        int y_pos = 100;

        if (gui_button("TILETYPE TO DRAW", 160, 20, 100, 30, 0))
            _draw_tile_id = _selected_tile;

        if (gui_button("TOGGLE FRONT TILES", 160, 60, 100, 30, 0))
            _front_tile_toggle = _front_tile_toggle ? 0 : 1;

        if (gui_button("TOGGLE MIDDLE TILE", 160, 100, 100, 30, 0))
            _middle_tile_toggle = _middle_tile_toggle ? 0 : 1;

        stbsp_sprintf(buf, "OFFSET: %i , %i",
            (int)editor_tile_defs[_selected_tile].offset_x,
            (int)editor_tile_defs[_selected_tile].offset_y);

        gui_text(buf, 0, 160, -100);
        gui_text("X: ", 0, 120, -60);

        if (gui_button("x-", 140, -65, 25, 25, 0) || (held_timer >= 0.25
            && gui_repeat_invisible_button("x-", 140, -65, 25, 25, 0)))
            --editor_tile_defs[_selected_tile].offset_x;
        if (gui_button("x+", 180, -65, 25, 25, 0) || (held_timer >= 0.25
            && gui_repeat_invisible_button("x+", 180, -65, 25, 25, 0)))
            ++editor_tile_defs[_selected_tile].offset_x;

        gui_text("Y: ", 0, 120, -25);
        if (gui_button("y-", 140, -30, 25, 25, 0) || (held_timer >= 0.25
            && gui_repeat_invisible_button("y-", 140, -30, 25, 25, 0)))
            --editor_tile_defs[_selected_tile].offset_y;
        if (gui_button("y+", 180, -30, 25, 25, 0) || (held_timer >= 0.25
            && gui_repeat_invisible_button("y+", 180, -30, 25, 25, 0)))
            ++editor_tile_defs[_selected_tile].offset_y;

        gui_origin(GUI_TOP_LEFT);

        gui_texture(&_tile_sheet_tex->tex,
            editor_tile_defs[_draw_tile_id].clip,
            x_pos, y_pos - tile_px_h / 2);

        gui_texture(&_tile_sheet_tex->tex,
            editor_tile_defs[_draw_tile_id].clip,
            x_pos + tile_px_w / 2, y_pos - tile_px_h / 4);

        gui_texture(&_tile_sheet_tex->tex,
            editor_tile_defs[_draw_tile_id].clip,
            x_pos + tile_px_w, y_pos);

        gui_texture(&_tile_sheet_tex->tex,
            editor_tile_defs[_draw_tile_id].clip,
            x_pos - tile_px_w / 2, y_pos - tile_px_h / 4);

        if (_middle_tile_toggle)
            gui_texture(&_tile_sheet_tex->tex,
                editor_tile_defs[_draw_tile_id].clip, x_pos, y_pos);

        gui_texture(&_tile_sheet_tex->tex,
            editor_tile_defs[_draw_tile_id].clip,
            x_pos - tile_px_w, y_pos);

        gui_texture(&_tile_sheet_tex->tex,
            editor_tile_defs[_selected_tile].clip,
            x_pos + (int)editor_tile_defs[_selected_tile].offset_x,
            y_pos + (int)editor_tile_defs[_selected_tile].offset_y);

        if (_front_tile_toggle)
        {
            gui_texture(&_tile_sheet_tex->tex,
                editor_tile_defs[_draw_tile_id].clip,
                x_pos + tile_px_w / 2, y_pos + tile_px_h / 4);
            gui_texture(&_tile_sheet_tex->tex,
                editor_tile_defs[_draw_tile_id].clip,
                x_pos - tile_px_w / 2, y_pos + tile_px_h / 4);
            gui_texture(&_tile_sheet_tex->tex,
                editor_tile_defs[_draw_tile_id].clip,
                x_pos, y_pos + tile_px_h / 2);
        }
        gui_end_win();

        /* SET MANUALLY CLIP */

        if (_manual_clip_win)
        {
            gui_origin(GUI_TOP_LEFT);
            _manual_clip_win_id = gui_begin_empty_win("manual clip",
                RESOLUTION_W - 300, RESOLUTION_H / 3, 300, 210, 0);

            if (gui_get_active_win_id() == _manual_clip_win_id)
                _can_write = 1;

            gui_text("CLIP[0] : ", 0, 10, 20);
            gui_text("CLIP[1] : ", 0, 10, 64);
            gui_text("CLIP[2] : ", 0, 10, 108);
            gui_text("CLIP[3] : ", 0, 10, 152);
            gui_text("NAME ", 0, 130, 20);

            if (gui_text_input(NAME, _name_buf, 0, 130, 40, 100, 20))
            {
                _text_allowed = 1;
                _input_buf = _name_buf;
            }
            if (gui_text_input(CLIP_0, _clip0_buf, 0, 55, 20, 50, 20))
            {
                _text_allowed = 0;
                _input_buf = _clip0_buf;
            }
            if (gui_text_input(CLIP_1, _clip1_buf, 0, 55, 64, 50, 20))
            {
                _text_allowed = 0;
                _input_buf = _clip1_buf;
            }
            if (gui_text_input(CLIP_2, _clip2_buf, 0, 55, 108, 50, 20))
            {
                _text_allowed = 0;
                _input_buf = _clip2_buf;
            }
            if (gui_text_input(CLIP_3, _clip3_buf, 0, 55, 152, 50, 20))
            {
                _text_allowed = 0;
                _input_buf = _clip3_buf;
            }
            if (gui_tex_button("SET CLIP", 120, 100, 128, 36, 0))
            {
                _can_write = 0;
                _input_buf = 0;
                strcpy(editor_tile_defs[_selected_tile].name, _name_buf);

                editor_tile_defs[_selected_tile].clip[0] =
                    (float)atoi(_clip0_buf);
                editor_tile_defs[_selected_tile].clip[1] =
                    (float)atoi(_clip1_buf);
                editor_tile_defs[_selected_tile].clip[2] =
                    (float)atoi(_clip2_buf);
                editor_tile_defs[_selected_tile].clip[3] =
                    (float)atoi(_clip3_buf);
            }
            gui_end_win();
        }
    }
    else
    {
        gui_origin(GUI_CENTER_CENTER);
        if(!_png_not_dragged)
            gui_text_s("DRAG TILE SHEET OR TILE DATA FILE TO HERE", 0, 0, 0, 2.0f);
        else
            gui_text_s("WRONG FILE TYPE DRAGGED. DRAG .png or .dat FILE",
                0, 0, 0, 2.0f);
    }

    if (_help_window_toggle)
        _help_window();
    sb_end();
    gui_end();

    if (BUTTON_DOWN(BUTTON_BIT(BUTTON_LEFT)))
    {
        if (gui_is_any_element_pressed())
        {
            if (gui_get_active_win_id() == win_tiles_id)
            {
                static int anchor_x, anchor_y;

                if (!_mouse_left_pressed)
                {
                    _click_coord_x = _tile_win_x;
                    _click_coord_y = _tile_win_y;
                    anchor_x = mouse_state.x;
                    anchor_y = mouse_state.y;
                    _mouse_left_pressed = 1;
                }
                _tile_win_x = mouse_state.x - (anchor_x - _click_coord_x);
                _tile_win_y = mouse_state.y - (anchor_y - _click_coord_y);
            }
        }
        if (gui_get_active_win_id() != _manual_clip_win_id)
        {
            _text_allowed	= 0;
            _can_write		= 0;
        }
    }

    if (!BUTTON_DOWN(BUTTON_BIT(BUTTON_LEFT)))
    {
        if (_mouse_left_pressed)
        {
            _mouse_left_pressed = 0;
        }
    }

    if (gui_is_a_button_pressed())
        held_timer += dt;
    else
        held_timer = 0;
    r_swap_buffers(&main_window);
}

void
tile_editor_open()
{
    strcpy(_auto_chop_width_buf, "");
    strcpy(_auto_chop_height_buf, "");
    strcpy(_auto_chop_space_buf, "");



    /* Ints */
    _selected_tile			= 1;
    _tile_selector			= 1;
    _draw_tile_id			= 1;
    _tile_win_x				= 2;
    _tile_win_y				= 2;
    _chop_width				= 0;
    _chop_height			= 0;
    _tile_indic				= 0;
    _min_chop_value			= 8;
    _confirm_value			= 0;
    _horizontal_toggle		= 1;


    /* Bools */
    _passthrough_toggle		= 0;
    _auto_chop_win			= 0;
    _manual_clip_win		= 1;
    _can_write				= 0;
    _front_tile_toggle		= 0;
    _middle_tile_toggle		= 0;
    _mouse_left_pressed		= 0;
    _error_message			= 0;
    _text_allowed			= 0;
    _help_window_toggle		= 0;
    _tile_sheet				= 1;
    _sheet_dragged			= 0;
    _png_not_dragged		= 0;

    black[0]				= 0;
    black[1]				= 0;
    black[2]				= 0;
    black[3]				= 255;

    int ret					= 0;

    strcpy(_save_path, "muta-data/common/editor_tiles.dat");
    _arrow_pointer = as_claim_tex_by_name("green isometric direction arrow", 1);

    darr_reserve(editor_tile_defs, 256);
    darr_push_empty(editor_tile_defs);

    ret = _read_tile_defs(_save_path);
    if (ret != 0)
        DEBUG_PRINTFF("Load failed. Error: %i\n", ret);
}

void
tile_editor_close()
    {darr_free(editor_tile_defs);}

void
tile_editor_keydown(int key, bool32 is_repeat)
{
    switch (key)
    {
    case KEYCODE(ESCAPE):
    {
        if (!_help_window_toggle || !_can_write)
            _confirm_value = 1;
        if(!_sheet_dragged)
            core_set_screen(&main_menu_screen);
    } break;
    case KEYCODE(DELETE):
    {
        if(!_can_write)
            _delete_clip();
    } break;
    case KEYCODE(1):
    {
    }
    case KEYCODE(BACKSPACE):
    {
        if (_can_write || _auto_chop_win)
        {
            _input_buf_offset = (int)strlen(_input_buf);
            if (_input_buf_offset > 0)
                _input_buf[_input_buf_offset - 1] = 0;
        }
    } break;
    case KEYCODE(a):
    {
        if (!_can_write)
        {
            --editor_tile_defs[_selected_tile].clip[0];
            --editor_tile_defs[_selected_tile].clip[2];
        }
    }
    break;
    case KEYCODE(d):
    {
        if (!_can_write)
        {
            ++editor_tile_defs[_selected_tile].clip[0];
            ++editor_tile_defs[_selected_tile].clip[2];
        }
    } break;
    case KEYCODE(w):
    {
        if (!_can_write)
        {
            --editor_tile_defs[_selected_tile].clip[1];
            --editor_tile_defs[_selected_tile].clip[3];
        }
    } break;
    case KEYCODE(s):
    {
        if (!_can_write)
        {
            ++editor_tile_defs[_selected_tile].clip[1];
            ++editor_tile_defs[_selected_tile].clip[3];
        }

    } break;
    case KEYCODE(e):
    {

    } break;
    case KEYCODE(q):
    {

    } break;
    case KEYCODE(h):
    {
        if(!_can_write)
        _help_window_toggle = _help_window_toggle ? 0 : 1;
    } break;
    case KEYCODE(RIGHT):
    {
        if (!_can_write)
            ++editor_tile_defs[_selected_tile].clip[2];
    } break;
    case KEYCODE(LEFT):
    {
        if (!_can_write)
            --editor_tile_defs[_selected_tile].clip[2];
    } break;
    case KEYCODE(UP):
    {
        if (!_can_write)
            --editor_tile_defs[_selected_tile].clip[3];

    } break;
    case KEYCODE(DOWN):
    {
        if (!_can_write)
            ++editor_tile_defs[_selected_tile].clip[3];
    } break;
    case KEYCODE(PAGEUP):
    {
        _tile_selector -= 1;
        if (_tile_selector < 1)
            _tile_selector = (int)darr_num(editor_tile_defs) - 1;

        _selected_tile -= 1;
        if (_selected_tile < 1)
            _selected_tile = (int)darr_num(editor_tile_defs) - 1;
    } break;
    case KEYCODE(PAGEDOWN):
    {
        _tile_selector += 1;
        if (_tile_selector >(int)darr_num(editor_tile_defs) - 1)
            _tile_selector = 1;

        _selected_tile += 1;
        if (_selected_tile > (int)darr_num(editor_tile_defs) - 1)
            _selected_tile = 1;
    } break;
    }
}

void
tile_editor_mousewheel(int x, int y)
{
    _tile_selector -= y;
    _tile_indic += y;

    if (_tile_selector < 1)
    {
        _tile_selector = (int)darr_num(editor_tile_defs) - 1;
    }
    if (_tile_selector >(int)darr_num(editor_tile_defs) - 1)
    {
        _tile_selector = 1;
    }
    if (_tile_indic < 0)
    {
        _tile_indic = (int)darr_num(editor_tile_defs) - 2;
    }
    if (_tile_indic > (int)darr_num(editor_tile_defs) - 2)
    {
        _tile_indic = 0;
    }

}

void
tile_editor_text_input(const char *text)
{
    char    buf[256];

    if (_can_write || _auto_chop_win)
    {
        _input_buf_offset = (int)strlen(_input_buf);
        if (_input_buf_offset < INPUT_FIELD_LIMIT - 1)
        {
            strcpy(buf, text);
            if (!_text_allowed)
                str_strip_non_numbers(buf);
            else
                strip_illegal_chat_msg_symbols(buf);
            strncat(_input_buf, buf, sizeof(_input_buf) - 1);
        }
    }
}

void
tile_editor_file_drop(const char *path)
{
    char temp[256];
    char temp_save_path[256];
    strcpy(temp, path);
    char *base_path = SDL_GetBasePath();
    darr_clear(editor_tile_defs);

    while (temp == strstr(temp, base_path))
        memmove(temp, temp + strlen(base_path),
            1 + strlen(temp + strlen(base_path)));
    char file[256];
    strcpy(file, path);

    strcpy(temp_save_path, temp);
    char *file_extension;
    file_extension = strtok(temp, ".");
    if (!file_extension)
        goto clean;

    file_extension = strtok(0, ".");

    if (file_extension && !str_insensitive_cmp(file_extension, "png"))
    {
        tex_asset_t *tex = 0;
        FIXME();
        if (tex)
        {
            strcpy(_tex_path, temp_save_path);
            int loop = (int)strlen(_tex_path);
            for (int i = 0; i < loop; ++i)
            {
                if (_tex_path[i] == '\\')
                    _tex_path[i] = '/';
            }
            _tile_sheet_tex = tex;
            _sheet_dragged = 1;
            _png_not_dragged = 0;
        }
    } else
        if (file_extension && !str_insensitive_cmp(file_extension, "dat"))
        {
            _read_tile_defs(file);
            tex_asset_t *tex = 0;
            FIXME();
            if (tex)
            {
                strcpy(_save_path, temp_save_path);
                int loop = (int)strlen(_save_path);
                for (int i = 0; i < loop; ++i)
                {
                    if (_save_path[i] == '\\')
                        _save_path[i] = '/';
                }
                _tile_sheet_tex = tex;
                _sheet_dragged = 1;
                _png_not_dragged = 0;
            }
        }
        else
            goto clean;

    return;
clean:
    _png_not_dragged = 1;
    DEBUG_PRINTF("'%s' is not valid file for tile editor\n", file);
}

static void
_load_tiles()
{
    int rows			= 6;
    int offset			= tile_px_w / 2;
    int index;
    uint8 quad_col[4];
    quad_col[0]			= 0;
    quad_col[1]			= 0;
    quad_col[2]			= 0;
    quad_col[3]			= 128;

    gui_quad(0, 0, tile_px_w + offset * 2,
        RESOLUTION_H, quad_col);

    int tile_spacing;
    for (int i = 0; i < rows; ++i)
    {
        if (darr_num(editor_tile_defs) > 0)
        {
            char buf[256];
            index = _tile_selector + i;

            if (index > (int)darr_num(editor_tile_defs) - 1)
            {
                index -= darr_num(editor_tile_defs) - 1;
            }

            tile_spacing = offset * i;

            if (gui_invisible_button("lol", 50,
                tile_px_h * i + offset + tile_spacing,
                tile_px_w, tile_px_h, 0))
            {
                _selected_tile		= index;
                _tile_indic			= i;
                _error_message		= 0;
                _manual_clip_win	= 1;
                stbsp_sprintf(buf, "%d", 
                    (int)editor_tile_defs[_selected_tile].clip[0]);
                strcpy(_clip0_buf, buf);
                _input_buf			= _clip0_buf;
            }

            gui_texture(&_tile_sheet_tex->tex,
                editor_tile_defs[index].clip,
                50, tile_px_h * i + offset + tile_spacing + 
                (int)editor_tile_defs[index].offset_y); 

        stbsp_sprintf(buf, "ID: %d", index);
        gui_text(buf, 0, 50 - offset, tile_px_h * i + tile_px_h + tile_spacing);
        }
    }
    _draw_rect(50, tile_px_h * _tile_indic + offset + (offset * _tile_indic),
        tile_px_w, tile_px_h, black);
}

static const char *
_ramp_to_text(int ramp_id)
{
    float default_clip[4] = { 0.0f, 0.0f, 32.0f, 32.0f };
    switch (ramp_id)
    {
    case 0:
    {
        return "RAMP: NONE";
    }
    case 1:
    {
        default_clip[0] = 96.0f; default_clip[2] = 128.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 1);
        return "RAMP: NORTH";
    }
    case 2:
    {
        default_clip[0] = 64.0f; default_clip[2] = 96.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 1);
        return "RAMP: EAST";
    }
    case 3:
    {
        default_clip[0] = 64.0f; default_clip[2] = 96.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 0);
        return "RAMP: SOUTH";
    }
    case 4:
    {
        default_clip[0] = 96.0f; default_clip[2] = 128.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 0);
        return "RAMP: WEST";
    }
    case 5:
    {
        default_clip[0] = 128.0f; default_clip[2] = 160.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 236, 310, 1.0f, 1);
        return "RAMP: NORTHEAST";
    }
    case 6:
    {
        default_clip[0] = 0.0f; default_clip[2] = 32.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 0);
        return "RAMP: NORTHWEST";
    }
    case 7:
    {
        default_clip[0] = 32.0f; default_clip[2] = 64.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 0);
        return "RAMP: SOUTHEAST";
    }
    case 8:
    {
        default_clip[0] = 128.0f; default_clip[2] = 160.0f;
        sb_sprite_f(&_arrow_pointer->tex, default_clip,
            RESOLUTION_W - 230, 310, 1.0f, 0);
        return "RAMP: SOUTHWEST";
    }
    }
    return "RAMP_NONE";
}

static void
_draw_rect(int x, int y, int w, int h, uint8 col[4])
{
    gui_quad(x - 2, y - 2, w + 4, 2, col);
    gui_quad(x - 2, y, 2, h + 2, col);
    gui_quad(x, y + h, w + 2, 2, col);
    gui_quad(x + w, y, 2, h + 2, col);
}

static int
_read_tile_defs(const char *fp)
{
    editor_tile_t *def;

    int ret = 0;
    FILE *f = fopen(fp, "r");
    if (!f) { ret = 1; goto cleanup;}

    const int line_len = 256;
    char line[256];
    int index = 0;

    /* Read the definitions */

    char    name[64];
    char    path[256];
    int     passthrough;
    int     ramp;
    int     tile_width;
    int     tile_height;
    int     tile_top_pixel_height;
    float   clip[4];
    float   offset_x;
    float   offset_y;
    int     data_line;
    float   default_clip[4] = {0.0f, 0.0f, (float)tile_px_w, (float)tile_px_h};

    while (fgets(line, line_len, f))
    {
        if (sscanf(line, "tile texture path = %s", path) == 1)
            strcpy(_tex_path, path);
        if (sscanf(line, "tile pixel width	= %d", &tile_width) == 1)
            _tile_width = tile_width;
        if (sscanf(line, "tile pixel height	= %d", &tile_height) == 1)
            _tile_height = tile_height;
        if (sscanf(line, "tile top pixel height	= %d",
            &tile_top_pixel_height) == 1)
            _tile_top_pixel_height = tile_top_pixel_height;
        if (sscanf(line, "[%d]", &index) == 1)
        {
            editor_tile_t tmp = {0};
            darr_reserve(editor_tile_defs, (uint32)(index + 1));
            editor_tile_defs[index] = tmp;
            def = &editor_tile_defs[index];
        } else
        if (def)
        {
            if (index != 0)
            {
                if (sscanf(line, "NAME: %s64", name) == 1)
                    strcpy(def->name, name);
                else if (sscanf(line, "PASSTHROUGH: %i", &passthrough) == 1)
                    def->passthrough = passthrough ? 1 : 0;
                else if (sscanf(line, "RAMP: %i", &ramp) == 1)
                    def->ramp = ramp;
                else if ((data_line = sscanf(line, "CLIP: %f, %f, %f, %f", 
                    &clip[0], &clip[1], &clip[2], &clip[3])) >= 1)
                {
                    if (data_line != 4)
                    {
                        for (int i = 0; i < 4; ++i)
                            def->clip[i] = default_clip[i];
                        continue;
                    }
                    else
                    {
                        for (int i = 0; i < 4; ++i)
                            def->clip[i] = clip[i];
                    }
                }
                else if (sscanf(line, "OFFSET X: %f", &offset_x) == 1)
                    def->offset_x = offset_x;
                else if (sscanf(line, "OFFSET Y: %f", &offset_y) == 1)
                    def->offset_y = offset_y;
            }
        }
    }

    cleanup:;
        safe_fclose(f);
            return ret;
}

static int
_save_tile_defs(const char *fp)
{
    FILE *f = fopen(fp, "w+");
    if (!f)
        return 1;

    fprintf(f, "tile texture path		= %s\n", _tex_path);
    fprintf(f, "tile pixel width		= %d\n", _tile_width);
    fprintf(f, "tile pixel height		= %d\n", _tile_height);
    fprintf(f, "tile top pixel height	= %d\n\n", _tile_top_pixel_height);

    for (int i = 1; i <= (int)darr_num(editor_tile_defs) - 1; ++i)
    {
        fprintf(f, "[%d]\n", i);
        fprintf(f, "NAME: %s\n", editor_tile_defs[i].name);
        fprintf(f, "PASSTHROUGH: %d\n", editor_tile_defs[i].passthrough);
        fprintf(f, "RAMP: %i\n", editor_tile_defs[i].ramp);
        fprintf(f, "CLIP: %d, %d, %d, %d\n", 
            (int)editor_tile_defs[i].clip[0],
            (int)editor_tile_defs[i].clip[1],
            (int)editor_tile_defs[i].clip[2], 
            (int)editor_tile_defs[i].clip[3]);
        fprintf(f, "OFFSET X: %d\n", (int)editor_tile_defs[i].offset_x);
        fprintf(f, "OFFSET Y: %d\n\n",
            (int)editor_tile_defs[i].offset_y);
    }
    safe_fclose(f);
    return 0;
}

static void
_auto_chop()
{
    int vertical_loops = (int)(_tile_sheet_tex->tex.w /
        (_chop_width + _chop_spacing * 2));
    int horizontal_loops = (int)(_tile_sheet_tex->tex.h /
        (_chop_height + _chop_spacing * 2));
    int a, b;
    int h_loops, w_loops;
    int index = 0;
    editor_tile_t *def;
    editor_tile_t tmp = { 0 };
    int clip0, clip1, clip2, clip3;
    if (_horizontal_toggle)
    {
        h_loops = horizontal_loops;
        w_loops = vertical_loops;
    } else
    {
        h_loops = vertical_loops;
        w_loops = horizontal_loops;
    }

    for (int j = 0; j < h_loops; ++j)
    {
        for (int i = 0; i < w_loops; ++i)
        {
            if (!_horizontal_toggle)
            {
                a = j;
                b = i;
            } else
            {
                a = i;
                b = j;
            }

            ++index;
            clip0 = (a * _chop_width) + (_chop_spacing * a * 2)
                + _chop_spacing;
            clip1 = (b * _chop_height) + (_chop_spacing * b * 2)
                + _chop_spacing;
            clip2 = (_chop_width + (_chop_width * a)) +
                (_chop_spacing * a * 2) + _chop_spacing;
            clip3 = (_chop_height + (_chop_height * b)) + 
                (_chop_spacing * b * 2) + _chop_spacing;

            if ((uint32)index < darr_num(editor_tile_defs))
            {
                def             = &editor_tile_defs[index];
                def->clip[0]    = (float)clip0;
                def->clip[1]    = (float)clip1;
                def->clip[2]    = (float)clip2;
                def->clip[3]    = (float)clip3;
            } else
            {
                uint32 num = darr_num(editor_tile_defs);
                muta_assert(num < 512);
                darr_reserve(editor_tile_defs, (uint32)(index + 1));
                editor_tile_defs[index] = tmp;
                editor_tile_defs[index].clip[0] = (float)clip0;
                editor_tile_defs[index].clip[1] = (float)clip1;
                editor_tile_defs[index].clip[2] = (float)clip2;
                editor_tile_defs[index].clip[3] = (float)clip3;
            }
        }
    }
}

static void
_delete_clip()
{
    if (_selected_tile != 0 && darr_num(editor_tile_defs) - 1 >= 2)
    {
        darr_erase(editor_tile_defs, _selected_tile);

        if (_selected_tile == (int)darr_num(editor_tile_defs))
            _selected_tile -= darr_num(editor_tile_defs) - 1;

        if (_tile_selector ==
            (int)darr_num(editor_tile_defs) - (_tile_indic - 1))
            --_tile_selector;
    }
}

static void
_help_window()
{
    gui_origin(GUI_TOP_CENTER);
    gui_begin_win("HELP", 0, 0, RESOLUTION_W, RESOLUTION_H, 0);
    gui_text("HELP WINDOW: ", 0, 0, 0);
    
    gui_text(" Escape: Shuts down editor. NOTE! "
        "All unsaved changes will be lost", 0, 0, 20);
    gui_text(" WASD: Moves manually selected tile's clip ", 0, 0, 60);
    gui_text(" Numpad 2: Increases selected tile clip height ", 0, 0, 80);
    gui_text(" Numpad 8: Decreases selected tile clip height ", 0, 0, 100);
    gui_text(" Numpad 4: Decreases selected tile clip width ", 0, 0, 120);
    gui_text(" Numpad 6: Increases sselected tile clip width ", 0, 0, 140);
    gui_text(" Up 'n down arrows: Moves selected tile up or down on the list ",
        0, 0, 180);
    gui_text(" Mouse scroll: Scrolls tiletypes on left bar ", 0, 0, 220);
    gui_text(" Click tilesheet and drag: Moves the texture around ",
        0, 0, 240);
    gui_text(" AUTO CHOP: Automatically cuts tilesheet into clips. "
        "You can toggle if it happens horizontal or vertical wise. " 
        "Width or height can't be under 8.", 0, 0, 280);
    gui_text(" SAVE: Saves all changes to txt file. Remember to save often ",
        0, 0, 320);
    
    gui_text(" DELETE CLIP: Deletes currently selected tile ", 0, 0, 360);
    gui_text(" CREATE NEW CLIP: Creates new empty tile ", 0, 0, 400);
    gui_text(" PASSTHROUGH TOGGLE: Allows you to switch if tile is "
        "passthrough(1) or no(0) ", 0, 0, 440);
    gui_text(" RAMP: Clicking this will change tile's ramp type ", 0, 0, 480);
    gui_text(" PRESS SET CLIP FOR MANUAL TILE CHANGES ", 0, 0, 520);
    gui_text(" X-, X+, Y-, Y+ BUTTONS: Changes tile offset ", 0, 0, 560);
    gui_text(" TILETYPE TO DRAW: Changes the surrounding tile. ", 0, 0,600);
    gui_text(" TOGGLE FRONT TILES: Shows/hides front tiles", 0, 0, 640);
    gui_text(" TOGGLE MIDDLE TILE: Shows/hides middle tile if needed ",
        0, 0, 680);
    gui_text(" txt file needs to be in /client/rundir/data folder. "
        "Named editor_tiles.txt. If file does not exist, "
        "saving will create it for you", 0, 0, 720);
    gui_text(" GIVE FEEDBACK!!", 0, 0, 760);

    gui_end_win();
}

static void
_error_message_window(char *txt)
{
    stbsp_sprintf(txt, "WIDTH OR HEIGHT CAN'T BE UNDER %d!!",
        _min_chop_value);

    gui_origin(GUI_CENTER_CENTER);
    gui_begin_empty_win("ERROR WIN", 0, 0, 300, 100, 0);
    if (gui_button(txt, 0, 0, 300, 100, 0))
        _error_message = 0;
    gui_end_win();
}

static void
_confirm_action(char *txt, void (*yes_answer)())
{	
    gui_origin(GUI_TOP_CENTER);
    gui_begin_win("CONFIRM", 0, RESOLUTION_H/3, 300, 150, 0);
    gui_text(txt, 0, 0, 30);	
    if (gui_button("YES", -60, 70, 80, 40, 0))			
        yes_answer();
    if (gui_button("NO", 60, 70, 80, 40, 0))
        _confirm_value = 0;
    gui_end_win();
}

static void
_main_menu()
{
    _confirm_value = 0;
    core_set_screen(&main_menu_screen);	
}

static void
_insert_path_to_save()
{
    gui_origin(GUI_TOP_CENTER);
    gui_begin_win("SAVE PATH", 0, RESOLUTION_H / 3, 500, 150, 0);
    gui_text("INSERT SAVE PATH: ", 0, 0, 5);
    if (gui_text_input(PATH, _save_path_buf, 0, 0, 30, 300, 20))
    {
        _can_write = 1;
        _text_allowed = 1;
        _input_buf = _save_path_buf;
    }

    if (gui_button("SAVE", -50, 100, 50, 25, 0))
    {
        strtok(_save_path_buf, ".");
        stbsp_snprintf(_save_path, 256, "%s.dat", _save_path_buf);

        _can_write = 0;
        _text_allowed = 0;

        if (!_save_tile_defs(_save_path))
        {
            _confirm_value = 0;
            _can_write = 0;
            _text_allowed = 0;
            _input_buf = 0;
        }
        else
            DEBUG_PRINTF("TALLENNUS EPAONNISTUI\n");
    }
    if (gui_button("CANCEL", 50, 100, 50, 25, 0))
    {
        _confirm_value = 0;
        _confirm_value = 0;
        _can_write = 0;
        _text_allowed = 0;
    }
    gui_end_win();
}
