--[[
local window = create_window("window 1");

print("window is:")
print(window)

show_window(window)

set_window_width(window, 200);
set_window_height(window, 512);
set_window_position(window, 100, 400)

local text1 = create_text(window, "huhhels")

set_text_position(text1, 0, 0)
set_text_origin(text1, "CENTER_CENTER")
set_text_font(text1, "munro10")
set_text_scale(text1, 2.0)

local win2 = create_window("window 2")
show_window(win2)
set_window_width(win2, 300)
set_window_height(win2, 300)

local fps_text = create_text(win2, "0")
set_text_origin(fps_text, "CENTER_CENTER")
set_text_scale(fps_text, 1.5)

-- Event handling 
--
function on_update(event)
    set_text(text1, string.format("%f", event.delta));
    set_text(fps_text, string.format("%f", get_fps()))
    --print(event.delta)
end

listen_to_event(window, "UPDATE")
set_event_handler(window, "UPDATE", on_update)

local button1 = create_button(window, "press me")
set_button_position(button1, 20, 20)
set_button_width(button1, 64)
set_button_height(button1, 48)

function on_click()
    print("button clicked!")
end

set_button_handler(button1, on_click)

tex1 = create_texture(win2, "assets/textures/misc/matlock.png")
tex2 = create_texture(window, "assets/textures/misc/matlock.png")
set_texture_scale(tex2, 2.0)
set_texture_vertical_scale(tex2, 2.0)
set_texture_horizontal_scale(tex2, 1.0)
set_texture_position(tex2, 180, 50)
set_texture_clip(tex2, 20, 20, 52, 52)

set_texture_origin(tex1, "BOTTOM_RIGHT")

b2 = create_button(window, "dont press me")
set_button_width(b2, 32)
set_button_height(b2, 32)
set_button_origin(b2, "BOTTOM_RIGHT")

set_texture_file(tex1, "assets/textures/misc/ae_tile.png")
]]--

