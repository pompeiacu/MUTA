local mmw = {} -- main menu window
local kbw = {} -- key bindings window
local chat = {}

local menu_button_width     = 124
local menu_button_height    = 32

local function toggle_main_menu()
    if is_window_shown(mmw.window) then
        hide_window(mmw.window)
    else
        show_window(mmw.window)
    end
end

local function on_escape()
    if is_window_shown(kbw.window) then
        hide_key_bindings_window()
        show_window(mmw.window)
    else
        toggle_main_menu()
    end
end

local function hide_bind_popup_window()
    ungrab_keyboard_for_window(kbw.bind_popup.window)
    hide_window(kbw.bind_popup.window)
end

local function show_key_bindings_window()
    show_window(kbw.window)
    hide_window(mmw.window)
end

local function hide_key_bindings_window()
    hide_bind_popup_window()
    hide_window(kbw.window)
    show_window(mmw.window)
end

local function init_main_menu_window()
    mmw.window = create_window("Main Menu")

    set_window_origin(mmw.window, "CENTER_CENTER")
    set_window_width(mmw.window, menu_button_width + 6)
    set_window_height(mmw.window, 160)
    set_window_position(mmw.window, 0, 0)

    mmw.log_out_button = create_button(mmw.window, "Log Out")
    set_button_origin(mmw.log_out_button, "TOP_CENTER")
    set_button_width(mmw.log_out_button, menu_button_width)
    set_button_height(mmw.log_out_button, menu_button_height)
    set_button_callback(mmw.log_out_button, log_out)
    set_button_position(mmw.log_out_button, 0, mmw.log_out_button, 2)

    mmw.exit_button = create_button(mmw.window, "Exit")
    set_button_origin(mmw.exit_button, "TOP_CENTER")
    set_button_width(mmw.exit_button, menu_button_width)
    set_button_height(mmw.exit_button, menu_button_height)
    set_button_callback(mmw.exit_button, exit_game)
    set_button_position(mmw.exit_button, 0, 2 + menu_button_height + 2)

    mmw.resume_button = create_button(mmw.window, "Resume")
    set_button_origin(mmw.resume_button, "TOP_CENTER")
    set_button_width(mmw.resume_button, menu_button_width)
    set_button_height(mmw.resume_button, menu_button_height)
    set_button_callback(mmw.resume_button, toggle_main_menu)
    set_button_position(mmw.resume_button, 0, 2 + menu_button_height * 2 + 4)

    mmw.key_bindings_button = create_button(mmw.window,
        "Key Bindings")
    set_button_origin(mmw.key_bindings_button, "TOP_CENTER")
    set_button_width(mmw.key_bindings_button, menu_button_width)
    set_button_height(mmw.key_bindings_button, menu_button_height)
    set_button_callback(mmw.key_bindings_button, show_key_bindings_window)
    set_button_position(mmw.key_bindings_button, 0,
        2 + menu_button_height * 3 + 4)
end

local function init_key_bindings_window()
    kbw.window = create_window("Key Bindings")
    set_window_width(kbw.window, 800)
    set_window_height(kbw.window, 600)
    set_window_origin(kbw.window, "CENTER_CENTER")

    kbw.key_slot = 1
    kbw.key_bind_name = ""

    kbw.bind_popup = {}
    kbw.bind_popup.window = create_window("Bind Key")
    set_window_width(kbw.bind_popup.window, 262)
    set_window_height(kbw.bind_popup.window, 200)
    set_window_origin(kbw.bind_popup.window, "CENTER_CENTER")
    set_window_parent(kbw.bind_popup.window, kbw.window)

    --unbind button
    kbw.bind_popup.unbind_button = create_button(kbw.bind_popup.window,
        "Unbind")
    set_button_width(kbw.bind_popup.unbind_button, 124)
    set_button_height(kbw.bind_popup.unbind_button, 32)
    set_button_origin(kbw.bind_popup.unbind_button, "BOTTOM_LEFT")
    set_button_position(kbw.bind_popup.unbind_button, 4, 4)

    --cancel button
    kbw.bind_popup.cancel_button = create_button(kbw.bind_popup.window,
        "Cancel")
    set_button_width(kbw.bind_popup.cancel_button, 124)
    set_button_height(kbw.bind_popup.cancel_button, 32)
    set_button_origin(kbw.bind_popup.cancel_button, "BOTTOM_RIGHT")
    set_button_position(kbw.bind_popup.cancel_button, 4, 4)
    set_button_callback(kbw.bind_popup.cancel_button, hide_bind_popup_window)

    kbw.bind_popup.text1 = create_text(kbw.bind_popup.window, "")
    set_text_origin(kbw.bind_popup.text1, "CENTER_CENTER")
    set_text_position(kbw.bind_popup.text1, 0, -10)
    kbw.bind_popup.text2 = create_text(kbw.bind_popup.window, "")
    set_text_origin(kbw.bind_popup.text2, "CENTER_CENTER")
    set_text_position(kbw.bind_popup.text2, 0, 10)

    kbw.buttons = {}

    local kbs   = get_key_bindings()
    local y     = 4 

    local i = 0

    for k, v in pairs(kbs) do
        local button_table = {}
        local text = create_text(kbw.window, k)
        set_text_position(text, 4, y)
        set_text_origin("TOP_LEFT")
        local bw = 230
        local bh = 18
        local button = create_button(kbw.window, v.key1)
        set_button_position(button, 4 + 256, y)
        set_button_width(button, bw)
        set_button_height(button, bh)

        local function callback1()
            set_text_string(kbw.bind_popup.text1, k)
            set_text_string(kbw.bind_popup.text2, v.key1)
            show_window(kbw.bind_popup.window)
            grab_keyboard_for_window(kbw.bind_popup.window)
            kbw.key_slot        = 1
            kbw.key_bind_name   = k
        end

        set_button_callback(button, callback1)
        button_table[1] = button
        button = create_button(kbw.window, v.key2)
        set_button_position(button, 4 + 512, y)
        set_button_width(button, bw)
        set_button_height(button, bh)

        local function callback2()
            set_text_string(kbw.bind_popup.text1, k)
            set_text_string(kbw.bind_popup.text2, v.key2)
            show_window(kbw.bind_popup.window)
            grab_keyboard_for_window(kbw.bind_popup.window)
            kbw.key_slot        = 2
            kbw.key_bind_name   = k
        end

        set_button_callback(button, callback2)
        button_table[2] = button
        y = y + 16
        i = i + 1
        kbw.buttons[k] = button_table
    end

    kbw.exit_button = create_button(kbw.window, "Back")
    set_button_origin(kbw.exit_button, "BOTTOM_RIGHT")
    set_button_width(kbw.exit_button, 64)
    set_button_height(kbw.exit_button, 24)
    set_button_callback(kbw.exit_button, hide_key_bindings_window)
    set_button_position(kbw.exit_button, 4, 4)
end

local function init_chat()
    chat.window = create_window("chat")
    set_window_origin(chat.window, "BOTTOM_LEFT")
    set_window_position(chat.window, 4, 68)
    set_window_width(chat.window, 400)
    set_window_height(chat.window, 270)
    show_window(chat.window)
    chat.texts = {}
    y = 0
    for i = 11, 1, -1 do
        chat.texts[i] = create_text(chat.window, "")
        set_text_origin(chat.texts[i], "BOTTOM_LEFT")
        set_text_position(chat.texts[i], 4, y)
        y = y + get_text_height(chat.texts[i])
    end
    chat.edit_window = create_window("##chat edit box")
    set_window_origin(chat.edit_window, "BOTTOM_LEFT")
    set_window_position(chat.edit_window, 4, 4);
    set_window_width(chat.edit_window, 400)
    set_window_height(chat.edit_window, 58)
    show_window(chat.edit_window)
    chat.edit_box = create_text_input(chat.edit_window, "chat", 255)
    set_text_input_origin(chat.edit_box, "CENTER_CENTER")
    set_text_input_width(chat.edit_box, 392)
    set_text_input_height(chat.edit_box, 28)
    local function on_submit(edit_box)
        send_chat_message(get_text_input_content(chat.edit_box))
        clear_text_input(chat.edit_box)
    end
    set_text_input_submit_callback(chat.edit_box, on_submit)
end

local function on_key_up(e)
    if is_window_active(kbw.bind_popup.window) then
        kbs = get_key_bindings()
        kb  = kbs[kbw.key_bind_name]
        bind_key(kbw.key_bind_name, e.key, kbw.key_slot)
        hide_bind_popup_window()
    end
end

local function on_key_bind_changed(e)
    local b = kbw.buttons[e.name]
    if b == nil then
        print("nil!")
    else
        set_button_title(b[1], e.key1)
        set_button_title(b[2], e.key2)
    end
end

local function on_chat_log_entry(e)
    for i = 1, 11 do
        set_text_string(chat.texts[i], get_text_string(chat.texts[i + 1]))
        print(string.format("set string to %s", get_text_string(chat.texts[i + 1])))
    end
    set_text_string(chat.texts[10],
        string.format("%s: %s", e.sender, e.message))
    y = 0
    for i = 11, 1, -1 do
        set_text_origin(chat.texts[i], "BOTTOM_LEFT")
        set_text_position(chat.texts[i], 4, y)
        y = y + get_text_height(chat.texts[i])
    end
end

local function activate_chat_edit_box()
    print("activate chat!")
    if not is_text_input_active(chat.edit_box) then
        set_active_text_input(chat.edit_box)
    end
end

-- Initialization
init_main_menu_window()
init_key_bindings_window()
init_chat()

listen_to_event(kbw.window, "KEY_BIND_CHANGED")
set_event_callback(kbw.window, "KEY_BIND_CHANGED", on_key_bind_changed)

listen_to_event(kbw.bind_popup.window, "KEY_UP")
set_event_callback(kbw.bind_popup.window, "KEY_UP", on_key_up)

listen_to_event(chat.window, "CHAT_LOG_ENTRY")
set_event_callback(chat.window, "CHAT_LOG_ENTRY", on_chat_log_entry)

create_key_bind("Toggle Main Menu", on_escape)
bind_key("Toggle Main Menu", "Escape", 1)

create_key_bind("Activate Chat Edit Box", activate_chat_edit_box)
bind_key("Activate Chat Edit Box", "Enter", 1)
